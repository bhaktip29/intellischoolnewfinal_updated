//
//  StudentListByClassOrDivVC.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 25/07/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class StudentListByClassOrDivVCTeacher: BaseViewControllerTeacher,UITableViewDataSource,UITableViewDelegate {

    let cellId = "CellId"
    var studentList = [StudentTeacher]()
    var schoolClass: SchoolClassTeacher?
    var noOfItemsChecked: Int = 0
    let vw = UIView()
    
    let tableView : UITableView = {
        let tblView = UITableView()
        tblView.translatesAutoresizingMaskIntoConstraints = false
        tblView.separatorInset = UIEdgeInsets.zero
        return tblView
    }()
    
    let headerView:UIView = {
        let headerView = UIView()
        headerView.backgroundColor = UIColor(red:0.86, green:0.86, blue:0.86, alpha:1.0)
        headerView.translatesAutoresizingMaskIntoConstraints = false
        return headerView
    }()
    
    let sendButton:UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setImage(UIImage(named: "ic_send_All"), for: .normal)
        btn.addTarget(self, action: #selector(showSendSMSVC), for: .touchUpInside)
        return btn
    }()
    
    let selectAllBtn:UIButton = {
        let btn = UIButton(type: .system)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Select All", for: .normal)
        btn.addTarget(self, action: #selector(handleSelectAll), for: .touchUpInside)
        return btn
    }()
    
    let bottomLabel : UILabel = {
        let lbl = UILabel()
        lbl.text = "Please select at least one student above."
        lbl.font = UIFont.boldSystemFont(ofSize: 13)
        lbl.backgroundColor = UIColor.lightGray
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.white
        lbl.numberOfLines = 0
        return lbl
    }()
    
    private func setUpTableView() {
        
        view.addSubview(tableView)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        // needed constarunts x,y,w,h
        tableView.topAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: vw.topAnchor).isActive = true
        view.bringSubviewToFront(vw)
    }
    
    private func setUpBottomLabel() {
        
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = UIColor.lightGray
        view.addSubview(vw)
        // needed constarunts x,y,w,h
        vw.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        if #available(iOS 11.0, *) {
            vw.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            vw.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
        vw.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        vw.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        vw.addSubview(bottomLabel)
        // needed constarunts x,y,w,h
        bottomLabel.leftAnchor.constraint(equalTo: vw.leftAnchor,constant: 8).isActive = true
        bottomLabel.bottomAnchor.constraint(equalTo: vw.bottomAnchor).isActive = true
        bottomLabel.widthAnchor.constraint(equalTo: vw.widthAnchor,constant:-60).isActive = true
        bottomLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        bottomLabel.sizeToFit()
        
        vw.addSubview(sendButton)
        // needed constarunts x,y,w,h
        sendButton.rightAnchor.constraint(equalTo: vw.rightAnchor,constant: -10).isActive = true
        sendButton.centerYAnchor.constraint(equalTo: vw.centerYAnchor,constant:-25).isActive = true
        sendButton.widthAnchor.constraint(equalToConstant:45).isActive = true
        sendButton.heightAnchor.constraint(equalToConstant: 45).isActive = true
    }
    
    private func setUpHeaderView() {
        
        view.addSubview(headerView)
        // needed constraints x,y,w,h
        if #available(iOS 11.0, *) {
            headerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        } else {
            headerView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        }
        headerView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        headerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        headerView.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        let rollLbl = createLabel(title: "Roll", textAlignment: .justified)
        
        headerView.addSubview(rollLbl)
        // needed constraints x,y,w,h
        rollLbl.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: 48).isActive = true
        rollLbl.topAnchor.constraint(equalTo: headerView.topAnchor).isActive = true
        rollLbl.widthAnchor.constraint(equalToConstant: 40).isActive = true
        rollLbl.bottomAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
        
        headerView.addSubview(selectAllBtn)
        // needed constraints x,y,w,h
        selectAllBtn.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        selectAllBtn.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        selectAllBtn.widthAnchor.constraint(equalToConstant:90).isActive = true
        selectAllBtn.heightAnchor.constraint(equalTo: headerView.heightAnchor).isActive = true
        
        let separatorView = UIView()
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        separatorView.backgroundColor = UIColor.gray
        
        headerView.addSubview(separatorView)
        // needed constraints x,y,w,h
        separatorView.leftAnchor.constraint(equalTo: rollLbl.rightAnchor).isActive = true
        separatorView.topAnchor.constraint(equalTo: headerView.topAnchor).isActive = true
        separatorView.widthAnchor.constraint(equalToConstant: 1).isActive = true
        separatorView.bottomAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
        
        let nameLbl = createLabel(title: "Name", textAlignment: .justified)
        
        headerView.addSubview(nameLbl)
        // needed constraints x,y,w,h
        nameLbl.rightAnchor.constraint(equalTo: selectAllBtn.leftAnchor).isActive = true
        nameLbl.leftAnchor.constraint(equalTo: rollLbl.rightAnchor,constant:10).isActive = true
        nameLbl.topAnchor.constraint(equalTo: headerView.topAnchor).isActive = true
        nameLbl.bottomAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
    }
    
    private func createLabel(title: String?, textAlignment : NSTextAlignment) -> UILabel {
        let lbl = UILabel()
        lbl.text = title
        lbl.font = UIFont.boldSystemFont(ofSize: 13)
        lbl.textAlignment = textAlignment
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }
    
    //MARK:- View Controllers Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        tableView.register(StudentCellTeacher.self, forCellReuseIdentifier: cellId)
        if let className = schoolClass?.className, let div = schoolClass?.divisionName {
            self.title = "Class \(className) \(div)"
        }
        
        tableView.cellLayoutMarginsFollowReadableWidth = false
        tableView.allowsMultipleSelection = true
        tableView.setEditing(true, animated: true)
        setUpHeaderView()
        setUpBottomLabel()
        setUpTableView()
        getStudentList()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        guard let selectedIndexPaths = tableView.indexPathsForSelectedRows, selectedIndexPaths.count > 0 else {
            return
        }
        for indexPath in selectedIndexPaths {
            tableView.deselectRow(at: indexPath , animated: true)
            let student:StudentTeacher = studentList[indexPath.row]
            student.isChecked = !student.isChecked
        }
        noOfItemsChecked = 0
    }
    
    var isAllSelected = false
    
    @objc func handleSelectAll() {
 
        if isAllSelected {
            isAllSelected = false
            let totalRows = tableView.numberOfRows(inSection: 0)
            for row in 0..<totalRows {
                let index = IndexPath(row: row, section: 0)
                tableView.deselectRow(at: index, animated: false)
                let student = studentList[row]
                student.isChecked = !student.isChecked
                noOfItemsChecked = 0
            }
        }else{
            isAllSelected = true
            let totalRows = tableView.numberOfRows(inSection: 0)
            studentList.forEach({ $0.isChecked = false })
            for row in 0..<totalRows {
                let index = IndexPath(row: row, section: 0)
                tableView.selectRow(at: index, animated: false, scrollPosition: .none)
                let student = studentList[row]
                student.isChecked = !student.isChecked
                noOfItemsChecked = studentList.count
            }
        }
    }
    
    //MARK:- TableView Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
       return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return studentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! StudentCellTeacher
        
        let student: StudentTeacher = studentList[indexPath.row]
        cell.backgroundColor = UIColor.white
        cell.rollLabel.text = student.studentRollNo.description
        cell.nameLabel.text = student.studentName
        if indexPath.row % 2 == 0 {
            //even number
        } else {
            cell.backgroundColor = UIColor(red:0.90, green:0.90, blue:0.90, alpha:1.0)
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return UITableViewCell.EditingStyle.init(rawValue: 3)!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let student = studentList[indexPath.row]
        student.isChecked = !student.isChecked
        noOfItemsChecked = noOfItemsChecked < studentList.count ? noOfItemsChecked+1 : 0
        isAllSelected = false
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath : IndexPath) {
        let schoolClass = studentList[indexPath.row]
        schoolClass.isChecked = !schoolClass.isChecked
        noOfItemsChecked = noOfItemsChecked > 0 ? noOfItemsChecked-1 : 0
        isAllSelected = false
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    @objc func showSendSMSVC() {
    
        if(noOfItemsChecked == 0){
            UtilTeacher.invokeAlertMethod("", strBody: "Select at least one student", delegate: nil,vcobj:self)
        }else {
            var students : String = ""
            var intellinectsIds = [String]()
            
            var firstName = ""
            var isFirstNameStored = false
            for i in (0..<studentList.count){
                let student:StudentTeacher = studentList[i]
                if(student.isChecked){
                    if !isFirstNameStored, let nm = student.studentName {
                        firstName = nm
                        isFirstNameStored = true
                    }
                    if let name = student.studentName, let intId = student.intellinectId {
                        students += name + ", "
                        intellinectsIds.append(intId)
                    }
                }
            }
            students.removeLast()
            students.removeLast()
            if(noOfItemsChecked == studentList.count){
                students = "All Student"
            } else if noOfItemsChecked > 1  {
                students = "\(firstName) and \(noOfItemsChecked - 1) others"
            }else {
                students = firstName
            }
        
            let vc = SendSMSVCTeacher()
            vc.title = "SMS"
            vc.schoolClass = self.schoolClass
            vc.studentNameList = students
            vc.studentIntelIds = intellinectsIds
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK:- Server Methods
    
    func getStudentList(){
        
        if UtilTeacher.validateNetworkConnection(self) {
            showActivityIndicatory(uiView: view)
            let userDefaults = UserDefaults.standard
            let parameters = [
                "school_db_settings_array": userDefaults.object(forKey: "SCHOOL_DB_SETTINGS") ?? "school_db_settings_array",
                "action": "StudentList",
                "class_id": schoolClass!.classID.description,
                "division_id": schoolClass!.divisionID.description,
                ] as [String : Any]
            Alamofire.request(WebServiceUrls.attendanceService,method: .post,parameters: parameters).responseJSON { [weak self] (response) in
                self?.stopActivityIndicator()
                if response.result.error != nil {
                    UtilTeacher.invokeAlertMethod("Failed!", strBody: "Error Occured While Processing Data ", delegate: nil,vcobj : self!)
                }
                if let jsonresponse = response.result.value {
                    
                    let originalResponse = JSON(jsonresponse)
                    self?.getSuccessResponse(originalResponse: originalResponse)
                }
            }
        }
    }
    
    func getSuccessResponse(originalResponse : JSON)
    {
        if(originalResponse.count > 0)
        {
            for (key,subJson):(String, JSON) in originalResponse {
                if key == "student_list_array" {
                    for item in subJson.arrayValue {
                        let student = StudentTeacher()
                        student.studentName = item["student_name"].stringValue.capitalized
                        student.studentId = Int(item["student_id"].stringValue)!
                        student.studentRollNo = Int(item["student_rollno"].stringValue)!
                        student.intellinectId = item["intellinectid"].stringValue
                        studentList.append(student)
                    }
                }
            }
            tableView.reloadData()
            tableView.animateTable()
        }
    }
}

class StudentCellTeacher: UITableViewCell {
    
    let rollLabel: UILabel = {
        let rollLbl = UILabel()
        rollLbl.textAlignment = .center
        rollLbl.font = UIFont(name: "Helvetica Neue", size: 13)
        rollLbl.translatesAutoresizingMaskIntoConstraints = false
        return rollLbl
    }()
    
    let nameLabel: UILabel = {
        let nameLbl = UILabel()
        nameLbl.font = UIFont(name: "Helvetica Neue", size: 13)
        nameLbl.textAlignment = .justified
        nameLbl.translatesAutoresizingMaskIntoConstraints = false
        return nameLbl
    }()
    
    func setViews() {
        
        self.contentView.addSubview(rollLabel)
        // needed constraints x,y,w,h
        rollLabel.leftAnchor.constraint(equalTo: self.contentView.leftAnchor).isActive = true
        rollLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor).isActive = true
        rollLabel.widthAnchor.constraint(equalToConstant: 40).isActive = true
        rollLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor).isActive = true

        self.contentView.addSubview(nameLabel)
        // needed constraints x,y,w,h
        nameLabel.rightAnchor.constraint(equalTo: self.contentView.rightAnchor).isActive = true
        nameLabel.leftAnchor.constraint(equalTo: rollLabel.rightAnchor,constant: 12).isActive = true
        nameLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor).isActive = true
        nameLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor).isActive = true
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        setViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

