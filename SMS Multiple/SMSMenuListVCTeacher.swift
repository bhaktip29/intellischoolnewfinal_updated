//
//  SMSMenuListVC.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 25/07/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SMSMenuListVCTeacher: BaseMenuVCTeacher,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    //MARK:- View Controllers Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        self.title = "SMS List"
        getSMSCountData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
    }
    
    func getLabel() -> UILabel {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.font = UIFont(name: "Helvetica Neue", size: 13)
        lbl.textAlignment = .center
        lbl.layer.borderColor = UIColor.lightGray.cgColor
        lbl.layer.borderWidth = 0.3
        return lbl
    }
    
    //MARK:- CollectionView Methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return msgCounts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: listCellId, for: indexPath) as! ListMenuCell
        
        let pMsg = msgCounts[indexPath.row]
        
        for i in 0...pMsg.countArray.count {
            let lbl = getLabel()
            switch i+1 {
            case 1:
                lbl.text = months[indexPath.row]
                break
            case 2,3,4,5:
                let counClass = pMsg.countArray[i-1]
                lbl.text = counClass.count
                break
            default:
                break
            }
            cell.stackView.addArrangedSubview(lbl)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let vc = SMSListDisplayVCTeacher()
        vc.title = self.title
        let pMsg = msgCounts[indexPath.row]
        if let yr = pMsg.year, let mnth = pMsg.month {
            vc.selectedYear = yr
            vc.selectedMonth = mnth
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Server Methods
    
    func clearValues() {
        months.removeAll()
        msgCounts.removeAll()
        group.removeAll()
        group.append("Month")
    }
    
    // Get SMS count Data
    func getSMSCountData() {
        
        if UtilTeacher.validateNetworkConnection(self) {

            let userDefaults = UserDefaults.standard
            let  parameters = [
                "school_db_settings_array": userDefaults.object(forKey: "SCHOOL_DB_SETTINGS") ?? "NA",
                "action": "SMSCount",
                "employee_id": userDefaults.string(forKey: "EMPLOYEE_ID") ?? "NA",
                "role_id": userDefaults.string(forKey: "EMPLOYEE_ROLE_ID") ?? "NA",
                "school_employee_class_setting_array": userDefaults.string(forKey: "SUBJECT_SETTINGS") ?? "NA"] as [String : Any]
            showActivityIndicatory(uiView: view)
            Alamofire.request(WebServiceUrls.multiSMSService,method: .post,parameters: parameters)
                .responseJSON {[weak self] (response) in
                    self?.stopActivityIndicator()
                    if response.result.error != nil {
                        UtilTeacher.invokeAlertMethod("Failed!", strBody: "Error Occured While Processing Data ", delegate: nil,vcobj : self!)
                    }
                    if let jsonresponse = response.result.value {
                        self?.getSuccessResponse(originalResponse: JSON(jsonresponse))
                    }
            }
        }
    }
    
    func getSuccessResponse(originalResponse : JSON)
    {
        clearValues()
        print("Original Response",originalResponse)
        if originalResponse.count > 0
        {
            for (key,subJson):(String, JSON) in originalResponse {
                if(key == "class_group")
                {
                    if subJson.stringValue != "NULL" {
                        for item in subJson.arrayValue {
                            let groupItem = item.dictionaryValue
                            if let groupName = groupItem["group_name"]?.stringValue {
                                switch groupName {
                                case "Pre Primary":
                                    group.append("Pre-Pri")
                                    break
                                case "Primary":
                                    group.append("Pri")
                                    break
                                case "Secondary":
                                    group.append("Sec")
                                    break
                                case "Jr College":
                                    group.append("Jr Clg")
                                    break
                                default:
                                    break
                                }
                            }
                        }
                    }
                }
                
                if(key == "month")
                {
                    for item in subJson.arrayValue {
                        months.append(item.stringValue)
                    }
                }
                
                if(key == "message")
                {
                    for dic in subJson.arrayValue {
                        let pMsg = ParticularMessage()
                        var tempArray = [CountClass]()
                        for item in dic["particular_message"].arrayValue {
                            let msgCount = CountClass()
                            msgCount.groupName = item.dictionaryValue["group_name"]?.stringValue
                            msgCount.count = item.dictionaryValue["no_count"]?.stringValue
                            msgCount.classGroupId = item.dictionaryValue["class_group_id"]?.stringValue
                            tempArray.append(msgCount)
                        }
                        pMsg.month = dic["month_no"].stringValue
                        pMsg.year = dic["year"].stringValue
                        pMsg.countArray = tempArray
                        msgCounts.append(pMsg)
                    }
                }
            }
            setViewsAfterResponse()
            collectionView.reloadData()
        }
    }
}
