//
//  ClassDivListVC.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 25/07/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ClassDivListVCTeacher: BaseViewControllerTeacher,UITableViewDelegate,UITableViewDataSource {
    
    var classList = [SchoolClassTeacher]()
    let cellId = "CellIdentifire"
    
    let bottomLabel : UILabel = {
        let lbl = UILabel()
        lbl.text = DisplayTextMsgs.calssDivisionSelectionText
        lbl.font = UIFont.boldSystemFont(ofSize: 13)
        lbl.backgroundColor = UIColor.lightGray
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.white
        return lbl
    }()
    
    let tableView : UITableView = {
        
        let tblView = UITableView()
        tblView.tableFooterView = UIView()
        tblView.translatesAutoresizingMaskIntoConstraints = false
        tblView.separatorInset = UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 4)
        tblView.separatorInset = UIEdgeInsets.zero
        return tblView
    }()
    
    //MARK:- Custom Views Methods
    
    private func setUpTableView() {
        
        view.addSubview(tableView)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        // needed constarunts x,y,w,h
        tableView.topAnchor.constraint(equalTo:view.topAnchor, constant : 64).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: bottomLabel.topAnchor).isActive = true
    }
    
    private func setUpBottomLabel() {
        
        view.addSubview(bottomLabel)
        // needed constarunts x,y,w,h
        bottomLabel.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        if #available(iOS 11.0, *) {
            bottomLabel.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            bottomLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
        bottomLabel.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        bottomLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    // MARK:- View Controllers Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 25.0/255.0, green: 89.0/255.0, blue: 194.0/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(handleSlide))
        navigationItem.leftBarButtonItem?.tintColor = .white
        
        navigationItem.title = ViewControllerTitlesTeacher.classOrDivison
        tableView.cellLayoutMarginsFollowReadableWidth = false
        tableView.register(ClassCell.self, forCellReuseIdentifier: cellId)
        setUpBottomLabel()
        setUpTableView()
        setUpMenuList()
        getClassesAndDivisions()
    }
    
    @objc func handleSlide() {
           print("back button tapped..")
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
                  let controller = storyboard.instantiateViewController(withIdentifier: "vc")
                  self.present(controller, animated: true, completion: nil)
       }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
    }
    
    private func setUpMenuList(){
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image:UIImage(named:"iconList"), style: .plain, target: self, action: #selector(handleList))
    }
    
    @objc func handleList() {
        navigationController?.pushViewController(SMSMenuListVCTeacher(), animated: true)
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return classList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ClassCell
        let schoolClass:SchoolClassTeacher = classList[indexPath.row]
        cell.textLabel?.text = "Class \(schoolClass.className!) \(schoolClass.divisionName!)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if UtilTeacher.validateNetworkConnection(self){
            let vc = StudentListByClassOrDivVCTeacher()
            vc.schoolClass = classList[indexPath.row]
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    //MARK:- Server methods
    
    func getClassesAndDivisions()
    {
        if UtilTeacher.validateNetworkConnection(self){
            let userDefaults = UserDefaults.standard
            
            let parameters = [
                "school_db_settings_array":  userDefaults.string(forKey: "SCHOOL_DB_SETTINGS") ?? "school_db_settings",
                "school_employee_class_setting_array":  userDefaults.string(forKey: "SUBJECT_SETTINGS") ?? "subject_settings",
                "action":"ClassListIOS",
                "employee_id" : userDefaults.string(forKey: "EMPLOYEE_ID") ?? "employee_id",
                "employee_role_id": userDefaults.string(forKey: "EMPLOYEE_ROLE_ID") ?? "employee_role_id"] as [String : Any]
            showActivityIndicatory(uiView: view)
            Alamofire.request(WebServiceUrls.attendanceService,method:.post,parameters: parameters)
                .responseJSON{ [weak self] (response)  in
                    self?.stopActivityIndicator()
                    if response.result.error != nil {
                        UtilTeacher.invokeAlertMethod("Failed!", strBody: "Error Occured While Processing Data ", delegate: nil,vcobj : self!)
                    }
                    if let jsonresponse = response.result.value {
                        
                        let originalResponse = JSON(jsonresponse)
                        UtilTeacher.LoaderEnd()
                        self?.getSuccessResponse(originalResponse: originalResponse)
                    }
            }
        }
    }
    
    func getSuccessResponse(originalResponse : JSON)
    {
        if(originalResponse.count > 0)
        {
            for (key,subJson):(String, JSON) in originalResponse {
                
                if key == "response" {
                    UtilTeacher.invokeAlertMethod("Failed!", strBody: "No class details available..!!!", delegate: nil,vcobj : self)
                    break
                }else{
                    
                    let schoolClass : SchoolClassTeacher = SchoolClassTeacher()
                    schoolClass.classID = Int(subJson["class_id"].stringValue)!
                    schoolClass.boardID = Int(subJson["board_id"].stringValue)!
                    schoolClass.divisionID = Int(subJson["division_id"].stringValue)!
                    schoolClass.className = subJson["class_name"].stringValue
                    schoolClass.divisionName = subJson["division_title"].stringValue
                    classList.append(schoolClass)
                }
            }
            tableView.reloadData()
            tableView.animateTable()
        }
    }
}

