//
//  SMSListDisplayVC.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 26/07/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SMSListDisplayVCTeacher: BaseViewControllerTeacher,UITableViewDelegate,UITableViewDataSource {
    
    let displayCellId = "SMSListCell"
    var smsDetails = [SMSDetail]()
    var selectedMonth : String = String()
    var selectedYear : String = String()
    var gradientLayer: CAGradientLayer!
    
    let tableView: UITableView = {
        let tbl = UITableView()
        tbl.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tbl.translatesAutoresizingMaskIntoConstraints = false
        return tbl
    }()
    
    //MARK:- Custom Views Setup Methods
    
    private func setUpTableView() {
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        tableView.backgroundColor = UIColor.white
        
        view.addSubview(tableView)
        // needed constyarints
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    //MARK:- View Controller Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(SMSDetailDisplayCell.self, forCellReuseIdentifier: displayCellId)
        setUpTableView()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // createGradientLayer()
        getSMSData()
    }
    
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [UIColor.white.cgColor,UIColor.blue.cgColor]
        gradientLayer.locations = [0.0,1.0]
        self.view.layer.addSublayer(gradientLayer)
        view.bringSubviewToFront(tableView)
        view.bringSubviewToFront(actInd)
    }
    
    func convertDate(_ inputDate:Date)-> String {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "dd MMM yyyy"
        return inputFormatter.string(from: inputDate)
    }
    
    //MARK:- Table View Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.smsDetails.count == 0{
            self.tableView.setEmptyMessage("SMS List not found!")
        }else{
            self.tableView.restore()
        }
        return smsDetails.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: displayCellId, for: indexPath) as! SMSDetailDisplayCell
        
//        smsDetails.sort(by: { $0.classId > $1.classId })
        let sms:SMSDetail = smsDetails[indexPath.row]
        cell.descriptionLbl.text = sms.smsContent
        cell.nameLbl.text = sms.teacherName
        cell.dateLbl.text = convertDate(sms.date)
        cell.classLbl.text = "\(sms.className ?? "") \(sms.divisionName ?? "")"
        
        if(sms.studentName.count < 1){
            cell.studentListBtn.isHidden = true
        } else {
            cell.studentListBtn.isHidden = false
        }
        cell.studentListBtn.tag = indexPath.row
        cell.studentListBtn.addTarget(self, action: #selector(displayStudentList), for: .touchUpInside)
        return cell
    }

    @objc func displayStudentList(_ sender: UIButton){
    
        let vc = StudentListVCTeacher()
        let tag = sender.tag
        let sms:SMSDetail = smsDetails[tag]
        vc.studentList = sms.studentName 
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChild(vc)
        view.addSubview(vc.view)
    }
 
    //MARK:- Server Methods

    // Get SMS Data
    private func getSMSData()
    {
        if UtilTeacher.validateNetworkConnection(self) {

            let userDefaults = UserDefaults.standard
            let parameters = [
                "school_db_settings_array":  userDefaults.string(forKey: "SCHOOL_DB_SETTINGS") ?? "school_db_settings",
                "action":"SMSDetails",
                "month":selectedMonth,
                "year":selectedYear,
                "employee_id": userDefaults.string(forKey: "EMPLOYEE_ID") ?? "employee_id",
                "role_id": userDefaults.string(forKey: "EMPLOYEE_ROLE_ID") ?? "employee_role_id"] as [String : Any]
            showActivityIndicatory(uiView: view)
            Alamofire.request(WebServiceUrls.multiSMSService,method: .post,parameters: parameters)
                .responseJSON {[weak self] (response) in
                    self?.stopActivityIndicator()
                    if response.result.error != nil {
                        DispatchQueue.main.async {
                            self?.tableView.reloadData()
                            self?.tableView.animateTable()
                        }
//                        UtilTeacher.invokeAlertMethod("Failed!", strBody: "Error Occured While Processing Data ", delegate: nil,vcobj : self!)
                    }
                    if let jsonresponse = response.result.value {
                        let originalResponse = JSON(jsonresponse)
                        self?.getSMSResponse(originalResponse: originalResponse)
                    }
            }
        }
    }
    private func getSMSResponse(originalResponse : JSON)
    {
        if(originalResponse.count > 0)
        {
            for (_,subJson):(String, JSON) in originalResponse {
                
                let smsDetail = SMSDetail()
                smsDetail.classId = Int(subJson["class_id"].stringValue)!
                smsDetail.divisionId = subJson["division_id"].stringValue
                smsDetail.divisionName = subJson["division_name"].stringValue
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat =  "yyyy-MM-dd"
                smsDetail.date =  dateFormatter.date(from: subJson["date"].stringValue) ?? Date()
                smsDetail.teacherName = subJson["teacher_name"].stringValue
                smsDetail.studentName = subJson["student_name"].arrayValue.map({ $0.stringValue })
                smsDetail.className = subJson["class_name"].stringValue
                smsDetail.smsContent = subJson["sms_content"].stringValue
                smsDetails.append(smsDetail)
            }
            smsDetails.sort(by: {$0.date.timeIntervalSinceNow > $1.date.timeIntervalSinceNow })
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.tableView.animateTable()
            }
        }
    }
 }

class SMSDetail {

    var classId: Int = 0
    var divisionId: String?
    var divisionName: String?
    var date = Date()
    var teacherName: String?
    var className: String?
    var studentName = [String]()
    var smsContent: String?
}

class SMSDetailDisplayCell: UITableViewCell {

    let dateLbl: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .right
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let classLbl: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.textAlignment = .left
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let nameLbl:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.black
        lbl.textAlignment = .justified
        lbl.numberOfLines = 0
        lbl.font = UIFont.boldSystemFont(ofSize: 13)
        return lbl
    }()
    
    let descriptionLbl:UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .justified
        let font = UIFont.systemFont(ofSize: 15)
        lbl.font = font
        lbl.adjustsFontSizeToFitWidth = true
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 0
        lbl.backgroundColor = UIColor.lightGray.withAlphaComponent(0.4)
        lbl.layer.cornerRadius = 3
        lbl.layer.masksToBounds = true
        let text = lbl.text ?? "" as String
        let size = text.size(withAttributes: [NSAttributedString.Key.font:font])
        lbl.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height+20)
        //lbl.amx_autoScaleEnabled = true

        lbl.sizeToFit()
        return lbl
    }()
    
    let studentListBtn: UIButton = {
        let btn = UIButton(type: .system)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Student List", for: .normal)
        btn.contentHorizontalAlignment = .right
        return btn
    }()
    
    let containerView:UIView = {
        let v = CardView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = UIColor.white
        v.layer.cornerRadius = 3
        v.layer.masksToBounds = true
        return v
    }()
    
    func setUpViews() {
        
        self.addSubview(containerView)
        // needed constraints
        containerView.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 8).isActive = true
        containerView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        containerView.widthAnchor.constraint(equalTo:self.widthAnchor,constant: -16).isActive = true
        containerView.heightAnchor.constraint(equalTo: self.heightAnchor,constant: -8).isActive = true
        
        containerView.addSubview(dateLbl)
        // needed constraints
        dateLbl.rightAnchor.constraint(equalTo: containerView.rightAnchor,constant: -8).isActive = true
        dateLbl.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        dateLbl.widthAnchor.constraint(equalTo:containerView.widthAnchor,multiplier:1/2,constant:-8).isActive = true
        dateLbl.heightAnchor.constraint(equalToConstant:25).isActive = true
        
        containerView.addSubview(classLbl)
        // needed constraints
        classLbl.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        classLbl.leftAnchor.constraint(equalTo: containerView.leftAnchor,constant: 8).isActive = true
        classLbl.widthAnchor.constraint(equalTo: containerView.widthAnchor, multiplier: 1/2,constant: -8).isActive = true
        classLbl.heightAnchor.constraint(equalToConstant:25).isActive = true
        
        containerView.addSubview(studentListBtn)
        // needed constraints
        studentListBtn.topAnchor.constraint(equalTo: dateLbl.bottomAnchor).isActive = true
        studentListBtn.rightAnchor.constraint(equalTo: containerView.rightAnchor,constant:-8).isActive = true
        studentListBtn.widthAnchor.constraint(equalTo: containerView.widthAnchor, multiplier: 1/2).isActive = true
        studentListBtn.heightAnchor.constraint(equalToConstant:25).isActive = true
        
        containerView.addSubview(nameLbl)
        // needed constraints
        nameLbl.topAnchor.constraint(equalTo: classLbl.bottomAnchor).isActive = true
        nameLbl.leftAnchor.constraint(equalTo: containerView.leftAnchor,constant: 8).isActive = true
        nameLbl.rightAnchor.constraint(equalTo: studentListBtn.leftAnchor).isActive = true
        nameLbl.heightAnchor.constraint(equalToConstant:25).isActive = true
        
        containerView.addSubview(descriptionLbl)
        // needed constraints
        descriptionLbl.topAnchor.constraint(equalTo: nameLbl.bottomAnchor,constant: 8).isActive = true
        descriptionLbl.rightAnchor.constraint(equalTo: containerView.rightAnchor,constant: -8).isActive = true
        descriptionLbl.widthAnchor.constraint(equalTo: containerView.widthAnchor,constant: -16).isActive = true
        descriptionLbl.bottomAnchor.constraint(equalTo: containerView.bottomAnchor,constant: -8).isActive = true
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        setUpViews()
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
