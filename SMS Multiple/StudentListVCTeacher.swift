//
//  StudentListVC.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 27/07/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit

class StudentListVCTeacher: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var studentList = [String]() {
        didSet {
            tableView.reloadData()
        }
    }
    let cellIdentifire = "StudentCell"
    
    let tableView : UITableView = {
        let tblView = UITableView()
        tblView.translatesAutoresizingMaskIntoConstraints = false
        tblView.separatorInset = UIEdgeInsets.zero
        return tblView
    }()
    
    let headerLabel : UILabel = {
        let lbl = UILabel()
        lbl.text = "Student Name"
        lbl.textAlignment = .center
        lbl.font = UIFont.boldSystemFont(ofSize: 13)
        lbl.backgroundColor = CustomColor.greenColor
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.white
        return lbl
    }()
    
    let OkBtn:UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Ok", for: .normal)
        btn.layer.borderColor = TheamColors.baseColor.cgColor
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.borderWidth = 0.3
        btn.layer.cornerRadius = 5
        btn.layer.masksToBounds = true
        btn.addTarget(self, action: #selector(handleCancel), for: .touchUpInside)
        return btn
    }()
    
    //MARK:- Custom View Setup Methods
    
    private func setUpViews() {

        let vw  = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = UIColor.white
        vw.layer.cornerRadius = 5
        vw.layer.masksToBounds = true
        
        view.addSubview(vw)
        // needed constraints x,y,w,h
        vw.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        vw.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        vw.widthAnchor.constraint(equalToConstant: 300).isActive = true
        vw.heightAnchor.constraint(equalToConstant: 350).isActive = true

        vw.addSubview(OkBtn)
        // needed constraints x,y,w,h
        OkBtn.bottomAnchor.constraint(equalTo: vw.bottomAnchor,constant:-4).isActive = true
        OkBtn.rightAnchor.constraint(equalTo: vw.rightAnchor,constant:-8).isActive = true
        OkBtn.widthAnchor.constraint(equalTo:vw.widthAnchor,constant:-16).isActive = true
        OkBtn.heightAnchor.constraint(equalToConstant:40).isActive = true
        
        vw.addSubview(headerLabel)
        // needed constraints x,y,w,h
        headerLabel.topAnchor.constraint(equalTo: vw.topAnchor).isActive = true
        headerLabel.rightAnchor.constraint(equalTo: vw.rightAnchor).isActive = true
        headerLabel.widthAnchor.constraint(equalTo:vw.widthAnchor).isActive = true
        headerLabel.heightAnchor.constraint(equalToConstant:30).isActive = true
        
        vw.addSubview(tableView)
        // needed constyarints
        tableView.leftAnchor.constraint(equalTo: vw.leftAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: headerLabel.bottomAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: vw.rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: OkBtn.topAnchor).isActive = true
    }
    
    // MARK:- ViewController methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(StuentListNameCell.self, forCellReuseIdentifier:cellIdentifire)
        tableView.dataSource = self
        tableView.delegate = self
        setUpViews()
    }
    
    //MARK:- TableView Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return studentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifire, for: indexPath) as! StuentListNameCell
        cell.textLabel?.text = studentList[indexPath.row].capitalized
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }

    @objc func handleCancel() {
        self.removeFromParent()
        self.view.removeFromSuperview()
    }
}

class StuentListNameCell: UITableViewCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
