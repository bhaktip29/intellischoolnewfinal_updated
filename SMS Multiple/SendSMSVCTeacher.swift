//
//  SendSMSVC.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 27/07/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SendSMSVCTeacher: BaseViewControllerTeacher,UITextViewDelegate {

    var cellId = "AttachmentCell"
    var studentNameList:String?
    var schoolClass: SchoolClassTeacher?
    var studentIntelIds = [String]()
    
    let studentNameListLbl : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.white
        lbl.backgroundColor = CustomColor.greenColor
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 0
        lbl.sizeToFit()
        return lbl
    }()
    
    let textView : UITextView = {
        let txtView = UITextView()
        txtView.translatesAutoresizingMaskIntoConstraints = false
        txtView.layer.borderColor = UIColor.gray.cgColor
        txtView.layer.borderWidth = 0.3
        txtView.layer.cornerRadius = 2
        txtView.layer.masksToBounds = true
        txtView.font = UIFont.systemFont(ofSize: 15)
        txtView.autocorrectionType = .no
        txtView.backgroundColor = UIColor.groupTableViewBackground.withAlphaComponent(0.1)
        txtView.text = PlaceHolderStrings.tvPlaceHolder
        txtView.amx_autoScaleFont(forReferenceScreenSize: .size4Inch)
        return txtView
    }()
    let sendBtn : UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setImage(UIImage(named: "ic_send_All"), for: .normal)
        btn.addTarget(self, action: #selector(handleSend), for: .touchUpInside)
        print("buttonclick")
//        btn.backgroundColor = CustomColor.greenColor
        return btn
    }()

    var yAnchor : NSLayoutConstraint?
    var txtViewBottomAnchor:NSLayoutConstraint?
    
    //MARK:- Custom Views Setup Methods
    
    private func setUpViews() {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = CustomColor.greenColor
        
        view.addSubview(vw)
        // needed constraints x,y,w,h
        if #available(iOS 11.0, *) {
            vw.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        } else {
            vw.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        }
        vw.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        vw.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        vw.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        vw.addSubview(studentNameListLbl)
        // needed constraints x,y,w,h
        studentNameListLbl.topAnchor.constraint(equalTo: vw.topAnchor).isActive = true
        studentNameListLbl.leftAnchor.constraint(equalTo: vw.leftAnchor,constant:8).isActive = true
        studentNameListLbl.widthAnchor.constraint(equalTo: vw.widthAnchor,constant:-16).isActive = true
        studentNameListLbl.heightAnchor.constraint(equalTo:vw.heightAnchor).isActive = true

        view.addSubview(textView)
        // needed constraints x,y,w,h
        textView.topAnchor.constraint(equalTo: vw.bottomAnchor,constant:1).isActive = true
        textView.leftAnchor.constraint(equalTo: view.leftAnchor,constant:1).isActive = true
        textView.widthAnchor.constraint(equalTo: view.widthAnchor,constant:-2).isActive = true
        txtViewBottomAnchor = textView.bottomAnchor.constraint(equalTo:view.bottomAnchor,constant:-1)
        txtViewBottomAnchor?.isActive = true
        
        setUpBottomView()
    }
    
    var sendBtnBottomAnchor:NSLayoutConstraint?
    
    private func setUpBottomView() {
 
        view.addSubview(sendBtn)
        // needed constraints x,y,w,h
        if #available(iOS 11.0, *) {
            sendBtnBottomAnchor = sendBtn.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,constant:-8)
        } else {
            sendBtnBottomAnchor = sendBtn.bottomAnchor.constraint(equalTo: view.bottomAnchor,constant:-8)
        }
        sendBtnBottomAnchor?.isActive = true
        sendBtn.rightAnchor.constraint(equalTo: view.rightAnchor,constant:-8).isActive = true
        sendBtn.widthAnchor.constraint(equalToConstant: 45).isActive = true
        sendBtn.heightAnchor.constraint(equalToConstant: 45).isActive = true
        sendBtn.layer.cornerRadius = 22.5
        sendBtn.layer.masksToBounds = true
    }
    
    //MARK:- View Controllers Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViews()
        textView.delegate = self
        view.backgroundColor = UIColor.white
        studentNameListLbl.text = "To:- \(studentNameList ?? "")"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupKeyboardObservers()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK:- Handler Methods
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == PlaceHolderStrings.tvPlaceHolder {
            textView.text = ""
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        textView.resignFirstResponder()
    }
    
    //MARK:- Keyborad Show/Hide Methods
    
    func setupKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func handleKeyboardWillShow(_ notification: Notification) {
        let keyboardFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
        let keyboardDuration = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue
        yAnchor?.constant = -(keyboardFrame!.height+1)
        txtViewBottomAnchor?.constant = -(keyboardFrame!.height+1)
        sendBtnBottomAnchor?.constant = -(keyboardFrame!.height+8)
        UIView.animate(withDuration: keyboardDuration!, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func handleKeyboardWillHide(_ notification: Notification) {
        let keyboardDuration = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue
        
        yAnchor?.constant = 0
        txtViewBottomAnchor?.constant = -1
        sendBtnBottomAnchor?.constant = -8
        UIView.animate(withDuration: keyboardDuration!, animations: {
            self.view.layoutIfNeeded()
            if self.textView.text == "" {
                self.textView.text = PlaceHolderStrings.tvPlaceHolder
            }
        })
    }
    
    //MARK:- Server Methods
    
    @objc func handleSend() {
        
        let messageToBeSend:String = textView.text
        if (messageToBeSend.isEqual("") || messageToBeSend.isEqual(PlaceHolderStrings.tvPlaceHolder)){
            UtilTeacher.invokeAlertMethod("Sending Failed!", strBody: "Please enter message", delegate: nil,vcobj:self)
    
        }else {
            sendSMS(messageToBeSend)
        }
    }
    
    func sendSMS(_ message:String) {
        
        let userDefaults = UserDefaults.standard
        showActivityIndicatory(uiView: view)
        let  parameters = [
            "employee_id": userDefaults.string(forKey: "EMPLOYEE_ID") ?? "error",
            "school_db_settings_array": userDefaults.object(forKey: "SCHOOL_DB_SETTINGS") ?? "error",
            "sms_message": message,
            "sms_settings": userDefaults.object(forKey: "SMS_SETTINGS") ?? "sms_settings",
            "class_id": schoolClass!.classID.description,
            "division_id": schoolClass!.divisionID.description,
            "action": "SendSMSForMultipleStudent",
            "intellinectid": studentIntelIds.description] as [String : Any]
       
        
//        debugPrint(parameters["school_db_settings_array"] as Any)
//        debugPrint(parameters["class_id"] as Any)
        
        Alamofire.request(WebServiceUrls.studentInfoService,method: .post,parameters: parameters)
            .responseJSON { [weak self] (response) in
                print(WebServiceUrls.studentInfoService as Any)
                print(response.result.value as Any)
                self?.stopActivityIndicator()
                if response.result.error != nil {
                    UtilTeacher.invokeAlertMethod("Failed!", strBody: "Error Occured While Processing Data ", delegate: nil,vcobj : self!)
                }
                if let jsonresponse = response.result.value {
                    let originalResponse = JSON(jsonresponse)
                    self?.getSuccessResponse(originalResponse: originalResponse)
                }
        }
    }

    func getSuccessResponse(originalResponse : JSON)
    {
        if(originalResponse.count > 0)
        {
            for (key,subJson):(String, JSON) in originalResponse {
                if key == "send_sms" {
                    if subJson.boolValue {
                        let alertController = UIAlertController(title: "", message: "SMS sent successfully", preferredStyle: UIAlertController.Style.alert)
                        
                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
                        { [weak self](result : UIAlertAction) -> Void in
                            
                            var controllers = self?.navigationController?.viewControllers
                            controllers?.removeLast()
                            let vc = SMSMenuListVCTeacher()
                            controllers?.append(vc)
                            self?.navigationController?.viewControllers = controllers!
                        }
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                    break
                }
            }
        }
    }
}
