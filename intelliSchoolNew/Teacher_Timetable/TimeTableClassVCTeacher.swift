//
//  TimeTableClassVC.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 20/06/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class TimeTableClassVCTeacher : BaseViewControllerTeacher,UITableViewDataSource,UITableViewDelegate {
    
    var schoolClasses = [SchoolClassTeacher]()
    let cellId = "CellIdentifire"
    var caseNumber:Int = 0
    
    let tableView : UITableView = {
        let tblView = UITableView()
        tblView.translatesAutoresizingMaskIntoConstraints = false
        tblView.separatorInset = UIEdgeInsets.zero
        return tblView
    }()
    
    //MARK:- Custom Views Setup Methods
    
    private func setUpTableView() {
        
        view.addSubview(tableView)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        // needed constarunts x,y,w,h
        tableView.topAnchor.constraint(equalTo:view.topAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    //MARK:- View Controller Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(handleSlide))
        self.title = ViewControllerTitlesTeacher.classOrDivison
        tableView.cellLayoutMarginsFollowReadableWidth = false
        tableView.register(ClassCell.self, forCellReuseIdentifier: cellId)
        setUpTableView()
        getClassList()
    }
    @objc func handleSlide() {
        print("back button tapped..")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "vc")
        self.present(controller, animated: true, completion: nil)
       }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolClasses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ClassCell
        let schoolClass:SchoolClassTeacher = schoolClasses[indexPath.row]
        cell.textLabel?.text = "Class \(schoolClass.className!) \(schoolClass.divisionName!)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if caseNumber == 7 {
            let vc = ClassTimeTableVCTeacher()
            vc.schoolClass = schoolClasses[indexPath.row]
            navigationController?.pushViewController(vc, animated: true)
//        } else{
//            let vc = ExamTimeTableListVCTeacher()
//            vc.schoolClass = schoolClasses[indexPath.row]
//            navigationController?.pushViewController(vc, animated: true)
//        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    //MARK:- Server Methods
    
    private func getClassList() {
        
        if UtilTeacher.validateNetworkConnection(self) {
            
            let userDefaults = UserDefaults.standard
            let parameters = [
                "school_db_settings_array": userDefaults.object(forKey: "SCHOOL_DB_SETTINGS") ?? "school_db_settings_array",
                "school_employee_class_setting_array":  userDefaults.string(forKey: "SUBJECT_SETTINGS") ?? "subject_settings",
                "action":"ClassListIOS",
                "employee_id" : userDefaults.string(forKey: "EMPLOYEE_ID") ?? "employee_id",
                "employee_role_id": userDefaults.string(forKey: "EMPLOYEE_ROLE_ID") ?? "employee_role_id"] as [String : Any]
            
            showActivityIndicatory(uiView: view)
            Alamofire.request(WebServiceUrls.attendanceService,method: .post,parameters: parameters)
                .responseJSON { [weak self] (response) in
                    self?.stopActivityIndicator()
                    print("TimeTableClassVCTeacher parameters \(parameters)")
                    print("TimeTableClassVCTeacher response\(response.result.value)")
                    if response.result.error != nil {
                        UtilTeacher.invokeAlertMethod("Failed!", strBody: "Error Occured While Processing Data ", delegate: nil,vcobj : self!)
                    }
                    if let jsonresponse = response.result.value {
                        let originalResponse = JSON(jsonresponse)
                        self?.parseResponse(originalRespose: originalResponse)
                    }
            }
        }
    }
    
    private func parseResponse(originalRespose: JSON) {
        
        if(originalRespose.count > 0)
        {
            for (key,subJson):(String, JSON) in originalRespose {
                
                if key == "response" {
                    UtilTeacher.invokeAlertMethod("Failed!", strBody: "No class details available..!!!", delegate: nil,vcobj : self)
                    break
                }else{
                    
                    let schoolClass = SchoolClassTeacher()
                    schoolClass.classID = Int(subJson["class_id"].stringValue)!
                    schoolClass.boardID = Int(subJson["board_id"].stringValue)!
                    schoolClass.divisionID = Int(subJson["division_id"].stringValue)!
                    schoolClass.className = subJson["class_name"].stringValue
                    schoolClass.divisionName = subJson["division_title"].stringValue
                    schoolClasses.append(schoolClass)
                }
            }
            tableView.reloadData()
            tableView.animateTable()
        }
    }
}
