//
//  ExamTimeTableDisVC.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 21/06/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ExamTimeTableDisVCTeacher: BaseViewControllerTeacher,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    let cellId = "TimeCell"
    let headerCellId  = "HeaderCell"

    var schoolClass = SchoolClassTeacher()
    var examName : String?
    var timeTableList = [ExamTimeTableTeacher]()
    
    struct Headers {
        static let Date = "Date"
        static let Time = "Time"
        static let Subject = "Subject"
        static let PType = "Type"
        static let Syllabus = "Syllabus"
    }

    let collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        let clView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        clView.backgroundColor = UIColor.groupTableViewBackground
        clView.translatesAutoresizingMaskIntoConstraints = false
        return clView
    }()
    
    let examNameLbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textAlignment = .center
        lbl.font = UIFont.systemFont(ofSize: 13)
        return lbl
    }()
    
    // MARK:- Custom Views Setup Methods
    
    private func setUpCollectionView() {
        
        view.addSubview(collectionView)
        collectionView.dataSource = self
        collectionView.delegate = self
        
        // needed constarunts x,y,w,h
        collectionView.topAnchor.constraint(equalTo: examNameLbl.bottomAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    private func setUpExamNameLabel() {
        
        view.addSubview(examNameLbl)
        //needed constraints
        examNameLbl.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        examNameLbl.topAnchor.constraint(equalTo: view.topAnchor,constant:64).isActive = true
        examNameLbl.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        examNameLbl.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    
    //MARK:- View Controllers Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        collectionView.register(ExamTimeTableCVCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.register(ExamTimeTableHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader , withReuseIdentifier: headerCellId)
        if let className = schoolClass.className, let division = schoolClass.divisionName {
            self.title = " Class \(className) \(division)"
        }
        setUpExamNameLabel()
        if let name = examName {
            examNameLbl.text = name
        }
        setUpCollectionView()
    }
    
    //MARK:- CollectionView Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return timeTableList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ExamTimeTableCVCellTeacher
        
        let examTT = timeTableList[indexPath.row] as ExamTimeTableTeacher
        cell.timeLbl.text = examTT.date?.replacingOccurrences(of: "-", with: "/")
        cell.timeLbl1.text = examTT.time?.replacingOccurrences(of: " to ", with: "-")
        cell.timeLbl2.text = examTT.subject
        cell.timeLbl3.text = examTT.type  ?? "--"
        cell.timeLbl4.text = examTT.syllabus
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 35)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerCellId, for: indexPath as IndexPath) as! ExamTimeTableHeaderTeacher
        headerView.headerLbl.text = Headers.Date
        headerView.headerLbl1.text = Headers.Time
        headerView.headerLbl2.text = Headers.Subject
        headerView.headerLbl3.text = Headers.PType
        headerView.headerLbl4.text = Headers.Syllabus
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
    {
        return CGSize(width: collectionView.bounds.width, height: 30)
    }
    
   }

class ExamTimeTableCVCellTeacher :UICollectionViewCell {
    
    var timeLbl = UILabel()
    var timeLbl1 = UILabel()
    var timeLbl2 = UILabel()
    var timeLbl3 = UILabel()
    var timeLbl4 = UILabel()
    
    private func setUpLbels() {
        
        var lbls = [UILabel]()
        
        func getLabel() -> UILabel {
            let lbl = UILabel()
            lbl.textColor = UIColor.black
            lbl.font = UIFont(name: "Helvetica Neue", size: 11)
            lbl.textAlignment = .center
            lbl.numberOfLines = 0
            lbl.layer.borderColor = UIColor.lightGray.cgColor
            lbl.layer.borderWidth = 0.3
            lbl.sizeToFit()
            return lbl
        }
        
        timeLbl = getLabel()
        lbls.append(timeLbl)
        timeLbl1 = getLabel()
        lbls.append(timeLbl1)
        timeLbl2 = getLabel()
        lbls.append(timeLbl2)
        timeLbl3 = getLabel()
        lbls.append(timeLbl3)
        timeLbl4 = getLabel()
        lbls.append(timeLbl4)
        
        let stackView = UIStackView(arrangedSubviews: lbls)
        stackView.alignment = .fill
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(stackView)
        // needed constarints
        stackView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        stackView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        stackView.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        setUpLbels()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ExamTimeTableHeaderTeacher: UICollectionReusableView {
    
    var headerLbl = UILabel()
    var headerLbl1 = UILabel()
    var headerLbl2 = UILabel()
    var headerLbl3 = UILabel()
    var headerLbl4 = UILabel()
    
    private func setUpLbels() {
        
        var lbls = [UILabel]()
        
        func getLabel() -> UILabel {
            let lbl = UILabel()
            lbl.textColor = UIColor.black
            lbl.font = UIFont.boldSystemFont(ofSize: 13) //UIFont(name: "Helvetica Neue", size: 13)
            lbl.textAlignment = .center
            lbl.layer.borderColor = UIColor.darkGray.cgColor
            lbl.layer.borderWidth = 0.3
            return lbl
        }
        
        headerLbl = getLabel()
        lbls.append(headerLbl)
        headerLbl1 = getLabel()
        lbls.append(headerLbl1)
        headerLbl2 = getLabel()
        lbls.append(headerLbl2)
        headerLbl3 = getLabel()
        lbls.append(headerLbl3)
        headerLbl4 = getLabel()
        lbls.append(headerLbl4)
        
        let stackView = UIStackView(arrangedSubviews: lbls)
        stackView.alignment = .fill
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(stackView)
        // needed constarints
        stackView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        stackView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        stackView.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.lightGray
        setUpLbels()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

