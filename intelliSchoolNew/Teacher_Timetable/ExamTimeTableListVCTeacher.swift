//
//  ExamTimeTableListVC.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 20/06/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ExamTimeTableListVCTeacher: BaseViewControllerTeacher,UIPickerViewDelegate,UIPickerViewDataSource {

    let vcTitle = "Examination Selection"
    let select = "---Select---"
    
    var schoolClass = SchoolClassTeacher()
    var selectedExam = String()
    private var examList = [String]()
    private var examListWithTTDetails = [String: [ExamTimeTableTeacher]]()
    private var selectedIndex : Int = 0
    
    let containerView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    let errorLbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textAlignment = .center
        lbl.text = "Ooops....TimeTable details not availables!!"
        return lbl
    }()
    
    let pickerView: UIPickerView = {
        let picker = UIPickerView()
        picker.translatesAutoresizingMaskIntoConstraints = false
        return picker
    }()
    
    //MARK:- Custom Views Setup Methods
    
    private func setUpErrorLabel() {
        
        view.addSubview(errorLbl)
        //needed constraints
        errorLbl.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        errorLbl.topAnchor.constraint(equalTo: view.topAnchor,constant:72).isActive = true
        errorLbl.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        errorLbl.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }

    private func setUpContainerView() {
        
        view.addSubview(containerView)
        //needed constraints
        containerView.leftAnchor.constraint(equalTo: view.leftAnchor,constant:8).isActive = true
        containerView.topAnchor.constraint(equalTo: view.topAnchor,constant:72).isActive = true
        containerView.widthAnchor.constraint(equalTo: view.widthAnchor,constant:-16).isActive = true
        containerView.heightAnchor.constraint(equalTo:view.heightAnchor,multiplier:0.33).isActive = true
        
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textAlignment = .center
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.text = "Select Exam for TimeTable"
        
        containerView.addSubview(lbl)
        //needed constraints
        lbl.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
        lbl.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        lbl.widthAnchor.constraint(equalTo: containerView.widthAnchor).isActive = true
        lbl.heightAnchor.constraint(equalToConstant: 50).isActive = true
    
        let submitBtn = UIButton(type: .system)
        submitBtn.translatesAutoresizingMaskIntoConstraints = false
        submitBtn.setTitle("Submit", for: .normal)
        submitBtn.backgroundColor = TheamColors.baseColor
        submitBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        submitBtn.layer.cornerRadius = 3
        submitBtn.layer.masksToBounds = true
        submitBtn.addTarget(self, action: #selector(handleSubmit), for: .touchUpInside)
        submitBtn.tintColor = UIColor.white
        
        containerView.addSubview(submitBtn)
        //needed constraints
        submitBtn.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
        submitBtn.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
        submitBtn.widthAnchor.constraint(equalTo: containerView.widthAnchor).isActive = true
        submitBtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        containerView.addSubview(pickerView)
        //needed constraints
        pickerView.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
        pickerView.topAnchor.constraint(equalTo: lbl.bottomAnchor).isActive = true
        pickerView.widthAnchor.constraint(equalTo: containerView.widthAnchor).isActive = true
        pickerView.bottomAnchor.constraint(equalTo:submitBtn.topAnchor).isActive = true
    }
    
    //MARK:- View Controller Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = vcTitle
        view.backgroundColor = UIColor.white
        errorLbl.isHidden = true
        containerView.isHidden = true
        setUpContainerView()
        setUpErrorLabel()
        getTimeTableDetails()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        selectedExam = ""
        selectedIndex = 0
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: true)
    }
    
    //MARK:- PickerView Methods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return examList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return examList[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        selectedIndex = row
        selectedExam = examList[row]
    }
    
    //MARK:- Custom Action Methods
    
    private func addRefreshButton(){
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(handleRefresh))
    }
    
    @objc func handleRefresh(){
        getTimeTableDetails()
    }
    
    @objc func handleSubmit() {
        
        if selectedExam == select as String || selectedExam == "" {
            UtilTeacher.invokeAlertMethod("Message", strBody: "Please select Exam", delegate: self, vcobj: self)
        }else {
            let vc = ExamTimeTableDisVCTeacher()
            vc.timeTableList = examListWithTTDetails[selectedExam]!
            vc.examName = selectedExam
            vc.schoolClass = schoolClass
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    private func refreshExamList() {
        examList.removeAll()
        examList.append(select as String)
    }
    
    //MARK: -  Server Methods
    
    private func getTimeTableDetails() {
        
        if UtilTeacher.validateNetworkConnection(self) {
            
            let userDefault = UserDefaults.standard
            let parameters = [
                "action":"getExamTimeTable",
                "SCHOOL_ID": userDefault.value(forKey:"SCHOOL_ID") ?? "55",
                "class_id":  schoolClass.classID.description,
                "board_id":schoolClass.boardID.description] as [String : Any]
            showActivityIndicatory(uiView: view)
            Alamofire.request(WebServiceUrls.timeTableService,method:.get,parameters: parameters)
                
                .responseJSON {[weak self] (response) in
                    print("ExamTimeTableListVCTeacher parameters>\(parameters)")
                    self?.refreshExamList()
                    self?.stopActivityIndicator()
                    guard response.result.error == nil else {
                        self?.errorLbl.isHidden = false
                        return
                    }
                    // make sure we got JSON and it's a dictionary
                    guard let json1 = response.result.value as? [Any] else {
                        print("didn't get todo object as JSON from API")
                        return
                    }
                    if json1.count > 0 {
                        for exam in json1 {
                            guard let  jsondic = exam as? [String: Any] else {
                                print("didn't get todo object as JSON from API")
                                return
                            }
                            self?.examList.append((jsondic["exam_name"] as? String)!)
                            guard let ttdetails = jsondic["exam_details"] as? String else {
                                print("didn't get todo object as JSON from API")
                                
                                return
                            }
                            let ttdet = JSON(ttdetails).rawString()
                            let json: AnyObject? = ttdet?.parseJSONString
                            
                            guard let tt   =  json  as? [[String:AnyObject]] else {
                                print("didn't get todo object as JSON from API")
                                return
                            }
                            
                            var timeTableList = [ExamTimeTableTeacher]()
                            for details in tt  {
                                let examTimeTable = ExamTimeTableTeacher()
                                
                                examTimeTable.date = details["exam_date"] as? String
                                examTimeTable.time = details["exam_time"] as? String
                                examTimeTable.subject = details["subject"] as? String
                                examTimeTable.type = details["type_of_paper"] as? String
                                examTimeTable.syllabus = details["syllabus_of_exam"] as? String
                                timeTableList.append(examTimeTable)
                            }
                            self?.examListWithTTDetails[(jsondic["exam_name"] as? String)!] = timeTableList
                            self?.selectedExam = ""
                            self?.selectedIndex = 0
                        }
                        DispatchQueue.main.async {
                            self?.errorLbl.isHidden = true
                            self?.containerView.isHidden = false
                            self?.pickerView.reloadAllComponents()
                            self?.pickerView.selectRow(0, inComponent: 0, animated: true)
                            self?.addRefreshButton()
                        }
                    }else {
                        self?.errorLbl.isHidden = false
                        self?.containerView.isHidden = true
                    }
            }
        }
    }
}
