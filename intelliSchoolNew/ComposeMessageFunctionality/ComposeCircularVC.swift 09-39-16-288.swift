//
//  ComposeCircularVC.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 21/11/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import OpalImagePicker
import Photos
import MobileCoreServices
import AMXFontAutoScale

class ComposeCircularVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,OpalImagePickerControllerDelegate,UIImagePickerControllerDelegate {
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    var attachmentList = [String]()
    var attachData = [Data]()
           var mimeTypes = [String]()
           var attachExtensions = [String]()

    
    @IBOutlet weak var SMS_imgView: UIImageView!
     @IBOutlet weak var Circular_imgView: UIImageView!
     @IBOutlet weak var Msg_imgView: UIImageView!
     @IBOutlet weak var Homework_imgView: UIImageView!

    @IBOutlet weak var SelectClassTableView: UITableView!
    @IBOutlet weak var SelectClassPopupView: UIView!
    @IBOutlet weak var SelectClassGroup_TableView: UITableView!
    @IBOutlet weak var SelectClassGroup_PopupView: UIView!
    @IBOutlet weak var SelectGroup_check1: UIButton!
    @IBOutlet weak var SelectGroup_Check2: UIButton!
    @IBOutlet weak var SelectClassOutlet: UIButton!
    @IBOutlet weak var SelectClassGroupOulet: UIButton!
    @IBOutlet weak var SelectGroupOutlet: UIButton!
    @IBOutlet weak var SelectGroupPopup: UIView!
    @IBOutlet var ParentView: UIView!
    @IBOutlet weak var TitleText: UITextField!
    
    @IBOutlet weak var DescriptionText: UITextField!
    
    let url = "http://commn.intellischools.com/hmcs_angular_api/public/api/homeworkAllInfo"
    let url1 = "http://commn.intellischools.com/hmcs_angular_api/public/api/classByClassgroup"
    let send_url = "http://commn.intellischools.com/hmcs_angular_api/public/api/saveCircular_Android"
    
    var classgrouplabltext : String! = "SELECT CLASSGROUP"
    var classlabltext : String! = "SELECT CLASS"
    var classlbltext_forSendApi : String! = "SELECT Class"
    
    var userList = [ClassGroup_DataModel]()
    var userList1 = [Class_DataModel]()
    
    let imgViewTransparent = UIView()
    static let screenSize = UIScreen.main.bounds
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

        self.TitleText.delegate = self
        self.DescriptionText.delegate = self

        imgViewTransparent.isHidden = true
        self.SelectGroupPopup.isHidden = true
        self.SelectClassGroup_PopupView.isHidden = true
        self.SelectClassPopupView.isHidden = true
        
        UserDefaults.standard.set("0", forKey: "GROUP_CHECK_CIRCULAR")
        UserDefaults.standard.set("0", forKey: "CLASSGROUPCHECKCIRCULAR")
        UserDefaults.standard.set("0", forKey: "CLASSCHECKCIRCULAR")
        
        SelectGroup_check1.setImage(UIImage(named: "checkbox-inactive"), for: .normal)
        SelectGroup_Check2.setImage(UIImage(named: "checkbox-inactive"), for: .normal)
        
        self.SelectGroupPopup.center = CGPoint(x: self.ParentView.bounds.midX, y: self.ParentView.bounds.midY)
        self.SelectClassGroup_PopupView.center = CGPoint(x: self.ParentView.bounds.midX, y: self.ParentView.bounds.midY)
        self.SelectClassPopupView.center = CGPoint(x: self.ParentView.bounds.midX, y: self.ParentView.bounds.midY)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(Homework_imgView(tapGestureRecognizer:)))
                      Homework_imgView.isUserInteractionEnabled = true
                      Homework_imgView.addGestureRecognizer(tapGestureRecognizer)
               
               let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(Msg_imgView(tapGestureRecognizer1:)))
               Msg_imgView.isUserInteractionEnabled = true
               Msg_imgView.addGestureRecognizer(tapGestureRecognizer1)
               
               let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(Circular_imgView(tapGestureRecognizer2:)))
               Circular_imgView.isUserInteractionEnabled = true
               Circular_imgView.addGestureRecognizer(tapGestureRecognizer2)
               
               let tapGestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(SMS_imgView(tapGestureRecognizer3:)))
               SMS_imgView.isUserInteractionEnabled = true
               SMS_imgView.addGestureRecognizer(tapGestureRecognizer3)
        
        // Select Group API..
               let parameter = [
                            "schoolCode" : "INTELL",
                            "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MjUwMDU2MiwiZXhwIjoxNTcyNTA0MTYyLCJuYmYiOjE1NzI1MDA1NjIsImp0aSI6IjZPdU9JUzhCaFhXRkVTdHQiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.B_xqLJGxhurHfPneH2iDVMI8aoJYGKfemevlxsDAWkQ"
                            ] as [String : Any]
                        
                     print("parameter for homeworkAllInfo:\(parameter)")
                        Alamofire.request(url, method: .post, parameters: parameter).responseJSON {
                            response in
                         print("Response for homeworkAllInfo : \(response)")
                            guard let value = response.result.value as? [String : Any] else {
                                return
                             
                            }
                         switch response.result {
                         case .success:
                              let status = value["data"]
                             if ((response.result.value) != nil) {
                                 
                                 let jsondataCopy = JSON(status)
                                 // ClassGroup..==============================================
                                 let recordsArray: [NSDictionary] = jsondataCopy["classGroupList"].arrayObject as! [NSDictionary]
                                 
                                   print("jsondataCopy : \(jsondataCopy)")
                                  print("recordsArray : \(recordsArray)")
                                 
                                 for item in recordsArray {
                                     let name = item.value(forKey: "classGroupName")
                                     let group_id = item.value(forKey: "classGroupId")
                                     
                                     let userObj = ClassGroup_DataModel(First_Lable: name! as? String, Second_Lable: group_id as! Int)
                                     
                                     print("name : \(name!)")
                                      print("classGroupId : \(group_id!)")
                                     
                                     print("Data.count : \(self.userList.count)")
                                     
                                     self.userList.append(userObj)
                                 }
                                     
                                self.SelectClassGroup_TableView.reloadData()
                             
                             }
                         case .failure(_):
                             print("")
                         
                     }
                 }
    }

    @IBAction func BackBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                      let controller = storyboard.instantiateViewController(withIdentifier: "vc")
                      self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func AttachmentBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            if attachmentList.count == 5{
                      let alert = UIAlertController(title: nil, message: "You can upload maximum 5 attachments.", preferredStyle: .alert)
                      alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                      self.present(alert, animated: true, completion: nil)
                  }else{
                      let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                      alert.addAction(UIAlertAction(title: "Photos", style: .default, handler: { (alert) in
                          self.pickImage()
                      }))
                      alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (alert) in
                          self.pickDocument()
                      }))
                      alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                      self.present(alert, animated: true, completion: nil)
                  }
            
        }
    }
    @IBAction func SendBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            if(UserDefaults.standard.string(forKey: "CLASSCHECKCIRCULAR") == "1" && TitleText.text?.count != 0 && DescriptionText.text?.count != 0)
            {
                if(attachmentList.count < 1)
                {

                    
            let send_parameter = [
                              "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MTY1NTI3NCwiZXhwIjoxNTcxNjU4ODc0LCJuYmYiOjE1NzE2NTUyNzQsImp0aSI6IkhHVjM1VEhNczluSVJsS3AiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.42-1tkg8oGcrKAEwvp_yZdc2zn8nAOJ7BugTtfSsCLQ",
                              "schoolCode" : "INTELL",
                               "type" : "3",
                               "grouptype" : "class|ClassGroup",
                               "title" : TitleText.text!, //"Test",
                               "message" : DescriptionText.text!, //"Test1",
                               "is_academicYear" : "2019-2020",
                               "is_entry_by" : "INTELL19E00SCH001",
                               "is_employee" : "INTELL19E00SCH001",
                               "is_ip_addr" : "test",
                               "group" : String(classgrouplabltext), //"2|Primary",
                               "class" : String(classlbltext_forSendApi)  // INTELL7A|A|7|IV|2|Primary|Intellischools
                              ] as [String : Any]
                
                       print("parameter for send_url for Circular:\(send_parameter)")
                          Alamofire.request(send_url, method: .post, parameters: send_parameter).responseJSON {
                              response in
                              print("Response for send_url for Circular : \(response)")
                              guard let value = response.result.value as? [String : Any] else {
                                  return
                              }
                           
                            switch response.result {
                                    case .success:
                                         let data = value["data"]
                                        if ((response.result.value) != nil) {
                                            
                                            let jsondataCopy = JSON(data)
                                            // ClassGroup..==============================================
                                            let recordsArray: [NSDictionary] = jsondataCopy["message"].arrayObject as! [NSDictionary]
                                            
                                              print("jsondataCopy for send Message : \(jsondataCopy)")
                                             print("recordsArray for send Message : \(recordsArray)")
                                            
                                            for item in recordsArray {
                                                let msg = item.value(forKey: "msg")
                                                print("msg : \(msg!)")
                                                self.view.makeToast(message: msg as! String, duration: 0.7, position: HRToastPositionCenter as AnyObject)
                                            }
                                            self.TitleText.text = ""
                                            self.DescriptionText.text = ""
                                        }
                                    case .failure(_):
                                        print("")
                                }
                   }
            }
                else
                {
                    let send_parameter = [
                          "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MTY1NTI3NCwiZXhwIjoxNTcxNjU4ODc0LCJuYmYiOjE1NzE2NTUyNzQsImp0aSI6IkhHVjM1VEhNczluSVJsS3AiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.42-1tkg8oGcrKAEwvp_yZdc2zn8nAOJ7BugTtfSsCLQ",
                          "schoolCode" : "INTELL",
                           "type" : "3",
                           "grouptype" : "class|ClassGroup",
                           "title" : TitleText.text!, //"Test",
                           "message" : DescriptionText.text!, //"Test1",
                           "is_academicYear" : "2019-2020",
                           "is_entry_by" : "INTELL19E00SCH001",
                           "is_employee" : "INTELL19E00SCH001",
                           "is_ip_addr" : "test",
                           "group" : String(classgrouplabltext), //"2|Primary",
                           "class" : String(classlbltext_forSendApi),  // INTELL7A|A|7|IV|2|Primary|Intellischools
                             "is_attachment[]": attachData
                             ] as NSMutableDictionary
                         
                           // "is_attachment" : ""
                          //]
                        
                    /*    Alamofire.upload(multipartFormData: { multipartFormData in
                            if self.attachExtensions.count > 0{
                                let count = self.attachData.count
                                for i in 0..<count{
                                    let uuid = UUID().uuidString
                                 print("is_attachment..")
                                    multipartFormData.append(self.attachData[i], withName: "is_attachment", fileName: "\(uuid).\(self.attachExtensions[i])" , mimeType: self.mimeTypes[i])
                                 print("multipartFormData : \(multipartFormData)")
                                }
                            }
                            for (key, value) in send_parameter {
                                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                            }
                        }, usingThreshold:UInt64.init(),
                            to: send_url,
                            method: .post,
                        
                        encodingCompletion: { (result) in
                            switch result {
                            case .success(let upload, _ , _):
                                upload.uploadProgress(closure: { (progress) in
                                    print(upload.uploadProgress)
                                })
                                upload.responseJSON { response in
                                 print("parameter for send_url with attchment :\(send_parameter)")
                                 print("Response for send_url for attachment : \(String(describing: response.result.value))")
                                  
                                 guard let value = response.result.value as? [String : Any] else {
                                         return
                                 }
                                }
                             break
                            case .failure(let encodingError):
                                print("failed")
                                print(encodingError)
                            }
                        })*/
                         
                         let request =  createRequest(param: send_parameter , strURL: send_url)
                                 print("Send Parameters : \(send_parameter)")
                                
                                let session = URLSession.shared
                                showActivityIndicatory(uiView: view)
                                let task = session.dataTask(with: request as URLRequest) { [weak self](data, response, error) in
                                    print("Send Response : \(response)")
                                    guard let data:Data = data as Data?, let _:URLResponse = response, error == nil else {
                                        self?.stopActivityIndicator()
                                        UtilTeacher.invokeAlertMethod("Failed!", strBody: "Can't upload attachement", delegate: nil,vcobj : self!)
                                        return
                                    }
                                    do {
                                  
                                        let jsonResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: Any]
                                        let path = jsonResult["path"] as? [String] ?? []
                            
                               //         self?.sendData(messageToBeSend,path)
                                        DispatchQueue.main.async {
                                            self?.stopActivityIndicator()
                                     //       self?.reSetAllControls()
                                        }
                                    } catch let error as NSError {
                                        print(error)
                                    }
                                }
                                task.resume()
                     }
                     

               
        }
            else if(TitleText.text?.count == 0)
            {
                self.view.makeToast(message: "Please Enter Title.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
                else if(DescriptionText.text?.count == 0)
                {
                    self.view.makeToast(message: "Please Enter Description.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                }
            else
            {
                 self.view.makeToast(message: "Please Select Class first.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ComposeCircularVC")
            self.present(controller, animated: false, completion: nil)
        }
    }
    func reSetAllControls() {
        //    textView.text = PlaceHolderStrings.tvPlaceHolder
     //       textField.placeholder = PlaceHolderStrings.tfPlacehoder
            attachData.removeAll()
            mimeTypes.removeAll()
            attachExtensions.removeAll()
            attachmentList.removeAll()
        //    tableView.reloadData()
        }
        func showActivityIndicatory(uiView: UIView) {
    
            let loadingView: UIView = UIView()
            loadingView.frame =  CGRect(x: 0, y: 0, width: 80.0, height: 80.0)
            loadingView.center = uiView.center
            loadingView.backgroundColor = UIColor(red: 44/255, green: 44/255, blue: 44/255, alpha: 0.7)
            loadingView.clipsToBounds = true
            loadingView.layer.cornerRadius = 10
            actInd.frame = CGRect(x: 0, y: 0, width: 40.0, height: 40.0)
            actInd.style =
                UIActivityIndicatorView.Style.whiteLarge
            actInd.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
            loadingView.addSubview(actInd)
            uiView.addSubview(loadingView)
            actInd.startAnimating()
        }
        func stopActivityIndicator() {
            actInd.stopAnimating()
            let view = actInd.superview
            view?.removeFromSuperview()
        }
        
        func generateBoundaryString() -> String
          {
              return "Boundary-\(NSUUID().uuidString)"
          }
          
          func createBodyWithParameters(parameters: NSMutableDictionary?,boundary: String) -> NSData {
              let body = NSMutableData()
              if parameters != nil {
                  for (key, value) in parameters! {
                      if(value is String || value is NSString){
                          body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                          body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
                          body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
                      }else if(value is [Data]){
                          var i = 0;
                          for data in value as! [Data]{
                              let uuidStr = UUID().uuidString
                              let filename = "\(uuidStr).\(attachExtensions[i])"
                              print("filename", filename)
                               print("Mime", mimeTypes[i])
                              body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                              body.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n".data(using: String.Encoding.utf8)!)
                              body.append("Content-Type: \(mimeTypes[i])\r\n\r\n".data(using: String.Encoding.utf8)!)
                              body.append(data)
                              body.append("\r\n".data(using: String.Encoding.utf8)!)
                              i += 1;
                          }
                      }
                  }
              }
              body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
              return body
          }
        func createRequest (param : NSMutableDictionary , strURL : String) -> NSURLRequest {
            let boundary = generateBoundaryString()
            let url = NSURL(string: strURL)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            request.httpBody = createBodyWithParameters(parameters: param , boundary: boundary) as Data
            return request
        }
    
    
    //MARK:- ImagePickerView Methods
        
        func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
            for i in 0..<images.count{
                self.attachData.append(images[i].jpegData(compressionQuality: 0.5)!)
                self.mimeTypes.append("image/jpeg")
                self.attachExtensions.append("jpeg")
            }
            print("attachData : \(attachData)")
            print("mimeTypes : \(mimeTypes)")
            print("attachExtensions : \(attachExtensions)")
        }
        
        func imagePicker(_ picker: OpalImagePickerController, didFinishPickingAssets assets: [PHAsset]) {
            for asset in assets {
                if let name = asset.value(forKey: "filename") {
                    attachmentList.append(name as! String)
                }
            }
             print("attachmentList : \(attachmentList)")
         //   tableView.reloadData()
            picker.dismiss(animated: true, completion: nil)
        }

    @IBAction func HomeworkBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            UserDefaults.standard.set("0", forKey: "GROUP_CHECK")
            UserDefaults.standard.set("0", forKey: "CLASSGROUPCHECK")
            UserDefaults.standard.set("0", forKey: "CLASSCHECK")
            UserDefaults.standard.set("0", forKey: "DIVISIONCHECK")
            UserDefaults.standard.set("0", forKey: "SUBCHECK")
            
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let controller = storyboard.instantiateViewController(withIdentifier: "Compose_HomeworkVC")
               self.present(controller, animated: false, completion: nil)
        }
    }
    @IBAction func CircularBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ComposeCircularVC")
        self.present(controller, animated: false, completion: nil)
        }
    }
    @IBAction func MessageBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Compose_MessageVC")
        self.present(controller, animated: false, completion: nil)
        }
    }
    
    @IBAction func SMSBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let controller = storyboard.instantiateViewController(withIdentifier: "Compose_SMSVC")
               self.present(controller, animated: false, completion: nil)
        }
    }
    
    @IBAction func SelectGroupType(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            if (!ConnectionCheck.isConnectedToNetwork())
            {
                self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
            else
            {
                self.imgViewTransparent.frame = CGRect(x: 0, y: 0, width: ComposeHomeworkVC.screenSize.width, height: ComposeHomeworkVC.screenSize.height)
                                
                self.imgViewTransparent.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                                
                self.view.addSubview(self.imgViewTransparent)
                self.imgViewTransparent.isHidden = false
                self.imgViewTransparent.addSubview(self.SelectGroupPopup)
                self.SelectGroupPopup.isHidden = false
            }
        }
    }
    
    @IBAction func SelectClassGroupBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            if (!ConnectionCheck.isConnectedToNetwork()) {
                       
                       self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                   }
                   else
                   {
                   UserDefaults.standard.set("0", forKey: "CLASSGROUPCHECKCIRCULAR")
                   if(UserDefaults.standard.string(forKey: "GROUP_CHECK_CIRCULAR") == "1")
                   {
                       self.imgViewTransparent.frame = CGRect(x: 0, y: 0, width: ComposeHomeworkVC.screenSize.width, height: ComposeHomeworkVC.screenSize.height)
                                       
                       self.imgViewTransparent.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                                       
                       self.view.addSubview(self.imgViewTransparent)
                       self.imgViewTransparent.isHidden = false
                       self.imgViewTransparent.addSubview(self.SelectClassGroup_PopupView)
                       self.SelectClassGroup_PopupView.isHidden = false
                   }
                   else if(UserDefaults.standard.string(forKey: "GROUP_CHECK_CIRCULAR") == "2")
                   {
                             
                   }
                   else
                   {
                       self.view.makeToast(message: "Please Select Group Type first.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                   }
                   }
        }
    }
    @IBAction func SelectClassBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            if (!ConnectionCheck.isConnectedToNetwork()) {
                
                self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
            else
            {
       //     UserDefaults.standard.set("0", forKey: "CLASSCHECKCIRCULAR")
            if(UserDefaults.standard.string(forKey: "CLASSGROUPCHECKCIRCULAR") == "0")
            {
                self.view.makeToast(message: "Please Select ClassGroup first.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
            else
            {
                self.imgViewTransparent.frame = CGRect(x: 0, y: 0, width: ComposeHomeworkVC.screenSize.width, height: ComposeHomeworkVC.screenSize.height)
                                
                self.imgViewTransparent.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                                
                self.view.addSubview(self.imgViewTransparent)
                self.imgViewTransparent.isHidden = false
                self.imgViewTransparent.addSubview(self.SelectClassPopupView)
                self.SelectClassPopupView.isHidden = false
            }
            }
        }
    }
    
    @IBAction func HomeBtn(_ sender: Any)
    {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "vc")
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    @IBAction func ComposeBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ComposeCircularVC")
        self.present(controller, animated: false, completion: nil)
        }
    }
    @IBAction func SupportBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        }
    }
    
    @IBAction func SelectGroupCheck1(_ sender: Any) {
        UserDefaults.standard.set("1", forKey: "GROUP_CHECK_CIRCULAR")
               SelectGroup_check1.setImage(UIImage(named: "checkbox-active"), for: .normal)
               SelectGroup_Check2.setImage(UIImage(named: "checkbox-inactive"), for: .normal)
    }
    @IBAction func SelectGroupCheck2(_ sender: Any) {
        UserDefaults.standard.set("2", forKey: "GROUP_CHECK_CIRCULAR")
        SelectGroup_Check2.setImage(UIImage(named: "checkbox-active"), for: .normal)
        SelectGroup_check1.setImage(UIImage(named: "checkbox-inactive"), for: .normal)
    }
    
    @IBAction func SelectGroupOkBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        if(UserDefaults.standard.string(forKey: "GROUP_CHECK_CIRCULAR") == "0")
        {
            self.view.makeToast(message: "Please Select Group Type.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            if(UserDefaults.standard.string(forKey: "GROUP_CHECK_CIRCULAR") == "1")
            {
                SelectGroupOutlet.titleLabel?.text = "CLASS|CLASSGROUP"
            }
            else if(UserDefaults.standard.string(forKey: "GROUP_CHECK_CIRCULAR") == "2")
            {
                SelectGroupOutlet.titleLabel?.text = "CLASS|SPECIFICGROUP"
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                  let controller = storyboard.instantiateViewController(withIdentifier: "ComposeCircular_SpecificClassVC")
                                  self.present(controller, animated: false, completion: nil)
            }
        self.imgViewTransparent.isHidden = true
        self.SelectGroupPopup.isHidden = true
        }
        }
    }
    
    @IBAction func SelectClassGroupOkBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
                
                self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
            else
            {
         if(UserDefaults.standard.string(forKey: "CLASSGROUPCHECKCIRCULAR") == "1")
         {
            
            self.imgViewTransparent.isHidden = true
            self.SelectClassGroup_PopupView.isHidden = true
            
            let parameter1 = [
                       "schoolCode" : "INTELL",
                       "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MTY1NTI3NCwiZXhwIjoxNTcxNjU4ODc0LCJuYmYiOjE1NzE2NTUyNzQsImp0aSI6IkhHVjM1VEhNczluSVJsS3AiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.42-1tkg8oGcrKAEwvp_yZdc2zn8nAOJ7BugTtfSsCLQ",
                        "academicYear" : "2019-2020",
                        "groupValue" : String(classgrouplabltext)
                       ] as [String : Any]
                   
                print("parameter for classByClassgroup:\(parameter1)")
                   Alamofire.request(url1, method: .post, parameters: parameter1).responseJSON {
                       response in
                       print("Response for classByClassgroup : \(response)")
                       guard let value = response.result.value as? [String : Any] else {
                           return
                       }
                    
                       switch response.result {
                           
                       case .success:
                        
                           let jsondataCopy = JSON(value)
                           let data: [NSDictionary] = jsondataCopy["data"].arrayObject as! [NSDictionary]
                           print("data>>\(data)")
                           if(self.userList1 != nil)
                           {
                           self.userList1.removeAll()
                           }
                           for item in data {
                           let classes = item.value(forKey: "classes")
                              print("name >>: \(classes!)")
                              
                              let classes2 = JSON(classes)
                              let data2: [NSDictionary] = classes2.arrayObject as! [NSDictionary]
                              print("data>>\(data)")
                              for item in data2
                              {
                                      let classId = item.value(forKey: "classId")
                                      print("classId >>: \(classId!)")
                                      let className = item.value(forKey: "className")
                                      print("className >>: \(className!)")
                                  
                                  
                                  let userObj1 = Class_DataModel(First_Lable: className! as? String, Second_Lable: classId as! Int)
                                  
                                  
                                  print("Data.count : \(self.userList1.count)")
                                  
                                  self.userList1.append(userObj1)
                              }
                          
                           }
                          self.SelectClassTableView.reloadData()
                        
                        
                    case .failure(_):
                        print("")
                    
                }
            }
            
         }
         else{
            self.view.makeToast(message: "Please Select Class Group Type.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
            }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == SelectClassGroup_TableView)
        {
            return userList.count
        }
        else if (tableView == SelectClassTableView)
        {
            return userList1.count
        }
        
        return Int()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (tableView == SelectClassGroup_TableView)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectClassGroupCircularCell", for: indexPath) as! SelectClassGroupCircularCell
            cell.SelectClassGroupCircular_Check.image = UIImage(named: "checkbox-inactive")
            let userObjec = userList[indexPath.row]
            cell.SelectClassGroupCircularCell_lbl.text = userObjec.getName()
        
            return cell
        }
        else if (tableView == SelectClassTableView)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectClass_CircularCell", for: indexPath) as! SelectClass_CircularCell
            cell.SelectClass_CircularCell_check.image = UIImage(named: "checkbox-inactive")
                let userObjec = userList1[indexPath.row]
            cell.SelectClass_CircularCell_lbl.text = userObjec.getName()
            
                return cell
        }
        
        
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (tableView == SelectClassGroup_TableView)
        {
        let cell = self.SelectClassGroup_TableView.cellForRow(at: indexPath) as!SelectClassGroupCircularCell
        let userObjec = userList[indexPath.row]
            cell.SelectClassGroupCircular_Check.image = UIImage(named: "checkbox-active")
         UserDefaults.standard.set("1", forKey: "CLASSGROUPCHECKCIRCULAR")
        self.SelectClassGroupOulet.titleLabel?.text = String(userObjec.getId()) + " | " + userObjec.getName()
        self.classgrouplabltext = String(userObjec.getId()) + "|" + userObjec.getName()
        }
        else if (tableView == SelectClassTableView)
        {
            let cell = self.SelectClassTableView.cellForRow(at: indexPath) as! SelectClass_CircularCell
             let userObjec1 = userList1[indexPath.row]
            cell.SelectClass_CircularCell_check.image = UIImage(named: "checkbox-active")
             UserDefaults.standard.set("1", forKey: "CLASSCHECKCIRCULAR")
            self.SelectClassOutlet.titleLabel?.text = String(userObjec1.getId()) + " | " + userObjec1.getName()
            self.classlabltext = String(userObjec1.getId()) + "|" + userObjec1.getName()
            self.classlbltext_forSendApi = self.classlabltext + "|" + self.classgrouplabltext
        }
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if (tableView == SelectClassGroup_TableView)
        {
        let cell = self.SelectClassGroup_TableView.cellForRow(at: indexPath) as!SelectClassGroupCircularCell
            cell.SelectClassGroupCircular_Check.image = UIImage(named: "checkbox-inactive")
        }
        else if (tableView == SelectClassTableView)
        {
           let cell = self.SelectClassTableView.cellForRow(at: indexPath) as! SelectClass_CircularCell
            cell.SelectClass_CircularCell_check.image = UIImage(named: "checkbox-inactive")
        }
       
    }
    
    @IBAction func SelectClassTableViewOkBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        if(UserDefaults.standard.string(forKey: "CLASSCHECKCIRCULAR") == "1")
        {
            self.imgViewTransparent.isHidden = true
            self.SelectClassPopupView.isHidden = true
        }
        else
        {
             self.view.makeToast(message: "Please Select Class.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
          if (!ConnectionCheck.isConnectedToNetwork()) {
              self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
          }
          else
          {
           TitleText.resignFirstResponder()
            DescriptionText.resignFirstResponder()
        }
        return (true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
         if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
             if self.ParentView.frame.origin.y == 0 {
                 self.ParentView.frame.origin.y -= keyboardSize.height
             }
         }
     }

     @objc func keyboardWillHide(notification: NSNotification) {
         if self.ParentView.frame.origin.y != 0 {
             self.ParentView.frame.origin.y = 0
         }
     }
    @objc func Homework_imgView(tapGestureRecognizer: UITapGestureRecognizer)
       {
           if (!ConnectionCheck.isConnectedToNetwork())
           {
               self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
           }
           else
           {
               UserDefaults.standard.set("0", forKey: "GROUP_CHECK")
               UserDefaults.standard.set("0", forKey: "CLASSGROUPCHECK")
               UserDefaults.standard.set("0", forKey: "CLASSCHECK")
               UserDefaults.standard.set("0", forKey: "DIVISIONCHECK")
               UserDefaults.standard.set("0", forKey: "SUBCHECK")
               
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
                  let controller = storyboard.instantiateViewController(withIdentifier: "Compose_HomeworkVC")
                  self.present(controller, animated: false, completion: nil)
           }
       }
       @objc func Msg_imgView(tapGestureRecognizer1: UITapGestureRecognizer)
          {
              if (!ConnectionCheck.isConnectedToNetwork())
                     {
                         self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                     }
                     else
                     {
                     let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     let controller = storyboard.instantiateViewController(withIdentifier: "Compose_MessageVC")
                     self.present(controller, animated: false, completion: nil)
                     }
          }
       @objc func Circular_imgView(tapGestureRecognizer2: UITapGestureRecognizer)
          {
              if (!ConnectionCheck.isConnectedToNetwork())
                     {
                         self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                     }
                     else
                     {
                     let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     let controller = storyboard.instantiateViewController(withIdentifier: "ComposeCircularVC")
                     self.present(controller, animated: false, completion: nil)
                     }
          }
       @objc func SMS_imgView(tapGestureRecognizer3: UITapGestureRecognizer)
           {
             if (!ConnectionCheck.isConnectedToNetwork())
                        {
                            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                        }
                        else
                        {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                               let controller = storyboard.instantiateViewController(withIdentifier: "Compose_SMSVC")
                               self.present(controller, animated: false, completion: nil)
                        }
            }
       func pickDocument(){
           let documentPicker = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: .import)
         documentPicker.delegate = self
           self.present(documentPicker, animated: true, completion: nil)
       }
       func pickImage(){
           let imagePicker = OpalImagePickerController()
           //Change color of selection overlay to white
           imagePicker.selectionTintColor = UIColor.white.withAlphaComponent(0.7)
           
           //Change color of image tint to black
           imagePicker.selectionImageTintColor = UIColor.black
           
           //Change image to X rather than checkmark
           //        imagePicker.selectionImage = UIImage(named: "x_image")
           
           //Change status bar style
           imagePicker.statusBarPreference = UIStatusBarStyle.lightContent
           
           //Limit maximum allowed selections to 5
           imagePicker.maximumSelectionsAllowed = 5 - self.attachmentList.count
           
           //Only allow image media type assets
           imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
           imagePicker.imagePickerDelegate = self
           present(imagePicker, animated: true, completion: nil)
       }
}
extension ComposeCircularVC : UIDocumentPickerDelegate{
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let docUrl = url as URL
        let docData  = try! Data(contentsOf: docUrl)
        self.attachData.append(docData)
        self.attachmentList.append(url.lastPathComponent)
        self.attachExtensions.append("pdf")
        self.mimeTypes.append("application/pdf")
 //       self.tableView.reloadData()
    }
}
