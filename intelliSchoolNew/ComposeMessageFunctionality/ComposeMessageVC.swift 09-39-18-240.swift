//
//  ComposeMessageVC.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 21/11/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import OpalImagePicker
import Photos
import MobileCoreServices
import AMXFontAutoScale


class ComposeMessageVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,OpalImagePickerControllerDelegate,UIImagePickerControllerDelegate {
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    var attachmentList = [String]()
    var attachData = [Data]()
    var mimeTypes = [String]()
    var attachExtensions = [String]()

    
    @IBOutlet weak var SMS_imgView: UIImageView!
    @IBOutlet weak var Circular_imgView: UIImageView!
    @IBOutlet weak var Msg_imgView: UIImageView!
    @IBOutlet weak var Homework_imgView: UIImageView!

    @IBOutlet weak var SelectDivisionMsg_Popup: UIView!
    @IBOutlet weak var SelectDivisionMsg_TableView: UITableView!
    @IBOutlet weak var SelectClassMsg_TableView: UITableView!
    @IBOutlet weak var SelectClassMsg_PopupView: UIView!
    @IBOutlet weak var SelectClassGroupTableView: UITableView!
    @IBOutlet weak var SelectClassGroupPopupView: UIView!
    @IBOutlet weak var SelectGroupOutlet: UIButton!
    @IBOutlet weak var SelectClassGroupOutlet: UIButton!
    @IBOutlet weak var SelectDivisionOutlet: UIButton!
    @IBOutlet weak var SelectClassOutlet: UIButton!
    @IBOutlet weak var SelectGroupPopUpView: UIView!
    @IBOutlet weak var SelectGroup_check1: UIButton!
    @IBOutlet weak var SelectGroup_Check2: UIButton!
    @IBOutlet var ParentView: UIView!
    @IBOutlet weak var TitleText: UITextField!
    @IBOutlet weak var DescriptionText: UITextField!
    
    let url = "http://commn.intellischools.com/hmcs_angular_api/public/api/homeworkAllInfo"
    let url1 = "http://commn.intellischools.com/hmcs_angular_api/public/api/classByClassgroup"
    let url2 = "http://commn.intellischools.com/hmcs_angular_api/public/api/divisionByClasses"
    let send_url = "http://commn.intellischools.com/hmcs_angular_api/public/api/saveMessage_Android"
    
    var classgrouplabltext : String! = "SELECT CLASSGROUP"
    var classlabltext : String! = "SELECT CLASS"
    var divisionlabltext : String! = "SELECT DIVISION"
    var divisionlabltext_forSendApi : String! = "SELECT DIVISION"
    var ClassDivisionIdlasttext : String! = "Test"
    
    var userList = [ClassGroup_DataModel]()
    var userList1 = [Class_DataModel]()
    var userList2 = [Division_DataModel]()
    
    let imgViewTransparent = UIView()
        static let screenSize = UIScreen.main.bounds
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.TitleText.delegate = self
        self.DescriptionText.delegate = self

        imgViewTransparent.isHidden = true
        self.SelectGroupPopUpView.isHidden = true
        self.SelectClassGroupPopupView.isHidden = true
        self.SelectClassMsg_PopupView.isHidden = true
        self.SelectDivisionMsg_Popup.isHidden = true
        
          UserDefaults.standard.set("0", forKey: "GROUP_CHECK_MSG")
          UserDefaults.standard.set("0", forKey: "CLASSGROUPCHECKMSG")
          UserDefaults.standard.set("0", forKey: "CLASSCHECKMSG")
          UserDefaults.standard.set("0", forKey: "DIVISIONCHECKMSG")
        
        self.SelectGroupPopUpView.center = CGPoint(x: self.ParentView.bounds.midX,
               y: self.ParentView.bounds.midY)
        self.SelectClassGroupPopupView.center = CGPoint(x: self.ParentView.bounds.midX,
        y: self.ParentView.bounds.midY)
        self.SelectClassMsg_PopupView.center = CGPoint(x: self.ParentView.bounds.midX,
        y: self.ParentView.bounds.midY)
        self.SelectDivisionMsg_Popup.center = CGPoint(x: self.ParentView.bounds.midX,
        y: self.ParentView.bounds.midY)
        
        SelectGroup_check1.setImage(UIImage(named: "checkbox-inactive"), for: .normal)
        SelectGroup_Check2.setImage(UIImage(named: "checkbox-inactive"), for: .normal)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(Homework_imgView(tapGestureRecognizer:)))
               Homework_imgView.isUserInteractionEnabled = true
               Homework_imgView.addGestureRecognizer(tapGestureRecognizer)
        
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(Msg_imgView(tapGestureRecognizer1:)))
        Msg_imgView.isUserInteractionEnabled = true
        Msg_imgView.addGestureRecognizer(tapGestureRecognizer1)
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(Circular_imgView(tapGestureRecognizer2:)))
        Circular_imgView.isUserInteractionEnabled = true
        Circular_imgView.addGestureRecognizer(tapGestureRecognizer2)
        
        let tapGestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(SMS_imgView(tapGestureRecognizer3:)))
        SMS_imgView.isUserInteractionEnabled = true
        SMS_imgView.addGestureRecognizer(tapGestureRecognizer3)
        
        // Select Group API..
        let parameter = [
                     "schoolCode" : "INTELL",
                     "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MjUwMDU2MiwiZXhwIjoxNTcyNTA0MTYyLCJuYmYiOjE1NzI1MDA1NjIsImp0aSI6IjZPdU9JUzhCaFhXRkVTdHQiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.B_xqLJGxhurHfPneH2iDVMI8aoJYGKfemevlxsDAWkQ"
                     ] as [String : Any]
                 
              print("parameter for homeworkAllInfo:\(parameter)")
                 Alamofire.request(url, method: .post, parameters: parameter).responseJSON {
                     response in
                  print("Response for homeworkAllInfo : \(response)")
                     guard let value = response.result.value as? [String : Any] else {
                         return
                      
                     }
                  switch response.result {
                  case .success:
                       let status = value["data"]
                      if ((response.result.value) != nil) {
                          
                          let jsondataCopy = JSON(status)
                          // ClassGroup..==============================================
                          let recordsArray: [NSDictionary] = jsondataCopy["classGroupList"].arrayObject as! [NSDictionary]
                          
                            print("jsondataCopy : \(jsondataCopy)")
                           print("recordsArray : \(recordsArray)")
                          
                          for item in recordsArray {
                              let name = item.value(forKey: "classGroupName")
                              let group_id = item.value(forKey: "classGroupId")
                              
                              let userObj = ClassGroup_DataModel(First_Lable: name! as? String, Second_Lable: group_id as! Int)
                              
                              print("name : \(name!)")
                               print("classGroupId : \(group_id!)")
                              
                              print("Data.count : \(self.userList.count)")
                              
                              self.userList.append(userObj)
                          }
                              
                         self.SelectClassGroupTableView.reloadData()
                      
                      }
                  case .failure(_):
                      print("")
                  
              }
          }
        
    }
    
    @IBAction func BackBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                   let controller = storyboard.instantiateViewController(withIdentifier: "vc")
                   self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func AttachmentBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            if attachmentList.count == 5{
                       let alert = UIAlertController(title: nil, message: "You can upload maximum 5 attachments.", preferredStyle: .alert)
                       alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                       self.present(alert, animated: true, completion: nil)
                   }else{
                       let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                       alert.addAction(UIAlertAction(title: "Photos", style: .default, handler: { (alert) in
                           self.pickImage()
                       }))
                       alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (alert) in
                           self.pickDocument()
                       }))
                       alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                       self.present(alert, animated: true, completion: nil)
                   }
        }
    }
    @IBAction func SendBtn(_ sender: Any) {
          if (!ConnectionCheck.isConnectedToNetwork())
              {
                  self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
              }
              else
              {
                  if(UserDefaults.standard.string(forKey: "DIVISIONCHECKMSG") == "1" && TitleText.text?.count != 0 && DescriptionText.text?.count != 0)
                  {
                    if(attachmentList.count < 1)
                    {

                  let send_parameter = [
                                    "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MTY1NTI3NCwiZXhwIjoxNTcxNjU4ODc0LCJuYmYiOjE1NzE2NTUyNzQsImp0aSI6IkhHVjM1VEhNczluSVJsS3AiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.42-1tkg8oGcrKAEwvp_yZdc2zn8nAOJ7BugTtfSsCLQ",
                                    "schoolCode" : "INTELL",
                                     "type" : "2",
                                     "grouptype" : "class|ClassGroup",
                                     "title" : TitleText.text!, //"Test",
                                     "message" : DescriptionText.text!, //"Test1",
                                     "is_academicYear" : "2019-2020",
                                     "is_entry_by" : "INTELL19E00SCH001",
                                     "is_employee" : "INTELL19E00SCH001",
                                     "is_ip_addr" : "test",
                                     "group" : String(classgrouplabltext), //"2|Primary",
                                     "division" : String(divisionlabltext_forSendApi)  // INTELL7A|A|7|IV|2|Primary|Intellischools
                                    ] as [String : Any]
                      
                             print("parameter for send_url for MSG:\(send_parameter)")
                                Alamofire.request(send_url, method: .post, parameters: send_parameter).responseJSON {
                                    response in
                                    print("Response for send_url for MSG : \(response)")
                                    guard let value = response.result.value as? [String : Any] else {
                                        return
                                    }
                                 
                                  switch response.result {
                                          case .success:
                                               let data = value["data"]
                                              if ((response.result.value) != nil) {
                                                  
                                                  let jsondataCopy = JSON(data)
                                                  // ClassGroup..==============================================
                                                  let recordsArray: [NSDictionary] = jsondataCopy["message"].arrayObject as! [NSDictionary]
                                                  
                                                    print("jsondataCopy for send Message : \(jsondataCopy)")
                                                   print("recordsArray for send Message : \(recordsArray)")
                                                  
                                                  for item in recordsArray {
                                                      let msg = item.value(forKey: "msg")
                                                      print("msg : \(msg!)")
                                                      self.view.makeToast(message: msg as! String, duration: 0.7, position: HRToastPositionCenter as AnyObject)
                                                  }
                                                self.TitleText.text = ""
                                                self.DescriptionText.text = ""
                                              }
                                          case .failure(_):
                                              print("")
                                      }
                         }
                }
                else
                  {
                    let send_parameter = [
                         "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MTY1NTI3NCwiZXhwIjoxNTcxNjU4ODc0LCJuYmYiOjE1NzE2NTUyNzQsImp0aSI6IkhHVjM1VEhNczluSVJsS3AiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.42-1tkg8oGcrKAEwvp_yZdc2zn8nAOJ7BugTtfSsCLQ",
                         "schoolCode" : "INTELL",
                          "type" : "2",
                          "grouptype" : "class|ClassGroup",
                          "title" : TitleText.text!, //"Test",
                          "message" : DescriptionText.text!, //"Test1",
                          "is_academicYear" : "2019-2020",
                          "is_entry_by" : "INTELL19E00SCH001",
                          "is_employee" : "INTELL19E00SCH001",
                          "is_ip_addr" : "test",
                          "group" : String(classgrouplabltext), //"2|Primary",
                          "division" : String(divisionlabltext_forSendApi),  // INTELL7A|A|7|IV|2|Primary|Intellischools
                            "is_attachment[]": attachData
                            ] as NSMutableDictionary
                        
                          // "is_attachment" : ""
                         //]
                       
                   /*    Alamofire.upload(multipartFormData: { multipartFormData in
                           if self.attachExtensions.count > 0{
                               let count = self.attachData.count
                               for i in 0..<count{
                                   let uuid = UUID().uuidString
                                print("is_attachment..")
                                   multipartFormData.append(self.attachData[i], withName: "is_attachment", fileName: "\(uuid).\(self.attachExtensions[i])" , mimeType: self.mimeTypes[i])
                                print("multipartFormData : \(multipartFormData)")
                               }
                           }
                           for (key, value) in send_parameter {
                               multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                           }
                       }, usingThreshold:UInt64.init(),
                           to: send_url,
                           method: .post,
                       
                       encodingCompletion: { (result) in
                           switch result {
                           case .success(let upload, _ , _):
                               upload.uploadProgress(closure: { (progress) in
                                   print(upload.uploadProgress)
                               })
                               upload.responseJSON { response in
                                print("parameter for send_url with attchment :\(send_parameter)")
                                print("Response for send_url for attachment : \(String(describing: response.result.value))")
                                 
                                guard let value = response.result.value as? [String : Any] else {
                                        return
                                }
                               }
                            break
                           case .failure(let encodingError):
                               print("failed")
                               print(encodingError)
                           }
                       })*/
                        
                        let request =  createRequest(param: send_parameter , strURL: send_url)
                                print("Send Parameters : \(send_parameter)")
                               
                               let session = URLSession.shared
                               showActivityIndicatory(uiView: view)
                               let task = session.dataTask(with: request as URLRequest) { [weak self](data, response, error) in
                                   print("Send Response : \(response)")
                                   guard let data:Data = data as Data?, let _:URLResponse = response, error == nil else {
                                       self?.stopActivityIndicator()
                                       UtilTeacher.invokeAlertMethod("Failed!", strBody: "Can't upload attachement", delegate: nil,vcobj : self!)
                                       return
                                   }
                                   do {
                                 
                                       let jsonResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: Any]
                                       let path = jsonResult["path"] as? [String] ?? []
                           
                              //         self?.sendData(messageToBeSend,path)
                                       DispatchQueue.main.async {
                                           self?.stopActivityIndicator()
                                    //       self?.reSetAllControls()
                                       }
                                   } catch let error as NSError {
                                       print(error)
                                   }
                               }
                               task.resume()
                    }

                
              }
                  else if(TitleText.text?.count == 0)
                  {
                      self.view.makeToast(message: "Please Enter Title.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                  }
                      else if(DescriptionText.text?.count == 0)
                      {
                          self.view.makeToast(message: "Please Enter Description.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                      }
                  else
                  {
                       self.view.makeToast(message: "Please Select Division first.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                  }
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "Compose_MessageVC")
                self.present(controller, animated: false, completion: nil)
              }
    }
    @IBAction func HomeworkBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            UserDefaults.standard.set("0", forKey: "GROUP_CHECK")
            UserDefaults.standard.set("0", forKey: "CLASSGROUPCHECK")
            UserDefaults.standard.set("0", forKey: "CLASSCHECK")
            UserDefaults.standard.set("0", forKey: "DIVISIONCHECK")
            UserDefaults.standard.set("0", forKey: "SUBCHECK")
            
            
            
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let controller = storyboard.instantiateViewController(withIdentifier: "Compose_HomeworkVC")
               self.present(controller, animated: false, completion: nil)
        }
    }
    @IBAction func CircularBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ComposeCircularVC")
        self.present(controller, animated: false, completion: nil)
        }
    }
    @IBAction func MessageBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            UserDefaults.standard.set("0", forKey: "GROUP_CHECK_MSG")
            UserDefaults.standard.set("0", forKey: "CLASSGROUPCHECKMSG")
            UserDefaults.standard.set("0", forKey: "CLASSCHECKMSG")
            UserDefaults.standard.set("0", forKey: "DIVISIONCHECKMSG")
            
            
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Compose_MessageVC")
        self.present(controller, animated: false, completion: nil)
        }
    }
    @IBAction func SMSBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let controller = storyboard.instantiateViewController(withIdentifier: "Compose_SMSVC")
               self.present(controller, animated: false, completion: nil)
        }
    }
    @IBAction func SelectGroupType(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            self.imgViewTransparent.frame = CGRect(x: 0, y: 0, width: ComposeHomeworkVC.screenSize.width, height: ComposeHomeworkVC.screenSize.height)
                            
            self.imgViewTransparent.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                            
            self.view.addSubview(self.imgViewTransparent)
            self.imgViewTransparent.isHidden = false
            self.imgViewTransparent.addSubview(self.SelectGroupPopUpView)
            self.SelectGroupPopUpView.isHidden = false
        }
    }
    
    @IBAction func SelectClassGroup(_ sender: Any) {
       
        if (!ConnectionCheck.isConnectedToNetwork()) {
                   
                   self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
               }
               else
               {
               UserDefaults.standard.set("0", forKey: "CLASSGROUPCHECKMSG")
               if(UserDefaults.standard.string(forKey: "GROUP_CHECK_MSG") == "1")
               {
                   self.imgViewTransparent.frame = CGRect(x: 0, y: 0, width: ComposeHomeworkVC.screenSize.width, height: ComposeHomeworkVC.screenSize.height)
                                   
                   self.imgViewTransparent.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                                   
                   self.view.addSubview(self.imgViewTransparent)
                   self.imgViewTransparent.isHidden = false
                   self.imgViewTransparent.addSubview(self.SelectClassGroupPopupView)
                   self.SelectClassGroupPopupView.isHidden = false
               }
               else if(UserDefaults.standard.string(forKey: "GROUP_CHECK_MSG") == "2")
               {
                         
               }
               else
               {
                   self.view.makeToast(message: "Please Select Group Type first.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
               }
               }
    }
    
    @IBAction func SelectClass(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            UserDefaults.standard.set("0", forKey: "CLASSCHECKMSG")
                   if(UserDefaults.standard.string(forKey: "CLASSGROUPCHECKMSG") == "0")
                   {
                       self.view.makeToast(message: "Please Select ClassGroup first.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                   }
                   else
                   {
                       self.imgViewTransparent.frame = CGRect(x: 0, y: 0, width: ComposeHomeworkVC.screenSize.width, height: ComposeHomeworkVC.screenSize.height)
                                       
                       self.imgViewTransparent.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                                       
                       self.view.addSubview(self.imgViewTransparent)
                       self.imgViewTransparent.isHidden = false
                       self.imgViewTransparent.addSubview(self.SelectClassMsg_PopupView)
                       self.SelectClassMsg_PopupView.isHidden = false
                   }
        }
    }
    
    @IBAction func SelectDivision(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
                 
                 self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
             }
             else
             {
             UserDefaults.standard.set("0", forKey: "DIVISIONCHECKMSG")
             if(UserDefaults.standard.string(forKey: "CLASSCHECKMSG") == "0")
             {
                 self.view.makeToast(message: "Please Select Class first.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
             }
             else
             {
                 self.imgViewTransparent.frame = CGRect(x: 0, y: 0, width: ComposeHomeworkVC.screenSize.width, height: ComposeHomeworkVC.screenSize.height)
                                 
                 self.imgViewTransparent.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                                 
                 self.view.addSubview(self.imgViewTransparent)
                 self.imgViewTransparent.isHidden = false
        //         DivisionTableView.frame = CGRect(x: 30,y: 180, width: 359, height: 196)
                 self.imgViewTransparent.addSubview(self.SelectDivisionMsg_Popup)
                 self.SelectDivisionMsg_Popup.isHidden = false
             }
             }
    }
    
    @IBAction func HomeBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                   let controller = storyboard.instantiateViewController(withIdentifier: "vc")
            self.present(controller, animated: true, completion: nil)
        }
    }
    @IBAction func ComposeBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Compose_MessageVC")
        self.present(controller, animated: false, completion: nil)
        }
    }
    @IBAction func SupportBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        }
    }
    
    @IBAction func SelectGroup_OK_Btn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        if(UserDefaults.standard.string(forKey: "GROUP_CHECK_MSG") == "0")
        {
            self.view.makeToast(message: "Please Select Group Type.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            if(UserDefaults.standard.string(forKey: "GROUP_CHECK_MSG") == "1")
            {
                SelectGroupOutlet.titleLabel?.text = "CLASS|CLASSGROUP"
            }
            else if(UserDefaults.standard.string(forKey: "GROUP_CHECK_MSG") == "2")
            {
                SelectGroupOutlet.titleLabel?.text = "CLASS|SPECIFICGROUP"
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "Compose_Msg_SpecificClass")
                self.present(controller, animated: false, completion: nil)
             
            }
        self.imgViewTransparent.isHidden = true
        self.SelectGroupPopUpView.isHidden = true
        }
        }
        
    }
    @IBAction func SelectGroupCheck1(_ sender: Any) {
        UserDefaults.standard.set("1", forKey: "GROUP_CHECK_MSG")
        SelectGroup_check1.setImage(UIImage(named: "checkbox-active"), for: .normal)
        SelectGroup_Check2.setImage(UIImage(named: "checkbox-inactive"), for: .normal)
    }
    
    @IBAction func SelectGroupCheck2(_ sender: Any) {
        UserDefaults.standard.set("2", forKey: "GROUP_CHECK_MSG")
        SelectGroup_Check2.setImage(UIImage(named: "checkbox-active"), for: .normal)
        SelectGroup_check1.setImage(UIImage(named: "checkbox-inactive"), for: .normal)
    }
    @IBAction func SelectClassGroupOkButton(_ sender: Any) {
          if (!ConnectionCheck.isConnectedToNetwork()) {
               
               self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
           }
           else
           {
        if(UserDefaults.standard.string(forKey: "CLASSGROUPCHECKMSG") == "1")
        {
           
           self.imgViewTransparent.isHidden = true
           self.SelectClassGroupPopupView.isHidden = true
           
           let parameter1 = [
                      "schoolCode" : "INTELL",
                      "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MTY1NTI3NCwiZXhwIjoxNTcxNjU4ODc0LCJuYmYiOjE1NzE2NTUyNzQsImp0aSI6IkhHVjM1VEhNczluSVJsS3AiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.42-1tkg8oGcrKAEwvp_yZdc2zn8nAOJ7BugTtfSsCLQ",
                       "academicYear" : "2019-2020",
                       "groupValue" : String(classgrouplabltext)
                      ] as [String : Any]
                  
               print("parameter for classByClassgroup:\(parameter1)")
                  Alamofire.request(url1, method: .post, parameters: parameter1).responseJSON {
                      response in
                      print("Response for classByClassgroup : \(response)")
                      guard let value = response.result.value as? [String : Any] else {
                          return
                      }
                   
                      switch response.result {
                          
                      case .success:
                       
                          let jsondataCopy = JSON(value)
                          let data: [NSDictionary] = jsondataCopy["data"].arrayObject as! [NSDictionary]
                          print("data>>\(data)")
                          if(self.userList1 != nil)
                          {
                          self.userList1.removeAll()
                          }
                          for item in data {
                          let classes = item.value(forKey: "classes")
                             print("name >>: \(classes!)")
                             
                             let classes2 = JSON(classes)
                             let data2: [NSDictionary] = classes2.arrayObject as! [NSDictionary]
                             print("data>>\(data)")
                             for item in data2
                             {
                                     let classId = item.value(forKey: "classId")
                                     print("classId >>: \(classId!)")
                                     let className = item.value(forKey: "className")
                                     print("className >>: \(className!)")
                                 
                                 
                                 let userObj1 = Class_DataModel(First_Lable: className! as? String, Second_Lable: classId as! Int)
                                 
                                 
                                 print("Data.count : \(self.userList1.count)")
                                 
                                 self.userList1.append(userObj1)
                             }
                         
                          }
                         self.SelectClassMsg_TableView.reloadData()
                       
                       
                   case .failure(_):
                       print("")
                   
               }
           }
           
        }
        else{
           self.view.makeToast(message: "Please Select Class Group Type.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
           }
           }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == SelectClassGroupTableView)
        {
            return userList.count
        }
        else if (tableView == SelectClassMsg_TableView)
        {
            return userList1.count
        }
        else if (tableView == SelectDivisionMsg_TableView)
        {
            return userList2.count
        }
        
        return Int()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (tableView == SelectClassGroupTableView)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectClassGroupMsgCell", for: indexPath) as! SelectClassGroupMsgCell
            cell.SelectClassGroupCheck.image = UIImage(named: "checkbox-inactive")
            let userObjec = userList[indexPath.row]
            cell.SelectClassGroupLbl.text = userObjec.getName()
        
            return cell
        }
        else if (tableView == SelectClassMsg_TableView)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectClassMsgTableViewCell", for: indexPath) as! SelectClassMsgTableViewCell
            cell.SelectClassMsgCheck.image = UIImage(named: "checkbox-inactive")
                let userObjec = userList1[indexPath.row]
            cell.SelectClassMsg_lbl.text = userObjec.getName()
            
                return cell
        }
        else if (tableView == SelectDivisionMsg_TableView)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectDivisionMsgTableViewCell", for: indexPath) as! SelectDivisionMsgTableViewCell
            cell.SelectDivisionMsgCheck.image = UIImage(named: "checkbox-inactive")
                let userObjec = userList2[indexPath.row]
            cell.SelectDivisionMsgLbl.text = userObjec.classDivisionId()
            
                return cell
        }
        
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (tableView == SelectClassGroupTableView)
        {
        let cell = self.SelectClassGroupTableView.cellForRow(at: indexPath) as!SelectClassGroupMsgCell
        let userObjec = userList[indexPath.row]
            cell.SelectClassGroupCheck.image = UIImage(named: "checkbox-active")
         UserDefaults.standard.set("1", forKey: "CLASSGROUPCHECKMSG")
        self.SelectClassGroupOutlet.titleLabel?.text = String(userObjec.getId()) + " | " + userObjec.getName()
        self.classgrouplabltext = String(userObjec.getId()) + "|" + userObjec.getName()
        }
        else if (tableView == SelectClassMsg_TableView)
        {
            let cell = self.SelectClassMsg_TableView.cellForRow(at: indexPath) as! SelectClassMsgTableViewCell
             let userObjec1 = userList1[indexPath.row]
            cell.SelectClassMsgCheck.image = UIImage(named: "checkbox-active")
             UserDefaults.standard.set("1", forKey: "CLASSCHECKMSG")
            self.SelectClassOutlet.titleLabel?.text = String(userObjec1.getId()) + " | " + userObjec1.getName()
            self.classlabltext = String(userObjec1.getId()) + "|" + userObjec1.getName()
        }
        else if (tableView == SelectDivisionMsg_TableView)
        {
            let cell = self.SelectDivisionMsg_TableView.cellForRow(at: indexPath) as! SelectDivisionMsgTableViewCell
             let userObjec2 = userList2[indexPath.row]
            cell.SelectDivisionMsgCheck.image = UIImage(named: "checkbox-active")
             UserDefaults.standard.set("1", forKey: "DIVISIONCHECKMSG")
            self.SelectDivisionOutlet.titleLabel?.text = String(userObjec2.classId()) + " | " + userObjec2.classDivisionId()
            self.divisionlabltext = String(userObjec2.classId()) + "|" + userObjec2.classDivisionId()
            self.ClassDivisionIdlasttext = userObjec2.classDivisionId()?.last?.description // as String
            print("ClassDivisionIdlasttextfor Msg : \(String(describing: ClassDivisionIdlasttext))")
            self.divisionlabltext_forSendApi = userObjec2.classDivisionId() + "|" + ClassDivisionIdlasttext! + "|" + classlabltext + "|" + classgrouplabltext + "|" + userObjec2.classAliase()
            print("divisionlabltext_forSendApi_for Msg : \(divisionlabltext_forSendApi!)")
        }
        
    
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if (tableView == SelectClassGroupTableView)
        {
        let cell = self.SelectClassGroupTableView.cellForRow(at: indexPath) as!SelectClassGroupMsgCell
            cell.SelectClassGroupCheck.image = UIImage(named: "checkbox-inactive")
        }
        else if (tableView == SelectClassMsg_TableView)
        {
           let cell = self.SelectClassMsg_TableView.cellForRow(at: indexPath) as! SelectClassMsgTableViewCell
            cell.SelectClassMsgCheck.image = UIImage(named: "checkbox-inactive")
        }
        else if (tableView == SelectDivisionMsg_TableView)
        {
           let cell = self.SelectDivisionMsg_TableView.cellForRow(at: indexPath) as! SelectDivisionMsgTableViewCell
            cell.SelectDivisionMsgCheck.image = UIImage(named: "checkbox-inactive")
        }
        
       
    }
    
    @IBAction func SelectClassMsgOkBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
                  
                  self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
              }
              else
              {
           if(UserDefaults.standard.string(forKey: "CLASSCHECKMSG") == "1")
           {
              
              self.imgViewTransparent.isHidden = true
              self.SelectClassMsg_PopupView.isHidden = true
              
           let parameter2 = [
                      "schoolCode" : "INTELL",
                      "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MTY1NTI3NCwiZXhwIjoxNTcxNjU4ODc0LCJuYmYiOjE1NzE2NTUyNzQsImp0aSI6IkhHVjM1VEhNczluSVJsS3AiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.42-1tkg8oGcrKAEwvp_yZdc2zn8nAOJ7BugTtfSsCLQ",
                       "academicYear" : "2019-2020",
                       "classValue" : String(classlabltext)
                      ] as [String : Any]
                  
               print("parameter for divisionByClasse:\(parameter2)")
                  Alamofire.request(url2, method: .post, parameters: parameter2).responseJSON {
                      response in
                      print("Response for divisionByClasse : \(response)")
                      guard let value = response.result.value as? [String : Any] else {
                          return
                      }
                   
                      switch response.result {
                          
                      case .success:
                       
                          let jsondataCopy = JSON(value)
                          let data: [NSDictionary] = jsondataCopy["data"].arrayObject as! [NSDictionary]
                          print("data>>\(data)")
                          if(self.userList2 != nil)
                          {
                          self.userList2.removeAll()
                          }
                          for item in data {
                          let divison = item.value(forKey: "divison")
                             print("name >>: \(divison!)")
                             
                             let divison2 = JSON(divison)
                             let data2: [NSDictionary] = divison2.arrayObject as! [NSDictionary]
                             print("data2>>\(data2)")
                             for item in data2
                             {
                                let classId = item.value(forKey: "classId")
                                    print("classId >>: \(classId!)")
                                    let classDivisionId = item.value(forKey: "classDivisionId")
                                    print("classDivisionId >>: \(classDivisionId!)")
                                    let className = item.value(forKey: "className")
                                    print("className >>: \(className!)")
                                    let classGroupId = item.value(forKey: "classGroupId")
                                    print("classGroupId >>: \(classGroupId!)")
                                    let classGroupName = item.value(forKey: "classGroupName")
                                    print("classGroupName >>: \(classGroupName!)")
                                    let classAliase = item.value(forKey: "classAliase")
                                    print("classAliase >>: \(classAliase!)")
                                 
                                 let userObj2 = Division_DataModel(First_Lable: classDivisionId! as? String, Second_Lable: classId as! Int, Third_Lable: className! as? String, Fourth_Lable: classGroupId as! Int,Fifth_Lable: classGroupName! as? String, Sixth_Lable: classAliase! as? String )
                                 
                                 print("Data.count : \(self.userList2.count)")
                                 
                                 self.userList2.append(userObj2)
                             }
                         
                          }
                         self.SelectDivisionMsg_TableView.reloadData()
                       
                       
                   case .failure(_):
                       print("")
                   
               }
           }
              
           }
           else{
              self.view.makeToast(message: "Please Select Class Group Type.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
              }
              }
    }
    
    @IBAction func SelectDivisionOkBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
              
              self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
          }
          else
          {
          if(UserDefaults.standard.string(forKey: "DIVISIONCHECKMSG") == "1")
          {
          self.imgViewTransparent.isHidden = true
          self.SelectDivisionMsg_Popup.isHidden = true
          }
          else
          {
              self.view.makeToast(message: "Please Select Division.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
          }
          }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
          if (!ConnectionCheck.isConnectedToNetwork()) {
              self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
          }
          else
          {
           TitleText.resignFirstResponder()
            DescriptionText.resignFirstResponder()
        }
        return (true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
         if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
             if self.ParentView.frame.origin.y == 0 {
                 self.ParentView.frame.origin.y -= keyboardSize.height
             }
         }
     }

     @objc func keyboardWillHide(notification: NSNotification) {
         if self.ParentView.frame.origin.y != 0 {
             self.ParentView.frame.origin.y = 0
         }
     }
    @objc func Homework_imgView(tapGestureRecognizer: UITapGestureRecognizer)
    {
       if (!ConnectionCheck.isConnectedToNetwork())
       {
           self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
       }
       else
       {
           UserDefaults.standard.set("0", forKey: "GROUP_CHECK")
           UserDefaults.standard.set("0", forKey: "CLASSGROUPCHECK")
           UserDefaults.standard.set("0", forKey: "CLASSCHECK")
           UserDefaults.standard.set("0", forKey: "DIVISIONCHECK")
           UserDefaults.standard.set("0", forKey: "SUBCHECK")
           
           
           
       let storyboard = UIStoryboard(name: "Main", bundle: nil)
              let controller = storyboard.instantiateViewController(withIdentifier: "Compose_HomeworkVC")
              self.present(controller, animated: false, completion: nil)
       }
    }
    @objc func Msg_imgView(tapGestureRecognizer1: UITapGestureRecognizer)
    {
       if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            UserDefaults.standard.set("0", forKey: "GROUP_CHECK_MSG")
            UserDefaults.standard.set("0", forKey: "CLASSGROUPCHECKMSG")
            UserDefaults.standard.set("0", forKey: "CLASSCHECKMSG")
            UserDefaults.standard.set("0", forKey: "DIVISIONCHECKMSG")
            
            
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Compose_MessageVC")
        self.present(controller, animated: false, completion: nil)
        }
    }
    @objc func Circular_imgView(tapGestureRecognizer2: UITapGestureRecognizer)
       {
           if (!ConnectionCheck.isConnectedToNetwork())
                 {
                     self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                 }
                 else
                 {
                 let storyboard = UIStoryboard(name: "Main", bundle: nil)
                 let controller = storyboard.instantiateViewController(withIdentifier: "ComposeCircularVC")
                 self.present(controller, animated: false, completion: nil)
                 }
       }
    @objc func SMS_imgView(tapGestureRecognizer3: UITapGestureRecognizer)
        {
          if (!ConnectionCheck.isConnectedToNetwork())
                {
                    self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                }
                else
                {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                       let controller = storyboard.instantiateViewController(withIdentifier: "Compose_SMSVC")
                       self.present(controller, animated: false, completion: nil)
                }
        }
    
    func reSetAllControls() {
        //    textView.text = PlaceHolderStrings.tvPlaceHolder
     //       textField.placeholder = PlaceHolderStrings.tfPlacehoder
            attachData.removeAll()
            mimeTypes.removeAll()
            attachExtensions.removeAll()
            attachmentList.removeAll()
        //    tableView.reloadData()
        }
        func showActivityIndicatory(uiView: UIView) {
    
            let loadingView: UIView = UIView()
            loadingView.frame =  CGRect(x: 0, y: 0, width: 80.0, height: 80.0)
            loadingView.center = uiView.center
            loadingView.backgroundColor = UIColor(red: 44/255, green: 44/255, blue: 44/255, alpha: 0.7)
            loadingView.clipsToBounds = true
            loadingView.layer.cornerRadius = 10
            actInd.frame = CGRect(x: 0, y: 0, width: 40.0, height: 40.0)
            actInd.style =
                UIActivityIndicatorView.Style.whiteLarge
            actInd.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
            loadingView.addSubview(actInd)
            uiView.addSubview(loadingView)
            actInd.startAnimating()
        }
        func stopActivityIndicator() {
            actInd.stopAnimating()
            let view = actInd.superview
            view?.removeFromSuperview()
        }
        
        func generateBoundaryString() -> String
          {
              return "Boundary-\(NSUUID().uuidString)"
          }
          
          func createBodyWithParameters(parameters: NSMutableDictionary?,boundary: String) -> NSData {
              let body = NSMutableData()
              if parameters != nil {
                  for (key, value) in parameters! {
                      if(value is String || value is NSString){
                          body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                          body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
                          body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
                      }else if(value is [Data]){
                          var i = 0;
                          for data in value as! [Data]{
                              let uuidStr = UUID().uuidString
                              let filename = "\(uuidStr).\(attachExtensions[i])"
                              print("filename", filename)
                               print("Mime", mimeTypes[i])
                              body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                              body.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n".data(using: String.Encoding.utf8)!)
                              body.append("Content-Type: \(mimeTypes[i])\r\n\r\n".data(using: String.Encoding.utf8)!)
                              body.append(data)
                              body.append("\r\n".data(using: String.Encoding.utf8)!)
                              i += 1;
                          }
                      }
                  }
              }
              body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
              return body
          }
        func createRequest (param : NSMutableDictionary , strURL : String) -> NSURLRequest {
            let boundary = generateBoundaryString()
            let url = NSURL(string: strURL)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            request.httpBody = createBodyWithParameters(parameters: param , boundary: boundary) as Data
            return request
        }
    
    
    //MARK:- ImagePickerView Methods
        
        func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
            for i in 0..<images.count{
                self.attachData.append(images[i].jpegData(compressionQuality: 0.5)!)
                self.mimeTypes.append("image/jpeg")
                self.attachExtensions.append("jpeg")
            }
            print("attachData : \(attachData)")
            print("mimeTypes : \(mimeTypes)")
            print("attachExtensions : \(attachExtensions)")
        }
        
        func imagePicker(_ picker: OpalImagePickerController, didFinishPickingAssets assets: [PHAsset]) {
            for asset in assets {
                if let name = asset.value(forKey: "filename") {
                    attachmentList.append(name as! String)
                }
            }
             print("attachmentList : \(attachmentList)")
         //   tableView.reloadData()
            picker.dismiss(animated: true, completion: nil)
        }
    
    func pickDocument(){
              let documentPicker = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: .import)
            documentPicker.delegate = self as! UIDocumentPickerDelegate
              self.present(documentPicker, animated: true, completion: nil)
          }
          func pickImage(){
              let imagePicker = OpalImagePickerController()
              //Change color of selection overlay to white
              imagePicker.selectionTintColor = UIColor.white.withAlphaComponent(0.7)
              
              //Change color of image tint to black
              imagePicker.selectionImageTintColor = UIColor.black
              
              //Change image to X rather than checkmark
              //        imagePicker.selectionImage = UIImage(named: "x_image")
              
              //Change status bar style
              imagePicker.statusBarPreference = UIStatusBarStyle.lightContent
              
              //Limit maximum allowed selections to 5
              imagePicker.maximumSelectionsAllowed = 5 - self.attachmentList.count
              
              //Only allow image media type assets
              imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
              imagePicker.imagePickerDelegate = self
              present(imagePicker, animated: true, completion: nil)
          }

    
}
extension ComposeMessageVC : UIDocumentPickerDelegate{
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let docUrl = url as URL
        let docData  = try! Data(contentsOf: docUrl)
        self.attachData.append(docData)
        self.attachmentList.append(url.lastPathComponent)
        self.attachExtensions.append("pdf")
        self.mimeTypes.append("application/pdf")
 //       self.tableView.reloadData()
    }
}
