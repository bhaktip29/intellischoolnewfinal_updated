//
//  ComposeSMS_GroupNonTeachingStaffVC.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 11/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ComposeSMS_GroupNonTeachingStaffVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var SMS_imgView: UIImageView!
    @IBOutlet weak var Circular_imgView: UIImageView!
    @IBOutlet weak var Msg_imgView: UIImageView!
    @IBOutlet weak var Homework_imgView: UIImageView!

    @IBOutlet weak var SelectReceiverTypeLayout: UIButton!
    @IBOutlet weak var SelectReceiversTableView: UITableView!
    @IBOutlet weak var SelectReceiversPopupView: UIView!
    @IBOutlet var ParentView: UIView!
    @IBOutlet weak var DescriptionText: UITextField!
    
    @IBOutlet weak var SelectMsgTableView: UITableView!
    @IBOutlet weak var SelectMsgTypePopup: UIView!
    
    @IBOutlet weak var SelectMsgTypeLayout: UIButton!
    let imgViewTransparent = UIView()
      static let screenSize = UIScreen.main.bounds
    
    let MsgType: [String] = ["Group", "Individual"]
    let ReceiverType: [String] = ["Student", "Teaching Staff", "Non-Teaching Staff", "To Mobile Number"]
    let send_url = "http://commn.intellischools.com/hmcs_angular_api/public/api/sendSMS_android"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgViewTransparent.isHidden = true
        
        self.SelectMsgTypePopup.isHidden = true
        self.SelectReceiversPopupView.isHidden = true
        
        UserDefaults.standard.set("1", forKey: "MESSAGECHECK")
        UserDefaults.standard.set("1", forKey: "RECEIVERCHECK")
        UserDefaults.standard.set("1", forKey: "MESSAGECHECKOPTION")
        UserDefaults.standard.set("1", forKey: "RECEIVERCHECKOPTION")
        
        
        self.SelectMsgTypePopup.center = CGPoint(x: self.ParentView.bounds.midX, y: self.ParentView.bounds.midY)
        self.SelectReceiversPopupView.center = CGPoint(x: self.ParentView.bounds.midX, y: self.ParentView.bounds.midY)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.DescriptionText.delegate = self
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(Homework_imgView(tapGestureRecognizer:)))
               Homework_imgView.isUserInteractionEnabled = true
               Homework_imgView.addGestureRecognizer(tapGestureRecognizer)
        
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(Msg_imgView(tapGestureRecognizer1:)))
        Msg_imgView.isUserInteractionEnabled = true
        Msg_imgView.addGestureRecognizer(tapGestureRecognizer1)
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(Circular_imgView(tapGestureRecognizer2:)))
        Circular_imgView.isUserInteractionEnabled = true
        Circular_imgView.addGestureRecognizer(tapGestureRecognizer2)
        
        let tapGestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(SMS_imgView(tapGestureRecognizer3:)))
        SMS_imgView.isUserInteractionEnabled = true
        SMS_imgView.addGestureRecognizer(tapGestureRecognizer3)
    }
    
    @IBAction func BackBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                   let controller = storyboard.instantiateViewController(withIdentifier: "vc")
                   self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func AttachmentBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        }
    }
    @IBAction func SendBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            if (!ConnectionCheck.isConnectedToNetwork())
                          {
                              self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                          }
                          else
                          {
                              if(UserDefaults.standard.string(forKey: "RECEIVERCHECK") == "1" && DescriptionText.text?.count != 0)
                              {
                                  print("Receivers Group selected..")
                                let send_parameter = [
                                              "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MTYwMjcxMSwiZXhwIjoxNTcxNjA2MzExLCJuYmYiOjE1NzE2MDI3MTEsImp0aSI6ImhQa0M2WmRjT1J4dXhkQlUiLCJzdWIiOjUyLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.84Ps9Zj6ykr82801MDoa4vc3Fiy26S5zfB-_acsQATE",
                                              "schoolCode" : "INTELL",
                                               "message_type" : "group",
                                               "receiver_group_type" : "nonteachingstaff",
                                               "sms_sender" : "INTELL19E00SCH002",
                                               "message" : DescriptionText.text!, //"Test1",
                                               "academicYear" : "2019-2020",
                                               "ip_addr" : "test"
                                              ] as [String : Any]
                                
                                       print("parameter for send_url sms group teaching:\(send_parameter)")
                                          Alamofire.request(send_url, method: .post, parameters: send_parameter).responseJSON {
                                              response in
                                              print("Response for send_url sms group teaching staff : \(response)")
                                              guard let value = response.result.value as? [String : Any] else {
                                                  return
                                              }
                                        
                                            switch response.result {
                                                    case .success:
                                                    //     let data = value["data"]
                                                        if ((response.result.value) != nil) {
                                                            
                                                            let jsondataCopy = JSON(value)
                                                            // ClassGroup..==============================================
                                                            let recordsArray: [NSDictionary] = jsondataCopy["message"].arrayObject as! [NSDictionary]
                                                            
                                                         //     print("jsondataCopy for send homework : \(jsondataCopy)")
                                                         //    print("recordsArray for send homework : \(recordsArray)")
                                                            
                                                            for item in recordsArray {
                                                                let msg = item.value(forKey: "msg")
                                                                print("msg : \(msg!)")
                                                                self.view.makeToast(message: msg as! String, duration: 0.7, position: HRToastPositionCenter as AnyObject)
                                                            }
                                                            self.DescriptionText.text = ""
                                                        }
                                                    case .failure(_):
                                                        print("")
                                                }
                                   }
                              
                              }
                                  else if(DescriptionText.text?.count == 0)
                                  {
                                      self.view.makeToast(message: "Please Enter Message.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                                  }
                              else
                              {
                                   self.view.makeToast(message: "Please Select Recevier's Group first.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                              }
                          }
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "Compose_SMSVC")
            self.present(controller, animated: false, completion: nil)
        }
    }
    @IBAction func HomeworkBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            UserDefaults.standard.set("0", forKey: "GROUP_CHECK")
            UserDefaults.standard.set("0", forKey: "CLASSGROUPCHECK")
            UserDefaults.standard.set("0", forKey: "CLASSCHECK")
            UserDefaults.standard.set("0", forKey: "DIVISIONCHECK")
            UserDefaults.standard.set("0", forKey: "SUBCHECK")
            
                 let storyboard = UIStoryboard(name: "Main", bundle: nil)
                       let controller = storyboard.instantiateViewController(withIdentifier: "Compose_HomeworkVC")
                       self.present(controller, animated: false, completion: nil)
        }
    }
    @IBAction func CircularBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ComposeCircularVC")
        self.present(controller, animated: false, completion: nil)
        }
    }
    @IBAction func MessageBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Compose_MessageVC")
        self.present(controller, animated: false, completion: nil)
        }
    }
    @IBAction func SmsBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let controller = storyboard.instantiateViewController(withIdentifier: "Compose_SMSVC")
               self.present(controller, animated: false, completion: nil)
        }
    }
    @IBAction func HomeBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "vc")
        self.present(controller, animated: true, completion: nil)
        }
    }
    @IBAction func ComposeBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Compose_SMSVC")
        self.present(controller, animated: false, completion: nil)
        }
    }
    @IBAction func SupportBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
          if (!ConnectionCheck.isConnectedToNetwork()) {
              self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
          }
          else
          {
            DescriptionText.resignFirstResponder()
        }
        return (true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    @objc func keyboardWillShow(notification: NSNotification) {
         if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
             if self.ParentView.frame.origin.y == 0 {
                 self.ParentView.frame.origin.y -= keyboardSize.height
             }
         }
     }

     @objc func keyboardWillHide(notification: NSNotification) {
         if self.ParentView.frame.origin.y != 0 {
             self.ParentView.frame.origin.y = 0
         }
     }

    
    @IBAction func SelectMessageType(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        self.imgViewTransparent.frame = CGRect(x: 0, y: 0, width: ComposeSMS_GroupNonTeachingStaffVC.screenSize.width, height: ComposeSMS_GroupNonTeachingStaffVC.screenSize.height)
                                      
                      self.imgViewTransparent.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                                      
                      self.view.addSubview(self.imgViewTransparent)
                      self.imgViewTransparent.isHidden = false
                      self.imgViewTransparent.addSubview(self.SelectMsgTypePopup)
                      self.SelectMsgTypePopup.isHidden = false
        }
    }
    
    @IBAction func SelectReceivesGroup(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
                              
                              self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                          }
                          else
                          {
                          if(UserDefaults.standard.string(forKey: "MESSAGECHECK") == "1")
                          {
                              self.imgViewTransparent.frame = CGRect(x: 0, y: 0, width: ComposeSMS_GroupNonTeachingStaffVC.screenSize.width, height: ComposeSMS_GroupNonTeachingStaffVC.screenSize.height)
                                              
                              self.imgViewTransparent.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                                              
                              self.view.addSubview(self.imgViewTransparent)
                              self.imgViewTransparent.isHidden = false
                              self.imgViewTransparent.addSubview(self.SelectReceiversPopupView)
                              self.SelectReceiversPopupView.isHidden = false
                          }
                          
                          else
                          {
                              self.view.makeToast(message: "Please Select Message Type first.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                          }
                          }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == SelectMsgTableView)
        {
            return MsgType.count
        }
        if (tableView == SelectReceiversTableView)
        {
            return ReceiverType.count
        }
        return Int()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if (tableView == SelectMsgTableView)
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectSMS_messageTypeCell", for: indexPath) as! SelectSMS_messageTypeCell
        cell.SelectMsgTypeCheck.image = UIImage(named: "checkbox-inactive")
    //    let userObjec = userList_specificClassGroup1[indexPath.row]
        cell.SelectMessageType_lbl.text = MsgType[indexPath.row]
        
        return cell
    }
    else if (tableView == SelectReceiversTableView)
    {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectReceiversGroupCell", for: indexPath) as! SelectReceiversGroupCell
        cell.SelectReceiversCheck.image = UIImage(named: "checkbox-inactive")
        //    let userObjec = userList_specificClassGroup1[indexPath.row]
        cell.SelectReceiversLbl.text = ReceiverType[indexPath.row]
            
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (tableView == SelectMsgTableView)
        {
            let cell = self.SelectMsgTableView.cellForRow(at: indexPath) as! SelectSMS_messageTypeCell
            
            let userObjec = MsgType[indexPath.row]
            cell.SelectMsgTypeCheck.image = UIImage(named: "checkbox-active")
            UserDefaults.standard.set("1", forKey: "MESSAGECHECK")
            UserDefaults.standard.set(indexPath.row + 1, forKey: "MESSAGECHECKOPTION")
            self.SelectMsgTypeLayout.titleLabel?.text = userObjec //String(userObjec.getId()) + " | " + userObjec.getName()
//    self.selectSpecificClassgroup = String(userObjec.getId()) + "|" + userObjec.getName()
    }
      else if (tableView == SelectReceiversTableView)
                {
                    let cell = self.SelectReceiversTableView.cellForRow(at: indexPath) as! SelectReceiversGroupCell
                    let userObjec = ReceiverType[indexPath.row]
                    cell.SelectReceiversCheck.image = UIImage(named: "checkbox-active")
                    UserDefaults.standard.set("1", forKey: "RECEIVERCHECK")
                    UserDefaults.standard.set(indexPath.row + 1, forKey: "RECEIVERCHECKOPTION")
                    self.SelectReceiverTypeLayout?.titleLabel?.text = userObjec //String(userObjec.getId()) + " | " + userObjec.getName()
        //    self.selectSpecificClassgroup = String(userObjec.getId()) + "|" + userObjec.getName()
            }
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    if (tableView == SelectMsgTableView)
    {
    let cell = self.SelectMsgTableView.cellForRow(at: indexPath) as! SelectSMS_messageTypeCell
        cell.SelectMsgTypeCheck.image = UIImage(named: "checkbox-inactive")
    }
     else if (tableView == SelectReceiversTableView)
        {
        let cell = self.SelectReceiversTableView.cellForRow(at: indexPath) as! SelectReceiversGroupCell
            cell.SelectReceiversCheck.image = UIImage(named: "checkbox-inactive")
        }
    }
    @IBAction func SelectMsgTypeOkBtn(_ sender: Any) {
      if (!ConnectionCheck.isConnectedToNetwork())
               {
                   self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
               }
               else
               {
               if(UserDefaults.standard.string(forKey: "MESSAGECHECK") == "0")
               {
                   self.view.makeToast(message: "Please Select Message Type.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
               }
               else
               {
                 /*  if(UserDefaults.standard.string(forKey: "MESSAGECHECK") == "1")
                   {
                       SelectGroupOutlet.titleLabel?.text = "CLASS|CLASSGROUP"
                       let storyboard = UIStoryboard(name: "Main", bundle: nil)
                       let controller = storyboard.instantiateViewController(withIdentifier: "Compose_MessageVC")
                       self.present(controller, animated: false, completion: nil)
                   }
                   else if(UserDefaults.standard.string(forKey: "GROUP_CHECK") == "2")
                   {
                       SelectGroupOutlet.titleLabel?.text = "CLASS|SPECIFICGROUP"
                       
                   }*/
               self.imgViewTransparent.isHidden = true
               self.SelectMsgTypePopup.isHidden = true
               }
               }
           }
    @IBAction func SelectReceiversOkButton(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        if(UserDefaults.standard.string(forKey: "RECEIVERCHECK") == "0")
        {
            self.view.makeToast(message: "Please Select Receiver's Group.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            if(UserDefaults.standard.string(forKey: "RECEIVERCHECKOPTION") == "1" && UserDefaults.standard.string(forKey: "MESSAGECHECKOPTION") == "1")
            {
            //    SelectGroupOutlet.titleLabel?.text = "CLASS|CLASSGROUP"
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "ComposeSmsGroupStudentVC")
                self.present(controller, animated: false, completion: nil)
            }
            else if(UserDefaults.standard.string(forKey: "RECEIVERCHECKOPTION") == "1" && UserDefaults.standard.string(forKey: "MESSAGECHECKOPTION") == "2")
             {
           //      SelectGroupOutlet.titleLabel?.text = "CLASS|CLASSGROUP"
                 let storyboard = UIStoryboard(name: "Main", bundle: nil)
                 let controller = storyboard.instantiateViewController(withIdentifier: "ComposeSMS_IndividualStudentVC")
                 self.present(controller, animated: false, completion: nil)
             }
            else if(UserDefaults.standard.string(forKey: "RECEIVERCHECKOPTION") == "2" && UserDefaults.standard.string(forKey: "MESSAGECHECKOPTION") == "1")
            {
            //    SelectGroupOutlet.titleLabel?.text = "CLASS|CLASSGROUP"
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "ComposeSMS_GroupTeachingStaff")
                self.present(controller, animated: false, completion: nil)
                
            }
             else if(UserDefaults.standard.string(forKey: "RECEIVERCHECKOPTION") == "2" && UserDefaults.standard.string(forKey: "MESSAGECHECKOPTION") == "2")
             {
          //       SelectGroupOutlet.titleLabel?.text = "CLASS|CLASSGROUP"
                 let storyboard = UIStoryboard(name: "Main", bundle: nil)
                 let controller = storyboard.instantiateViewController(withIdentifier: "ComposeSMS_TeachingFunctionalityVC")
                 self.present(controller, animated: false, completion: nil)
                 
             }
             else if(UserDefaults.standard.string(forKey: "RECEIVERCHECKOPTION") == "3" && UserDefaults.standard.string(forKey: "MESSAGECHECKOPTION") == "1")
             {
          //       SelectGroupOutlet.titleLabel?.text = "CLASS|CLASSGROUP"
                 let storyboard = UIStoryboard(name: "Main", bundle: nil)
                 let controller = storyboard.instantiateViewController(withIdentifier: "ComposeSMS_GroupNonTeachingStaffVC")
                 self.present(controller, animated: false, completion: nil)
                 
             }
             else if(UserDefaults.standard.string(forKey: "RECEIVERCHECKOPTION") == "3" && UserDefaults.standard.string(forKey: "MESSAGECHECKOPTION") == "2")
                         {
                     //        SelectGroupOutlet.titleLabel?.text = "CLASS|CLASSGROUP"
                             let storyboard = UIStoryboard(name: "Main", bundle: nil)
                             let controller = storyboard.instantiateViewController(withIdentifier: "ComposeSmsNonteachingFunctionalityVC")
                             self.present(controller, animated: false, completion: nil)
                             
                         }
             else if(UserDefaults.standard.string(forKey: "RECEIVERCHECKOPTION") == "4" && UserDefaults.standard.string(forKey: "MESSAGECHECKOPTION") == "1")
             {
        //         SelectGroupOutlet.titleLabel?.text = "CLASS|CLASSGROUP"
                 let storyboard = UIStoryboard(name: "Main", bundle: nil)
                 let controller = storyboard.instantiateViewController(withIdentifier: "ComposeSMS_Group_ToNumberVC")
                 self.present(controller, animated: false, completion: nil)
                 
             }
             else if(UserDefaults.standard.string(forKey: "RECEIVERCHECKOPTION") == "4" && UserDefaults.standard.string(forKey: "MESSAGECHECKOPTION") == "2")
                         {
                 //            SelectGroupOutlet.titleLabel?.text = "CLASS|CLASSGROUP"
                             let storyboard = UIStoryboard(name: "Main", bundle: nil)
                             let controller = storyboard.instantiateViewController(withIdentifier: "ComposeSMS_Individual_ToNumber")
                             self.present(controller, animated: false, completion: nil)
                             
                         }
             
        self.imgViewTransparent.isHidden = true
        self.SelectReceiversPopupView.isHidden = true
        }
        }
    }
    @objc func Homework_imgView(tapGestureRecognizer: UITapGestureRecognizer)
    {
       if (!ConnectionCheck.isConnectedToNetwork())
              {
                  self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
              }
              else
              {
                  UserDefaults.standard.set("0", forKey: "GROUP_CHECK")
                  UserDefaults.standard.set("0", forKey: "CLASSGROUPCHECK")
                  UserDefaults.standard.set("0", forKey: "CLASSCHECK")
                  UserDefaults.standard.set("0", forKey: "DIVISIONCHECK")
                  UserDefaults.standard.set("0", forKey: "SUBCHECK")
                  
                       let storyboard = UIStoryboard(name: "Main", bundle: nil)
                             let controller = storyboard.instantiateViewController(withIdentifier: "Compose_HomeworkVC")
                             self.present(controller, animated: false, completion: nil)
              }
    }
    @objc func Msg_imgView(tapGestureRecognizer1: UITapGestureRecognizer)
    {
       if (!ConnectionCheck.isConnectedToNetwork())
              {
                  self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
              }
              else
              {
              let storyboard = UIStoryboard(name: "Main", bundle: nil)
              let controller = storyboard.instantiateViewController(withIdentifier: "Compose_MessageVC")
              self.present(controller, animated: false, completion: nil)
              }
    }
    @objc func Circular_imgView(tapGestureRecognizer2: UITapGestureRecognizer)
       {
           if (!ConnectionCheck.isConnectedToNetwork())
           {
               self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
           }
           else
           {
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           let controller = storyboard.instantiateViewController(withIdentifier: "ComposeCircularVC")
           self.present(controller, animated: false, completion: nil)
           }
       }
    @objc func SMS_imgView(tapGestureRecognizer3: UITapGestureRecognizer)
        {
          if (!ConnectionCheck.isConnectedToNetwork())
                 {
                     self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                 }
                 else
                 {
                 let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let controller = storyboard.instantiateViewController(withIdentifier: "Compose_SMSVC")
                        self.present(controller, animated: false, completion: nil)
                 }
        }
}
