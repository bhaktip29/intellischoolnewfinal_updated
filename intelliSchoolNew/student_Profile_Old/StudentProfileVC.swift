//
//  StudentProfileVC.swift
//  SchoolProtoTypeApp
//
//  Created by Intellinects on 21/09/17.
//  Copyright © 2017 Intellinects Ventures. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class StudentProfileVC: BaseViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CalenderDelegate {
    
    var topAnchorForMiddleView: NSLayoutConstraint?
    let cellId = "Cell"
    let headerCellId  = "HeaderCell"
    var student = StudentInfo()
    
    var selectedIndex: Int = 0
    
    let middleView : UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    let profileImageView: UIImageView = {
        let imgView = UIImageView()
        imgView.layer.cornerRadius = 3
        imgView.layer.masksToBounds = true
        imgView.layer.borderColor = UIColor.gray.cgColor
        imgView.layer.borderWidth = 0.3
        imgView.contentMode = .scaleToFill
        imgView.image = UIImage(named: "sp_placeholder")
        imgView.translatesAutoresizingMaskIntoConstraints  = false
        return imgView
    }()
    
    let nameLbl : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.boldSystemFont(ofSize: 13)
        lbl.text = "Name : "
        lbl.numberOfLines = 0
        lbl.sizeToFit()
        lbl.textAlignment = .left
        return lbl
    }()
    
    let classLbl : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.boldSystemFont(ofSize: 13)
        lbl.text = "Class : "
        lbl.textAlignment = .left
        return lbl
    }()

    let rollNoLbl : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.boldSystemFont(ofSize: 13)
        lbl.text = "Roll No : "
        lbl.textAlignment = .left
        return lbl
    }()
    
    let grNoLbl : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.boldSystemFont(ofSize: 13)
        lbl.text = "Gr. No : "
        lbl.textAlignment = .left
        return lbl
    }()
    
    let tabView : UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    let rightConatinerView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    let segmentControl : UISegmentedControl = {
        let segControl = UISegmentedControl()
        segControl.backgroundColor = GlobleConstants.homeScreenIconColor
        #if SHARDAMANDIR
        segControl.tintColor = UIColor.white
        #elseif DRANTONIODA
        segControl.tintColor = UIColor.white
        #else
        segControl.tintColor = GlobleConstants.navigationBarColorTint
        #endif
        segControl.translatesAutoresizingMaskIntoConstraints = false
        return segControl
    }()
    
    let headerLbl : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.backgroundColor = UIColor.cyan.withAlphaComponent(0.2)
        lbl.textAlignment = .center
        return lbl
    }()
    
    //MARK: - Custom View Setup Methods
    
    func setUpHeaderLbl() {
        
        view.addSubview(headerLbl)
        // needed constarints
        headerLbl.topAnchor.constraint(equalTo: middleView.bottomAnchor,constant: 4).isActive = true
        headerLbl.leftAnchor.constraint(equalTo: view.leftAnchor,constant:1).isActive = true
        headerLbl.rightAnchor.constraint(equalTo: view.rightAnchor,constant:-2).isActive = true
        headerLbl.heightAnchor.constraint(equalToConstant:35).isActive = true
    }
    
    private func setMiddelView() {
        let model = UIDevice.current.modelName
        var constant: CGFloat = 0.0
        if model == "iPhone10,6" || model == "iPhone10,3" || (model == "x86_64" && UIDevice.current.model == "iPhone") {
            constant = 86.0
        }else {
            constant = 64.0
        }
        view.addSubview(middleView)
        // needed constarint x,y,w,h
        middleView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        middleView.topAnchor.constraint(equalTo: view.topAnchor,constant:constant).isActive = true
        middleView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        middleView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 1/3).isActive = true
        
        // setup tabs
        setUpTabView()
        setupSegmentControl()
        
        middleView.addSubview(profileImageView)
        // needed constarint x,y,w,h
        profileImageView.leftAnchor.constraint(equalTo: middleView.leftAnchor,constant: 8).isActive = true
        profileImageView.topAnchor.constraint(equalTo: middleView.topAnchor,constant: 8).isActive = true
        profileImageView.widthAnchor.constraint(equalTo: middleView.widthAnchor, multiplier: 1/3,constant:20).isActive = true
        profileImageView.bottomAnchor.constraint(equalTo: segmentControl.topAnchor, constant: -12).isActive = true
        
        middleView.addSubview(rightConatinerView)
        // needed constarint x,y,w,h
        rightConatinerView.rightAnchor.constraint(equalTo: middleView.rightAnchor,constant: 8).isActive = true
        rightConatinerView.topAnchor.constraint(equalTo: middleView.topAnchor).isActive = true
        rightConatinerView.widthAnchor.constraint(equalTo: middleView.widthAnchor, multiplier: 0.6,constant: -16).isActive = true
        rightConatinerView.bottomAnchor.constraint(equalTo: segmentControl.topAnchor).isActive = true
        
        setUpRightContainer()
    }
    
    private func setupSegmentControl() {
        
        middleView.addSubview(segmentControl)
        // needed constarint x,y,w,h
        segmentControl.leftAnchor.constraint(equalTo: middleView.leftAnchor,constant:-1).isActive = true
        segmentControl.bottomAnchor.constraint(equalTo: middleView.bottomAnchor).isActive = true
        segmentControl.widthAnchor.constraint(equalTo: middleView.widthAnchor,constant:2).isActive = true
        segmentControl.heightAnchor.constraint(equalToConstant: 44).isActive = true
    }
    
    private func setUpRightContainer() {
        
        rightConatinerView.addSubview(nameLbl)
        // needed constarint x,y,w,h
        nameLbl.topAnchor.constraint(equalTo: rightConatinerView.topAnchor,constant: 8).isActive = true
        nameLbl.leftAnchor.constraint(equalTo: rightConatinerView.leftAnchor,constant:8).isActive = true
        nameLbl.widthAnchor.constraint(equalTo: rightConatinerView.widthAnchor,constant:-8).isActive = true
        nameLbl.heightAnchor.constraint(equalTo: rightConatinerView.heightAnchor,multiplier:1/4).isActive = true
        
        rightConatinerView.addSubview(classLbl)
        // needed constarint x,y,w,h
        classLbl.topAnchor.constraint(equalTo: nameLbl.bottomAnchor,constant:8).isActive = true
        classLbl.leftAnchor.constraint(equalTo: rightConatinerView.leftAnchor,constant:8).isActive = true
        classLbl.widthAnchor.constraint(equalTo: rightConatinerView.widthAnchor,constant:-8).isActive = true
        classLbl.heightAnchor.constraint(equalTo: rightConatinerView.heightAnchor,multiplier:1/4,constant:-16).isActive = true
        
        rightConatinerView.addSubview(rollNoLbl)
        // needed constarint x,y,w,h
        rollNoLbl.topAnchor.constraint(equalTo: classLbl.bottomAnchor,constant: 8).isActive = true
        rollNoLbl.leftAnchor.constraint(equalTo: rightConatinerView.leftAnchor,constant:8).isActive = true
        rollNoLbl.widthAnchor.constraint(equalTo: rightConatinerView.widthAnchor,constant:-16).isActive = true
        rollNoLbl.heightAnchor.constraint(equalTo: rightConatinerView.heightAnchor,multiplier:1/4,constant:-16).isActive = true

        rightConatinerView.addSubview(grNoLbl)
        // needed constarint x,y,w,h
        grNoLbl.topAnchor.constraint(equalTo: rollNoLbl.bottomAnchor,constant:8).isActive = true
        grNoLbl.leftAnchor.constraint(equalTo: rightConatinerView.leftAnchor,constant:8).isActive = true
        grNoLbl.widthAnchor.constraint(equalTo: rightConatinerView.widthAnchor,constant:-16).isActive = true
        grNoLbl.heightAnchor.constraint(equalTo: rightConatinerView.heightAnchor,multiplier:1/4,constant:-16).isActive = true
    }
    
    private func setUpTabView() {
        view.addSubview(tabView)
        // needed constarint x,y,w,h
        tabView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tabView.topAnchor.constraint(equalTo: middleView.bottomAnchor,constant: 4).isActive = true
        tabView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        tabView.bottomAnchor.constraint(equalTo:view.bottomAnchor).isActive = true
        tabView.backgroundColor = UIColor.groupTableViewBackground.withAlphaComponent(0.1)
    }
    
    //MARK: - View Controller Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 25.0/255.0, green: 89.0/255.0, blue: 194.0/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(handleSlide))
        navigationItem.leftBarButtonItem?.tintColor = .white
        
        view.backgroundColor = UIColor.white
        self.title = "Profile"
        setUpViews()
        addSwipeFeatureToTabView()
        getProfileData()
        
        nameLbl.text = "\(UserDefaults.standard.string(forKey: "\(SDefaultKeys.name)\(UserDefaults.standard.integer(forKey: SDefaultKeys.selectedStudent))")?.capitalized ?? "")"
        let cls = UserDefaults.standard.string(forKey:"isirsClassName")as! String
        let div = UserDefaults.standard.string(forKey:"isirsdivisionName")as! String
        print("clsclscls\(cls)")
        classLbl.text = "Class : \(cls)-\(div)"
    }
    
    @objc func handleSlide() {
        print("back button tapped..")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let controller = storyboard.instantiateViewController(withIdentifier: "vc")
               self.present(controller, animated: true, completion: nil)
    }
    
    //MARK:- Custom Methods
    
    func setUpViews() {
        setMiddelView()
        setUpHeaderLbl()
        addSegmentsToSegmentControl()
    }
    
    func addSwipeFeatureToTabView() {
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes))
        swipeLeft.direction = .left
        tabView.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes))
        swipeRight.direction = .right
        tabView.addGestureRecognizer(swipeRight)
    }
    
    func addSegmentsToSegmentControl() {
        
        segmentControl.addTarget(self, action: #selector(handleSegmentSelection), for: .valueChanged)
        segmentControl.insertSegment(with:UIImage(named:"ic_profile_white"), at: 0, animated: true)
        segmentControl.insertSegment(with: UIImage(named:"ic_attendance_white"), at: 1, animated: true)
        segmentControl.insertSegment(with: UIImage(named:"ic_evaluation_white"), at: 2, animated: true)
        segmentControl.selectedSegmentIndex = 0
    }
    
    func setUpCollectionViewForTab() {
        
        tabView.subviews.forEach({ $0.removeFromSuperview() })
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        
        func setCollection(_ collectionView:UICollectionView?) {
            
            collectionView?.translatesAutoresizingMaskIntoConstraints = false
            collectionView?.backgroundColor = UIColor.white
            tabView.addSubview(collectionView!)
            if selectedIndex == 1 {
                guard let sView = stackView else {
                    return
                }
                collectionView?.topAnchor.constraint(equalTo: sView.bottomAnchor).isActive = true
            }else {
                collectionView?.topAnchor.constraint(equalTo: headerLbl.bottomAnchor,constant:1).isActive = true
            }
            collectionView?.leftAnchor.constraint(equalTo: tabView.leftAnchor,constant:1).isActive = true
            collectionView?.rightAnchor.constraint(equalTo: tabView.rightAnchor,constant:-2).isActive = true
            collectionView?.bottomAnchor.constraint(equalTo: tabView.bottomAnchor,constant:-2).isActive = true
        }
        
        switch selectedIndex+1 {
            
        case 1:
            headerLbl.text = "More Details"
            let collectionView = ProfileDetailsCVC(frame: CGRect.zero, collectionViewLayout: layout)
            setCollection(collectionView)
            collectionView.studentInfo = student
            collectionView.reloadData()
            break
        case 2:
            headerLbl.text = "Attendance Summary"
            setupLabelForAttendanceSummary()
            let collectionView = MonthlyAttendanceCVC(frame: CGRect.zero, collectionViewLayout: layout)
            setCollection(collectionView)
            collectionView.cDelegate = self
            collectionView.board = student.board ?? ""
            collectionView.classId = student.classId ?? ""
            collectionView.getAttendanceByDate(Date())
            collectionView.reloadData()
            break
        case 3:
            headerLbl.text = "Academic Record"
            let msgLbl = UILabel()
            msgLbl.translatesAutoresizingMaskIntoConstraints = false
            msgLbl.text = "This service is proposed for academic year 2019-20."
            msgLbl.textAlignment = .center
            msgLbl.numberOfLines = 0
            msgLbl.sizeToFit()
            tabView.addSubview(msgLbl)
            
            msgLbl.topAnchor.constraint(equalTo: headerLbl.bottomAnchor,constant:1).isActive = true
            msgLbl.leftAnchor.constraint(equalTo: tabView.leftAnchor,constant:1).isActive = true
            msgLbl.rightAnchor.constraint(equalTo: tabView.rightAnchor,constant:-2).isActive = true
            msgLbl.bottomAnchor.constraint(equalTo: tabView.bottomAnchor,constant:-2).isActive = true
            break
        default:
            break
        }
    }
    
    var stackView: UIStackView?
    
    private func setupLabelForAttendanceSummary() {
        
        var timeLbl = UILabel()
        var timeLbl1 = UILabel()
        var timeLbl2 = UILabel()
        var lbls = [UILabel]()
        
        func getLabel() -> UILabel {
            let lbl = UILabel()
            lbl.textColor = UIColor.black
            lbl.font = UIFont.systemFont(ofSize: 13)
            lbl.textAlignment = .center
            lbl.numberOfLines = 0
            lbl.layer.borderColor = UIColor.gray.cgColor
            lbl.layer.borderWidth = 0.3
            lbl.backgroundColor = UIColor.cyan.withAlphaComponent(0.2)
            lbl.sizeToFit()
            return lbl
        }
        
        timeLbl = getLabel()
        timeLbl.text = "Month"
        lbls.append(timeLbl)
        timeLbl1 = getLabel()
        timeLbl1.text = "Working Days"
        lbls.append(timeLbl1)
        timeLbl2 = getLabel()
        timeLbl2.text = "Present Days"
        lbls.append(timeLbl2)
        
        stackView = UIStackView(arrangedSubviews: lbls)
        stackView?.alignment = .fill
        stackView?.axis = .horizontal
        stackView?.distribution = .fillEqually
        stackView?.translatesAutoresizingMaskIntoConstraints = false
        
        tabView.addSubview(stackView!)
        // needed constarints
        stackView?.leftAnchor.constraint(equalTo: tabView.leftAnchor,constant:0.5).isActive = true
        stackView?.topAnchor.constraint(equalTo: headerLbl.bottomAnchor,constant:1).isActive = true
        stackView?.widthAnchor.constraint(equalTo: tabView.widthAnchor,constant:-1).isActive = true
        stackView?.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    
        @objc func handleSwipes(sender: UISwipeGestureRecognizer) {
        if (sender.direction == .right) {
            if selectedIndex > 0 {
                selectedIndex = selectedIndex - 1
                segmentControl.selectedSegmentIndex = selectedIndex
                setUpCollectionViewForTab()
            }
        }
        if (sender.direction == .left) {
            if selectedIndex < 3 {
                selectedIndex = selectedIndex + 1
                segmentControl.selectedSegmentIndex = selectedIndex
                if selectedIndex != 3 {
                    setUpCollectionViewForTab()
                }
            }
        }
    }
    
    @objc func handleSegmentSelection(sender: UISegmentedControl) {
        selectedIndex = sender.selectedSegmentIndex
        setUpCollectionViewForTab()
    }
    
    //MARK:- Custom Delegate Methods
    
    func handleDisplayCalender(_ selectedMonth: MonthDetails){
        let calenderVC = AttendanceVC()
        calenderVC.cmonth = selectedMonth.name
        navigationController?.pushViewController(calenderVC, animated: true)
    }
    //MARK:- Server Methods
    
    func getProfileData(){
        
        if Util.validateNetworkConnection(self){
            showActivityIndicatory(uiView: view)

          let  parameters = [
                "school_id": GlobleConstants.schoolID,
                "action": "StudentInfo",
                "intellinectid":UserDefaults.standard.string(forKey: "intellinectsId_Parent") as! String
            
            ] as [String : Any]
            
            print("profile paramaters: >>>>:",parameters)
            Alamofire.request(ParentsWebUrls.studentInfo, method: .post,parameters: parameters)
                .responseJSON { [weak self] (response) in
                    self?.stopActivityIndicator()
                    print("profile response >>>>",response.result)
                    
                    if response.result.error != nil {
                      Util.alert("Failed!", message: "Error Occured While Processing Data ",controller:self!)
                    }
                    if let jsonresponse = response.result.value {
                        let originalResponse = JSON(jsonresponse)
                        self?.getSuccessResponse(originalResponse: originalResponse)
                        DispatchQueue.main.async {
                            self?.rollNoLbl.text = "Roll No :  \(self?.student.rollNo ?? "")"
                            print("self?.student.rollNo\(self?.student.rollNo)")
                            self?.grNoLbl.text = "Gr No :  \(self?.student.grNumber ?? "")"
                            self?.setUpCollectionViewForTab()
                        }
                    }
            }
        }
    }
    
    func getSuccessResponse(originalResponse : JSON)
    {
        print("original resp>>>>",originalResponse)
        if(originalResponse.count > 0)
        {
            student.rollNo = originalResponse["roll_no"].stringValue
            UserDefaults.standard.set(originalResponse["roll_no"].stringValue, forKey: "\(SDefaultKeys.rollNumber)\(UserDefaults.standard.integer(forKey: SDefaultKeys.selectedStudent))")
            student.fatherName = originalResponse["father_name"].stringValue
            student.motherName = originalResponse["mother_name"].stringValue
            student.intellinectsID = originalResponse["intellinectId"].stringValue
            student.bloodGroup = originalResponse["bloodGroup"].stringValue
            student.address = originalResponse["address"].stringValue
            student.classId = originalResponse["classId"].stringValue
            student.allergy = originalResponse["allergy"].stringValue
            student.fatherMobile = originalResponse["father_mobile"].stringValue
            student.motherMobile = originalResponse["mother_mobile"].stringValue
            student.board = originalResponse["board"].stringValue
            student.birthDate = originalResponse["birthDate"].stringValue
            student.adharNumber = originalResponse["aadharCard"].stringValue
            student.udiseNo = originalResponse["udiseNo"].stringValue
            student.grNumber = originalResponse["gr_no"].stringValue
            student.imageUrl = originalResponse["photo_url"].stringValue
          //  guard let imgUrl = student.imageUrl?.removingPercentEncoding,
                //imgUrl != "",
                var imgUrl1 = UserDefaults.standard.string(forKey: "profile") as! String
            print("imgUrl1imgUrl1\(imgUrl1)")
            let url = URL(string: imgUrl1)
             //   else { return }
            UserDefaults.standard.set(imgUrl1, forKey: "\(SDefaultKeys.studentProfile)\(UserDefaults.standard.integer(forKey: SDefaultKeys.selectedStudent))")
            UserDefaults.standard.synchronize()
            let data = try? Data(contentsOf: url!)
            DispatchQueue.main.async {
                if data != nil {
                    self.profileImageView.image = UIImage(data: data!)
                }
            }
        }
    }
}


