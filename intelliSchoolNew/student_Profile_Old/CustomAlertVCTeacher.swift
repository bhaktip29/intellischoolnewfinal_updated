//
//  CustomAlertVC.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 10/06/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AMXFontAutoScale
import OpalImagePicker
import Photos
import MobileCoreServices
struct ButtonsName {
    static let cancel = "Cancel"
    static let send = "Send"
    static let save = "Save"
}

protocol JournalAddedDelegate {
    func handleJournal(_ status : Bool)
}

class CustomAlertVCTeacher: UIViewController,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource,OpalImagePickerControllerDelegate {
    
    var delegate: JournalAddedDelegate?
    var teacher = TeacherInfo()
    var studentInformation = StudentInformation()
        let headerCellId = "CellHeaderCell"
    let cellId = "TeacherInfoCell"
    var clickedButtonIndex: Int = 0
    var categories = ["--Scroll for Category--"]
    var categoryIds = ["0"]
    var attachmentList = [String]()
    var attachmentImgsData = [UIImage]()
    var selectedCategoryId: String?
    var selectedCategory: String?
    var studentOrStaff: Int = 0
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    var attachData = [Data]()
    var mimeTypes = [String]()
    var attachExtensions = [String]()
    let sendButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle(ButtonsName.send, for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(handleSend), for: .touchUpInside)
        return btn
    }()
    
    let msgLbl: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .left
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.textColor = UIColor.black
        lbl.text = "SMS will be sent via school_id."
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let cancelButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.layer.cornerRadius = 2
        btn.layer.masksToBounds = true
        btn.setTitle(ButtonsName.cancel, for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(handleClose), for: .touchUpInside)
        return btn
    }()
    
    let msgTextView : UITextView = {
        let tv = UITextView()
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.text = "Enter your message here...."
        tv.amx_autoScaleFont(forReferenceScreenSize: .size4Inch)
        return tv
    }()
    
    let separatorView: UIView = {
        let v = UIView()
        v.backgroundColor = CustomColor.greenColor
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    let popUpContainerView : UIView = {
        let vv = UIView()
        vv.backgroundColor = UIColor.white
        vv.layer.cornerRadius = 3
        vv.layer.masksToBounds = true
        vv.translatesAutoresizingMaskIntoConstraints = false
        return vv
    }()
    
    let pickerView: UIPickerView = {
        let picker = UIPickerView()
        picker.backgroundColor = TheamColors.baseColor
        picker.tintColor = UIColor.white
        picker.translatesAutoresizingMaskIntoConstraints = false
        return picker
    }()
    
    var yAnchor : NSLayoutConstraint?
    
    let tableView : UITableView = {
        let tblView = UITableView()
        tblView.translatesAutoresizingMaskIntoConstraints = false
        tblView.separatorInset = UIEdgeInsets.zero
        tblView.layer.borderColor = UIColor.gray.cgColor
        tblView.layer.borderWidth = 0.3
        tblView.layer.cornerRadius = 2
        return tblView
    }()
    
    let collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        let clView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        clView.backgroundColor = UIColor.white
        clView.translatesAutoresizingMaskIntoConstraints = false
        return clView
    }()
    
    let headerView:UILabel = {
        let headerView = UILabel()
        headerView.backgroundColor = CustomColor.greenColor
        headerView.text = "Personal Information"
        headerView.textColor = UIColor.white
        headerView.textAlignment = .center
        headerView.translatesAutoresizingMaskIntoConstraints = false
        return headerView
    }()
    
    //MARK:- View SetUp Methods

    
    func showActivityIndicatory(uiView: UIView) {
        
        let loadingView: UIView = UIView()
        loadingView.frame =  CGRect(x: 0, y: 0, width: 80.0, height: 80.0)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor(red: 44/255, green: 44/255, blue: 44/255, alpha: 0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        actInd.frame = CGRect(x: 0, y: 0, width: 40.0, height: 40.0)
        actInd.style =
            UIActivityIndicatorView.Style.whiteLarge
        actInd.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        loadingView.addSubview(actInd)
        uiView.addSubview(loadingView)
        actInd.startAnimating()
    }
    
    func stopActivityIndicator() {
        actInd.stopAnimating()
        let view = actInd.superview
        view?.removeFromSuperview()
    }
    
    private func setUpCollectionView() {
        
        popUpContainerView.addSubview(cancelButton)
        cancelButton.centerXAnchor.constraint(equalTo: popUpContainerView.centerXAnchor).isActive = true
        cancelButton.bottomAnchor.constraint(equalTo: popUpContainerView.bottomAnchor,constant: -4).isActive = true
        cancelButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        cancelButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        cancelButton.setTitle("OK", for: .normal)
        
        popUpContainerView.addSubview(collectionView)
        
        // needed constarunts x,y,w,h
        collectionView.topAnchor.constraint(equalTo: popUpContainerView.topAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: popUpContainerView.leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: popUpContainerView.rightAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: cancelButton.topAnchor,constant:-4).isActive = true
    }
    
    private func setUpHeaderView() {
        
        popUpContainerView.addSubview(headerView)
        // needed constraints x,y,w,h
        headerView.topAnchor.constraint(equalTo: popUpContainerView.topAnchor).isActive = true
        headerView.leftAnchor.constraint(equalTo: popUpContainerView.leftAnchor).isActive = true
        headerView.widthAnchor.constraint(equalTo: popUpContainerView.widthAnchor).isActive = true
        headerView.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    
    let overlayBtn : UIButton = {
        let btn = UIButton(type: .system)
        btn.tintColor = UIColor.clear
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(handleOverlay), for: .touchUpInside)
        return btn
    }()
    
    @objc func handleOverlay() {
        msgTextView.resignFirstResponder()
    }
    
    private func setUpOverlayBtn() {
        
        view.addSubview(overlayBtn)
        // needed constarunts x,y,w,h
        overlayBtn.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        overlayBtn.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        overlayBtn.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        overlayBtn.heightAnchor.constraint(equalTo:view.heightAnchor).isActive = true
    }
    
    private func setUpProfileViews() {
       
        setUpContainerViews()
        heightAnchor?.constant = 200
//        setUpHeaderView()
        collectionView.register(TeacherInfoCell.self, forCellWithReuseIdentifier: cellId)
        setUpCollectionView()
    }
    
    var heightAnchor :NSLayoutConstraint?
    
    private func setUpContainerViews() {
        
        view.addSubview(popUpContainerView)
        yAnchor = popUpContainerView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        yAnchor?.isActive = true
        popUpContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        popUpContainerView.widthAnchor.constraint(equalTo: view.widthAnchor,constant: -48).isActive = true
        heightAnchor = popUpContainerView.heightAnchor.constraint(equalTo: view.heightAnchor,multiplier: 1/3)
        heightAnchor?.isActive = true
    }
    
    private func setUpSendSMSViews() {
        
        setUpContainerViews()
        
        popUpContainerView.addSubview(sendButton)
        sendButton.rightAnchor.constraint(equalTo: popUpContainerView.rightAnchor,constant: -8).isActive = true
        sendButton.bottomAnchor.constraint(equalTo: popUpContainerView.bottomAnchor,constant: -8).isActive = true
        sendButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        sendButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        popUpContainerView.addSubview(cancelButton)
        cancelButton.rightAnchor.constraint(equalTo: sendButton.leftAnchor,constant: -16).isActive = true
        cancelButton.bottomAnchor.constraint(equalTo: popUpContainerView.bottomAnchor,constant: -8).isActive = true
        cancelButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        cancelButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        popUpContainerView.addSubview(msgLbl)
        msgLbl.leftAnchor.constraint(equalTo: popUpContainerView.leftAnchor,constant: 10).isActive = true
        msgLbl.bottomAnchor.constraint(equalTo: sendButton.topAnchor).isActive = true
        msgLbl.widthAnchor.constraint(equalTo:  popUpContainerView.widthAnchor).isActive = true
        msgLbl.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        popUpContainerView.addSubview(msgTextView)
        msgTextView.topAnchor.constraint(equalTo: popUpContainerView.topAnchor).isActive = true
        msgTextView.centerXAnchor.constraint(equalTo: popUpContainerView.centerXAnchor).isActive = true
        msgTextView.widthAnchor.constraint(equalTo: popUpContainerView.widthAnchor,constant: -16).isActive = true
        msgTextView.bottomAnchor.constraint(equalTo: msgLbl.topAnchor).isActive = true
        
        popUpContainerView.addSubview(separatorView)
        separatorView.topAnchor.constraint(equalTo: msgTextView.bottomAnchor).isActive = true
        separatorView.leftAnchor.constraint(equalTo: popUpContainerView.leftAnchor,constant: 3).isActive = true
        separatorView.widthAnchor.constraint(equalTo: popUpContainerView.widthAnchor, constant:-6).isActive = true
        separatorView.heightAnchor.constraint(equalToConstant:1).isActive = true
    }
    
    let btnContainerView = UIView()
    
    private func setUpJournalEntryViews() {
        
        setUpContainerViews()
        heightAnchor?.constant = 100
        popUpContainerView.addSubview(pickerView)
        pickerView.topAnchor.constraint(equalTo: popUpContainerView.topAnchor).isActive = true
        pickerView.leftAnchor.constraint(equalTo: popUpContainerView.leftAnchor).isActive = true
        pickerView.widthAnchor.constraint(equalTo:popUpContainerView.widthAnchor).isActive = true
        pickerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        btnContainerView.translatesAutoresizingMaskIntoConstraints = false
        btnContainerView.backgroundColor = CustomColor.greenColor
        popUpContainerView.addSubview(btnContainerView)
        
        btnContainerView.bottomAnchor.constraint(equalTo: popUpContainerView.bottomAnchor).isActive = true
        btnContainerView.centerXAnchor.constraint(equalTo: popUpContainerView.centerXAnchor).isActive = true
        btnContainerView.widthAnchor.constraint(equalTo: popUpContainerView.widthAnchor).isActive = true
        btnContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        btnContainerView.addSubview(cancelButton)
        cancelButton.setTitleColor(UIColor.white, for: .normal)
        cancelButton.backgroundColor = UIColor(red:0.32, green:0.49, blue:0.35, alpha:1.0)
        cancelButton.centerXAnchor.constraint(equalTo: btnContainerView.centerXAnchor).isActive = true
        cancelButton.centerYAnchor.constraint(equalTo: btnContainerView.centerYAnchor).isActive = true
        cancelButton.widthAnchor.constraint(equalTo: btnContainerView.widthAnchor, multiplier: 1/3).isActive = true
        cancelButton.heightAnchor.constraint(equalTo: btnContainerView.heightAnchor,constant:-12).isActive = true
        
        let saveBtn = UIButton()
        saveBtn.translatesAutoresizingMaskIntoConstraints = false
        saveBtn.layer.cornerRadius = 2
        saveBtn.layer.masksToBounds = true
        saveBtn.backgroundColor = UIColor(red:0.32, green:0.49, blue:0.35, alpha:1.0)
        saveBtn.setTitle(ButtonsName.save, for: .normal)
        saveBtn.addTarget(self, action: #selector(handleSave), for: .touchUpInside)
        btnContainerView.addSubview(saveBtn)
        
        saveBtn.setTitleColor(UIColor.white, for: .normal)
        saveBtn.leftAnchor.constraint(equalTo: cancelButton.rightAnchor,constant: 4).isActive = true
        saveBtn.centerYAnchor.constraint(equalTo: btnContainerView.centerYAnchor).isActive = true
        saveBtn.widthAnchor.constraint(equalTo: btnContainerView.widthAnchor, multiplier: 1/3,constant:-8).isActive = true
        saveBtn.heightAnchor.constraint(equalTo: btnContainerView.heightAnchor,constant:-12).isActive = true
        
        let attchmentBtn = UIButton()
        attchmentBtn.translatesAutoresizingMaskIntoConstraints = false
        attchmentBtn.layer.cornerRadius = 2
        attchmentBtn.layer.masksToBounds = true
        attchmentBtn.backgroundColor = UIColor(red:0.32, green:0.49, blue:0.35, alpha:1.0)
        attchmentBtn.setImage(UIImage(named:"ic_file_attachment_white"), for: .normal)
        attchmentBtn.addTarget(self, action: #selector(chooseAttachement), for: .touchUpInside)
        btnContainerView.addSubview(attchmentBtn)
        
        attchmentBtn.setTitleColor(UIColor.white, for: .normal)
        attchmentBtn.leftAnchor.constraint(equalTo: btnContainerView.leftAnchor,constant: 4).isActive = true
        attchmentBtn.centerYAnchor.constraint(equalTo: btnContainerView.centerYAnchor).isActive = true
        attchmentBtn.widthAnchor.constraint(equalTo: btnContainerView.widthAnchor, multiplier: 1/3,constant:-8).isActive = true
        attchmentBtn.heightAnchor.constraint(equalTo: btnContainerView.heightAnchor,constant:-12).isActive = true
        
        setUpTableView()
        tableView.isHidden = true
        popUpContainerView.addSubview(msgTextView)
        msgTextView.topAnchor.constraint(equalTo: pickerView.bottomAnchor).isActive = true
        msgTextView.centerXAnchor.constraint(equalTo: popUpContainerView.centerXAnchor).isActive = true
        msgTextView.widthAnchor.constraint(equalTo: popUpContainerView.widthAnchor).isActive = true
        msgTextView.bottomAnchor.constraint(equalTo: tableView.topAnchor,constant:-3).isActive = true
    }
    
    private func setUpTableView() {
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 40
        tableView.backgroundColor = UIColor.white
        popUpContainerView.addSubview(tableView)
        // needed constyarints
        tableView.centerXAnchor.constraint(equalTo: popUpContainerView.centerXAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: btnContainerView.topAnchor,constant:-1).isActive = true
        tableView.widthAnchor.constraint(equalTo: popUpContainerView.widthAnchor,constant:-2).isActive = true
        tableView.heightAnchor.constraint(equalToConstant:40).isActive = true
    }
    
    //MARK:- Keyborad Show/Hide Methods
    
    func setupKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func handleKeyboardWillShow(_ notification: Notification) {
        
        let keyboardDuration = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue
        yAnchor?.constant = -100
        UIView.animate(withDuration: keyboardDuration!, animations: {
            self.view.layoutIfNeeded()
        })
        if self.temp != "" {
            self.msgTextView.text = self.temp
        }
    }
    
    var temp = ""
    
    @objc func handleKeyboardWillHide(_ notification: Notification) {
        let keyboardDuration = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue
        temp = msgTextView.text
        yAnchor?.constant = 0
        UIView.animate(withDuration: keyboardDuration!, animations: {
            self.view.layoutIfNeeded()
            if self.msgTextView.text == "" {
                self.msgTextView.text = PlaceHolderStrings.tvPlaceHolder
            }
        })
    }
    
    //MARK:- View Controller Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpOverlayBtn()
        pickerView.dataSource = self
        pickerView.delegate = self
        tableView.register(DownloadeAbleCell.self, forCellReuseIdentifier: cellId)
        switch clickedButtonIndex {
        case 1:
            setUpSendSMSViews()
            break
        case 2:
            setUpJournalEntryViews()
            getCategories()
            break
        case 3:
            setUpProfileViews()
            break
        default: break
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupKeyboardObservers()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK:- Action Methods
    
    @objc func handleSend() {}
    
    @objc func chooseAttachement(){
        if self.attachmentList.count < 1{
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Photos", style: .default, handler: { (alert) in
                self.handleAttachement()
            }))
            alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (alert) in
                self.pickDocument()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: nil, message: "You can upload maximum 1 attachment.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func pickDocument(){
        let documentPicker = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        documentPicker.delegate = self
        self.present(documentPicker, animated: true, completion: nil)
    }
    func handleAttachement() {
        let imagePicker = OpalImagePickerController()
        //Change color of selection overlay to white
        imagePicker.selectionTintColor = UIColor.white.withAlphaComponent(0.7)
        //Change color of image tint to black
        imagePicker.selectionImageTintColor = UIColor.black
        //Limit maximum allowed selections to 1
        imagePicker.maximumSelectionsAllowed = 1
        //Only allow image media type assets
        imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
        imagePicker.imagePickerDelegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func handleSave() {}
    
    @objc func handleClose() {
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return categories.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return categories[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        selectedCategory = categories[row]
        selectedCategoryId = categoryIds[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        
        let strTitle = categories[row]
        let attString = NSAttributedString(string: strTitle, attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        return attString
    }
    
    
    //MARK:- TableView Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return attachmentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! DownloadeAbleCell
        cell.cImageView.image = UIImage(named: "iconDelete")
        cell.cImageView.tag = indexPath.row
        cell.cImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDeleteAttachment(_:))))
        cell.cImageView.isUserInteractionEnabled = true
        cell.nameLabel.text = attachmentList[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //MARK:- ImagePickerView Methods
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        for i in 0..<images.count{
            self.attachData.append(images[i].jpegData(compressionQuality: 0.5)!)
            self.mimeTypes.append("image/jpeg")
            self.attachExtensions.append("jpeg")
        }
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingAssets assets: [PHAsset]) {
        for asset in assets {
            if let name = asset.value(forKey: "filename") {
                attachmentList.append(name as! String)
            }
        }
        tableView.isHidden = attachmentList.count == 0 ? true : false
        tableView.reloadData()
        picker.dismiss(animated: true, completion: nil)
    }

    @objc func handleDeleteAttachment(_ sender:UIGestureRecognizer) {
        guard let tag = sender.view?.tag else {
            return
        }
        self.attachmentList.remove(at: tag)
        self.attachData.remove(at: tag)
        self.attachExtensions.remove(at: tag)
        self.mimeTypes.remove(at: tag)
        tableView.isHidden = attachmentList.count == 0 ? true : false
        tableView.reloadData()
    }
    
    //MARK:- Server Methods
    
    func getCategories(){
        
        if UtilTeacher.validateNetworkConnection(self) {
            showActivityIndicatory(uiView: view)
            let userDefaults = UserDefaults.standard
            let parameters = [
                "school_db_settings_array": userDefaults.object(forKey: "SCHOOL_DB_SETTINGS") ?? "school_db_settings_array",
                "action": "JournalCategoryList"
                ] as [String : Any]
            
            let url = studentOrStaff == 1 ? WebServiceUrls.studentInfoService : WebServiceUrls.teacherProfileService
            print(studentOrStaff, url)
            Alamofire.request(url,method: .post,parameters: parameters).responseJSON { [weak self] (response) in
                self?.stopActivityIndicator()
                if response.result.error != nil {
                    UtilTeacher.invokeAlertMethod("Failed!", strBody: "Error Occured While Processing Data ", delegate: nil,vcobj : self!)
                }
                if let jsonresponse = response.result.value {
                    
                    let originalResponse = JSON(jsonresponse)
                    self?.getSuccessResponse(originalResponse: originalResponse)
                }
            }
        }
    }
    
    func getSuccessResponse(originalResponse : JSON)
    {
        if(originalResponse.count > 0)
        {
            for (_,subJson):(String, JSON) in originalResponse {
                categoryIds.append(subJson["id"].stringValue)
                categories.append(subJson["category"].stringValue)
            }
            pickerView.reloadAllComponents()
        }
    }
}

class TeacherInfoCell: UICollectionViewCell {
    
    let headerLabel: UILabel = {
        let statusLbl = UILabel()
        statusLbl.textAlignment = .left
        statusLbl.font = UIFont(name: "Helvetica Neue", size: 13)
        statusLbl.translatesAutoresizingMaskIntoConstraints = false
        statusLbl.numberOfLines = 0
        statusLbl.sizeToFit()
        return statusLbl
    }()
    
    let valueLabel: UILabel = {
        let nameLbl = UILabel()
        nameLbl.font = UIFont(name: "Helvetica Neue", size: 12)
        nameLbl.textAlignment = .justified
        nameLbl.translatesAutoresizingMaskIntoConstraints = false
        nameLbl.numberOfLines = 0
        nameLbl.sizeToFit()
        return nameLbl
    }()
    
    func setViews() {
        
        self.addSubview(headerLabel)
        // needed constraints x,y,w,h
        headerLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        headerLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        headerLabel.widthAnchor.constraint(equalTo:self.widthAnchor,multiplier:1/3,constant:8).isActive = true
        headerLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        let separatorView2 = UIView()
        separatorView2.translatesAutoresizingMaskIntoConstraints = false
        separatorView2.backgroundColor = UIColor.gray.withAlphaComponent(0.3)
        
        self.addSubview(separatorView2)
        // needed constraints x,y,w,h
        separatorView2.leftAnchor.constraint(equalTo: headerLabel.rightAnchor).isActive = true
        separatorView2.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        separatorView2.widthAnchor.constraint(equalToConstant: 1).isActive = true
        separatorView2.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        self.addSubview(valueLabel)
        // needed constraints x,y,w,h
        valueLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant:-8).isActive = true
        valueLabel.leftAnchor.constraint(equalTo: headerLabel.rightAnchor,constant: 8).isActive = true
        valueLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        valueLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setViews()
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 0.5).cgColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class SHeaderCellTeacher: UICollectionReusableView {
    
    let headerLabel: UILabel = {
        let statusLbl = UILabel()
        statusLbl.textAlignment = .center
        statusLbl.font = UIFont(name: "Helvetica Neue", size: 13)
        statusLbl.translatesAutoresizingMaskIntoConstraints = false
        statusLbl.numberOfLines = 0
        statusLbl.sizeToFit()
        return statusLbl
    }()

    func setViews() {
        
        self.addSubview(headerLabel)
        // needed constraints x,y,w,h
        headerLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        headerLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        headerLabel.widthAnchor.constraint(equalTo:self.widthAnchor,constant:-16).isActive = true
        headerLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setViews()
        self.backgroundColor = UIColor.cyan.withAlphaComponent(0.2)
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 0.5).cgColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension CustomAlertVCTeacher : UIDocumentPickerDelegate{
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let docUrl = url as URL
        let docData  = try! Data(contentsOf: docUrl)
        self.attachData.append(docData)
        self.attachmentList.append(url.lastPathComponent)
        tableView.isHidden = attachmentList.count == 0 ? true : false
        tableView.reloadData()
        self.attachExtensions.append("pdf")
        self.mimeTypes.append("application/pdf")
    }
}


