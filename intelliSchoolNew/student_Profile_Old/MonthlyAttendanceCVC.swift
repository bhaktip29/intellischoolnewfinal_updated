//
//  MonthlyAttendanceVC.swift
//  SchoolProtoTypeApp
//
//  Created by Intellinects on 18/09/17.
//  Copyright © 2017 Intellinects Ventures. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol CalenderDelegate {
    func handleDisplayCalender(_ selectedMonth: MonthDetails)
}

class MonthlyAttendanceCVC: UICollectionView,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    var board = ""
    var classId = ""
    var selectedDate = Date()
    var cDelegate: CalenderDelegate?
    var attendanceList = [MonthDetails]()
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    let cellId = "Cell"
    let headerCellId  = "HeaderCell"
    var noDataLabel = ""
    var schoolClass = SchoolClassTeacher()

    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        self.register(AttendanceCVCell.self, forCellWithReuseIdentifier: cellId)
        delegate = self
        dataSource = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- CollectionView Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.attendanceList.count == 0{
            self.setEmptyMessage(self.noDataLabel)
        }else{
            self.restore()
        }
        return attendanceList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! AttendanceCVCell
        
        let mList = attendanceList[indexPath.row]
        cell.Lbl.text = mList.name?.capitalized
        cell.Lbl1.text = mList.workingDay
        cell.Lbl2.text = mList.presentDay
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.frame.width, height: 45)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        cDelegate?.handleDisplayCalender(attendanceList[indexPath.row])
    }
    //MARK:- Server methods
    
    func getAttendanceByDate(_ date: Date)
    {
        if Util.validateNetworkConnection(self){
 
            let parameters = [
                "school_id": GlobleConstants.schoolID,
                "action": "MonthWiseAttendanceCount",
                "intellinectid": UserDefaults.standard.string(forKey: "intellinectsId_Parent") as! String,
                "board": "3",
                "class_id": "5"
                ] as [String : Any]
            showActivityIndicatory(actInd)
            Alamofire.request(ParentsWebUrls.studentInfo,method:.post,parameters: parameters)
                .responseJSON{ [weak self] (response)  in
//                    print(response.result.value)
                    print("MonthlyAttendanceCVC parameters\(parameters)")
                    print("MonthlyAttendanceCVC response.result\(response.result.value)")
                    self?.stopActivityIndicator(self?.actInd)
                    if let jsonresponse = response.result.value {
                        let originalResponse = JSON(jsonresponse)
                        self?.getSuccessResponse(originalResponse: originalResponse)
                    }
                    else{
                        self?.noDataLabel = "Attendance summary not found!"
                        DispatchQueue.main.async {
                            self?.reloadData()
                        }
                    }
            }
        }
    }
    
    func getSuccessResponse(originalResponse : JSON)
    {
        if(originalResponse.count > 0)
        {
            for item in originalResponse.arrayValue {
                var mDetail = MonthDetails()
                mDetail.name = item["month"].stringValue
                mDetail.absentDay = item["absentday"].stringValue
                mDetail.workingDay = item["workingDays"].stringValue
                mDetail.presentDay = item["presentDays"].stringValue
                attendanceList.append(mDetail)
            }
            reloadData()
        }else{
            self.noDataLabel = "Attendance summary not found!"
            DispatchQueue.main.async {
                self.reloadData()
            }
        }
    }
}

struct MonthDetails {
    var id: String?
    var name: String?
    var absentDay: String?
    var workingDay: String?
    var presentDay: String?
}

class AttendanceCVCell :UICollectionViewCell {
    
    var Lbl = UILabel()
    var Lbl1 = UILabel()
    var Lbl2 = UILabel()
    
    private func setUpLbels() {
        
        var lbls = [UILabel]()
        
        func getLabel() -> UILabel {
            let lbl = UILabel()
            lbl.textColor = UIColor.black
            lbl.font = UIFont(name: "Helvetica Neue", size: 13)
            lbl.textAlignment = .center
            lbl.numberOfLines = 0
            lbl.layer.borderColor = UIColor.lightGray.cgColor
            lbl.layer.borderWidth = 0.3
            lbl.sizeToFit()
            return lbl
        }
        
        Lbl = getLabel()
        lbls.append(Lbl)
        Lbl1 = getLabel()
        lbls.append(Lbl1)
        Lbl2 = getLabel()
        lbls.append(Lbl2)
        
        let stackView = UIStackView(arrangedSubviews: lbls)
        stackView.alignment = .fill
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(stackView)
        // needed constarints
        stackView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        stackView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        stackView.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        setUpLbels()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
