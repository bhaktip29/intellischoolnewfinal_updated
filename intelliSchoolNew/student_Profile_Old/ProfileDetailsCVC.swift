//
//  ProfileDetailsCVC.swift
//  SchoolProtoTypeApp
//
//  Created by Intellinects on 21/09/17.
//  Copyright © 2017 Intellinects Ventures. All rights reserved.
//

import UIKit

class ProfileDetailsCVC: UICollectionView,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    let headers = ["Personal Information","Parent's Information","Address Details","Student Identifires"]
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    let cellId = "Cell"
    let headerCellId  = "HeaderCell"
    var studentInfo = StudentInfo()
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        self.register(InfoCell.self, forCellWithReuseIdentifier: cellId)
        self.register(SHeaderCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader , withReuseIdentifier: headerCellId)
        delegate = self
        dataSource = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- CollectionView Methods

    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return headers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if section == 0 {
            return 3
        } else if section == 1 {
            return 4
        } else if section == 2 {
            return 1
        } else {
            return 3
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! InfoCell
        
        switch indexPath.section+1 {
        case 1:
            switch indexPath.row+1 {
            case 1:
                cell.headerLabel.text = "Blood Group"
                
                if studentInfo.bloodGroup != "" && studentInfo.bloodGroup != "null" && studentInfo.bloodGroup != "NULL" {
                    cell.valueLabel.text = studentInfo.bloodGroup
                } else {
                    cell.valueLabel.text = "N/A"
                }
                break
            case 2:
                cell.headerLabel.text = "Date of Birth"
                
                if studentInfo.birthDate != "" && studentInfo.birthDate != "null" && studentInfo.birthDate != "NULL" {
                    cell.valueLabel.text = studentInfo.birthDate
                } else {
                    cell.valueLabel.text = "N/A"
                }
                break
            case 3:
                cell.headerLabel.text = "Allergy"
                cell.valueLabel.text = "N/A"
                break
                
            default:
                break
            }
            
        case 2:
            switch indexPath.row+1 {
                
            case 1:
                cell.headerLabel.text = "Father Name"
                if studentInfo.fatherName?.trimmingCharacters(in: .whitespaces) != "" && studentInfo.fatherName != "null" && studentInfo.fatherName != "NULL" {
                    cell.valueLabel.text = studentInfo.fatherName
                } else {
                    cell.valueLabel.text = "Not updateed"
                }
                break
            case 2:
                cell.headerLabel.text = "Mother Name"
                
                if studentInfo.motherName?.trimmingCharacters(in: .whitespaces) != "" && studentInfo.motherName != "null" && studentInfo.motherName != "NULL" {
                    cell.valueLabel.text = studentInfo.motherName
                } else {
                    cell.valueLabel.text = "Not updateed"
                }
                break
                
            case 3:
                cell.headerLabel.text = "Father Mobile"
                if studentInfo.fatherMobile?.trimmingCharacters(in: .whitespaces) != "" && studentInfo.fatherMobile != "null" && studentInfo.fatherMobile != "NULL" {
                    cell.valueLabel.text = studentInfo.fatherMobile
                } else {
                    cell.valueLabel.text = "Not updateed"
                }
                break
            case 4:
                cell.headerLabel.text = "Mother Mobile"
                
                if studentInfo.motherMobile?.trimmingCharacters(in: .whitespaces) != "" && studentInfo.motherMobile != "null" && studentInfo.motherMobile != "NULL" {
                    cell.valueLabel.text = studentInfo.motherMobile
                } else {
                    cell.valueLabel.text = "Not updateed"
                }
                break
            default:
                break
            }
        case 3:
            switch indexPath.row+1 {
                
            case 1:
                cell.headerLabel.text = "Address"
                
                if studentInfo.address != "" && studentInfo.address != "null" && studentInfo.address != "NULL" {
                    cell.valueLabel.text = studentInfo.address
                } else {
                    cell.valueLabel.text = "N/A"
                }
                break
                
            default:
                break
            }
            
        case 4:
            switch indexPath.row+1 {
                
            case 1:
                cell.headerLabel.text = "U-DISE No "
                
                if studentInfo.udiseNo != "" && studentInfo.udiseNo != "null" && studentInfo.udiseNo != "NULL" {
                    cell.valueLabel.text = studentInfo.udiseNo
                } else {
                    cell.valueLabel.text = "N/A"
                }
                break
                
            case 2:
                cell.headerLabel.text = "Intellinects ID "
                
                if studentInfo.intellinectsID != "" && studentInfo.intellinectsID != "null" && studentInfo.intellinectsID != "NULL" {
                    cell.valueLabel.text = studentInfo.intellinectsID
                } else {
                    cell.valueLabel.text = "N/A"
                }
                break
            case 3:
                cell.headerLabel.text = "Adhar Card No"
                if studentInfo.adharNumber != "" && studentInfo.adharNumber != "null" && studentInfo.adharNumber != "NULL" {
                    cell.valueLabel.text = studentInfo.adharNumber
                } else {
                    cell.valueLabel.text = "N/A"
                }
                break

            default:
                break
            }
        default:
            break
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section+1 == 3 {
             return CGSize(width: self.frame.width, height: 50)
        }
        return CGSize(width: self.frame.width, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerCellId, for: indexPath as IndexPath) as! SHeaderCell
        headerView.headerLabel.text = headers[indexPath.section]
        
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
    {
        return CGSize(width: collectionView.bounds.width, height: 30)
    }
}


class InfoCell: UICollectionViewCell {
    
    let headerLabel: UILabel = {
        let statusLbl = UILabel()
        statusLbl.textAlignment = .center
        statusLbl.font = UIFont(name: "Helvetica Neue", size: 13)
        statusLbl.translatesAutoresizingMaskIntoConstraints = false
        statusLbl.numberOfLines = 0
        statusLbl.sizeToFit()
        return statusLbl
    }()
    
    let valueLabel: UILabel = {
        let nameLbl = UILabel()
        nameLbl.font = UIFont(name: "Helvetica Neue", size: 12)
        nameLbl.textAlignment = .center
        nameLbl.translatesAutoresizingMaskIntoConstraints = false
        nameLbl.numberOfLines = 0
        nameLbl.sizeToFit()
        return nameLbl
    }()
    
    func setViews() {
        
        self.addSubview(headerLabel)
        // needed constraints x,y,w,h
        headerLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        headerLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        headerLabel.widthAnchor.constraint(equalTo:self.widthAnchor,multiplier:1/2).isActive = true
        headerLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        let separatorView2 = UIView()
        separatorView2.translatesAutoresizingMaskIntoConstraints = false
        separatorView2.backgroundColor = UIColor.gray.withAlphaComponent(0.3)
        
        self.addSubview(separatorView2)
        // needed constraints x,y,w,h
        separatorView2.leftAnchor.constraint(equalTo: headerLabel.rightAnchor).isActive = true
        separatorView2.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        separatorView2.widthAnchor.constraint(equalToConstant: 1).isActive = true
        separatorView2.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        self.addSubview(valueLabel)
        // needed constraints x,y,w,h
        valueLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant:-8).isActive = true
        valueLabel.leftAnchor.constraint(equalTo: headerLabel.rightAnchor,constant:2).isActive = true
        valueLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        valueLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setViews()
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 0.5).cgColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class SHeaderCell: UICollectionReusableView {
    
    let headerLabel: UILabel = {
        let statusLbl = UILabel()
        statusLbl.textAlignment = .center
        statusLbl.font = UIFont(name: "Helvetica Neue", size: 13)
        statusLbl.translatesAutoresizingMaskIntoConstraints = false
        statusLbl.numberOfLines = 0
        statusLbl.sizeToFit()
        return statusLbl
    }()
    
    func setViews() {
        
        self.addSubview(headerLabel)
        // needed constraints x,y,w,h
        headerLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        headerLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        headerLabel.widthAnchor.constraint(equalTo:self.widthAnchor,constant:-16).isActive = true
        headerLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setViews()
        self.backgroundColor = UIColor.cyan.withAlphaComponent(0.2)
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 0.5).cgColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

struct StudentInfo {
    
    var rollNo: String?
    var fatherName: String?
    var motherName: String?
    var intellinectsID: String?
    var bloodGroup: String?
    var imageUrl: String?
    var address: String?
    var classId: String?
    var allergy: String?
    var fatherMobile: String?
    var motherMobile: String?
    var board: String?
    var birthDate: String?
    var adharNumber: String?
    var udiseNo: String?
    var grNumber: String?
}
