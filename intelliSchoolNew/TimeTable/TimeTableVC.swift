////
////  TimeTableVC.swift
////  Intellinects
////
////  Created by rupali  on 07/10/19.
////  Copyright © 2019 rupali . All rights reserved.
////
//
//import UIKit
//import Alamofire
//import SwiftyJSON
//
//
//class TimeTableVC: BaseViewController ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
//        var url = "http://chankya-admin.theironnetwork.org/user-pages/user-profile"
//
//        let cellId = "TimeCell"
//        let headerCellId  = "HeaderCell"
//        var classTimeTable = [ClassWeekDaysTimeTable]()
//        var schoolClass = SchoolClass()
//        var selectedIndex = Int()
//
//        let collectionView : UICollectionView = {
//            let layout = UICollectionViewFlowLayout()
//            layout.minimumLineSpacing = 0
//            layout.minimumInteritemSpacing = 0
//            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
//            let clView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
//            clView.backgroundColor = UIColor.groupTableViewBackground
//            clView.translatesAutoresizingMaskIntoConstraints = false
//            return clView
//        }()
//
//        let errorLbl: UILabel = {
//            let lbl = UILabel()
//            lbl.translatesAutoresizingMaskIntoConstraints = false
//            lbl.textAlignment = .center
//            lbl.text = "Ooops....TimeTable details not availables!!"
//            lbl.numberOfLines = 0
//            return lbl
//        }()
//
//        // MARK:- Custom Views Setup Methods
//
//        private func setUpCollectionView() {
//
//            view.addSubview(collectionView)
//            collectionView.dataSource = self
//            collectionView.delegate = self
//
//            // needed constarunts x,y,w,h
//            collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
//            collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
//            collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
//            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
//        }
//
//        private func setUpErrorLabel() {
//
//            view.addSubview(errorLbl)
//            //needed constraints
//            errorLbl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
//            errorLbl.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
//            errorLbl.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 8.0).isActive = true
//            errorLbl.rightAnchor.constraint(equalTo: view.rightAnchor,constant:-8.0).isActive = true
//            errorLbl.heightAnchor.constraint(equalToConstant: 50).isActive = true
//        }
//
//        //MARK:- View Controllers Methods
//
//        override func viewDidLoad() {
//            super.viewDidLoad()
//            //self.ViewControllerTitles.timetableVC
//            view.backgroundColor = UIColor.white
//            collectionView.isHidden = true
//            errorLbl.isHidden = true
//            collectionView.register(ClassTimeTableCVCell.self, forCellWithReuseIdentifier: cellId)
//            collectionView.register(ClassTimeTableHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader , withReuseIdentifier: headerCellId)
//            if let className = schoolClass.className, let division = schoolClass.divisionName {
//                self.title = "Class \(className) \(division)"
//
//            }
//            setUpCollectionView()
//            getTimeTableDetails()
//            setUpErrorLabel()
//        }
//
//        //MARK:- CollectionView Methods
//
//        func numberOfSections(in collectionView: UICollectionView) -> Int {
//            return 2
//        }
//
//        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//            return classTimeTable.count
//        }
//
//        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
//        {
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ClassTimeTableCVCell
//
//            let weekTime = classTimeTable[indexPath.row] as ClassWeekDaysTimeTable
//            cell.timeLbl.text = weekTime.time
//            if indexPath.section == 0 {
//                cell.timeLbl1.text = weekTime.monday == "BREAK" ? "~Break~" : weekTime.monday
//                cell.timeLbl2.text = weekTime.tuesday == "BREAK" ? "~Break~" : weekTime.tuesday
//                cell.timeLbl3.text = weekTime.wednesday == "BREAK" ? "~Break~" : weekTime.wednesday
//            }else {
//                cell.timeLbl1.text = weekTime.thursday == "BREAK" ? "~Break~" : weekTime.thursday
//                cell.timeLbl2.text = weekTime.friday == "BREAK" ? "~Break~" : weekTime.friday
//                cell.timeLbl3.text = weekTime.saturday == "BREAK" ? "~Break~" : weekTime.saturday
//            }
//            return cell
//        }
//
//        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//            return CGSize(width: view.frame.width, height: 35)
//        }
//
//        func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//
//            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerCellId, for: indexPath as IndexPath) as! ClassTimeTableHeader
//            headerView.headerLbl.text = "Time"
//
//            if indexPath.section == 0 {
//                headerView.headerLbl1.text = "Monday"
//                headerView.headerLbl2.text = "Tuesday"
//                headerView.headerLbl3.text = "Wednesday"
//            }else {
//                headerView.headerLbl1.text = "Thursday"
//                headerView.headerLbl2.text = "Friday"
//                headerView.headerLbl3.text = "Saturday"
//            }
//            return headerView
//        }
//
//        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
//        {
//            return CGSize(width: collectionView.bounds.width, height: 30)
//        }
//
//        //MARK: -  Custom Methods
//
//        private func getTimeTableDetails() {
//
//            if Util.validateNetworkConnection(self) {
//
//                let parameters = [
//                    "action":"getTimeTable",
//                    "SCHOOL_ID":GlobleConstants.schoolID,
//                    "deviceId":"".getUDIDCode(),
//                    "userId": UserDefaults.standard.string(forKey: "\(SDefaultKeys.studentId)\(UserDefaults.standard.integer(forKey: SDefaultKeys.selectedStudent))")?.capitalized ?? "",
//                    "role": UserDefaults.standard.string(forKey: SDefaultKeys.role) ?? ""
//                ] as [String : Any]
//
////                let parameters = [
////                                  "action":"getTimeTable",
////                                  "SCHOOL_ID":"15",
////                              "deviceId":"B891F86D-783C-4B0C-8067-A86BBD3E2D79",
////                                  "userId":"181",
////                                  "role":"Father"
////                              ] as [String : Any]
//
//                print("timetable params:>>>:",parameters)
//
//                showActivityIndicatory(uiView: view)
//                Alamofire.request(ParentsWebUrls.baseUrl,method:.get,parameters: parameters)
//                    .responseJSON { [weak self](response) in
//                        self?.stopActivityIndicator()
//                        print("timetable >>>> ",response.result.value as Any)
//                        if response.result.value == nil{
//                            self?.errorLbl.isHidden = false
//                        }else{
//                            guard response.result.error == nil else {
//                                return
//                            }
//                            // make sure we got JSON and it's a dictionary
//                            guard let json1 = response.result.value as? [Any] else {
//                                print("didn't get todo object as JSON from API")
//                                return
//                            }
//                            if json1.count > 0 {
//                                if let index = UserDefaults.standard.value(forKey: SDefaultKeys.selectedStudent){
//                                    self?.selectedIndex = index as! Int
//                                }else{
//                                    self?.selectedIndex = 0
//                                }
//                                guard let  jsondic = json1[(self?.selectedIndex)!] as? [String: Any] else {
//                                    return
//                                }
//                                guard let ttdetails = jsondic["tt_details"] as? String else {
//                                    return
//                                }
//                                let ttdet = JSON(ttdetails).rawString()
//                                let json: AnyObject? = ttdet?.parseJSONString
//                                
//
//                                guard let tt   =  json  as? [[String:AnyObject]] else {
//                                    return
//                                }
//                                for details in tt  {
//                                    let weekTimeTable = ClassWeekDaysTimeTable()
//                                    weekTimeTable.time = details["time"] as? String
//                                    weekTimeTable.totime = details["totime"] as? String
//                                    weekTimeTable.period = details["period"] as? String
//                                    weekTimeTable.monday = details["mon"] as? String
//                                    weekTimeTable.tuesday = details["tue"] as? String
//                                    weekTimeTable.wednesday = details["wed"] as? String
//                                    weekTimeTable.thursday = details["thur"] as? String
//                                    weekTimeTable.friday = details["fri"] as? String
//                                    weekTimeTable.saturday = details["sat"] as? String
//                                    weekTimeTable.sunday = details["sun"] as? String
//                                    self?.classTimeTable.append(weekTimeTable)
//                                }
//                                self?.collectionView.reloadData()
//                                self?.collectionView.isHidden = false
//                            }
//                            else{
//                                self?.errorLbl.isHidden = false
//                            }
//                        }
//                }
//            }
//        }
//    }
//
//    class ClassTimeTableCVCell :UICollectionViewCell {
//
//        var timeLbl = UILabel()
//        var timeLbl1 = UILabel()
//        var timeLbl2 = UILabel()
//        var timeLbl3 = UILabel()
//
//        private func setUpLbels() {
//
//            var lbls = [UILabel]()
//
//            func getLabel() -> UILabel {
//                let lbl = UILabel()
//                lbl.textColor = UIColor.black
//                lbl.font = UIFont(name: "Helvetica Neue", size: 12)
//                lbl.textAlignment = .center
//                lbl.numberOfLines = 0
//                lbl.layer.borderColor = UIColor.lightGray.cgColor
//                lbl.layer.borderWidth = 0.3
//                lbl.sizeToFit()
//                return lbl
//            }
//
//            timeLbl = getLabel()
//            lbls.append(timeLbl)
//            timeLbl1 = getLabel()
//            lbls.append(timeLbl1)
//            timeLbl2 = getLabel()
//            lbls.append(timeLbl2)
//            timeLbl3 = getLabel()
//            lbls.append(timeLbl3)
//
//
//            let stackView = UIStackView(arrangedSubviews: lbls)
//            stackView.alignment = .fill
//            stackView.axis = .horizontal
//            stackView.distribution = .fillEqually
//            stackView.translatesAutoresizingMaskIntoConstraints = false
//
//            self.addSubview(stackView)
//            // needed constarints
//            stackView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
//            stackView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
//            stackView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
//            stackView.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
//        }
//
//        override init(frame: CGRect) {
//            super.init(frame: frame)
//            self.backgroundColor = UIColor.white
//            setUpLbels()
//        }
//
//        required init?(coder aDecoder: NSCoder) {
//            fatalError("init(coder:) has not been implemented")
//        }
//    }
//
//    class ClassTimeTableHeader: UICollectionReusableView {
//
//        var headerLbl = UILabel()
//        var headerLbl1 = UILabel()
//        var headerLbl2 = UILabel()
//        var headerLbl3 = UILabel()
//
//        private func setUpLbels() {
//
//            var lbls = [UILabel]()
//
//            func getLabel() -> UILabel {
//                let lbl = UILabel()
//                lbl.textColor = UIColor.black
//                lbl.font = UIFont.boldSystemFont(ofSize: 13) //UIFont(name: "Helvetica Neue", size: 13)
//                lbl.textAlignment = .center
//                lbl.layer.borderColor = UIColor.darkGray.cgColor
//                lbl.layer.borderWidth = 0.3
//                return lbl
//            }
//
//            headerLbl = getLabel()
//            lbls.append(headerLbl)
//            headerLbl1 = getLabel()
//            lbls.append(headerLbl1)
//            headerLbl2 = getLabel()
//            lbls.append(headerLbl2)
//            headerLbl3 = getLabel()
//            lbls.append(headerLbl3)
//
//
//            let stackView = UIStackView(arrangedSubviews: lbls)
//            stackView.alignment = .fill
//            stackView.axis = .horizontal
//            stackView.distribution = .fillEqually
//            stackView.translatesAutoresizingMaskIntoConstraints = false
//
//            self.addSubview(stackView)
//            // needed constarints
//            stackView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
//            stackView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
//            stackView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
//            stackView.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
//        }
//
//        override init(frame: CGRect) {
//            super.init(frame: frame)
//            self.backgroundColor = UIColor.lightGray
//            setUpLbels()
//        }
//
//        required init?(coder aDecoder: NSCoder) {
//            fatalError("init(coder:) has not been implemented")
//        }
//    }
//
//
