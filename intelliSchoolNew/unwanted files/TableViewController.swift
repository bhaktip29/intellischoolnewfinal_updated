//
//  TableViewController.swift
//  intelliSchoolNew
//
//  Created by rupali  on 14/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class TableViewController: UIViewController , UITableViewDataSource , UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    var names = ["rupali","rupali","rupali","rupali","rupali","rupali",]
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return names.count
      //  return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableTableViewCell
        cell.lbl.text = names[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //{
       print("selected")
//        switch indexPath.row {
//        case 0: //For "one"
//            segueIdentifier = "showView1"
//        case 1: //For "two"
//            segueIdentifier = "showView2"
//        default: //For "three"
//            segueIdentifier = "showView3"
//        }
        
        // Create a variable that you want to send based on the destination view controller
        // You can get a reference to the data by using indexPath shown below
        let selectedProgram = indexPath.row
        
        // Create an instance of PlayerTableViewController and pass the variable
        let destinationVC = HomeworkVC()
       // destinationVC.programVar = selectedProgram
        print("selectedProgram\(selectedProgram)")
        // Let's assume that the segue name is called playerSegue
        // This will perform the segue and pre-load the variable for you to use
        UserDefaults.standard.set(selectedProgram, forKey: "selectedProgram")
        self.performSegue(withIdentifier: "abc", sender: self)
        
        
      //  var index = indexPath
       // self.performSegue(withIdentifier: "abc", sender: self)
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//        let controller = storyboard.instantiateViewController(withIdentifier: "sc") as! SecondViewController
//
//        self.navigationController?.pushViewController(controller, animated: true)
        
//        var window: UIWindow?
//        self.window = UIWindow(frame:UIScreen.main.bounds)
//        self.window?.makeKeyAndVisible()
//          self.window?.rootViewController = UINavigationController(rootViewController: CollapsibleTableViewController())
       // }<#code#>
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
