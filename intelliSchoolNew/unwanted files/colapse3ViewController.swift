//
//  colapse3ViewController.swift
//  intelliSchoolNew
//
//  Created by rupali  on 15/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class colapse3ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView! // 1
    var arrayHeader = [1, 1, 1, 1] // 2 Array of header, change it as per your uses
    var name = ["rupali1","rupali2","rupali3","rupali4","rupali5","rupali6","rupali7","rupali8"]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.tableView.dataSource = self // 3
        self.tableView.delegate = self
    }
}

extension colapse3ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    // 4
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40))
    //        viewHeader.backgroundColor = UIColor.darkGray // Changing the header background color to gray
    //        return viewHeader
    //    }
    
    // 5
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayHeader.count
    }
    
    //    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //        // 6 Change the number of row in section as per your uses
    //        return 4
    //    }
    
    // 7
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Colapse2TableViewCell
       var selectedProgram1 = UserDefaults.standard.integer(forKey: "selectedProgram")
        cell.lbl.text = name[selectedProgram1]
        //"section: \(indexPath.section)  row: \(indexPath.row)"
        return cell
    }
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //        <#code#>
    //    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40))
        viewHeader.backgroundColor = UIColor.darkGray // Changing the header background color to gray
        let button = UIButton(type: .custom)
        button.frame = viewHeader.bounds
        button.tag = section // Assign section tag to this button
        button.addTarget(self, action: #selector(tapSection(sender:)), for: .touchUpInside)
        button.setTitle("Section: \(section)", for: .normal)
        viewHeader.addSubview(button)
        return viewHeader
    }
    @objc func tapSection(sender: UIButton) {
        self.arrayHeader[sender.tag] = (self.arrayHeader[sender.tag] == 0) ? 1 : 0
        self.tableView.reloadSections([sender.tag], with: .fade)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // 6 Change the number of row in section as per your uses
        ///important line here we can change opening section and closing section
        
        // and name.count is numberOfRowsInSection
        // trcik 1 ===   name.count : 0 == closed collapsing sectio
        // trick 2 ===   0 : name.count ==opened collaspsing section try trick 1 and two
        return (self.arrayHeader[section] == 0) ? name.count : 0
        return name.count
    }
}

