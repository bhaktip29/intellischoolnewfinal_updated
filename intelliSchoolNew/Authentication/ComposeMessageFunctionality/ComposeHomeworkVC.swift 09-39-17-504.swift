//
//  ComposeMessageVC.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 20/11/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
//import FirebaseStorage
//import AVFoundation
import OpalImagePicker
import Photos
import MobileCoreServices
import AMXFontAutoScale

class ComposeHomeworkVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,OpalImagePickerControllerDelegate,UIImagePickerControllerDelegate /*UISearchBarDelegate*/ {
     var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    var attachData = [Data]()
    var mimeTypes = [String]()
    var attachExtensions = [String]()
    
    var logoImage: [UIImage] = [
        UIImage(named: "Homework.png")!,
        UIImage(named: "Evaluation.png")!
    ]
    
    @IBOutlet weak var SMS_imgView: UIImageView!
    @IBOutlet weak var Circular_imgView: UIImageView!
    @IBOutlet weak var Message_imgView: UIImageView!
    @IBOutlet weak var Homework_imgView: UIImageView!
    
    @IBOutlet weak var SearchBar: UIImageView!
    @IBOutlet var ParentView: UIView!
    @IBOutlet weak var SelectSubjectPopupView: UIView!
    
    @IBOutlet weak var SelectSubjectTableView: UITableView!
    
    @IBOutlet weak var SelectDivisionPopupView: UIView!
    
    @IBOutlet weak var DivisionTableView: UITableView!
    @IBOutlet weak var SelectSubBtnLayout: UIButton!
    @IBOutlet weak var SelectDivisionBtnLayout: UIButton!
    @IBOutlet weak var SelectClassBtnLayout: UIButton!
    @IBOutlet weak var SelectClassPopupView: UIView!
    @IBOutlet weak var SelectClassTableView: UITableView!
    @IBOutlet weak var SelectGroupTypeBtnLayout: UIButton!
    @IBOutlet weak var SelectGroup_Check1: UIButton!
    @IBOutlet weak var SelectGroup_Check2: UIButton!
    @IBOutlet weak var SelectClassGroupBtnLayout: UIButton!
    @IBOutlet weak var TitleText: UITextField!
    @IBOutlet weak var DescriptionText: UITextField!
    @IBOutlet weak var SelectGroupPopUp: UIView!
    @IBOutlet weak var SelectClassGroupPopup: UIView!
    let imgViewTransparent = UIView()
     static let screenSize = UIScreen.main.bounds
    var attachmentList = [String]()
    
      var searchedCountry = [String]()
  //    var searching = false
 
    
    @IBOutlet weak var ClassGroupTableView: UITableView!
    var userList = [ClassGroup_DataModel]()
    var userList1 = [Class_DataModel]()
    var userList2 = [Division_DataModel]()
    var userList3 = [Subject_DataModel]()
    
    var classgrouplabltext : String! = "SELECT CLASSGROUP"
    var classlabltext : String! = "SELECT CLASS"
    var divisionlabltext : String! = "SELECT DIVISION"
    var sublabltext : String! = "SELECT SUBJECT"
    var divisionlabltext_forSendApi : String! = "SELECT DIVISION"
    var ClassDivisionIdlasttext : String! = "Test"
    
    let url = "http://commn.intellischools.com/hmcs_angular_api/public/api/homeworkAllInfo"
    let url1 = "http://commn.intellischools.com/hmcs_angular_api/public/api/classByClassgroup"
    let url2 = "http://commn.intellischools.com/hmcs_angular_api/public/api/divisionByClasses"
    let url3 = "http://apps.intellinects.com/TestApi/public/api/subjectDetails"
    let send_url = "http://commn.intellischools.com/hmcs_angular_api/public/api/saveHomeWork_Android"
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.TitleText.delegate = self
        self.DescriptionText.delegate = self
        
       NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

        imgViewTransparent.isHidden = true
        self.SelectGroupPopUp.isHidden = true
        self.SelectClassGroupPopup.isHidden = true
        self.SelectDivisionPopupView.isHidden = true
        self.SelectSubjectPopupView.isHidden = true
         self.SelectClassPopupView.isHidden = true
        
        UserDefaults.standard.set("0", forKey: "GROUP_CHECK")
        UserDefaults.standard.set("0", forKey: "CLASSGROUPCHECK")
        UserDefaults.standard.set("0", forKey: "CLASSCHECK")
        UserDefaults.standard.set("0", forKey: "DIVISIONCHECK")
        UserDefaults.standard.set("0", forKey: "SUBCHECK")
        
        SelectGroup_Check1.setImage(UIImage(named: "checkbox-inactive"), for: .normal)
        SelectGroup_Check2.setImage(UIImage(named: "checkbox-inactive"), for: .normal)
        
        
        self.SelectClassGroupPopup.center = CGPoint(x: self.ParentView.bounds.midX,
        y: self.ParentView.bounds.midY)
        self.SelectSubjectPopupView.center = CGPoint(x: self.ParentView.bounds.midX,
        y: self.ParentView.bounds.midY)
        self.SelectDivisionPopupView.center = CGPoint(x: self.ParentView.bounds.midX,
        y: self.ParentView.bounds.midY)
        self.SelectClassPopupView.center = CGPoint(x: self.ParentView.bounds.midX,
        y: self.ParentView.bounds.midY)
        self.SelectGroupPopUp.center = CGPoint(x: self.ParentView.bounds.midX,
        y: self.ParentView.bounds.midY)
        
        self.TitleText.delegate = self
        self.DescriptionText.delegate = self
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(Homework_imgView(tapGestureRecognizer:)))
        Homework_imgView.isUserInteractionEnabled = true
        Homework_imgView.addGestureRecognizer(tapGestureRecognizer)
        
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(Message_imgView(tapGestureRecognizer1:)))
        Message_imgView.isUserInteractionEnabled = true
        Message_imgView.addGestureRecognizer(tapGestureRecognizer1)
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(Circular_imgView(tapGestureRecognizer2:)))
        Circular_imgView.isUserInteractionEnabled = true
        Circular_imgView.addGestureRecognizer(tapGestureRecognizer2)
        
        let tapGestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(SMS_imgView(tapGestureRecognizer3:)))
        SMS_imgView.isUserInteractionEnabled = true
        SMS_imgView.addGestureRecognizer(tapGestureRecognizer3)
        
        
        // Select Group API..
       let parameter = [
               "schoolCode" : "INTELL",
               "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MjUwMDU2MiwiZXhwIjoxNTcyNTA0MTYyLCJuYmYiOjE1NzI1MDA1NjIsImp0aSI6IjZPdU9JUzhCaFhXRkVTdHQiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.B_xqLJGxhurHfPneH2iDVMI8aoJYGKfemevlxsDAWkQ"
               ] as [String : Any]
           
        print("parameter for homeworkAllInfo:\(parameter)")
           Alamofire.request(url, method: .post, parameters: parameter).responseJSON {
               response in
            print("Response for homeworkAllInfo : \(response)")
               guard let value = response.result.value as? [String : Any] else {
                   return
                
               }
            switch response.result {
            case .success:
                 let status = value["data"]
                if ((response.result.value) != nil) {
                    
                    let jsondataCopy = JSON(status)
                    // ClassGroup..==============================================
                    let recordsArray: [NSDictionary] = jsondataCopy["classGroupList"].arrayObject as! [NSDictionary]
                    
                      print("jsondataCopy : \(jsondataCopy)")
                     print("recordsArray : \(recordsArray)")
                    
                    for item in recordsArray {
                        let name = item.value(forKey: "classGroupName")
                        let group_id = item.value(forKey: "classGroupId")
                        
                        let userObj = ClassGroup_DataModel(First_Lable: name! as? String, Second_Lable: group_id as! Int)
                        
                        print("name : \(name!)")
                         print("classGroupId : \(group_id!)")
                        
                        print("Data.count : \(self.userList.count)")
                        
                        self.userList.append(userObj)
                    }
                        
                   self.ClassGroupTableView.reloadData()
                
                }
            case .failure(_):
                print("")
            
        }
    }
   
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
          if (!ConnectionCheck.isConnectedToNetwork()) {
              self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
          }
          else
          {
           TitleText.resignFirstResponder()
            DescriptionText.resignFirstResponder()
        }
        return (true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func BackButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let controller = storyboard.instantiateViewController(withIdentifier: "vc")
               self.present(controller, animated: true, completion: nil)
        
    }
    @IBAction func AttachmentBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
      if attachmentList.count == 5{
                 let alert = UIAlertController(title: nil, message: "You can upload maximum 5 attachments.", preferredStyle: .alert)
                 alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                 self.present(alert, animated: true, completion: nil)
             }else{
                 let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                 alert.addAction(UIAlertAction(title: "Photos", style: .default, handler: { (alert) in
                     self.pickImage()
                 }))
                 alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (alert) in
                     self.pickDocument()
                 }))
                 alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                 self.present(alert, animated: true, completion: nil)
             }
       
        }
    }
    func pickDocument(){
          let documentPicker = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        documentPicker.delegate = self as! UIDocumentPickerDelegate
          self.present(documentPicker, animated: true, completion: nil)
      }
      func pickImage(){
          let imagePicker = OpalImagePickerController()
          //Change color of selection overlay to white
          imagePicker.selectionTintColor = UIColor.white.withAlphaComponent(0.7)
          
          //Change color of image tint to black
          imagePicker.selectionImageTintColor = UIColor.black
          
          //Change image to X rather than checkmark
          //        imagePicker.selectionImage = UIImage(named: "x_image")
          
          //Change status bar style
          imagePicker.statusBarPreference = UIStatusBarStyle.lightContent
          
          //Limit maximum allowed selections to 5
          imagePicker.maximumSelectionsAllowed = 5 - self.attachmentList.count
          
          //Only allow image media type assets
          imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
          imagePicker.imagePickerDelegate = self
          present(imagePicker, animated: true, completion: nil)
      }
    @IBAction func SendButton(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            if(UserDefaults.standard.string(forKey: "SUBCHECK") == "1" && TitleText.text?.count != 0 && DescriptionText.text?.count != 0)
            {
            if(attachmentList.count < 1)
            {
            let send_parameter = [
                              "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MTY1NTI3NCwiZXhwIjoxNTcxNjU4ODc0LCJuYmYiOjE1NzE2NTUyNzQsImp0aSI6IkhHVjM1VEhNczluSVJsS3AiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.42-1tkg8oGcrKAEwvp_yZdc2zn8nAOJ7BugTtfSsCLQ",
                              "schoolCode" : "INTELL",
                               "type" : "1",
                               "grouptype" : "class|ClassGroup",
                               "title" : TitleText.text!, //"Test",
                               "message" : DescriptionText.text!, //"Test1",
                               "is_academicYear" : "2019-2020",
                               "is_entry_by" : "INTELL19E00SCH001",
                               "is_employee" : "INTELL19E00SCH001",
                               "is_ip_addr" : "test",
                               "group" : String(classgrouplabltext), //"2|Primary",
                               "subject" : String(sublabltext), //100|English
                            "division" : String(divisionlabltext_forSendApi) // INTELL7A|A|7|IV|2|Primary|Intellischools
                            //    "is_attachment" : ""
                              ] //as [String : Any]
                
          /*      let image =  UIImage(named: "Homework.png")!
                let imageData = image.jpegData(compressionQuality: 0.75)
         
              Alamofire.upload(multipartFormData: { multipartFormData in
                    
                    multipartFormData.append(imageData!, withName: "is_attachment",fileName: "furkan.png" , mimeType: "image/png")
                    
                    for (key, value) in send_parameter
                        {
                            multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                        }
                        
                    }, usingThreshold:UInt64.init(),
                       to: send_url,

                               encodingCompletion: { encodingResult in
                                   switch encodingResult {
                                   case .success(let upload, _, _):
                                       upload.responseJSON { response in
                                           print("upload Success")
                                       }
                                   case .failure(let error):
                                       print("upload Failed With Error: ", error)
                                   }

                           })*/
                
                
                       print("parameter for send_url:\(send_parameter)")
                          Alamofire.request(send_url, method: .post, parameters: send_parameter).responseJSON {
                              response in
                              print("Response for send_url : \(response)")
                              guard let value = response.result.value as? [String : Any] else {
                                  return
                              }
                           
                            switch response.result {
                                    case .success:
                                         let data = value["data"]
                                        if ((response.result.value) != nil) {
                                            
                                            let jsondataCopy = JSON(data)
                                            // ClassGroup..==============================================
                                            let recordsArray: [NSDictionary] = jsondataCopy["message"].arrayObject as! [NSDictionary]
                                            
                                              print("jsondataCopy for send homework : \(jsondataCopy)")
                                             print("recordsArray for send homework : \(recordsArray)")
                                            
                                            for item in recordsArray {
                                                let msg = item.value(forKey: "msg")
                                                print("msg : \(msg!)")
                                                self.view.makeToast(message: msg as! String, duration: 0.7, position: HRToastPositionCenter as AnyObject)
                                            }
                                            self.TitleText.text = ""
                                            self.DescriptionText.text = ""
                                        }
                                    case .failure(_):
                                        print("")
                                }
                   }
            
            }
            else
            {
               let send_parameter = [
                 "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MTY1NTI3NCwiZXhwIjoxNTcxNjU4ODc0LCJuYmYiOjE1NzE2NTUyNzQsImp0aSI6IkhHVjM1VEhNczluSVJsS3AiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.42-1tkg8oGcrKAEwvp_yZdc2zn8nAOJ7BugTtfSsCLQ",
                 "schoolCode" : "INTELL",
                  "type" : "1",
                  "grouptype" : "class|ClassGroup",
                  "title" : TitleText.text!, //"Test",
                  "message" : DescriptionText.text!, //"Test1",
                  "is_academicYear" : "2019-2020",
                  "is_entry_by" : "INTELL19E00SCH001",
                  "is_employee" : "INTELL19E00SCH001",
                  "is_ip_addr" : "test",
                  "group" : String(classgrouplabltext), //"2|Primary",
                  "subject" : String(sublabltext), //100|English
                  "division" : String(divisionlabltext_forSendApi), // INTELL7A|A|7|IV|2|Primary|Intellischools
                    "is_attachment[]": attachData
                    ] as NSMutableDictionary
                
                  // "is_attachment" : ""
                 //]
               
           /*    Alamofire.upload(multipartFormData: { multipartFormData in
                   if self.attachExtensions.count > 0{
                       let count = self.attachData.count
                       for i in 0..<count{
                           let uuid = UUID().uuidString
                        print("is_attachment..")
                           multipartFormData.append(self.attachData[i], withName: "is_attachment", fileName: "\(uuid).\(self.attachExtensions[i])" , mimeType: self.mimeTypes[i])
                        print("multipartFormData : \(multipartFormData)")
                       }
                   }
                   for (key, value) in send_parameter {
                       multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                   }
               }, usingThreshold:UInt64.init(),
                   to: send_url,
                   method: .post,
               
               encodingCompletion: { (result) in
                   switch result {
                   case .success(let upload, _ , _):
                       upload.uploadProgress(closure: { (progress) in
                           print(upload.uploadProgress)
                       })
                       upload.responseJSON { response in
                        print("parameter for send_url with attchment :\(send_parameter)")
                        print("Response for send_url for attachment : \(String(describing: response.result.value))")
                         
                        guard let value = response.result.value as? [String : Any] else {
                                return
                        }
                       }
                    break
                   case .failure(let encodingError):
                       print("failed")
                       print(encodingError)
                   }
               })*/
                
                let request =  createRequest(param: send_parameter , strURL: send_url)
                        print("Send Parameters : \(send_parameter)")
                       
                       let session = URLSession.shared
                       showActivityIndicatory(uiView: view)
                       let task = session.dataTask(with: request as URLRequest) { [weak self](data, response, error) in
                           print("Send Response : \(response)")
                           guard let data:Data = data as Data?, let _:URLResponse = response, error == nil else {
                               self?.stopActivityIndicator()
                               UtilTeacher.invokeAlertMethod("Failed!", strBody: "Can't upload attachement", delegate: nil,vcobj : self!)
                               return
                           }
                           do {
                         
                               let jsonResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: Any]
                               let path = jsonResult["path"] as? [String] ?? []
                   
                      //         self?.sendData(messageToBeSend,path)
                               DispatchQueue.main.async {
                                   self?.stopActivityIndicator()
                            //       self?.reSetAllControls()
                               }
                           } catch let error as NSError {
                               print(error)
                           }
                       }
                       task.resume()
            }
            }
            else if(TitleText.text?.count == 0)
            {
                self.view.makeToast(message: "Please Enter Title.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
                else if(DescriptionText.text?.count == 0)
                {
                    self.view.makeToast(message: "Please Enter Description.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                }
            else
            {
                 self.view.makeToast(message: "Please Select Subject first.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Compose_HomeworkVC")
        self.present(controller, animated: false, completion: nil)
    }
    func reSetAllControls() {
    //    textView.text = PlaceHolderStrings.tvPlaceHolder
 //       textField.placeholder = PlaceHolderStrings.tfPlacehoder
        attachData.removeAll()
        mimeTypes.removeAll()
        attachExtensions.removeAll()
        attachmentList.removeAll()
    //    tableView.reloadData()
    }
    func showActivityIndicatory(uiView: UIView) {

        let loadingView: UIView = UIView()
        loadingView.frame =  CGRect(x: 0, y: 0, width: 80.0, height: 80.0)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor(red: 44/255, green: 44/255, blue: 44/255, alpha: 0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        actInd.frame = CGRect(x: 0, y: 0, width: 40.0, height: 40.0)
        actInd.style =
            UIActivityIndicatorView.Style.whiteLarge
        actInd.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        loadingView.addSubview(actInd)
        uiView.addSubview(loadingView)
        actInd.startAnimating()
    }
    func stopActivityIndicator() {
        actInd.stopAnimating()
        let view = actInd.superview
        view?.removeFromSuperview()
    }
    
    func generateBoundaryString() -> String
      {
          return "Boundary-\(NSUUID().uuidString)"
      }
      
      func createBodyWithParameters(parameters: NSMutableDictionary?,boundary: String) -> NSData {
          let body = NSMutableData()
          if parameters != nil {
              for (key, value) in parameters! {
                  if(value is String || value is NSString){
                      body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                      body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
                      body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
                  }else if(value is [Data]){
                      var i = 0;
                      for data in value as! [Data]{
                          let uuidStr = UUID().uuidString
                          let filename = "\(uuidStr).\(attachExtensions[i])"
                          print("filename", filename)
                           print("Mime", mimeTypes[i])
                          body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                          body.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n".data(using: String.Encoding.utf8)!)
                          body.append("Content-Type: \(mimeTypes[i])\r\n\r\n".data(using: String.Encoding.utf8)!)
                          body.append(data)
                          body.append("\r\n".data(using: String.Encoding.utf8)!)
                          i += 1;
                      }
                  }
              }
          }
          body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
          return body
      }
    func createRequest (param : NSMutableDictionary , strURL : String) -> NSURLRequest {
        let boundary = generateBoundaryString()
        let url = NSURL(string: strURL)
        let request = NSMutableURLRequest(url: url! as URL)
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = createBodyWithParameters(parameters: param , boundary: boundary) as Data
        return request
    }
    
    @IBAction func HomeButton(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let controller = storyboard.instantiateViewController(withIdentifier: "vc")
               self.present(controller, animated: true, completion: nil)
        }
    }
    
    @IBAction func ComposeButton(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Compose_HomeworkVC")
        self.present(controller, animated: false, completion: nil)
        }
    }
    
    @IBAction func SupportButton(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        }
    }
    
    @IBAction func HomeworkBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        UserDefaults.standard.set("0", forKey: "GROUP_CHECK")
        UserDefaults.standard.set("0", forKey: "CLASSGROUPCHECK")
        UserDefaults.standard.set("0", forKey: "CLASSCHECK")
        UserDefaults.standard.set("0", forKey: "DIVISIONCHECK")
        UserDefaults.standard.set("0", forKey: "SUBCHECK")
        
        SelectGroup_Check1.setImage(UIImage(named: "checkbox-inactive"), for: .normal)
        SelectGroup_Check2.setImage(UIImage(named: "checkbox-inactive"), for: .normal)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let controller = storyboard.instantiateViewController(withIdentifier: "Compose_HomeworkVC")
               self.present(controller, animated: false, completion: nil)
        }
    }
    @IBAction func CircularBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let controller = storyboard.instantiateViewController(withIdentifier: "ComposeCircularVC")
               self.present(controller, animated: false, completion: nil)
        }
    }
    @IBAction func MessageBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Compose_MessageVC")
        self.present(controller, animated: false, completion: nil)
        }
    }
    @IBAction func SMSBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Compose_SMSVC")
        self.present(controller, animated: false, completion: nil)
        }
    }
    
    @IBAction func SelectGroupType(_ sender: Any)
    {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        self.imgViewTransparent.frame = CGRect(x: 0, y: 0, width: ComposeHomeworkVC.screenSize.width, height: ComposeHomeworkVC.screenSize.height)
                        
        self.imgViewTransparent.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                        
        self.view.addSubview(self.imgViewTransparent)
        self.imgViewTransparent.isHidden = false
        self.imgViewTransparent.addSubview(self.SelectGroupPopUp)
        self.SelectGroupPopUp.isHidden = false
        }
    }
    
    @IBAction func SelectClassGroup(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        UserDefaults.standard.set("0", forKey: "CLASSGROUPCHECK")
        if(UserDefaults.standard.string(forKey: "GROUP_CHECK") == "1")
        {
            self.imgViewTransparent.frame = CGRect(x: 0, y: 0, width: ComposeHomeworkVC.screenSize.width, height: ComposeHomeworkVC.screenSize.height)
                            
            self.imgViewTransparent.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                            
            self.view.addSubview(self.imgViewTransparent)
            self.imgViewTransparent.isHidden = false
            self.imgViewTransparent.addSubview(self.SelectClassGroupPopup)
            self.SelectClassGroupPopup.isHidden = false
        }
        else if(UserDefaults.standard.string(forKey: "GROUP_CHECK") == "2")
        {
                  
        }
        else
        {
            self.view.makeToast(message: "Please Select Group Type first.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        }
    }
    @IBAction func SelectClass(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        UserDefaults.standard.set("0", forKey: "CLASSCHECK")
        if(UserDefaults.standard.string(forKey: "CLASSGROUPCHECK") == "0")
        {
            self.view.makeToast(message: "Please Select ClassGroup first.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            self.imgViewTransparent.frame = CGRect(x: 0, y: 0, width: ComposeHomeworkVC.screenSize.width, height: ComposeHomeworkVC.screenSize.height)
                            
            self.imgViewTransparent.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                            
            self.view.addSubview(self.imgViewTransparent)
            self.imgViewTransparent.isHidden = false
            self.imgViewTransparent.addSubview(self.SelectClassPopupView)
            self.SelectClassPopupView.isHidden = false
        }
        }
    }
    @IBAction func SelectDivision(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        UserDefaults.standard.set("0", forKey: "DIVISIONCHECK")
        if(UserDefaults.standard.string(forKey: "CLASSCHECK") == "0")
        {
            self.view.makeToast(message: "Please Select Class first.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            self.imgViewTransparent.frame = CGRect(x: 0, y: 0, width: ComposeHomeworkVC.screenSize.width, height: ComposeHomeworkVC.screenSize.height)
                            
            self.imgViewTransparent.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                            
            self.view.addSubview(self.imgViewTransparent)
            self.imgViewTransparent.isHidden = false
   //         DivisionTableView.frame = CGRect(x: 30,y: 180, width: 359, height: 196)
            self.imgViewTransparent.addSubview(self.SelectDivisionPopupView)
            self.SelectDivisionPopupView.isHidden = false
        }
        }
    }
    
    @IBAction func SelectSubject(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        UserDefaults.standard.set("0", forKey: "SUBCHECK")
        if(UserDefaults.standard.string(forKey: "DIVISIONCHECK") == "0")
        {
            self.view.makeToast(message: "Please Select Division first.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
                   self.imgViewTransparent.frame = CGRect(x: 0, y: 0, width: ComposeHomeworkVC.screenSize.width, height: ComposeHomeworkVC.screenSize.height)
                                   
                   self.imgViewTransparent.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                   self.view.addSubview(self.imgViewTransparent)
                   self.imgViewTransparent.isHidden = false
        
                   self.imgViewTransparent.addSubview(self.SelectSubjectPopupView)
                   self.SelectSubjectPopupView.isHidden = false
        }
        }
    }
    
   
    @IBAction func SelectGroupPopup_OkButton(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        if(UserDefaults.standard.string(forKey: "GROUP_CHECK") == "0")
        {
            self.view.makeToast(message: "Please Select Group Type.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            if(UserDefaults.standard.string(forKey: "GROUP_CHECK") == "1")
            {
                SelectGroupTypeBtnLayout.titleLabel?.text = "CLASS|CLASSGROUP"
            }
            else if(UserDefaults.standard.string(forKey: "GROUP_CHECK") == "2")
            {
                SelectGroupTypeBtnLayout.titleLabel?.text = "CLASS|SPECIFICGROUP"
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                  let controller = storyboard.instantiateViewController(withIdentifier: "Compose_Homework_SpecificClassVC")
                                  self.present(controller, animated: false, completion: nil)
            }
        self.imgViewTransparent.isHidden = true
        self.SelectGroupPopUp.isHidden = true
        }
        }
    }
    
    @IBAction func SearchTextBox(_ sender: Any)
    {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        }
    }
    @IBAction func SelectGroup_Check1(_ sender: Any) {
        UserDefaults.standard.set("1", forKey: "GROUP_CHECK")
        SelectGroup_Check1.setImage(UIImage(named: "checkbox-active"), for: .normal)
        SelectGroup_Check2.setImage(UIImage(named: "checkbox-inactive"), for: .normal)
        
    }
    @IBAction func SelectGroup_Check2(_ sender: Any) {
        UserDefaults.standard.set("2", forKey: "GROUP_CHECK")
        SelectGroup_Check2.setImage(UIImage(named: "checkbox-active"), for: .normal)
        SelectGroup_Check1.setImage(UIImage(named: "checkbox-inactive"), for: .normal)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == ClassGroupTableView)
        {
            return userList.count
        }
        else if (tableView == SelectClassTableView)
        {
            return userList1.count
        }
        else if (tableView == DivisionTableView)
        {
            return userList2.count
        }
        else if (tableView == SelectSubjectTableView)
        {
        /*    if searching {
                       return searchedCountry.count
                   } else {*/
                        return userList3.count
             //      }
        }
        return Int()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (tableView == ClassGroupTableView)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ClassGroupCell", for: indexPath) as! SelectClassGroup
            cell.SelectClassGroup_img.image = UIImage(named: "checkbox-inactive")
            let userObjec = userList[indexPath.row]
            cell.SelectClassGroupLbl.text = userObjec.getName()
        
            return cell
        }
        else if (tableView == SelectClassTableView)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ClassCell", for: indexPath) as! SelectClassCell
            cell.SelectClass_checkbox.image = UIImage(named: "checkbox-inactive")
                let userObjec = userList1[indexPath.row]
            cell.SelectClass_Label.text = userObjec.getName()
            
                return cell
        }
        else if (tableView == DivisionTableView)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DivisionCell", for: indexPath) as! SelectDivisionCell
            cell.DivisionCheckBox.image = UIImage(named: "checkbox-inactive")
                let userObjec = userList2[indexPath.row]
            cell.DivisionLable.text = userObjec.classDivisionId()
            
                return cell
        }
        else if (tableView == SelectSubjectTableView)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubjectCell", for: indexPath) as! SelectSebjectCell
            cell.Subject_check.image = UIImage(named: "checkbox-inactive")
          /*  if searching {
                cell.Subject_lbl.text = searchedCountry[indexPath.row]
                   } else {*/
                let userObjec = userList3[indexPath.row]
            cell.Subject_lbl.text = userObjec.getName()
         //   }
                return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (tableView == ClassGroupTableView)
        {
        let cell = self.ClassGroupTableView.cellForRow(at: indexPath) as!SelectClassGroup
        let userObjec = userList[indexPath.row]
        cell.SelectClassGroup_img.image = UIImage(named: "checkbox-active")
         UserDefaults.standard.set("1", forKey: "CLASSGROUPCHECK")
        self.SelectClassGroupBtnLayout.titleLabel?.text = String(userObjec.getId()) + " | " + userObjec.getName()
        self.classgrouplabltext = String(userObjec.getId()) + "|" + userObjec.getName()
        }
        else if (tableView == SelectClassTableView)
        {
            let cell = self.SelectClassTableView.cellForRow(at: indexPath) as! SelectClassCell
             let userObjec1 = userList1[indexPath.row]
            cell.SelectClass_checkbox.image = UIImage(named: "checkbox-active")
             UserDefaults.standard.set("1", forKey: "CLASSCHECK")
            self.SelectClassBtnLayout.titleLabel?.text = String(userObjec1.getId()) + " | " + userObjec1.getName()
            self.classlabltext = String(userObjec1.getId()) + "|" + userObjec1.getName()
        }
        else if (tableView == DivisionTableView)
        {
            let cell = self.DivisionTableView.cellForRow(at: indexPath) as! SelectDivisionCell
             let userObjec2 = userList2[indexPath.row]
            cell.DivisionCheckBox.image = UIImage(named: "checkbox-active")
             UserDefaults.standard.set("1", forKey: "DIVISIONCHECK")
            self.SelectDivisionBtnLayout.titleLabel?.text = String(userObjec2.classId()) + " | " + userObjec2.classDivisionId()
            self.divisionlabltext = String(userObjec2.classId()) + " | " + userObjec2.classDivisionId()
            self.ClassDivisionIdlasttext = userObjec2.classDivisionId()?.last?.description // as String
            print("ClassDivisionIdlasttext : \(String(describing: ClassDivisionIdlasttext))")
            self.divisionlabltext_forSendApi = userObjec2.classDivisionId() + "|" + ClassDivisionIdlasttext! + "|" + classlabltext + "|" + classgrouplabltext + "|" + userObjec2.classAliase()
            print("divisionlabltext_forSendApi : \(divisionlabltext_forSendApi!)")
            
           // INTELL7A|A|7|IV|2|Primary|Intellischools
        }
        else if (tableView == SelectSubjectTableView)
        {
            let cell = self.SelectSubjectTableView.cellForRow(at: indexPath) as! SelectSebjectCell
             let userObjec3 = userList3[indexPath.row]
            cell.Subject_check.image = UIImage(named: "checkbox-active")
             UserDefaults.standard.set("1", forKey: "SUBCHECK")
            self.SelectSubBtnLayout.titleLabel?.text = String(userObjec3.getId()) + " | " + userObjec3.getName()
            self.sublabltext = String(userObjec3.getId()) + "|" + userObjec3.getName()
        }
    
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if (tableView == ClassGroupTableView)
        {
        let cell = self.ClassGroupTableView.cellForRow(at: indexPath) as!SelectClassGroup
        cell.SelectClassGroup_img.image = UIImage(named: "checkbox-inactive")
        }
        else if (tableView == SelectClassTableView)
        {
           let cell = self.SelectClassTableView.cellForRow(at: indexPath) as! SelectClassCell
            cell.SelectClass_checkbox.image = UIImage(named: "checkbox-inactive")
        }
        else if (tableView == DivisionTableView)
        {
           let cell = self.DivisionTableView.cellForRow(at: indexPath) as! SelectDivisionCell
            cell.DivisionCheckBox.image = UIImage(named: "checkbox-inactive")
        }
        else if (tableView == SelectSubjectTableView)
        {
            let cell = self.SelectSubjectTableView.cellForRow(at: indexPath) as! SelectSebjectCell
            cell.Subject_check.image = UIImage(named: "checkbox-inactive")
        }
       
    }
    @IBAction func SelectClassGroupPopup_OkBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
     if(UserDefaults.standard.string(forKey: "CLASSGROUPCHECK") == "1")
     {
        
        self.imgViewTransparent.isHidden = true
        self.SelectClassGroupPopup.isHidden = true
        
        let parameter1 = [
                   "schoolCode" : "INTELL",
                   "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MTY1NTI3NCwiZXhwIjoxNTcxNjU4ODc0LCJuYmYiOjE1NzE2NTUyNzQsImp0aSI6IkhHVjM1VEhNczluSVJsS3AiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.42-1tkg8oGcrKAEwvp_yZdc2zn8nAOJ7BugTtfSsCLQ",
                    "academicYear" : "2019-2020",
                    "groupValue" : String(classgrouplabltext)
                   ] as [String : Any]
               
            print("parameter for classByClassgroup:\(parameter1)")
               Alamofire.request(url1, method: .post, parameters: parameter1).responseJSON {
                   response in
                   print("Response for classByClassgroup : \(response)")
                   guard let value = response.result.value as? [String : Any] else {
                       return
                   }
                
                   switch response.result {
                       
                   case .success:
                    
                       let jsondataCopy = JSON(value)
                       let data: [NSDictionary] = jsondataCopy["data"].arrayObject as! [NSDictionary]
                       print("data>>\(data)")
                       if(self.userList1 != nil)
                       {
                       self.userList1.removeAll()
                       }
                       for item in data {
                       let classes = item.value(forKey: "classes")
                          print("name >>: \(classes!)")
                          
                          let classes2 = JSON(classes)
                          let data2: [NSDictionary] = classes2.arrayObject as! [NSDictionary]
                          print("data>>\(data)")
                          for item in data2
                          {
                                  let classId = item.value(forKey: "classId")
                                  print("classId >>: \(classId!)")
                                  let className = item.value(forKey: "className")
                                  print("className >>: \(className!)")
                              
                              
                              let userObj1 = Class_DataModel(First_Lable: className! as? String, Second_Lable: classId as! Int)
                              
                              
                              print("Data.count : \(self.userList1.count)")
                              
                              self.userList1.append(userObj1)
                          }
                      
                       }
                      self.SelectClassTableView.reloadData()
                    
                    
                case .failure(_):
                    print("")
                
            }
        }
        
     }
     else{
        self.view.makeToast(message: "Please Select Class Group Type.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        }
}
    
    @IBAction func SelectClassOkButton(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        if(UserDefaults.standard.string(forKey: "CLASSCHECK") == "1")
        {
            self.imgViewTransparent.isHidden = true
            self.SelectClassPopupView.isHidden = true
            

            let parameter2 = [
                              "schoolCode" : "INTELL",
                              "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MTY1NTI3NCwiZXhwIjoxNTcxNjU4ODc0LCJuYmYiOjE1NzE2NTUyNzQsImp0aSI6IkhHVjM1VEhNczluSVJsS3AiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.42-1tkg8oGcrKAEwvp_yZdc2zn8nAOJ7BugTtfSsCLQ",
                               "academicYear" : "2019-2020",
                               "classValue" : String(classlabltext)
                              ] as [String : Any]
                          
                       print("parameter for divisionByClasse:\(parameter2)")
                          Alamofire.request(url2, method: .post, parameters: parameter2).responseJSON {
                              response in
                              print("Response for divisionByClasse : \(response)")
                              guard let value = response.result.value as? [String : Any] else {
                                  return
                              }
                           
                              switch response.result {
                                  
                              case .success:
                               
                                  let jsondataCopy = JSON(value)
                                  let data: [NSDictionary] = jsondataCopy["data"].arrayObject as! [NSDictionary]
                                  print("data>>\(data)")
                                  if(self.userList2 != nil)
                                  {
                                  self.userList2.removeAll()
                                  }
                                  for item in data {
                                  let divison = item.value(forKey: "divison")
                                     print("name >>: \(divison!)")
                                     
                                     let divison2 = JSON(divison)
                                     let data2: [NSDictionary] = divison2.arrayObject as! [NSDictionary]
                                     print("data2>>\(data2)")
                                     for item in data2
                                     {
                                        
                                            let classId = item.value(forKey: "classId")
                                            print("classId >>: \(classId!)")
                                            let classDivisionId = item.value(forKey: "classDivisionId")
                                            print("classDivisionId >>: \(classDivisionId!)")
                                            let className = item.value(forKey: "className")
                                            print("className >>: \(className!)")
                                            let classGroupId = item.value(forKey: "classGroupId")
                                            print("classGroupId >>: \(classGroupId!)")
                                            let classGroupName = item.value(forKey: "classGroupName")
                                            print("classGroupName >>: \(classGroupName!)")
                                            let classAliase = item.value(forKey: "classAliase")
                                            print("classAliase >>: \(classAliase!)")
                                         
                                         let userObj2 = Division_DataModel(First_Lable: classDivisionId! as? String, Second_Lable: classId as! Int, Third_Lable: className! as? String, Fourth_Lable: classGroupId as! Int,Fifth_Lable: classGroupName! as? String, Sixth_Lable: classAliase! as? String )
                                         
                                         
                                         print("Data.count : \(self.userList2.count)")
                                         
                                         self.userList2.append(userObj2)
                                     }
                                 
                                  }
                                 self.DivisionTableView.reloadData()
                               
                               
                           case .failure(_):
                               print("")
                           
                       }
                   }
        }
        else
        {
             self.view.makeToast(message: "Please Select Class.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        }
    }
    
    @IBAction func DivisionOkButton(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        if(UserDefaults.standard.string(forKey: "DIVISIONCHECK") == "1")
        {
        self.imgViewTransparent.isHidden = true
        self.SelectDivisionPopupView.isHidden = true
            
            let parameter3 = [
                       "Accept" : "application/json",
                       "Authorization" : "Bearer  eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjBlMTRlMzZlMDIyZDY1YzI2OTg5NDUxNTMzYWEwMTliNjIwNjU3NTRiNjI4MzZjOGZhZDE3ZGYxMDBiODk4ODhkMTcyMWZlZTM2OWY2ZjU4In0.eyJhdWQiOiIxIiwianRpIjoiMGUxNGUzNmUwMjJkNjVjMjY5ODk0NTE1MzNhYTAxOWI2MjA2NTc1NGI2MjgzNmM4ZmFkMTdkZjEwMGI4OTg4OGQxNzIxZmVlMzY5ZjZmNTgiLCJpYXQiOjE1MjY4Nzg0NjUsIm5iZiI6MTUyNjg3ODQ2NSwiZXhwIjoxNTU4NDE0NDY1LCJzdWIiOiIxNCIsInNjb3BlcyI6W119.P9-oUYQ0_oPelFlqKpRl6nM0PRrQPs2gXkamzSmWJvKBBISNdfaF8BLwnFj9muW9veKnQSliRefJPQGK32_aUeVYWFe4O22NGJ2S51fW5DUhcPTvzwIfynf4-W4fNHkt5ev9CSedfXNZSO3E4GDQNJAD6zU4uxotsTGQRAC8vXLk5wFjP-fmN-S_vSJzswHfUY1GmggtoVQ8j6fOuD2prw7K1z9UoVIa4mwGInQWc3EDEBmc1sBxh7e-AT2jN7J5arr3p8hMNEfO2jLWipBkcEkqe7CPLFmTImc820R45mM44q93w7_GyvU7QCzb3Oog7h8hBd4AhSymmAehnZTC42ZrgBgepJ3TYCCJR-zdZ5lSfpSH51mojHmnKLLEBUb6OPDCadUTXMMDkplSwLLvq1iA5jt0O1zep-XAHrjUx466GBQqZSkdjGOaI5Rzr2ZlnP0fiL2yx27bbaZ2Iiva2aYlg-E5y50c1KnfGEf-0OEQxkTNO-6p0gOR4FKvOhrt51NaKB3ipxdJLAj_CR_s6E6jPfBEQPY380gyiJO-chxXHaexhxgy8_rt22xiDODXZ_D82csw6DnCRO1dgCNN8JZihRbdJxffh-j2dvOXxHvDRNphW8WuBNK-utZk7SfGqoMRWyfTdv9R-Q11gFD3NrDgU1IJx8d6K6vbD6LVfS4",
                        "Content-Type" : "text/plain"
                       ] as [String : Any]
                   
                print("parameter for subjectDetails:\(parameter3)")
                   Alamofire.request(url3, method: .post, parameters: parameter3).responseJSON {
                       response in
                       print("Response for subjectDetails : \(response)")
                       guard let value = response.result.value as? [[String : Any]] else {
                           return
                       }
                    
                       switch response.result {
                           
                       case .success:
                        
                           let jsondataCopy = JSON(value)
                           print("Response for jsondataCopy : \(jsondataCopy)")
                           let data: [NSDictionary] = jsondataCopy.arrayObject as! [NSDictionary]
                           print("data>>\(data)")
                          /* if(self.userList3 != nil)
                           {
                           self.userList3.removeAll()
                           }*/
                           for item in data {
                          
                            let subjectGroupId = item.value(forKey: "subjectGroupId")
                            print("subjectGroupId >>: \(subjectGroupId!)")
                            let subjectName = item.value(forKey: "subjectName")
                            print("subjectName >>: \(subjectName!)")

                                  let userObj3 = Subject_DataModel(First_Lable: subjectName! as? String, Second_Lable: subjectGroupId as! Int)
                                  
                                  print("Data.count : \(self.userList3.count)")
                                  
                                  self.userList3.append(userObj3)
                           }
                          self.SelectSubjectTableView.reloadData()
                        
                        
                    case .failure(_):
                        print("")
                    
                }
            }
        }
        else
        {
            self.view.makeToast(message: "Please Select Division.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        }
    }
    @IBAction func SelectSub_OkBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        if(UserDefaults.standard.string(forKey: "SUBCHECK") == "1")
        {
        self.imgViewTransparent.isHidden = true
        self.SelectSubjectPopupView.isHidden = true
        }
        else
        {
            self.view.makeToast(message: "Please Select Subject.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
    }
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.ParentView.frame.origin.y == 0 {
                self.ParentView.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.ParentView.frame.origin.y != 0 {
            self.ParentView.frame.origin.y = 0
        }
    }
    @objc func Homework_imgView(tapGestureRecognizer: UITapGestureRecognizer)
    {
    //    let tappedImage = tapGestureRecognizer.view as! UIImageView

        // Your action
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        UserDefaults.standard.set("0", forKey: "GROUP_CHECK")
        UserDefaults.standard.set("0", forKey: "CLASSGROUPCHECK")
        UserDefaults.standard.set("0", forKey: "CLASSCHECK")
        UserDefaults.standard.set("0", forKey: "DIVISIONCHECK")
        UserDefaults.standard.set("0", forKey: "SUBCHECK")
        
        SelectGroup_Check1.setImage(UIImage(named: "checkbox-inactive"), for: .normal)
        SelectGroup_Check2.setImage(UIImage(named: "checkbox-inactive"), for: .normal)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let controller = storyboard.instantiateViewController(withIdentifier: "Compose_HomeworkVC")
               self.present(controller, animated: false, completion: nil)
        }
    }
    @objc func Message_imgView(tapGestureRecognizer1: UITapGestureRecognizer)
    {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Compose_MessageVC")
        self.present(controller, animated: false, completion: nil)
        }
    }
    @objc func Circular_imgView(tapGestureRecognizer2: UITapGestureRecognizer)
    {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let controller = storyboard.instantiateViewController(withIdentifier: "ComposeCircularVC")
               self.present(controller, animated: false, completion: nil)
        }
    }
    @objc func SMS_imgView(tapGestureRecognizer3: UITapGestureRecognizer)
    {
        if (!ConnectionCheck.isConnectedToNetwork()) {
                   
                   self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
               }
               else
               {
               let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let controller = storyboard.instantiateViewController(withIdentifier: "Compose_SMSVC")
               self.present(controller, animated: false, completion: nil)
               }
    }
    
    //MARK:- ImagePickerView Methods
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        for i in 0..<images.count{
            self.attachData.append(images[i].jpegData(compressionQuality: 0.5)!)
            self.mimeTypes.append("image/jpeg")
            self.attachExtensions.append("jpeg")
        }
        print("attachData : \(attachData)")
        print("mimeTypes : \(mimeTypes)")
        print("attachExtensions : \(attachExtensions)")
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingAssets assets: [PHAsset]) {
        for asset in assets {
            if let name = asset.value(forKey: "filename") {
                attachmentList.append(name as! String)
            }
        }
         print("attachmentList : \(attachmentList)")
     //   tableView.reloadData()
        picker.dismiss(animated: true, completion: nil)
    }
    
    /*func filterTableViewForEnterText(searchText: String) {
        searching = true
        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchText)
        
        let array = (self.userList3 as NSArray).filtered(using: searchPredicate)
        self.searchedCountry = array as! [String]
        SelectSubjectTableView.reloadData()
    }
    func searchDisplayController(controller: UISearchDisplayController, shouldReloadTableForSearchString searchString: String?) -> Bool {
        self.filterTableViewForEnterText(searchText: searchString!)
        return true
    }
    func searchDisplayController(controller: UISearchDisplayController,
        shouldReloadTableForSearchScope searchOption: Int) -> Bool {
        self.filterTableViewForEnterText(searchText: self.searchDisplayController!.searchBar.text!)
            return true
    }*/
}
/*extension ComposeHomeworkVC: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
      /*  searchedCountry =  userList3.filter{($0.getName()?.contains(searchBar.text!))!}
        searching = true
        SelectSubjectTableView.reloadData()*/
        searching = true
        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchText)
        
        let array = (self.userList3 as NSArray).filtered(using: searchPredicate)
        self.searchedCountry = array as! [String]
        SelectSubjectTableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        searchBar.text = ""
       SelectSubjectTableView.reloadData()
    }
    
}*/

