//
//  Compose_Homework_SpecificClassVC.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 28/11/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import OpalImagePicker
import Photos
import MobileCoreServices
import AMXFontAutoScale

class Compose_Homework_SpecificClassVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,OpalImagePickerControllerDelegate,UIImagePickerControllerDelegate {
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    var attachmentList = [String]()
    var attachData = [Data]()
           var mimeTypes = [String]()
           var attachExtensions = [String]()
    

    
    @IBOutlet weak var SMS_imgView: UIImageView!
    @IBOutlet weak var Circular_imgView: UIImageView!
    @IBOutlet weak var Msg_imgView: UIImageView!
    @IBOutlet weak var Homework_imgView: UIImageView!
    @IBOutlet var ParentView: UIView!
    @IBOutlet weak var SelectSpecificClassgroupLayout: UIButton!
    @IBOutlet weak var SelectSpecificClassGroupPopup: UIView!
    
    @IBOutlet weak var SelectSpecificClassGroupTableView: UITableView!
    
    @IBOutlet weak var SpecificClassPopup: UIView!
   
    @IBOutlet weak var SelectGroup_Check1: UIButton!
    
    @IBOutlet weak var SelectGroupOutlet: UIButton!
    @IBOutlet weak var SelectGroup_check2: UIButton!
    
    @IBOutlet weak var TitleText: UITextField!
    
    @IBOutlet weak var DescriptionText: UITextField!
    let imgViewTransparent = UIView()
    static let screenSize = UIScreen.main.bounds
    
    var selectSpecificClassgroup : String! = "SELECT CLASSGROUP"
    
    var userList_specificClassGroup = [ClassGroup_DataModel]()
    
    let url = "http://commn.intellischools.com/hmcs_angular_api/public/api/homeworkAllInfo"
    let send_url = "http://commn.intellischools.com/hmcs_angular_api/public/api/saveHomeWork_Android"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
               NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.TitleText.delegate = self
        self.DescriptionText.delegate = self

        self.TitleText.delegate = self
        self.DescriptionText.delegate = self
        imgViewTransparent.isHidden = true
        self.SelectSpecificClassGroupPopup.isHidden = true
        self.SpecificClassPopup.isHidden = true
         UserDefaults.standard.set("0", forKey: "GROUP_CHECK")
         UserDefaults.standard.set("0", forKey: "CLASSGROUPCHECK")
        
        self.SelectSpecificClassGroupPopup.center = CGPoint(x: self.ParentView.bounds.midX,
               y: self.ParentView.bounds.midY)
        self.SpecificClassPopup.center = CGPoint(x: self.ParentView.bounds.midX,
        y: self.ParentView.bounds.midY)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(Homework_imgView(tapGestureRecognizer:)))
               Homework_imgView.isUserInteractionEnabled = true
               Homework_imgView.addGestureRecognizer(tapGestureRecognizer)
        
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(Msg_imgView(tapGestureRecognizer1:)))
        Msg_imgView.isUserInteractionEnabled = true
        Msg_imgView.addGestureRecognizer(tapGestureRecognizer1)
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(Circular_imgView(tapGestureRecognizer2:)))
        Circular_imgView.isUserInteractionEnabled = true
        Circular_imgView.addGestureRecognizer(tapGestureRecognizer2)
        
        let tapGestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(SMS_imgView(tapGestureRecognizer3:)))
        SMS_imgView.isUserInteractionEnabled = true
        SMS_imgView.addGestureRecognizer(tapGestureRecognizer3)
               
        // Select Group API..
        let parameter = [
                    "schoolCode" : "INTELL",
                    "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MjUwMDU2MiwiZXhwIjoxNTcyNTA0MTYyLCJuYmYiOjE1NzI1MDA1NjIsImp0aSI6IjZPdU9JUzhCaFhXRkVTdHQiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.B_xqLJGxhurHfPneH2iDVMI8aoJYGKfemevlxsDAWkQ"
                    ] as [String : Any]
                
             print("parameter for homeworkAllInfo1:\(parameter)")
                Alamofire.request(url, method: .post, parameters: parameter).responseJSON {
                    response in
                 print("Response for homeworkAllInfo1 : \(response)")
                    guard let value = response.result.value as? [String : Any] else {
                        return
                     
                    }
                 switch response.result {
                 case .success:
                      let status = value["data"]
                     if ((response.result.value) != nil) {
                         
                         let jsondataCopy = JSON(status)
                         // ClassGroup..==============================================
                         let recordsArray: [NSDictionary] = jsondataCopy["schoolGroupList"].arrayObject as! [NSDictionary]
                         
                           print("jsondataCopy : \(jsondataCopy)")
                          print("recordsArray : \(recordsArray)")
                         
                         for item in recordsArray {
                             let name = item.value(forKey: "groupName")
                             let group_id = item.value(forKey: "id")
                             
                             let userObj = ClassGroup_DataModel(First_Lable: name! as? String, Second_Lable: group_id as! Int)
                             
                             print("name : \(name!)")
                              print("classGroupId : \(group_id!)")
                             
                             print("Data.count : \(self.userList_specificClassGroup.count)")
                             
                             self.userList_specificClassGroup.append(userObj)
                         }
                             
                        self.SelectSpecificClassGroupTableView.reloadData()
                     
                     }
                 case .failure(_):
                     print("")
                 
             }
         }
    }
    

    @IBAction func ComposeButton(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Compose_HomeworkVC")
        self.present(controller, animated: false, completion: nil)
        }
    }
    
    @IBAction func HomeButton(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                  let controller = storyboard.instantiateViewController(withIdentifier: "vc")
                  self.present(controller, animated: true, completion: nil)
    }
    }
    
    @IBAction func SupportButton(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        }
    }
    @IBAction func HomeworkBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        UserDefaults.standard.set("0", forKey: "GROUP_CHECK")
        UserDefaults.standard.set("0", forKey: "CLASSGROUPCHECK")
        UserDefaults.standard.set("0", forKey: "CLASSCHECK")
        UserDefaults.standard.set("0", forKey: "DIVISIONCHECK")
        UserDefaults.standard.set("0", forKey: "SUBCHECK")
        
        SelectGroup_Check1.setImage(UIImage(named: "checkbox-inactive"), for: .normal)
        SelectGroup_check2.setImage(UIImage(named: "checkbox-inactive"), for: .normal)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Compose_HomeworkVC")
        self.present(controller, animated: false, completion: nil)
        }
    }
    
    @IBAction func MessageBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let controller = storyboard.instantiateViewController(withIdentifier: "Compose_MessageVC")
    self.present(controller, animated: false, completion: nil)
        }
    }
    
    @IBAction func CirculatBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ComposeCircularVC")
        self.present(controller, animated: false, completion: nil)
        }
    }
    
    @IBAction func SmsBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Compose_SMSVC")
        self.present(controller, animated: false, completion: nil)
        }
    }
    @IBAction func Class_Specific(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        self.imgViewTransparent.frame = CGRect(x: 0, y: 0, width: ComposeHomeworkVC.screenSize.width, height: ComposeHomeworkVC.screenSize.height)
                               
               self.imgViewTransparent.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                               
               self.view.addSubview(self.imgViewTransparent)
               self.imgViewTransparent.isHidden = false
               self.imgViewTransparent.addSubview(self.SpecificClassPopup)
               self.SpecificClassPopup.isHidden = false
        }
    }
    
    @IBAction func SelectClassgrpBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
       self.imgViewTransparent.frame = CGRect(x: 0, y: 0, width: ComposeHomeworkVC.screenSize.width, height: ComposeHomeworkVC.screenSize.height)
                        
        self.imgViewTransparent.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                        
        self.view.addSubview(self.imgViewTransparent)
        self.imgViewTransparent.isHidden = false
        self.imgViewTransparent.addSubview(self.SelectSpecificClassGroupPopup)
        self.SelectSpecificClassGroupPopup.isHidden = false
        }
    }
    
    @IBAction func BackBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "vc")
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func SendBtn(_ sender: Any) {
       if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            if(UserDefaults.standard.string(forKey: "CLASSGROUPCHECK") == "1" && TitleText.text?.count != 0 && DescriptionText.text?.count != 0)
            {
                    if(attachmentList.count < 1)
                    {

            let send_parameter = [
                              "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MTY1NTI3NCwiZXhwIjoxNTcxNjU4ODc0LCJuYmYiOjE1NzE2NTUyNzQsImp0aSI6IkhHVjM1VEhNczluSVJsS3AiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.42-1tkg8oGcrKAEwvp_yZdc2zn8nAOJ7BugTtfSsCLQ",
                              "schoolCode" : "INTELL",
                               "type" : "1",
                               "grouptype" : "specific|SpecificGroup",
                               "title" : TitleText.text!, //"Test",
                                "message" : DescriptionText.text!, //"Test1",
                               "is_academicYear" : "2019-2020",
                               "is_entry_by" : "INTELL19E00SCH001",
                               "is_employee" : "INTELL19E00SCH001",
                               "is_ip_addr" : "test",
                               "group" : String(selectSpecificClassgroup), // "1|All Teachers"
                              ] as [String : Any]
                
                       print("parameter for send_url for Homework Specific:\(send_parameter)")
                          Alamofire.request(send_url, method: .post, parameters: send_parameter).responseJSON {
                              response in
                              print("Response for send_url for Homework Specific: \(response)")
                              guard let value = response.result.value as? [String : Any] else {
                                  return
                              }
                           
                            switch response.result {
                                    case .success:
                                         let data = value["data"]
                                        if ((response.result.value) != nil) {
                                            
                                            let jsondataCopy = JSON(data)
                                            // ClassGroup..==============================================
                                            let recordsArray: [NSDictionary] = jsondataCopy["message"].arrayObject as! [NSDictionary]
                                            
                                              print("jsondataCopy for send Message : \(jsondataCopy)")
                                             print("recordsArray for send Message : \(recordsArray)")
                                            
                                            for item in recordsArray {
                                                let msg = item.value(forKey: "msg")
                                                print("msg : \(msg!)")
                                                self.view.makeToast(message: msg as! String, duration: 0.7, position: HRToastPositionCenter as AnyObject)
                                            }
                                            self.TitleText.text = ""
                                            self.DescriptionText.text = ""
                                        }
                                    case .failure(_):
                                        print("")
                                }
                   }
                }
                else
                    {let send_parameter = [
                        "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MTY1NTI3NCwiZXhwIjoxNTcxNjU4ODc0LCJuYmYiOjE1NzE2NTUyNzQsImp0aSI6IkhHVjM1VEhNczluSVJsS3AiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.42-1tkg8oGcrKAEwvp_yZdc2zn8nAOJ7BugTtfSsCLQ",
                        "schoolCode" : "INTELL",
                         "type" : "1",
                         "grouptype" : "specific|SpecificGroup",
                         "title" : TitleText.text!, //"Test",
                          "message" : DescriptionText.text!, //"Test1",
                         "is_academicYear" : "2019-2020",
                         "is_entry_by" : "INTELL19E00SCH001",
                         "is_employee" : "INTELL19E00SCH001",
                         "is_ip_addr" : "test",
                         "group" : String(selectSpecificClassgroup), // "1|All Teachers"
                        "is_attachment[]": attachData
                    ] as NSMutableDictionary
                             
                               // "is_attachment" : ""
                              //]
                            
                        /*    Alamofire.upload(multipartFormData: { multipartFormData in
                                if self.attachExtensions.count > 0{
                                    let count = self.attachData.count
                                    for i in 0..<count{
                                        let uuid = UUID().uuidString
                                     print("is_attachment..")
                                        multipartFormData.append(self.attachData[i], withName: "is_attachment", fileName: "\(uuid).\(self.attachExtensions[i])" , mimeType: self.mimeTypes[i])
                                     print("multipartFormData : \(multipartFormData)")
                                    }
                                }
                                for (key, value) in send_parameter {
                                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                                }
                            }, usingThreshold:UInt64.init(),
                                to: send_url,
                                method: .post,
                            
                            encodingCompletion: { (result) in
                                switch result {
                                case .success(let upload, _ , _):
                                    upload.uploadProgress(closure: { (progress) in
                                        print(upload.uploadProgress)
                                    })
                                    upload.responseJSON { response in
                                     print("parameter for send_url with attchment :\(send_parameter)")
                                     print("Response for send_url for attachment : \(String(describing: response.result.value))")
                                      
                                     guard let value = response.result.value as? [String : Any] else {
                                             return
                                     }
                                    }
                                 break
                                case .failure(let encodingError):
                                    print("failed")
                                    print(encodingError)
                                }
                            })*/
                             
                             let request =  createRequest(param: send_parameter , strURL: send_url)
                                     print("Send Parameters : \(send_parameter)")
                                    
                                    let session = URLSession.shared
                                    showActivityIndicatory(uiView: view)
                                    let task = session.dataTask(with: request as URLRequest) { [weak self](data, response, error) in
                                        print("Send Response : \(response)")
                                        guard let data:Data = data as Data?, let _:URLResponse = response, error == nil else {
                                            self?.stopActivityIndicator()
                                            UtilTeacher.invokeAlertMethod("Failed!", strBody: "Can't upload attachement", delegate: nil,vcobj : self!)
                                            return
                                        }
                                        do {
                                      
                                            let jsonResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: Any]
                                            let path = jsonResult["path"] as? [String] ?? []
                                
                                   //         self?.sendData(messageToBeSend,path)
                                            DispatchQueue.main.async {
                                                self?.stopActivityIndicator()
                                         //       self?.reSetAllControls()
                                            }
                                        } catch let error as NSError {
                                            print(error)
                                        }
                                    }
                                    task.resume()

                }
        }
            else if(TitleText.text?.count == 0)
            {
                self.view.makeToast(message: "Please Enter Title.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
                else if(DescriptionText.text?.count == 0)
                {
                    self.view.makeToast(message: "Please Enter Description.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                }
            else
            {
                 self.view.makeToast(message: "Please Select Class Group first.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "Compose_HomeworkVC")
            self.present(controller, animated: false, completion: nil)
        }
    }
    
    @IBAction func AttachmentBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
            {
                self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
            else
            {
          if attachmentList.count == 5{
                     let alert = UIAlertController(title: nil, message: "You can upload maximum 5 attachments.", preferredStyle: .alert)
                     alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                     self.present(alert, animated: true, completion: nil)
                 }else{
                     let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                     alert.addAction(UIAlertAction(title: "Photos", style: .default, handler: { (alert) in
                         self.pickImage()
                     }))
                     alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (alert) in
                         self.pickDocument()
                     }))
                     alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                     self.present(alert, animated: true, completion: nil)
                 }
           
            }
        }
        func pickDocument(){
              let documentPicker = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: .import)
            documentPicker.delegate = self as! UIDocumentPickerDelegate
              self.present(documentPicker, animated: true, completion: nil)
          }
          func pickImage(){
              let imagePicker = OpalImagePickerController()
              //Change color of selection overlay to white
              imagePicker.selectionTintColor = UIColor.white.withAlphaComponent(0.7)
              
              //Change color of image tint to black
              imagePicker.selectionImageTintColor = UIColor.black
              
              //Change image to X rather than checkmark
              //        imagePicker.selectionImage = UIImage(named: "x_image")
              
              //Change status bar style
              imagePicker.statusBarPreference = UIStatusBarStyle.lightContent
              
              //Limit maximum allowed selections to 5
              imagePicker.maximumSelectionsAllowed = 5 - self.attachmentList.count
              
              //Only allow image media type assets
              imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
              imagePicker.imagePickerDelegate = self
              present(imagePicker, animated: true, completion: nil)
          }
    
    @IBAction func SelectGroup_Check1(_ sender: Any) {
        UserDefaults.standard.set("1", forKey: "GROUP_CHECK")
        SelectGroup_Check1.setImage(UIImage(named: "checkbox-active"), for: .normal)
        SelectGroup_check2.setImage(UIImage(named: "checkbox-inactive"), for: .normal)
    }
    
    @IBAction func SelectGroup_Check2(_ sender: Any) {
         UserDefaults.standard.set("2", forKey: "GROUP_CHECK")
         SelectGroup_check2.setImage(UIImage(named: "checkbox-active"), for: .normal)
         SelectGroup_Check1.setImage(UIImage(named: "checkbox-inactive"), for: .normal)
    }
    
    @IBAction func SpecificClass_OkBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        if(UserDefaults.standard.string(forKey: "GROUP_CHECK") == "0")
        {
            self.view.makeToast(message: "Please Select Group Type.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            if(UserDefaults.standard.string(forKey: "GROUP_CHECK") == "1")
            {
                SelectGroupOutlet.titleLabel?.text = "CLASS|CLASSGROUP"
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "Compose_HomeworkVC")
                self.present(controller, animated: false, completion: nil)
            }
            else if(UserDefaults.standard.string(forKey: "GROUP_CHECK") == "2")
            {
                SelectGroupOutlet.titleLabel?.text = "CLASS|SPECIFICGROUP"
                
            }
        self.imgViewTransparent.isHidden = true
        self.SpecificClassPopup.isHidden = true
        }
        }
    }
    
    @IBAction func SelectSpecificClassGroupOkBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork())
        {
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            if(UserDefaults.standard.string(forKey: "CLASSGROUPCHECK") == "1")
            {
               
               self.imgViewTransparent.isHidden = true
               self.SelectSpecificClassGroupPopup.isHidden = true
            }
            else
            {
               self.view.makeToast(message: "Please Select Class Group Type.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == SelectSpecificClassGroupTableView)
        {
            return userList_specificClassGroup.count
        }
        return Int()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if (tableView == SelectSpecificClassGroupTableView)
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectSpecificClassGroup_PopupCell", for: indexPath) as! SelectSpecificClassGroup_Popup
        cell.SpecificClassGroupCheck.image = UIImage(named: "checkbox-inactive")
        let userObjec = userList_specificClassGroup[indexPath.row]
        cell.SpecificClassGroupLbl.text = userObjec.getName()
    
        return cell
    }
    return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if (tableView == SelectSpecificClassGroupTableView)
    {
    let cell = self.SelectSpecificClassGroupTableView.cellForRow(at: indexPath) as!SelectSpecificClassGroup_Popup
    let userObjec = userList_specificClassGroup[indexPath.row]
    cell.SpecificClassGroupCheck.image = UIImage(named: "checkbox-active")
     UserDefaults.standard.set("1", forKey: "CLASSGROUPCHECK")
    self.SelectSpecificClassgroupLayout.titleLabel?.text = String(userObjec.getId()) + " | " + userObjec.getName()
    self.selectSpecificClassgroup = String(userObjec.getId()) + "|" + userObjec.getName()
    }
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    if (tableView == SelectSpecificClassGroupTableView)
    {
    let cell = self.SelectSpecificClassGroupTableView.cellForRow(at: indexPath) as!SelectSpecificClassGroup_Popup
    cell.SpecificClassGroupCheck.image = UIImage(named: "checkbox-inactive")
    }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
          if (!ConnectionCheck.isConnectedToNetwork()) {
              self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
          }
          else
          {
           TitleText.resignFirstResponder()
            DescriptionText.resignFirstResponder()
        }
        return (true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
         if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
             if self.ParentView.frame.origin.y == 0 {
                 self.ParentView.frame.origin.y -= keyboardSize.height
             }
         }
     }

     @objc func keyboardWillHide(notification: NSNotification) {
         if self.ParentView.frame.origin.y != 0 {
             self.ParentView.frame.origin.y = 0
         }
     }
    @objc func Homework_imgView(tapGestureRecognizer: UITapGestureRecognizer)
    {
        if (!ConnectionCheck.isConnectedToNetwork())
               {
                   self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
               }
               else
               {
               UserDefaults.standard.set("0", forKey: "GROUP_CHECK")
               UserDefaults.standard.set("0", forKey: "CLASSGROUPCHECK")
               UserDefaults.standard.set("0", forKey: "CLASSCHECK")
               UserDefaults.standard.set("0", forKey: "DIVISIONCHECK")
               UserDefaults.standard.set("0", forKey: "SUBCHECK")
               
               SelectGroup_Check1.setImage(UIImage(named: "checkbox-inactive"), for: .normal)
               SelectGroup_check2.setImage(UIImage(named: "checkbox-inactive"), for: .normal)
               
               let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let controller = storyboard.instantiateViewController(withIdentifier: "Compose_HomeworkVC")
               self.present(controller, animated: false, completion: nil)
               }
    }
    @objc func Msg_imgView(tapGestureRecognizer1: UITapGestureRecognizer)
       {
           if (!ConnectionCheck.isConnectedToNetwork())
               {
                   self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
               }
               else
               {
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           let controller = storyboard.instantiateViewController(withIdentifier: "Compose_MessageVC")
           self.present(controller, animated: false, completion: nil)
               }
       }
    @objc func Circular_imgView(tapGestureRecognizer2: UITapGestureRecognizer)
       {
           if (!ConnectionCheck.isConnectedToNetwork())
                  {
                      self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                  }
                  else
                  {
                  let storyboard = UIStoryboard(name: "Main", bundle: nil)
                  let controller = storyboard.instantiateViewController(withIdentifier: "ComposeCircularVC")
                  self.present(controller, animated: false, completion: nil)
                  }
       }
    @objc func SMS_imgView(tapGestureRecognizer3: UITapGestureRecognizer)
        {
              if (!ConnectionCheck.isConnectedToNetwork())
              {
                  self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
              }
              else
              {
              let storyboard = UIStoryboard(name: "Main", bundle: nil)
              let controller = storyboard.instantiateViewController(withIdentifier: "Compose_SMSVC")
              self.present(controller, animated: false, completion: nil)
              }
        }
    
    func reSetAllControls() {
        //    textView.text = PlaceHolderStrings.tvPlaceHolder
     //       textField.placeholder = PlaceHolderStrings.tfPlacehoder
            attachData.removeAll()
            mimeTypes.removeAll()
            attachExtensions.removeAll()
            attachmentList.removeAll()
        //    tableView.reloadData()
        }
        func showActivityIndicatory(uiView: UIView) {
    
            let loadingView: UIView = UIView()
            loadingView.frame =  CGRect(x: 0, y: 0, width: 80.0, height: 80.0)
            loadingView.center = uiView.center
            loadingView.backgroundColor = UIColor(red: 44/255, green: 44/255, blue: 44/255, alpha: 0.7)
            loadingView.clipsToBounds = true
            loadingView.layer.cornerRadius = 10
            actInd.frame = CGRect(x: 0, y: 0, width: 40.0, height: 40.0)
            actInd.style =
                UIActivityIndicatorView.Style.whiteLarge
            actInd.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
            loadingView.addSubview(actInd)
            uiView.addSubview(loadingView)
            actInd.startAnimating()
        }
        func stopActivityIndicator() {
            actInd.stopAnimating()
            let view = actInd.superview
            view?.removeFromSuperview()
        }
        
        func generateBoundaryString() -> String
          {
              return "Boundary-\(NSUUID().uuidString)"
          }
          
          func createBodyWithParameters(parameters: NSMutableDictionary?,boundary: String) -> NSData {
              let body = NSMutableData()
              if parameters != nil {
                  for (key, value) in parameters! {
                      if(value is String || value is NSString){
                          body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                          body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
                          body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
                      }else if(value is [Data]){
                          var i = 0;
                          for data in value as! [Data]{
                              let uuidStr = UUID().uuidString
                              let filename = "\(uuidStr).\(attachExtensions[i])"
                              print("filename", filename)
                               print("Mime", mimeTypes[i])
                              body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                              body.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n".data(using: String.Encoding.utf8)!)
                              body.append("Content-Type: \(mimeTypes[i])\r\n\r\n".data(using: String.Encoding.utf8)!)
                              body.append(data)
                              body.append("\r\n".data(using: String.Encoding.utf8)!)
                              i += 1;
                          }
                      }
                  }
              }
              body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
              return body
          }
        func createRequest (param : NSMutableDictionary , strURL : String) -> NSURLRequest {
            let boundary = generateBoundaryString()
            let url = NSURL(string: strURL)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            request.httpBody = createBodyWithParameters(parameters: param , boundary: boundary) as Data
            return request
        }
    
    
    //MARK:- ImagePickerView Methods
        
        func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
            for i in 0..<images.count{
                self.attachData.append(images[i].jpegData(compressionQuality: 0.5)!)
                self.mimeTypes.append("image/jpeg")
                self.attachExtensions.append("jpeg")
            }
            print("attachData : \(attachData)")
            print("mimeTypes : \(mimeTypes)")
            print("attachExtensions : \(attachExtensions)")
        }
        
        func imagePicker(_ picker: OpalImagePickerController, didFinishPickingAssets assets: [PHAsset]) {
            for asset in assets {
                if let name = asset.value(forKey: "filename") {
                    attachmentList.append(name as! String)
                }
            }
             print("attachmentList : \(attachmentList)")
         //   tableView.reloadData()
            picker.dismiss(animated: true, completion: nil)
        }
    

}
extension Compose_Homework_SpecificClassVC : UIDocumentPickerDelegate{
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let docUrl = url as URL
        let docData  = try! Data(contentsOf: docUrl)
        self.attachData.append(docData)
        self.attachmentList.append(url.lastPathComponent)
        self.attachExtensions.append("pdf")
        self.mimeTypes.append("application/pdf")
 //       self.tableView.reloadData()
    }
}
