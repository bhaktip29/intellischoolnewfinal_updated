//
//  ComposeSmsGroupStudentVC.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 10/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ComposeSmsGroupStudentVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate  {
    
    @IBOutlet weak var SelectStudentDivisionTableView: UITableView!
    @IBOutlet weak var SelectStudentDivisionPopupView: UIView!
    
    @IBOutlet weak var SelectStudentClassTableView: UITableView!
    @IBOutlet weak var SelectStudentClassPopupView: UIView!
    @IBOutlet weak var SelectDivisionLayout: UIButton!
    @IBOutlet weak var SelectClassLayout: UIButton!
    @IBOutlet weak var SelectClassGroupLayout: UIButton!
    
    @IBOutlet weak var SMS_imgView: UIImageView!
        @IBOutlet weak var Circular_imgView: UIImageView!
        @IBOutlet weak var Msg_imgView: UIImageView!
        @IBOutlet weak var Homework_imgView: UIImageView!

    @IBOutlet weak var SelectClassGroupPopupView: UIView!
    
    @IBOutlet weak var SelectClassGroupTableview: UITableView!
    
    
        @IBOutlet weak var SelectReceiverTypeLayout: UIButton!
        @IBOutlet weak var SelectReceiversTableView: UITableView!
        @IBOutlet weak var SelectReceiversPopupView: UIView!
        @IBOutlet var ParentView: UIView!
        @IBOutlet weak var DescriptionText: UITextField!
        
        @IBOutlet weak var SelectMsgTableView: UITableView!
        @IBOutlet weak var SelectMsgTypePopup: UIView!
        
        @IBOutlet weak var SelectMsgTypeLayout: UIButton!
        let imgViewTransparent = UIView()
          static let screenSize = UIScreen.main.bounds
    var selectClassgroup : String! = "SELECT CLASSGROUP"
     var selectClass : String! = "SELECT CLASS"
     var selectDivision : String! = "SELECT Division"
    var ClassDivisionIdlasttext : String! = "TEST"
        var divisionlabltext_forSendApi : String! = "TEST"
    
     var userList = [ClassGroup_DataModel]()
    var userList1 = [Class_DataModel]()
    var userList2 = [Division_DataModel]()
        
        let MsgType: [String] = ["Group", "Individual"]
        let ReceiverType: [String] = ["Student", "Teaching Staff", "Non-Teaching Staff", "To Mobile Number"]
    
    let url = "http://commn.intellischools.com/hmcs_angular_api/public/api/homeworkAllInfo"
    let url1 = "http://commn.intellischools.com/hmcs_angular_api/public/api/classByClassgroup"
     let url2 = "http://commn.intellischools.com/hmcs_angular_api/public/api/divisionByClasses"
    let send_url = "http://commn.intellischools.com/hmcs_angular_api/public/api/sendSMS_android"
        
        override func viewDidLoad() {
            super.viewDidLoad()
            imgViewTransparent.isHidden = true
            
            self.SelectMsgTypePopup.isHidden = true
            self.SelectReceiversPopupView.isHidden = true
            self.SelectClassGroupPopupView.isHidden = true
            self.SelectStudentClassPopupView.isHidden = true
            self.SelectStudentDivisionPopupView.isHidden = true
            
            UserDefaults.standard.set("1", forKey: "MESSAGECHECK")
            UserDefaults.standard.set("1", forKey: "RECEIVERCHECK")
            UserDefaults.standard.set("1", forKey: "MESSAGECHECKOPTION")
            UserDefaults.standard.set("1", forKey: "RECEIVERCHECKOPTION")
            UserDefaults.standard.set("0", forKey: "CLASSGROUPCHECK")
            UserDefaults.standard.set("0", forKey: "CLASSCHECK")
            UserDefaults.standard.set("0", forKey: "DIVISIONCHECK")
            
            
            self.SelectMsgTypePopup.center = CGPoint(x: self.ParentView.bounds.midX, y: self.ParentView.bounds.midY)
            self.SelectReceiversPopupView.center = CGPoint(x: self.ParentView.bounds.midX, y: self.ParentView.bounds.midY)
            self.SelectClassGroupPopupView.center = CGPoint(x: self.ParentView.bounds.midX, y: self.ParentView.bounds.midY)
             self.SelectStudentClassPopupView.center = CGPoint(x: self.ParentView.bounds.midX, y: self.ParentView.bounds.midY)
            self.SelectStudentDivisionPopupView.center = CGPoint(x: self.ParentView.bounds.midX, y: self.ParentView.bounds.midY)
            
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
            
            self.DescriptionText.delegate = self
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(Homework_imgView(tapGestureRecognizer:)))
                   Homework_imgView.isUserInteractionEnabled = true
                   Homework_imgView.addGestureRecognizer(tapGestureRecognizer)
            
            let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(Msg_imgView(tapGestureRecognizer1:)))
            Msg_imgView.isUserInteractionEnabled = true
            Msg_imgView.addGestureRecognizer(tapGestureRecognizer1)
            
            let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(Circular_imgView(tapGestureRecognizer2:)))
            Circular_imgView.isUserInteractionEnabled = true
            Circular_imgView.addGestureRecognizer(tapGestureRecognizer2)
            
            let tapGestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(SMS_imgView(tapGestureRecognizer3:)))
            SMS_imgView.isUserInteractionEnabled = true
            SMS_imgView.addGestureRecognizer(tapGestureRecognizer3)
            
            // Select Group API..
                 let parameter = [
                         "schoolCode" : "INTELL",
                         "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MjUwMDU2MiwiZXhwIjoxNTcyNTA0MTYyLCJuYmYiOjE1NzI1MDA1NjIsImp0aSI6IjZPdU9JUzhCaFhXRkVTdHQiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.B_xqLJGxhurHfPneH2iDVMI8aoJYGKfemevlxsDAWkQ"
                         ] as [String : Any]
                     
                  print("parameter for homeworkAllInfo:\(parameter)")
                     Alamofire.request(url, method: .post, parameters: parameter).responseJSON {
                         response in
                      print("Response for homeworkAllInfo : \(response)")
                         guard let value = response.result.value as? [String : Any] else {
                             return
                          
                         }
                      switch response.result {
                      case .success:
                           let status = value["data"]
                          if ((response.result.value) != nil) {
                              
                              let jsondataCopy = JSON(status)
                              // ClassGroup..==============================================
                              let recordsArray: [NSDictionary] = jsondataCopy["classGroupList"].arrayObject as! [NSDictionary]
                              
                                print("jsondataCopy : \(jsondataCopy)")
                               print("recordsArray : \(recordsArray)")
                              
                              for item in recordsArray {
                                  let name = item.value(forKey: "classGroupName")
                                  let group_id = item.value(forKey: "classGroupId")
                                  
                                  let userObj = ClassGroup_DataModel(First_Lable: name! as? String, Second_Lable: group_id as! Int)
                                  
                                  print("name : \(name!)")
                                   print("classGroupId : \(group_id!)")
                                  
                                  print("Data.count : \(self.userList.count)")
                                  
                                  self.userList.append(userObj)
                              }
                                  
                             self.SelectClassGroupTableview.reloadData()
                          
                          }
                      case .failure(_):
                          print("")
                      
                  }
              }
            
        }
        
        @IBAction func BackBtn(_ sender: Any) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                       let controller = storyboard.instantiateViewController(withIdentifier: "vc")
                       self.present(controller, animated: true, completion: nil)
        }
        
        @IBAction func AttachmentBtn(_ sender: Any) {
            if (!ConnectionCheck.isConnectedToNetwork())
            {
                self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
            else
            {
            }
        }
        @IBAction func SendBtn(_ sender: Any) {
            if (!ConnectionCheck.isConnectedToNetwork())
            {
                self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
            else
            {
                if (!ConnectionCheck.isConnectedToNetwork())
                              {
                                  self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                              }
                              else
                              {
                                  if(UserDefaults.standard.string(forKey: "DIVISIONCHECK") == "1" && DescriptionText.text?.count != 0)
                                  {
                                      print("Receivers Group selected..")
                                    let send_parameter = [
                                                  "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MTYwMjcxMSwiZXhwIjoxNTcxNjA2MzExLCJuYmYiOjE1NzE2MDI3MTEsImp0aSI6ImhQa0M2WmRjT1J4dXhkQlUiLCJzdWIiOjUyLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.84Ps9Zj6ykr82801MDoa4vc3Fiy26S5zfB-_acsQATE",
                                                  "schoolCode" : "INTELL",
                                                   "message_type" : "group",
                                                   "receiver_group_type" : "students",
                                                   "sms_sender" : "INTELL19E00SCH002",
                                                   "message" : DescriptionText.text!, //"Test1",
                                                   "academicYear" : "2019-2020",
                                                   "ip_addr" : "test",
                                                   "group" : String(selectClassgroup), //"2|Primary",
                                                   "class" : String(selectClass), //4|I
                                                   "division" : String(divisionlabltext_forSendApi) // INTELL4A|A|4|I|2|Primary|Intellischools
                                                  ] as [String : Any]
                                    
                                           print("parameter for send_url sms group student:\(send_parameter)")
                                              Alamofire.request(send_url, method: .post, parameters: send_parameter).responseJSON {
                                                  response in
                                                  print("Response for send_url sms group student : \(response)")
                                                  guard let value = response.result.value as? [String : Any] else {
                                                      return
                                                  }
                                            
                                                switch response.result {
                                                        case .success:
                                                        //     let data = value["data"]
                                                            if ((response.result.value) != nil) {
                                                                
                                                                let jsondataCopy = JSON(value)
                                                                // ClassGroup..==============================================
                                                                let recordsArray: [NSDictionary] = jsondataCopy["message"].arrayObject as! [NSDictionary]
                                                                
                                                         //         print("jsondataCopy for send homework : \(jsondataCopy)")
                                                         //        print("recordsArray for send homework : \(recordsArray)")
                                                                
                                                                for item in recordsArray {
                                                                    let msg = item.value(forKey: "msg")
                                                                    print("msg : \(msg!)")
                                                                    self.view.makeToast(message: msg as! String, duration: 0.7, position: HRToastPositionCenter as AnyObject)
                                                                }
                                                                self.DescriptionText.text = ""
                                                            }
                                                        case .failure(_):
                                                            print("")
                                                    }
                                       }
                                  
                                  }
                                      else if(DescriptionText.text?.count == 0)
                                      {
                                          self.view.makeToast(message: "Please Enter Message.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                                      }
                                  else
                                  {
                                       self.view.makeToast(message: "Please Select Division first.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                                  }
                              }
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "Compose_SMSVC")
                self.present(controller, animated: false, completion: nil)
            }
        }
        @IBAction func HomeworkBtn(_ sender: Any) {
            if (!ConnectionCheck.isConnectedToNetwork())
            {
                self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
            else
            {
                UserDefaults.standard.set("0", forKey: "GROUP_CHECK")
                UserDefaults.standard.set("0", forKey: "CLASSGROUPCHECK")
                UserDefaults.standard.set("0", forKey: "CLASSCHECK")
                UserDefaults.standard.set("0", forKey: "DIVISIONCHECK")
                UserDefaults.standard.set("0", forKey: "SUBCHECK")
                
                     let storyboard = UIStoryboard(name: "Main", bundle: nil)
                           let controller = storyboard.instantiateViewController(withIdentifier: "Compose_HomeworkVC")
                           self.present(controller, animated: false, completion: nil)
            }
        }
        @IBAction func CircularBtn(_ sender: Any) {
            if (!ConnectionCheck.isConnectedToNetwork())
            {
                self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
            else
            {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ComposeCircularVC")
            self.present(controller, animated: false, completion: nil)
            }
        }
        @IBAction func MessageBtn(_ sender: Any) {
            if (!ConnectionCheck.isConnectedToNetwork())
            {
                self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
            else
            {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "Compose_MessageVC")
            self.present(controller, animated: false, completion: nil)
            }
        }
        @IBAction func SmsBtn(_ sender: Any) {
            if (!ConnectionCheck.isConnectedToNetwork())
            {
                self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
            else
            {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                   let controller = storyboard.instantiateViewController(withIdentifier: "Compose_SMSVC")
                   self.present(controller, animated: false, completion: nil)
            }
        }
        @IBAction func HomeBtn(_ sender: Any) {
            if (!ConnectionCheck.isConnectedToNetwork())
            {
                self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
            else
            {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "vc")
            self.present(controller, animated: true, completion: nil)
            }
        }
        @IBAction func ComposeBtn(_ sender: Any) {
            if (!ConnectionCheck.isConnectedToNetwork())
            {
                self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
            else
            {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "Compose_SMSVC")
            self.present(controller, animated: false, completion: nil)
            }
        }
        @IBAction func SupportBtn(_ sender: Any) {
            if (!ConnectionCheck.isConnectedToNetwork())
            {
                self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
            else
            {
            }
        }
        func textFieldShouldReturn(_ textField: UITextField) -> Bool
        {
              if (!ConnectionCheck.isConnectedToNetwork()) {
                  self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
              }
              else
              {
                DescriptionText.resignFirstResponder()
            }
            return (true)
        }
        
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            self.view.endEditing(true)
        }

        @objc func keyboardWillShow(notification: NSNotification) {
             if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                 if self.ParentView.frame.origin.y == 0 {
                     self.ParentView.frame.origin.y -= keyboardSize.height
                 }
             }
         }

         @objc func keyboardWillHide(notification: NSNotification) {
             if self.ParentView.frame.origin.y != 0 {
                 self.ParentView.frame.origin.y = 0
             }
         }

        
        @IBAction func SelectMessageType(_ sender: Any) {
            if (!ConnectionCheck.isConnectedToNetwork()) {
                
                self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
            else
            {
            self.imgViewTransparent.frame = CGRect(x: 0, y: 0, width: ComposeSmsGroupStudentVC.screenSize.width, height: ComposeSmsGroupStudentVC.screenSize.height)
                                          
                          self.imgViewTransparent.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                                          
                          self.view.addSubview(self.imgViewTransparent)
                          self.imgViewTransparent.isHidden = false
                          self.imgViewTransparent.addSubview(self.SelectMsgTypePopup)
                          self.SelectMsgTypePopup.isHidden = false
            }
        }
        
        @IBAction func SelectReceivesGroup(_ sender: Any) {
            if (!ConnectionCheck.isConnectedToNetwork()) {
                                  
                                  self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                              }
                              else
                              {
                              if(UserDefaults.standard.string(forKey: "MESSAGECHECK") == "1")
                              {
                                  self.imgViewTransparent.frame = CGRect(x: 0, y: 0, width: ComposeSmsGroupStudentVC.screenSize.width, height: ComposeSmsGroupStudentVC.screenSize.height)
                                                  
                                  self.imgViewTransparent.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                                                  
                                  self.view.addSubview(self.imgViewTransparent)
                                  self.imgViewTransparent.isHidden = false
                                  self.imgViewTransparent.addSubview(self.SelectReceiversPopupView)
                                  self.SelectReceiversPopupView.isHidden = false
                              }
                              
                              else
                              {
                                  self.view.makeToast(message: "Please Select Message Type first.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                              }
                              }
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if (tableView == SelectMsgTableView)
            {
                return MsgType.count
            }
            else if (tableView == SelectReceiversTableView)
            {
                return ReceiverType.count
            }
            else if (tableView == SelectClassGroupTableview)
            {
                return userList.count
            }
            else if (tableView == SelectStudentClassTableView)
            {
                return userList1.count
            }
            else if (tableView == SelectStudentDivisionTableView)
            {
                return userList2.count
            }
            return Int()
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (tableView == SelectMsgTableView)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectSMS_messageTypeCell", for: indexPath) as! SelectSMS_messageTypeCell
            cell.SelectMsgTypeCheck.image = UIImage(named: "checkbox-inactive")
        //    let userObjec = userList_specificClassGroup1[indexPath.row]
            cell.SelectMessageType_lbl.text = MsgType[indexPath.row]
            
            return cell
        }
        else if (tableView == SelectReceiversTableView)
        {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SelectReceiversGroupCell", for: indexPath) as! SelectReceiversGroupCell
            cell.SelectReceiversCheck.image = UIImage(named: "checkbox-inactive")
            //    let userObjec = userList_specificClassGroup1[indexPath.row]
            cell.SelectReceiversLbl.text = ReceiverType[indexPath.row]
                
                return cell
        }
        else if (tableView == SelectClassGroupTableview)
        {
                           let cell = tableView.dequeueReusableCell(withIdentifier: "SelectSMS_studentClassGroup", for: indexPath) as! SelectSMS_studentClassGroup
                    cell.SelectStudentClassGroupCheck.image = UIImage(named: "checkbox-inactive")
                          let userObjec = userList[indexPath.row]
                    cell.SelectStudentClassGroupLbl.text = userObjec.getName()
                           
                           return cell
        }
        else if (tableView == SelectStudentClassTableView)
        {
                               let cell = tableView.dequeueReusableCell(withIdentifier: "SelectSMS_StudentClassCell", for: indexPath) as! SelectSMS_StudentClassCell
            cell.SelectSMS_StudentClassCheck.image = UIImage(named: "checkbox-inactive")
                              let userObjec = userList1[indexPath.row]
            cell.SelectSMS_StudentClasslbl.text = userObjec.getName()
                               
                               return cell
        }
            else if (tableView == SelectStudentDivisionTableView)
            {
                                   let cell = tableView.dequeueReusableCell(withIdentifier: "SelectSMSStudent_DivisionCell", for: indexPath) as! SelectSMSStudent_DivisionCell
                cell.SelectSMS_StudentDivisionCheck.image = UIImage(named: "checkbox-inactive")
                                  let userObjec = userList2[indexPath.row]
                cell.SelectSMS_StudentDivisionLbl.text = userObjec.classDivisionId()
                                   
                return cell
            }
        return UITableViewCell()
        }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            if (tableView == SelectMsgTableView)
            {
                let cell = self.SelectMsgTableView.cellForRow(at: indexPath) as! SelectSMS_messageTypeCell
                let userObjec = MsgType[indexPath.row]
                cell.SelectMsgTypeCheck.image = UIImage(named: "checkbox-active")
                UserDefaults.standard.set("1", forKey: "MESSAGECHECK")
                UserDefaults.standard.set(indexPath.row + 1, forKey: "MESSAGECHECKOPTION")
                self.SelectMsgTypeLayout.titleLabel?.text = userObjec //String(userObjec.getId()) + " | " + userObjec.getName()
    //    self.selectSpecificClassgroup = String(userObjec.getId()) + "|" + userObjec.getName()
        }
          else if (tableView == SelectReceiversTableView)
                    {
                        let cell = self.SelectReceiversTableView.cellForRow(at: indexPath) as! SelectReceiversGroupCell
                        let userObjec = ReceiverType[indexPath.row]
                        cell.SelectReceiversCheck.image = UIImage(named: "checkbox-active")
                        UserDefaults.standard.set("1", forKey: "RECEIVERCHECK")
                        UserDefaults.standard.set(indexPath.row + 1, forKey: "RECEIVERCHECKOPTION")
                        self.SelectReceiverTypeLayout?.titleLabel?.text = userObjec //String(userObjec.getId()) + " | " + userObjec.getName()
            //    self.selectSpecificClassgroup = String(userObjec.getId()) + "|" + userObjec.getName()
                }
            else if (tableView == SelectClassGroupTableview)
                    {
                        let cell = self.SelectClassGroupTableview.cellForRow(at: indexPath) as! SelectSMS_studentClassGroup
                        let userObjec = userList[indexPath.row]
                        cell.SelectStudentClassGroupCheck.image = UIImage(named: "checkbox-active")
                        UserDefaults.standard.set("1", forKey: "CLASSGROUPCHECK")
                        self.SelectClassGroupLayout?.titleLabel?.text = String(userObjec.getId()) + " | " + userObjec.getName()
                self.selectClassgroup = String(userObjec.getId()) + "|" + userObjec.getName()
                }
            else if (tableView == SelectStudentClassTableView)
                {
                    let cell = self.SelectStudentClassTableView.cellForRow(at: indexPath) as! SelectSMS_StudentClassCell
                    let userObjec = userList1[indexPath.row]
                    cell.SelectSMS_StudentClassCheck.image = UIImage(named: "checkbox-active")
                    UserDefaults.standard.set("1", forKey: "CLASSCHECK")
                    self.SelectClassLayout?.titleLabel?.text = String(userObjec.getId()) + " | " + userObjec.getName()
                    self.selectClass = String(userObjec.getId()) + "|" + userObjec.getName()
            }
            else if (tableView == SelectStudentDivisionTableView)
            {
                let cell = self.SelectStudentDivisionTableView.cellForRow(at: indexPath) as! SelectSMSStudent_DivisionCell
                 let userObjec2 = userList2[indexPath.row]
                cell.SelectSMS_StudentDivisionCheck.image = UIImage(named: "checkbox-active")
                 UserDefaults.standard.set("1", forKey: "DIVISIONCHECK")
                self.SelectDivisionLayout.titleLabel?.text = String(userObjec2.classId()) + " | " + userObjec2.classDivisionId()
                self.selectDivision = String(userObjec2.classId()) + "|" + userObjec2.classDivisionId()
                self.ClassDivisionIdlasttext = userObjec2.classDivisionId()?.last?.description // as String
                
                self.divisionlabltext_forSendApi = userObjec2.classDivisionId() + "|" + selectDivision! + "|" + selectClass + "|" + selectClassgroup + "|" + userObjec2.classAliase()
                
                
               // INTELL7A|A|7|IV|2|Primary|Intellischools
            }
        }
        func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if (tableView == SelectMsgTableView)
        {
        let cell = self.SelectMsgTableView.cellForRow(at: indexPath) as! SelectSMS_messageTypeCell
            cell.SelectMsgTypeCheck.image = UIImage(named: "checkbox-inactive")
        }
         else if (tableView == SelectReceiversTableView)
            {
            let cell = self.SelectReceiversTableView.cellForRow(at: indexPath) as! SelectReceiversGroupCell
                cell.SelectReceiversCheck.image = UIImage(named: "checkbox-inactive")
            }
            else if (tableView == SelectClassGroupTableview)
            {
            let cell = self.SelectClassGroupTableview.cellForRow(at: indexPath) as! SelectSMS_studentClassGroup
                cell.SelectStudentClassGroupCheck.image = UIImage(named: "checkbox-inactive")
            }
            else if (tableView == SelectStudentClassTableView)
            {
                       let cell = self.SelectStudentClassTableView.cellForRow(at: indexPath) as! SelectSMS_StudentClassCell
                cell.SelectSMS_StudentClassCheck.image = UIImage(named: "checkbox-inactive")
            }
            else if (tableView == SelectStudentDivisionTableView)
            {
                       let cell = self.SelectStudentDivisionTableView.cellForRow(at: indexPath) as! SelectSMSStudent_DivisionCell
                cell.SelectSMS_StudentDivisionCheck.image = UIImage(named: "checkbox-inactive")
            }
        }
        @IBAction func SelectMsgTypeOkBtn(_ sender: Any) {
          if (!ConnectionCheck.isConnectedToNetwork())
                   {
                       self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                   }
                   else
                   {
                   if(UserDefaults.standard.string(forKey: "MESSAGECHECK") == "0")
                   {
                       self.view.makeToast(message: "Please Select Message Type.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                   }
                   else
                   {
                     /*  if(UserDefaults.standard.string(forKey: "MESSAGECHECK") == "1")
                       {
                           SelectGroupOutlet.titleLabel?.text = "CLASS|CLASSGROUP"
                           let storyboard = UIStoryboard(name: "Main", bundle: nil)
                           let controller = storyboard.instantiateViewController(withIdentifier: "Compose_MessageVC")
                           self.present(controller, animated: false, completion: nil)
                       }
                       else if(UserDefaults.standard.string(forKey: "GROUP_CHECK") == "2")
                       {
                           SelectGroupOutlet.titleLabel?.text = "CLASS|SPECIFICGROUP"
                           
                       }*/
                   self.imgViewTransparent.isHidden = true
                   self.SelectMsgTypePopup.isHidden = true
                   }
                   }
               }
        @IBAction func SelectReceiversOkButton(_ sender: Any) {
            if (!ConnectionCheck.isConnectedToNetwork())
            {
                self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
            else
            {
            if(UserDefaults.standard.string(forKey: "RECEIVERCHECK") == "0")
            {
                self.view.makeToast(message: "Please Select Receiver's Group.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
            }
            else
            {
                if(UserDefaults.standard.string(forKey: "RECEIVERCHECKOPTION") == "1" && UserDefaults.standard.string(forKey: "MESSAGECHECKOPTION") == "1")
                {
                //    SelectGroupOutlet.titleLabel?.text = "CLASS|CLASSGROUP"
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "ComposeSmsGroupStudentVC")
                    self.present(controller, animated: false, completion: nil)
                }
                else if(UserDefaults.standard.string(forKey: "RECEIVERCHECKOPTION") == "1" && UserDefaults.standard.string(forKey: "MESSAGECHECKOPTION") == "2")
                 {
                //     SelectGroupOutlet.titleLabel?.text = "CLASS|CLASSGROUP"
                     let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     let controller = storyboard.instantiateViewController(withIdentifier: "ComposeSMS_IndividualStudentVC")
                     self.present(controller, animated: false, completion: nil)
                 }
                else if(UserDefaults.standard.string(forKey: "RECEIVERCHECKOPTION") == "2" && UserDefaults.standard.string(forKey: "MESSAGECHECKOPTION") == "1")
                {
                //    SelectGroupOutlet.titleLabel?.text = "CLASS|CLASSGROUP"
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "ComposeSMS_GroupTeachingStaff")
                    self.present(controller, animated: false, completion: nil)
                    
                }
                 else if(UserDefaults.standard.string(forKey: "RECEIVERCHECKOPTION") == "2" && UserDefaults.standard.string(forKey: "MESSAGECHECKOPTION") == "2")
                 {
             //        SelectGroupOutlet.titleLabel?.text = "CLASS|CLASSGROUP"
                     let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     let controller = storyboard.instantiateViewController(withIdentifier: "ComposeSMS_TeachingFunctionalityVC")
                     self.present(controller, animated: false, completion: nil)
                     
                 }
                 else if(UserDefaults.standard.string(forKey: "RECEIVERCHECKOPTION") == "3" && UserDefaults.standard.string(forKey: "MESSAGECHECKOPTION") == "1")
                 {
               //      SelectGroupOutlet.titleLabel?.text = "CLASS|CLASSGROUP"
                     let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     let controller = storyboard.instantiateViewController(withIdentifier: "ComposeSMS_GroupNonTeachingStaffVC")
                     self.present(controller, animated: false, completion: nil)
                     
                 }
                 else if(UserDefaults.standard.string(forKey: "RECEIVERCHECKOPTION") == "3" && UserDefaults.standard.string(forKey: "MESSAGECHECKOPTION") == "2")
                             {
                          //       SelectGroupOutlet.titleLabel?.text = "CLASS|CLASSGROUP"
                                 let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                 let controller = storyboard.instantiateViewController(withIdentifier: "ComposeSmsNonteachingFunctionalityVC")
                                 self.present(controller, animated: false, completion: nil)
                                 
                             }
                 else if(UserDefaults.standard.string(forKey: "RECEIVERCHECKOPTION") == "4" && UserDefaults.standard.string(forKey: "MESSAGECHECKOPTION") == "1")
                 {
       //              SelectGroupOutlet.titleLabel?.text = "CLASS|CLASSGROUP"
                     let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     let controller = storyboard.instantiateViewController(withIdentifier: "ComposeSMS_Group_ToNumberVC")
                     self.present(controller, animated: false, completion: nil)
                     
                 }
                 else if(UserDefaults.standard.string(forKey: "RECEIVERCHECKOPTION") == "4" && UserDefaults.standard.string(forKey: "MESSAGECHECKOPTION") == "2")
                             {
                     //            SelectGroupOutlet.titleLabel?.text = "CLASS|CLASSGROUP"
                                 let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                 let controller = storyboard.instantiateViewController(withIdentifier: "ComposeSMS_Individual_ToNumber")
                                 self.present(controller, animated: false, completion: nil)
                                 
                             }
                 
            self.imgViewTransparent.isHidden = true
            self.SelectReceiversPopupView.isHidden = true
            }
            }
        }
        @objc func Homework_imgView(tapGestureRecognizer: UITapGestureRecognizer)
        {
           if (!ConnectionCheck.isConnectedToNetwork())
                  {
                      self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                  }
                  else
                  {
                      UserDefaults.standard.set("0", forKey: "GROUP_CHECK")
                      UserDefaults.standard.set("0", forKey: "CLASSGROUPCHECK")
                      UserDefaults.standard.set("0", forKey: "CLASSCHECK")
                      UserDefaults.standard.set("0", forKey: "DIVISIONCHECK")
                      UserDefaults.standard.set("0", forKey: "SUBCHECK")
                      
                           let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                 let controller = storyboard.instantiateViewController(withIdentifier: "Compose_HomeworkVC")
                                 self.present(controller, animated: false, completion: nil)
                  }
        }
        @objc func Msg_imgView(tapGestureRecognizer1: UITapGestureRecognizer)
        {
           if (!ConnectionCheck.isConnectedToNetwork())
                  {
                      self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                  }
                  else
                  {
                  let storyboard = UIStoryboard(name: "Main", bundle: nil)
                  let controller = storyboard.instantiateViewController(withIdentifier: "Compose_MessageVC")
                  self.present(controller, animated: false, completion: nil)
                  }
        }
        @objc func Circular_imgView(tapGestureRecognizer2: UITapGestureRecognizer)
           {
               if (!ConnectionCheck.isConnectedToNetwork())
               {
                   self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
               }
               else
               {
               let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let controller = storyboard.instantiateViewController(withIdentifier: "ComposeCircularVC")
               self.present(controller, animated: false, completion: nil)
               }
           }
        @objc func SMS_imgView(tapGestureRecognizer3: UITapGestureRecognizer)
            {
              if (!ConnectionCheck.isConnectedToNetwork())
                     {
                         self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
                     }
                     else
                     {
                     let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let controller = storyboard.instantiateViewController(withIdentifier: "Compose_SMSVC")
                            self.present(controller, animated: false, completion: nil)
                     }
            }
    
    @IBAction func SelectGroupBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        if(UserDefaults.standard.string(forKey: "RECEIVERCHECK") == "1")
        {
            self.imgViewTransparent.frame = CGRect(x: 0, y: 0, width: ComposeSmsGroupStudentVC.screenSize.width, height: ComposeSmsGroupStudentVC.screenSize.height)
                            
            self.imgViewTransparent.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                            
            self.view.addSubview(self.imgViewTransparent)
            self.imgViewTransparent.isHidden = false
            self.imgViewTransparent.addSubview(self.SelectClassGroupPopupView)
            self.SelectClassGroupPopupView.isHidden = false
        }
        
        else
        {
            self.view.makeToast(message: "Please Select Receiver's Group first.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        }
    }
    
    @IBAction func SelectClassButton(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
                  
                  self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
              }
              else
              {
              UserDefaults.standard.set("0", forKey: "CLASSCHECK")
              if(UserDefaults.standard.string(forKey: "CLASSGROUPCHECK") == "0")
              {
                  self.view.makeToast(message: "Please Select ClassGroup first.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
              }
              else
              {
                  self.imgViewTransparent.frame = CGRect(x: 0, y: 0, width: ComposeSmsGroupStudentVC.screenSize.width, height: ComposeSmsGroupStudentVC.screenSize.height)
                                  
                  self.imgViewTransparent.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                                  
                  self.view.addSubview(self.imgViewTransparent)
                  self.imgViewTransparent.isHidden = false
                  self.imgViewTransparent.addSubview(self.SelectStudentClassPopupView)
                  self.SelectStudentClassPopupView.isHidden = false
              }
              }
    }
    @IBAction func SelectDivisionBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        UserDefaults.standard.set("0", forKey: "DIVISIONCHECK")
        if(UserDefaults.standard.string(forKey: "CLASSCHECK") == "0")
        {
            self.view.makeToast(message: "Please Select Class first.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
            self.imgViewTransparent.frame = CGRect(x: 0, y: 0, width: ComposeSmsGroupStudentVC.screenSize.width, height: ComposeSmsGroupStudentVC.screenSize.height)
                            
            self.imgViewTransparent.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                            
            self.view.addSubview(self.imgViewTransparent)
            self.imgViewTransparent.isHidden = false
            self.imgViewTransparent.addSubview(self.SelectStudentDivisionPopupView)
            self.SelectStudentDivisionPopupView.isHidden = false
        }
        }
    }
    @IBAction func SelectStudentGroupOkBtn(_ sender: Any) {
   
        if (!ConnectionCheck.isConnectedToNetwork()) {
                  
                  self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
              }
              else
              {
           if(UserDefaults.standard.string(forKey: "CLASSGROUPCHECK") == "1")
           {
              
              self.imgViewTransparent.isHidden = true
              self.SelectClassGroupPopupView.isHidden = true
              
              let parameter1 = [
                         "schoolCode" : "INTELL",
                         "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MTY1NTI3NCwiZXhwIjoxNTcxNjU4ODc0LCJuYmYiOjE1NzE2NTUyNzQsImp0aSI6IkhHVjM1VEhNczluSVJsS3AiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.42-1tkg8oGcrKAEwvp_yZdc2zn8nAOJ7BugTtfSsCLQ",
                          "academicYear" : "2019-2020",
                          "groupValue" : String(selectClassgroup)
                         ] as [String : Any]
                     
                  print("parameter for classByClassgroup:\(parameter1)")
                     Alamofire.request(url1, method: .post, parameters: parameter1).responseJSON {
                         response in
                         print("Response for classByClassgroup : \(response)")
                         guard let value = response.result.value as? [String : Any] else {
                             return
                         }
                      
                         switch response.result {
                             
                         case .success:
                          
                             let jsondataCopy = JSON(value)
                             let data: [NSDictionary] = jsondataCopy["data"].arrayObject as! [NSDictionary]
                             print("data>>\(data)")
                             if(self.userList1 != nil)
                             {
                             self.userList1.removeAll()
                             }
                             for item in data {
                             let classes = item.value(forKey: "classes")
                                print("name >>: \(classes!)")
                                
                                let classes2 = JSON(classes)
                                let data2: [NSDictionary] = classes2.arrayObject as! [NSDictionary]
                                print("data>>\(data)")
                                for item in data2
                                {
                                        let classId = item.value(forKey: "classId")
                                        print("classId >>: \(classId!)")
                                        let className = item.value(forKey: "className")
                                        print("className >>: \(className!)")
                                    
                                    
                                    let userObj1 = Class_DataModel(First_Lable: className! as? String, Second_Lable: classId as! Int)
                                    
                                    
                                    print("Data.count : \(self.userList1.count)")
                                    
                                    self.userList1.append(userObj1)
                                }
                            
                             }
                            self.SelectStudentClassTableView.reloadData()
                          
                          
                      case .failure(_):
                          print("")
                      
                  }
              }
              
           }
           else{
              self.view.makeToast(message: "Please Select Class Group Type.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
              }
              }
    }
    
    @IBAction func SelectStudentClassPopupOkBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
                  
                  self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
              }
              else
              {
              if(UserDefaults.standard.string(forKey: "CLASSCHECK") == "1")
              {
                  self.imgViewTransparent.isHidden = true
                  self.SelectStudentClassPopupView.isHidden = true
                  

                  let parameter2 = [
                                    "schoolCode" : "INTELL",
                                    "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MTY1NTI3NCwiZXhwIjoxNTcxNjU4ODc0LCJuYmYiOjE1NzE2NTUyNzQsImp0aSI6IkhHVjM1VEhNczluSVJsS3AiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.42-1tkg8oGcrKAEwvp_yZdc2zn8nAOJ7BugTtfSsCLQ",
                                     "academicYear" : "2019-2020",
                                     "classValue" : String(selectClass)
                                    ] as [String : Any]
                                
                             print("parameter for divisionByClasse:\(parameter2)")
                                Alamofire.request(url2, method: .post, parameters: parameter2).responseJSON {
                                    response in
                                    print("Response for divisionByClasse : \(response)")
                                    guard let value = response.result.value as? [String : Any] else {
                                        return
                                    }
                                 
                                    switch response.result {
                                        
                                    case .success:
                                     
                                        let jsondataCopy = JSON(value)
                                        let data: [NSDictionary] = jsondataCopy["data"].arrayObject as! [NSDictionary]
                                        print("data>>\(data)")
                                        if(self.userList2 != nil)
                                        {
                                        self.userList2.removeAll()
                                        }
                                        for item in data {
                                        let divison = item.value(forKey: "divison")
                                           print("name >>: \(divison!)")
                                           
                                           let divison2 = JSON(divison)
                                           let data2: [NSDictionary] = divison2.arrayObject as! [NSDictionary]
                                           print("data2>>\(data2)")
                                           for item in data2
                                           {
                                              
                                                  let classId = item.value(forKey: "classId")
                                                  print("classId >>: \(classId!)")
                                                  let classDivisionId = item.value(forKey: "classDivisionId")
                                                  print("classDivisionId >>: \(classDivisionId!)")
                                                  let className = item.value(forKey: "className")
                                                  print("className >>: \(className!)")
                                                  let classGroupId = item.value(forKey: "classGroupId")
                                                  print("classGroupId >>: \(classGroupId!)")
                                                  let classGroupName = item.value(forKey: "classGroupName")
                                                  print("classGroupName >>: \(classGroupName!)")
                                                  let classAliase = item.value(forKey: "classAliase")
                                                  print("classAliase >>: \(classAliase!)")
                                               
                                               let userObj2 = Division_DataModel(First_Lable: classDivisionId! as? String, Second_Lable: classId as! Int, Third_Lable: className! as? String, Fourth_Lable: classGroupId as! Int,Fifth_Lable: classGroupName! as? String, Sixth_Lable: classAliase! as? String )
                                               
                                               
                                               print("Data.count : \(self.userList2.count)")
                                               
                                               self.userList2.append(userObj2)
                                           }
                                       
                                        }
                                       self.SelectStudentDivisionTableView.reloadData()
                                     
                                     
                                 case .failure(_):
                                     print("")
                                 
                             }
                         }
              }
              else
              {
                   self.view.makeToast(message: "Please Select Class.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
              }
              }
    }
    @IBAction func SelectStudentDivisionPopupOkBtn(_ sender: Any) {
        if (!ConnectionCheck.isConnectedToNetwork()) {
            
            self.view.makeToast(message: "No Internet Connection", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        else
        {
        if(UserDefaults.standard.string(forKey: "DIVISIONCHECK") == "1")
        {
            self.imgViewTransparent.isHidden = true
            self.SelectStudentDivisionPopupView.isHidden = true
        }
        else
        {
             self.view.makeToast(message: "Please Select Division.", duration: 0.5, position: HRToastPositionCenter as AnyObject)
        }
        }
    }
}
