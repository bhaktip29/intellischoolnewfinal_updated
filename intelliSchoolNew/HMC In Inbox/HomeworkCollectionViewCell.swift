//
//  HomeworkCollectionViewCell.swift
//  intelliSchoolNew
//
//  Created by rupali  on 09/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class HomeworkCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var teacherName: UILabel!
    @IBOutlet weak var firstLetterLbl: UILabel!
    @IBOutlet weak var textView: UITextView!
}
