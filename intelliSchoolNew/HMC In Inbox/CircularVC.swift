//
//  CircularVC.swift
//  Intellinects
//
//  Created by rupali  on 09/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class CircularVC: BaseViewController , UICollectionViewDataSource , UICollectionViewDelegate {
    var datechange : String!
    var InfoList = [circular]()
    var date : String!
    @IBOutlet weak var collectionView: UICollectionView!
    var names = ["label","label","label","label","label","label","label","label","label","label","label","label","label","label","label","label","label",]

    override func viewDidLoad() {
        super.viewDidLoad()
        getNewsData()
        // Do any additional setup after loading the view.
    }
    

    func getNewsData()
    {
        var url : String = ""
        
        if Util.validateNetworkConnection(self){
            showActivityIndicatory(uiView: view)
            let sendDate = "2015-01-01"
            //            url =           "https://isirs.org/intellinect_dashboard/WS/index_main.php?action=GetCircular&SCHOOL_ID=8&deviceId=869447038367960&userId=266&role=Father"
            url = "http://commn.intellischools.com/hmcs_angular_api/public/api/circularList"
            let parameters = ["schoolCode" : "INTELL" ,
                              "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MjU5NjUxMiwiZXhwIjoxNTcyNjAwMTEyLCJuYmYiOjE1NzI1OTY1MTIsImp0aSI6ImZhamVkWlVLaGNYZlA0aksiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.7R4mHCUqizZpuOODJ2WhO9TALqOWdboA1D7sTOdRM1Q",
                              "is_academicYear" : "2019-2020"
                
                ] as [String:Any]
            Alamofire.request(url,method: .post,parameters: parameters)
                .responseJSON {[weak self] (response) in
                    self?.stopActivityIndicator()
                    if response.result.error != nil {
                        
                    }
                    print("response.reslt>>>>\(response.result)")
                    switch response.result {
                    case .success:
                        if let jsonresponse = response.result.value {
                            let originalResponse = JSON(jsonresponse)
                            print("Homeworkresponse1>>>>\(originalResponse)")
                            let data = originalResponse["data"]
                            let recordsArray: [NSDictionary] = data.arrayObject as! [NSDictionary]
                            
                            
                            print("recordsArray>>>>\(recordsArray)")
                            
                            
                            for item in recordsArray {
                                
                                
                                let postDate = item.value(forKey: "createdDate")
                                let fName = item.value(forKey: "fname")
                                let lName = item.value(forKey: "lname")
                                let title = item.value(forKey: "title")
                                let postContent = item.value(forKey: "msgContent")
                                let postTitle = item.value(forKey: "title")
                                let teacher1 = item.value(forKey: "teacher")
                                // let date = item.value(forKey: "postDate")
                                // var changeddate = self!.changeDate(date as! String)
                                print("postDate>>>>>\(postDate)")
                                print("postContent>>>> \(postContent!)")
                                print("title>>>>>\(title)")
                                print("fName>>>>>\(fName)")
                                
                                
                                //print("android_version \(postDate!)")
                                
                                
                                // date formatter
                                let dateFormatterGet = DateFormatter()
                                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                
                                let dateFormatterPrint = DateFormatter()
                                dateFormatterPrint.dateFormat = "MMM d"
                                self!.date = (postDate as! String)
                                if let date = dateFormatterGet.date(from: self!.date)
                                {
                                    
                                    //                                if let date = dateFormatterGet.date(from: "2016-02-29 12:24:26") {
                                    print("dateformaterrr>>>\(dateFormatterPrint.string(from: date))")
                                    self!.datechange = dateFormatterPrint.string(from: date)
                                }
                                else
                                {
                                    print("There was an error decoding the string")
                                }
                                
                                let Info = circular(postDate: self!.datechange as! String, postContent: postContent as! String , postTitle: postTitle as! String, fName: fName as! String, lName: lName as! String)
                                self!.InfoList.append(Info)
                                print("newsListforhomework>>>>>>>>>>>>>>>\(self!.InfoList)")
                                
                                
                            }
                            self?.collectionView.reloadData()
                        }
                    case .failure(_):
                        print("switch case erroe")
                    }
            }
        }
        
        
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return InfoList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "circular", for: indexPath) as! NewsCollectionViewCell
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: "circular", for: indexPath) as! CircularCollectionViewCell
        var User = InfoList[indexPath.row]
        cell.lbl1.text = User.getpostContent()
        cell.lbl2.text = User.getpostDate()
        cell.postTitle.text = User.getpostTitle()
        
        let a = User.getpostTitle()
        let firstLtter = String(a!.prefix(1))
        print("firstLtterfirstLtter\(firstLtter)")
        
        //
        cell.Name.text = firstLtter
        // Cell.firstLetterLbl.layer.cornerRadius = Cell.firstLetterLbl.frame.width/2
        cell.Name.layer.cornerRadius =  cell.Name.frame.width/2
        cell.Name.layer.masksToBounds = true
        
        //
        let color1 = UIColor(hexString: "#8a638f")
        let color2 = UIColor(hexString: "#0bc1f0")
        let color3 = UIColor(hexString: "#ffa200")
        let bgColors = [color1,color2,color3]
        
        
        cell.Name.backgroundColor = bgColors[indexPath.row % bgColors.count]
        return cell
    }
    


}
