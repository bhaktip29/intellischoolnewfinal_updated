//
//  InboxVC.swift
//  Intellinects
//
//  Created by rupali  on 07/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
//import PolioPager
class InboxVC:
    
UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIScrollViewDelegate {
    //  @IBOutlet weak var segmentedControl: CustomSegmentedContrl!
    private var pageController: UIPageViewController!
    private var arrVC:[UIViewController] = []
    private var currentPage: Int!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    
    /// Instantiate ViewControllers Here With Lazy Keyword
    
    // MARK: Order ViewController
        lazy var vc1: homewrokDashboardViewController = {
            
            
         
            var viewController = storyboard?.instantiateViewController(withIdentifier: "homewrokDashboardViewController") as! homewrokDashboardViewController
            
            //self.addViewControllerAsChildViewController(childViewController: viewController)
            
            return viewController
        }()
        
       
        
        lazy var vc2: MessageDashboardViewController = {
            
            var viewController = storyboard?.instantiateViewController(withIdentifier: "MessageDashboardViewController") as! MessageDashboardViewController
            //self.addViewControllerAsChildViewController(childViewController: viewController)
            return viewController
        }()
        lazy var vc3: CircularViewController = {
            
            var viewController = storyboard?.instantiateViewController(withIdentifier: "CircularViewController") as! CircularViewController
            //self.addViewControllerAsChildViewController(childViewController: viewController)
            
            return viewController
        }()
        

        lazy var vc4: SmsVC = {
            
            var viewController = storyboard?.instantiateViewController(withIdentifier: "view4") as! SmsVC
            //self.addViewControllerAsChildViewController(childViewController: viewController)
            return viewController
        }()
        
        override func viewDidLoad() {
            super.viewDidLoad()
        //   segmentedControl.changeUnderlinePosition()
            segmentedControl.addUnderlineForSelectedSegment()
            // segmentedControl.titleForSegment(at: 3) = "first"
            // Do any additional setup after loading the view, typically from a nib.
            // segmentedControl.layer.borderColor = UIColor.redC
            segmentedControl.backgroundColor = .clear
            segmentedControl.tintColor = .clear
            
            // we can add font for the buttons by using CustomSegmentedContrl pr on desktop
            segmentedControl.setTitleTextAttributes([
               NSAttributedString.Key.foregroundColor: UIColor.black
                ], for: .normal)
            
            segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.blue
                ], for: .selected)
            
            segmentedControl.backgroundColor = .white
          //  segmentedControl.addUnderlineForSelectedSegment([for: .selected])
            let buttonBar = UIView()
            // This needs to be false since we are using auto layout constraints
            // Below view.addSubview(segmentedControl)
            view.addSubview(buttonBar)
            buttonBar.translatesAutoresizingMaskIntoConstraints = false
         
            
            //start of underline code this lines are used to add underline
            
    //   buttonBar.backgroundColor = UIColor.orange
            
            // Constrain the top of the button bar to the bottom of the segmented control
    //        buttonBar.topAnchor.constraint(equalTo: segmentedControl.bottomAnchor).isActive = true
    //        buttonBar.heightAnchor.constraint(equalToConstant: 5).isActive = true
    //        // Constrain the button bar to the left side of the segmented control
    //        buttonBar.leftAnchor.constraint(equalTo: segmentedControl.leftAnchor).isActive = true
    //        // Constrain the button bar to the width of the segmented control divided by the number of segments
    //        buttonBar.widthAnchor.constraint(equalTo: segmentedControl.widthAnchor, multiplier: 1 / CGFloat(segmentedControl.numberOfSegments)).isActive = true
            
            
            //end of underline code
            ssss()
            currentPage = 0
            createPageViewController()
            
            arrVC.append(vc1)
            arrVC.append(vc2)
            arrVC.append(vc3)
            arrVC.append(vc4)
            
            
        }
    
    // var segmentedControl: CustomSegmentedContrl!
    
    func ssss() {
        
        segmentedControl.addTarget(self, action: #selector(onChangeOfSegment(_:)), for: .valueChanged)
  
    }
    //MARK: - CreatePagination
    
    private func createPageViewController() {
        
        pageController = UIPageViewController.init(transitionStyle: UIPageViewController.TransitionStyle.scroll, navigationOrientation: UIPageViewController.NavigationOrientation.horizontal, options: nil)
        
        pageController.view.backgroundColor = UIColor.clear
        pageController.delegate = self
        pageController.dataSource = self
        
        for svScroll in pageController.view.subviews as! [UIScrollView] {
            svScroll.delegate = self
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.pageController.view.frame = CGRect(x: 0, y: self.segmentedControl.frame.maxY, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            
        }
        
        // arrVC = [vc1, vc2, vc3]
        
        pageController.setViewControllers([vc1], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        
        self.addChild(pageController)
        self.view.addSubview(pageController.view)
        pageController.didMove(toParent: self)
    }
    
    
    private func indexofviewController(viewCOntroller: UIViewController) -> Int {
        if(arrVC .contains(viewCOntroller)) {
            return arrVC.index(of: viewCOntroller)!
        }
        
        return -1
    }
    
    
    //MARK: - Pagination Delegate Methods
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var index = indexofviewController(viewCOntroller: viewController)
        
        if(index != -1) {
            index = index - 1
        }
        
        if(index < 0) {
            return nil
        }
        else {
            return arrVC[index]
        }
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        var index = indexofviewController(viewCOntroller: viewController)
        
        if(index != -1) {
            index = index + 1
        }
        
        if(index >= arrVC.count) {
            return nil
        }
        else {
            return arrVC[index]
        }
        
    }
    
    func pageViewController(_ pageViewController1: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if(completed) {
            // segmentedControl.changeUnderlinePosition()
            currentPage = arrVC.index(of: (pageViewController1.viewControllers?.last)!)
            self.segmentedControl.selectedSegmentIndex = currentPage
            self.segmentedControl.changeUnderlinePosition()
          //  self.segmentedControl.addUnderlineForSelectedSegment()
            //     self.segmentedControl.updateSegmentedControlSegs(index: currentPage)
            
        }
        
    }
    
    
    
    @objc func onChangeOfSegment(_ sender: CustomSegmentedContrl) {}
    
    @IBAction func segmenttapped(_ sender: UISegmentedControl) {
        segmentedControl.changeUnderlinePosition()
      //  segmentedControl.addUnderlineForSelectedSegment()
        switch sender.selectedSegmentIndex {
        case 0:
            pageController.setViewControllers([arrVC[0]], direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: nil)
            currentPage = 0
            
        case 1:
            if currentPage > 1{
                pageController.setViewControllers([arrVC[1]], direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: nil)
                currentPage = 1
            }else{
                pageController.setViewControllers([arrVC[1]], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
                currentPage = 1
                
            }
        case 2:
            if currentPage < 2 {
                pageController.setViewControllers([arrVC[2]], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
                currentPage = 2
                
                
            }else{
                pageController.setViewControllers([arrVC[2]], direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: nil)
                currentPage = 2
                
            }
            case 3:
                if currentPage < 3 {
                    pageController.setViewControllers([arrVC[3]], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
                    currentPage = 3
                    
                    
                }else{
                    pageController.setViewControllers([arrVC[2]], direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: nil)
                    currentPage = 2
                    
                }
        default:
            break
        }
        
        
    }
    
    
}

extension UISegmentedControl{
//this two fun are used to add borders addUnderlineForSelectedSegment ,changeUnderlinePosition
    //https://stackoverflow.com/questions/42755590/how-to-display-only-bottom-border-for-selected-item-in-uisegmentedcontrol
    func addUnderlineForSelectedSegment(){
        //removeBorder()
        let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
        let underlineHeight: CGFloat = 2.0
        let underlineXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth))
        let underLineYPosition = self.bounds.size.height - 1.0
        let underlineFrame = CGRect(x: underlineXPosition, y: underLineYPosition, width: underlineWidth, height: underlineHeight)
        let underline = UIView(frame: underlineFrame)
        underline.backgroundColor = UIColor(red: 67/255, green: 129/255, blue: 244/255, alpha: 1.0)
        underline.tag = 1
        self.addSubview(underline)
    }
    
    func changeUnderlinePosition(){
        guard let underline = self.viewWithTag(1) else {return}
        let underlineFinalXPosition = (self.bounds.width / CGFloat(self.numberOfSegments)) * CGFloat(selectedSegmentIndex)
        UIView.animate(withDuration: 0.1, animations: {
            underline.frame.origin.x = underlineFinalXPosition
        })
    }
}

