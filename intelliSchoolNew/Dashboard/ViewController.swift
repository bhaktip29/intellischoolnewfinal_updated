//
//  ViewController.swift
//  Intellinects
//
//  Created by rupali  on 01/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Nuke
class ViewController: BaseViewController , UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout , UITableViewDelegate ,UITableViewDataSource, UITabBarControllerDelegate {
    var count : Int! = 0
    var List = [dashboard]()
    var servi : String!
    var celltext5 : String!
    var celltext = ["News", "Events" ,"Homework" ,"Message" , "Circular","BusTracking","StudentProfile","SMS"]
    var celltext2 = [dashboard2]()
    var tableviewArray = [ListOfRoles]()
    var tableviewArrayProfile = [ListOfRolesProfie]()
    var NamesAll = [String]()
    var name : String!
    var name1 : String!
    var name2 : String!
    var profile1 : String!
    var profile2 : String!
    static var isDashboardVC = false
    static var isParent = false
    var indexPath : IndexPath!
    static var isTC = false
    @IBOutlet weak var navBarProfile: UIImageView!
    @IBOutlet weak var Footer_Profile: UIButton!
    @IBOutlet weak var Footer_Compose: UIButton!
    @IBOutlet weak var Footer_ProfileLbl: UILabel!
    @IBOutlet weak var Footer_ComposeLbl: UILabel!
    @IBOutlet weak var viewForTabBar: UIView!
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var viewForNav: UIView!
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var viewForLogout: UIView!
    @IBOutlet weak var logoutImg: UIImageView!
    @IBOutlet weak var LogoutBtnTapped: UIButton!
    @IBOutlet weak var ListOfRolesLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var profileBtn: UIBarButtonItem!
    @IBOutlet weak var subViewForListOfRoles: UIView!
    @IBOutlet weak var subCollectionView: UICollectionView!
    let imgViewTransparent = UIView()
    static let screenSize = UIScreen.main.bounds
    @IBOutlet weak var subTableView: UITableView!
    let images = ["news","events","inbox","attendance","timetable","lms","evaluation","payment","sports","bus","healthcare","publishing","discover","Assess","examtimetable","faq","terms","ticket"]
    let names = ["news","events","inbox","attendance","timetable","lms","evaluation","payment","sports","bus","healthcare","publishing","discover","Assess","examtimetable","faq","terms","ticket"]
    //TableView
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("First Commit..")
        
        if ViewController.isTC == true
        {
            let controller = storyboard!.instantiateViewController(withIdentifier: "sliderVC")
            addChild(controller)
            //controller.view.frame = CGRect(x:0 , y: 0,width: self.view.frame.width,height: 0)
                        //CGRect(x: 0 , y: 0, width: UIScreen.main.bounds.size.width,
                                                  //  height: UIScreen.main.bounds.size.height-600) // or, better, turn off `translatesAutoresizingMaskIntoConstraints` and then define constraints for this subview
                    controller.view.widthAnchor.constraint(equalToConstant: 100).isActive = true
                        // flowHeightConstraint = controller.heightAnchor.constraint(equalToConstant: 30)
                        // flowHeightConstraint?.isActive = true
                    collectionView.addSubview(controller.view)
                    controller.didMove(toParent: self)
            ViewController.isTC = false

        }
        navBarProfile.layer.cornerRadius = navBarProfile.frame.size.width / 2;
        navBarProfile.clipsToBounds = true

        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let color1 = UIColor(hexString: "#f7f7f7")
        statusBarView.backgroundColor = color1
        view.addSubview(statusBarView)
        
        viewForLogout.addTopBorderWithColor(color: .lightGray, width: 1)
        ListOfRolesLbl.addBottomBorderWithColor(color: .lightGray, width: 1)
        self.subTableView.isHidden = true
        self.ListOfRolesLbl.isHidden = true
        self.imgViewTransparent.isHidden = true
        if(UserDefaults.standard.string(forKey: "COMPOSEON") == "0")
        {
        getData()
        self.Footer_Compose.isHidden = true
        self.Footer_ComposeLbl.isHidden = true
        self.Footer_Profile.isHidden = false
        self.Footer_ProfileLbl.isHidden = false
        
        self.Footer_Compose.isUserInteractionEnabled = false
        self.Footer_Profile.isUserInteractionEnabled = true
        }
        else
        {
         getData()
            self.Footer_Compose.isHidden = false
            self.Footer_ComposeLbl.isHidden = false
            self.Footer_Profile.isHidden = true
            self.Footer_ProfileLbl.isHidden = true
            
            self.Footer_Compose.isUserInteractionEnabled = true
            self.Footer_Profile.isUserInteractionEnabled = false
        }
        gettableviewdata2()
        let loginprofilename=UserDefaults.standard.value(forKey: "loginprofilename")
        print("loginprofilenameloginprofilenameloginprofilename\(loginprofilename)")
        print("NamesAllNamesAll\(NamesAll)")
        navigationBar.layer.cornerRadius = 10.0
        navigationBar.clipsToBounds = true
        var FULL_name = UserDefaults.standard.value(forKey: "full_name")
        print("FULL_namedashboard>>>>>>\(FULL_name)")
        imageview.image = UIImage(named: "ischool-logo442x148-1")
        tableView.dataSource = self
        tableView.delegate = self
        collectionView.dataSource = self
        collectionView.dataSource = self
        //collectionView.cell.(color: .red, width: 10)
        let collectionViewLayout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
         let getdbsetting_URL1 = "https://isirs.org/api" + "/v1.5/getdbsetting.php?"
        let getdbsetting_URL2 =  "mobile_no=" + UserDefaults.standard.string(forKey: "userId")!  + "&password=" + UserDefaults.standard.string(forKey: "password")!
        let getdbsetting_URL3 = "&school_id=" + GlobleConstants.schoolID
        let getdbsetting_URL = getdbsetting_URL1 + getdbsetting_URL2 + getdbsetting_URL3
        print("getdbsetting_URL api : (getdbsetting_URL)")
      Alamofire.request(getdbsetting_URL, method: .get, encoding: JSONEncoding.default)
        .responseJSON { response in

            print("Response for getdbsetting_URL : (response)")
          //  print("url1 (self.url1)")
            guard let value = response.result.value as? [String : Any] else {
                return
            }

             switch response.result
             {
                    case .success:
                    let data = value["data"] as? [String : Any]
           //         print("Response for getdbsetting data : (data)")
                    if ((response.result.value) != nil)
                    {

                     //   let jsondataCopy = JSON(data!)
                     //    print("jsondataCopy for getdbsetting_URL : (jsondataCopy)")
                        let recordsArray1 = data?["school_db_settings_array"] as? [String : Any]
                        print("recordsArray1 for sgetdbsetting_URL : (String(describing: recordsArray1))")

                   /*     let enc_settings = recordsArray1?["enc_settings"]
                        print("enc_settings for sgetdbsetting_URL : (String(describing: enc_settings))")
                        let enc_settings1 = enc_settings
                        print("enc_settings1 for sgetdbsetting_URL : (String(describing: enc_settings1))")
                        UserDefaults.standard.set(enc_settings1, forKey: "DB_SETTING")*/

                        let originalResponseEmployee = JSON(data!)
                        for (key,subJson):(String, JSON) in originalResponseEmployee {
                            if(key == "school_employee_class_setting_array"){
                                UserDefaults.standard.set(String(describing: subJson), forKey: "SUBJECT_SETTINGS")
                            }
                            else if(key == "school_db_settings_array"){
                                UserDefaults.standard.set(String(describing: subJson), forKey: "DB_SETTING")
                                UserDefaults.standard.set(String(describing: subJson), forKey: "SCHOOL_DB_SETTINGS")
                            }
                        }
                    }
                    case .failure(_):
                            print("")
                    }
        }

    }
    func getData()
    {
        var url : String = ""
        if Util.validateNetworkConnection(self){
            showActivityIndicatory(uiView: view)
            let sendDate = "2015-01-01"
            url = "https://apps.intellinects.com/TestApi/public/api/featuresBySchoolCode"
            let parameters =   [
                "product" : "Intellischools" ,
                "schoolCode" : "INTELL"
            ]
            Alamofire.request(url,method: .post,parameters: parameters)
                .responseJSON {[weak self] (response) in
                    self?.stopActivityIndicator()
                    if response.result.error != nil {
                    }
                    print("response.result>>>>\(response.result)")
                    switch response.result {
                    case .success:
                        if let jsonresponse = response.result.value {
                            let originalResponse = JSON(jsonresponse)
                            let recordsArray: [NSDictionary] = originalResponse.arrayObject as! [NSDictionary]
                            print("responseFORDASHBOARD>>>>\(originalResponse)")
                            print("recordsArray>>>>\(recordsArray)")
                            for item in recordsArray {
                                var serviceName = item.value(forKey: "serviceName")
                                let features = item.value(forKey: "features") as?  [NSDictionary]
                                var schoolCode = item.value(forKey: "schoolCode")
                                UserDefaults.standard.set(schoolCode, forKey: "schoolCode")
                                print("schoolCode >> \(schoolCode)")
                                print("features\(features)")
                                print("serviceName>>>>>\(serviceName)")
                                for item2 in features! {
                                    let icon = item2.value(forKey: "icon")
                                    var serviceName = item2.value(forKey: "serviceName")
                                    UserDefaults.standard.set(serviceName, forKey: "serviceName")
                                    self!.servi =   UserDefaults.standard.value(forKey: "serviceName") as! String
                                    print("servi >>>> \(self!.servi)")
                                    print("icon  : >>> \(icon)")
                                    print("serviceName : >>> \(serviceName)")
                                    if serviceName as! String == "HMCS"
                                      {
                                        serviceName = "Inbox"
                                    }
                                    else{
                                        print("hmcs not found")
                                    }
                                    if(UserDefaults.standard.string(forKey: "parent") == "0")
                                    {
                                        self!.celltext.append(serviceName as! String)

                                      if let index = self!.celltext.index(of: "Intelliadmissions")  {
                                          self!.celltext.remove(at: index)
                                    }
                                      if let index = self!.celltext.index(of: "Publishing")  {
                                          self!.celltext.remove(at: index)
                                    }
                                      if let index = self!.celltext.index(of: "Sports")  {
                                          self!.celltext.remove(at: index)
                                    }
                                      if let index = self!.celltext.index(of: "LMS")  {
                                          self!.celltext.remove(at: index)
                                    }
                                      if let index = self!.celltext.index(of: "Intellipay")  {
                                          self!.celltext.remove(at: index)
                                    }
                                      if let index = self!.celltext.index(of: "Evaluation")  {
                                          self!.celltext.remove(at: index)
                                    }
                                     
                                     self!.celltext.removeDuplicates()

                                      //  self!.celltext.append(serviceName as! String)
                                        self!.collectionView.reloadData()

                                    }
                                    else{
                                        self!.celltext.append(serviceName as! String)
                                        self!.celltext.removeDuplicates()

                                        self!.collectionView.reloadData()

                                    }
                                    let postinfo = dashboard(serviceName: serviceName as? String, icon: icon as? String)
                                    if let index = self!.celltext.index(of: "Intelliadmissions")  {
                                        self!.celltext.remove(at: index)
                                  }
                                    if let index = self!.celltext.index(of: "Publishing")  {
                                        self!.celltext.remove(at: index)
                                  }
                                    if let index = self!.celltext.index(of: "Sports")  {
                                        self!.celltext.remove(at: index)
                                                                   }
                               //     self!.celltext.append(serviceName as! String)
                                    
                                   // if serviceName == "HMCS"
                                    self!.List.append(postinfo)
                                    self?.collectionView.reloadData()
                                }}}
                    case .failure(_):
                        print("switch case erroe")
                    }
            }
        }
    }
   // CollectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return celltext.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! DashboardCollectionViewCell
        Cell.label.text =  celltext[indexPath.row]
        celltext5 = Cell.label.text
        if Cell.label.text == "News"
        {
            Cell.image.image = UIImage(named: "News")
        }
        else if Cell.label.text == "Homework"
        {
            Cell.image.image = UIImage(named: "Homework")
        }
        else if Cell.label.text == "Message"
        {
            Cell.image.image = UIImage(named: "Message")
        }
        else if Cell.label.text == "Feepayment"
        {
            Cell.image.image = UIImage(named: "Feepayment")
        }
        else if Cell.label.text == "Intellischool"
        {
            Cell.image.image = UIImage(named: "Intellischool")
        }
        else if Cell.label.text == "Publishing"
        {
            Cell.image.image = UIImage(named: "Publishing")
        }
        else if Cell.label.text == "TimeTable"
        {
            Cell.image.image = UIImage(named: "TimeTable")
        }
        else if Cell.label.text == "Sports"
        {
            Cell.image.image = UIImage(named: "Sports")
        }
        else if Cell.label.text == "LMS"
        {
            Cell.image.image = UIImage(named: "LMS")
        }
        else if Cell.label.text == "Inbox"
        {
            Cell.image.image = UIImage(named: "Inbox")
        }
        else if Cell.label.text == "Evaluation"
        {
            Cell.image.image = UIImage(named: "Evaluation")
        }
        else if Cell.label.text == "Circular"
        {
            Cell.image.image = UIImage(named: "Circular")
        }
        else if Cell.label.text == "Events"
        {
            Cell.image.image = UIImage(named: "Event")
        }
        else if Cell.label.text == "Intelliadmissions"
        {
            Cell.image.image = UIImage(named: "assess")
        }
        else if Cell.label.text == "Attendance"
        {
            Cell.image.image = UIImage(named: "attendance")
        }
        else if Cell.label.text == "Intellipay"
        {
            Cell.image.image = UIImage(named: "payment")
        }
        else if Cell.label.text == "HMCS"
        {
            Cell.image.image = UIImage(named: "Inbox")
        }
        else if Cell.label.text == "BusTracking"
        {
            Cell.image.image = UIImage(named: "bus")
        }
        else if Cell.label.text == "StudentProfile"
        {
            Cell.image.image = UIImage(named: "Circular")
        }
        else if Cell.label.text == "TimetableBeta"
        {
            Cell.image.image = UIImage(named: "")
        }
        else if Cell.label.text == "SMS"
        {
            Cell.image.image = UIImage(named: "Message")
        }
            else if Cell.label.text == "teacherClassDivVC"
            {
                Cell.image.image = UIImage(named: "ic_student_profile")
            }
        else
        {
            print ("Image not found")
        }
        Cell.btnTapped.setTitle(celltext[indexPath.row], for: .normal)
        Cell.btnTapped.setTitleColor(.clear, for: .normal)
        Cell.btnTapped.addTarget(self, action: #selector(btntappedAction), for: .touchUpInside)
        Cell.btnTapped.tag = indexPath.row
        Cell.layer.cornerRadius = 5.0
        Cell.clipsToBounds = true
        return Cell
    }
       func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var cell = collectionView.cellForItem(at: indexPath) as! DashboardCollectionViewCell?
        print("didSelectItemAtdidSelectItemAt")

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let bounds = collectionView.bounds
        return CGSize(width: bounds.width/2-11.5, height: self.collectionView.frame.width/4)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        //horizontal spacing
        return 1.5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    @objc func btntappedAction(sender: UIButton){
        var employee_role_id = UserDefaults.standard.integer(forKey: "EMPLOYEE_ROLE_ID")
        print("btntappedActionbtntappedActionbtntappedActionbtntappedAction")
        let tag = sender.tag
        print("sender.titleLabelsender.titleLabel\(sender.titleLabel?.text)")
        var title = sender.titleLabel
        print("celltext2celltext2celltext2celltext2celltext2>>>>>\(celltext2)")
      switch sender.titleLabel?.text {
        case "News":
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "News")
            self.present(controller, animated: true, completion: nil)
        case "Events":
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "Event")
            self.present(controller, animated: true, completion: nil)
        case "Homework":
            UserDefaults.standard.set("0", forKey: "isdashbordVC")
               UserDefaults.standard.set("1", forKey: "HMC")
               if(UserDefaults.standard.string(forKey: "parent") != "0") // For Parent old api
               {
                   ViewController.isDashboardVC = true
                   let vc = UINavigationController(rootViewController: HCMTableVC())
                   present(vc, animated: true, completion: nil)
               }
               else   // For Employee New api
               {
                   // Employee Old API
                   if(employee_role_id == 1 || employee_role_id == 2 || employee_role_id == 4 || employee_role_id == 5)
                   {
                       print("$ Employee Old API")
                        let vc = UINavigationController(rootViewController: BaseClassControllerTeacher())
                    //       vc.caseNumber = indexPath.row + 1
                           present(vc, animated: true, completion: nil)
                   }
                   else if employee_role_id ==  3
                   {

                        print("$$ Employee Old API")
                       let vc = UINavigationController(rootViewController:  MenuListVCTeacher())
                     //      vc.caseNumber = indexPath.row + 1
                           present(vc, animated: true, completion: nil)
                   }
                   else
                   {
                          displayNoPermissionAlert()
                   }
                   // Employee New API .....
            /*   let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let controller = storyboard.instantiateViewController(withIdentifier: "homewrokDashboardViewController")
               ViewController.isDashboardVC = true
               self.present(controller, animated: true, completion: nil)*/
               }
            
        case "Inbox":
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "InboxVC")
            ViewController.isDashboardVC = false
            UserDefaults.standard.set("0", forKey: "isdashbordVC")

            self.present(controller, animated: true, completion: nil)
            
        case "Message":
          ViewController.isDashboardVC = true
               UserDefaults.standard.set("0", forKey: "isdashbordVC")
               UserDefaults.standard.set("2", forKey: "HMC")
               if(UserDefaults.standard.string(forKey: "parent") != "0") // For Parent old api
               {
                   let vc = UINavigationController(rootViewController: HCMTableVC())
                   present(vc, animated: true, completion: nil)
               }
               else
               {
                 // Employee Old API
                if(employee_role_id == 1 || employee_role_id == 2 || employee_role_id == 4 || employee_role_id == 5)
                {
                     print("$ Employee Old API")
                      let vc = UINavigationController(rootViewController: BaseClassControllerTeacher())
                  //       vc.caseNumber = indexPath.row + 1
                         present(vc, animated: true, completion: nil)
                 }
                else if employee_role_id ==  3
                {
                      print("$$ Employee Old API")
                     let vc = UINavigationController(rootViewController: MenuListVCTeacher())
                   //      vc.caseNumber = indexPath.row + 1
                         present(vc, animated: true, completion: nil)
                 }
                 else
                {
                     displayNoPermissionAlert()
                 }
                 // Employee New API .....
            /*   let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let controller = storyboard.instantiateViewController(withIdentifier: "MessageDashboardViewController")
               self.present(controller, animated: true, completion: nil)*/
               }

        case "Circular":
          //     let employee_role_id = 3 //Int(userDefaults.string(forKey: "EMPLOYEE_ROLE_ID")!)
            ViewController.isDashboardVC = true
            UserDefaults.standard.set("0", forKey: "isdashbordVC")
            UserDefaults.standard.set("3", forKey: "HMC")
            if(UserDefaults.standard.string(forKey: "parent") != "0") // For Parent old api
            {
                let vc = UINavigationController(rootViewController: HCMTableVC())
                present(vc, animated: true, completion: nil)
            }
            else
            {
              // Employee Old API
             if(employee_role_id == 1 || employee_role_id == 2 || employee_role_id == 4)
             {
                  print("$ Employee Old API")
                   let vc = UINavigationController(rootViewController: BaseClassControllerTeacher())
               //       vc.caseNumber = indexPath.row + 1
                      present(vc, animated: true, completion: nil)
              }
             else
              {
                   print("$$ Employee Old API")
                  let vc = UINavigationController(rootViewController: MenuListVCTeacher())
                //      vc.caseNumber = indexPath.row + 1
                      present(vc, animated: true, completion: nil)
              }

            // Employee New API .....
          /*  let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "CircularViewController")
            self.present(controller, animated: true, completion: nil) */
            }
        case "HMCS":

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "InboxVC")
            ViewController.isDashboardVC = false
            UserDefaults.standard.set("1", forKey: "isdashbordVC")

            self.present(controller, animated: true, completion: nil)
        case "Evaluation":

//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let controller = storyboard.instantiateViewController(withIdentifier: "EvaluationVC")
//            ViewController.isDashboardVC = false
//            UserDefaults.standard.set("1", forKey: "isdashbordVC")
            let vc = UINavigationController(rootViewController: EvaluationVC())
            present(vc, animated: true, completion: nil)

          //  self.present(controller, animated: true, completion: nil)
        case "BusTracking":
            let vc = UINavigationController(rootViewController: BusTrackingVC())
                present(vc, animated: true, completion: nil)

        case "Attendance":
                if(UserDefaults.standard.string(forKey: "parent") == "0")
                {
                    let vc = UINavigationController(rootViewController: AttendanceTVCTeacher())
                              present(vc, animated: true, completion: nil)
                }
                else{
                let vc = UINavigationController(rootViewController: AttendanceVC())
                          present(vc, animated: true, completion: nil)
            }
        case "TimeTable":
            if(UserDefaults.standard.string(forKey: "parent") == "1")
            {
            let vc = UINavigationController(rootViewController: ClassTimeTableVC())
            present(vc, animated: true, completion: nil)
            }
            else{
            let vc = UINavigationController(rootViewController: TimeTableClassVCTeacher())
            present(vc, animated: true, completion: nil)
        }
        
        case "Intellipay":
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier:"intelliPayViewController")
            self.present(controller, animated: true, completion: nil)
        case "StudentProfile":

            if(UserDefaults.standard.string(forKey: "parent") == "1")
            {
            let vc = UINavigationController(rootViewController: StudentProfileVC())
            present(vc, animated: true, completion: nil)
            }
            else
            {
             let vc = UINavigationController(rootViewController: StudentProfileClassDivisionVCTeacher())
            present(vc, animated: true, completion: nil)
            }
        
        
        case "teacherClassDivVC":
        let vc = UINavigationController(rootViewController: teacherClassDivVC())
         present(vc, animated: true, completion: nil)
       
      case "TimetableBeta":
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let controller = storyboard.instantiateViewController(withIdentifier: "weeklyTimeTableViewController")
//   //     ViewController.isDashboardVC = false WeeklyTTViewController
//      //  UserDefaults.standard.set("1", forKey: "isdashbordVC")
//        self.present(controller, animated: true, completion: nil)
        let vc = UINavigationController(rootViewController: WeeklyTTViewController())
         present(vc, animated: true, completion: nil)
        
        case "SMS":
       
        let vc = UINavigationController(rootViewController: ClassDivListVCTeacher())
        present(vc, animated: true, completion: nil)
             //    present(vc, animated: true, completion: nil)
        default:
            print("default")
        }


        
        return
    }

    @objc func handleSlide() {
           print("back button tapped..")
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
                  let controller = storyboard.instantiateViewController(withIdentifier: "vc")
                  self.present(controller, animated: true, completion: nil)
       }



    

    

//    override func viewDidLayoutSubviews(){
//
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//
//        tableView.reloadData()
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      //  return NamesAll.count
    
        return tableviewArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        var tableviewArrayTV = tableviewArray[indexPath.row]
        //var tableviewArrayTv2 = tableviewArrayProfile[indexPath.row]
        
        let Cell2 =  tableView.dequeueReusableCell(withIdentifier: "tablecell", for: indexPath) as! SubTableViewCell
      //  Cell2.lbl.text = NamesAll[indexPath.row]
        
        let backgroundView = UIView()
        var color = UIColor(hexString: "#8bb0e2")
        backgroundView.backgroundColor = color
        Cell2.selectedBackgroundView = backgroundView
    
        Cell2.lbl.text = tableviewArrayTV.getemployeeName()
        
        Cell2.Profileimage.layer.cornerRadius =  Cell2.Profileimage.frame.width/2
        Cell2.roleLbl.text = tableviewArrayTV.getspec_role()

        if tableviewArrayTV.getProfile().isEmpty
        {
           Cell2.Profileimage.image = UIImage(named: "profile")
        }
        else {
                //Nuke image adding
        
                let photoUrl = NSURL(string: tableviewArrayTV.getProfile())
               
                let imgUrl = tableviewArrayTV.getProfile().removingPercentEncoding
                let options = ImageLoadingOptions(
                    placeholder: UIImage(named: "placeholder"),
                    transition: .fadeIn(duration: 0.33)
                )
Nuke.loadImage(with: URL(string: imgUrl!)!, options: options, into: Cell2.Profileimage)
            }
     

        return Cell2

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = self.tableView.cellForRow(at: indexPath) as! SubTableViewCell
        
        //====================
        let tableviewArrayTV = tableviewArray[indexPath.row]
              print("tableviewArrayTV.getemployeeName() : (String(describing: tableviewArrayTV.getemployeeName()))")
              print("tableviewArrayTV.getspec_role() : (String(describing: tableviewArrayTV.getspec_role()))")
              UserDefaults.standard.set(tableviewArrayTV.getspec_role(), forKey: "SELECTED_CLASS_DIV")
              UserDefaults.standard.set(tableviewArrayTV.getemployeeName(), forKey: "SELECTED_STUDENTNAME")

              //===================
        var color = UIColor(hexString: "#1862C6")
        cell.tintColor = color
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .checkmark

        }
//        if(tableviewArray[indexPath.row].employeeName == UserDefaults.standard.string(forKey: "EMPNAME"))
//        {
//            print("*********************")
//             UserDefaults.standard.set("1", forKey: "COMPOSEON")
//            self.Footer_Compose.isHidden = false
//            self.Footer_ComposeLbl.isHidden = false
//            self.Footer_Profile.isHidden = true
//            self.Footer_ProfileLbl.isHidden = true
//
//            self.Footer_Compose.isUserInteractionEnabled = true
//            self.Footer_Profile.isUserInteractionEnabled = false
//        }
//        else
//        {
//            UserDefaults.standard.set("0", forKey: "COMPOSEON")
//            self.Footer_Compose.isHidden = true
//            self.Footer_ComposeLbl.isHidden = true
//            self.Footer_Profile.isHidden = false
//            self.Footer_ProfileLbl.isHidden = false
//
//            self.Footer_Compose.isUserInteractionEnabled = false
//            self.Footer_Profile.isUserInteractionEnabled = true
//        }
     //   var celltext = ["News", "Events" ,"Homework" ,"Message" , "Circular","Attendance" ]

       // cell.selectionStyle = .blue


        
        
        if(tableviewArray[indexPath.row].employeeName == UserDefaults.standard.string(forKey: "EMPNAME"))
        {
  
                        print("*********************")
                         UserDefaults.standard.set("1", forKey: "COMPOSEON")
                        self.Footer_Compose.isHidden = false
                        self.Footer_ComposeLbl.isHidden = false
                        self.Footer_Profile.isHidden = true
                        self.Footer_ProfileLbl.isHidden = true
            
                        self.Footer_Compose.isUserInteractionEnabled = true
                        self.Footer_Profile.isUserInteractionEnabled = false
            
            
      //   celltext.remove(at: [2, 3 ,4])

//
//            if let index = celltext.index(of: "Intelliadmissions")  {
//                celltext.remove(at: index)
//            }
//            if let index = celltext.index(of: "Publishing")  {
//                celltext.remove(at: index)
//            }
//            if let index = celltext.index(of: "Sports")  {
//                celltext.remove(at: index)
//            }
//            if let index = celltext.index(of: "LMS")  {
//                celltext.remove(at: index)
//            }
//            if let index = celltext.index(of: "Intellipay")  {
//                celltext.remove(at: index)
//            }
//            if let index = celltext.index(of: "Evaluation")  {
//                celltext.remove(at: index)
//            }
////            if let index = celltext.index(of: "Inbox")  {
////                celltext.remove(at: index)
////            }
//            if let index = celltext.index(of: "Attendance")  {
//                celltext.remove(at: index)
//            }
//            if let index = celltext.index(of: "TimeTable")  {
//                celltext.remove(at: index)
//            }
//            collectionView.reloadData()
            UserDefaults.standard.set("0", forKey: "parent")
            getData()


        }
        else
        {
            
                        if let index = celltext.index(of: "SMS")  {
                            celltext.remove(at: index)
                        }
                        UserDefaults.standard.set("0", forKey: "COMPOSEON")
                        self.Footer_Compose.isHidden = true
                        self.Footer_ComposeLbl.isHidden = true
                        self.Footer_Profile.isHidden = false
                        self.Footer_ProfileLbl.isHidden = false
            
                        self.Footer_Compose.isUserInteractionEnabled = false
                        self.Footer_Profile.isUserInteractionEnabled = true
            
            
            UserDefaults.standard.set("1", forKey: "parent")
            getData()
           collectionView.reloadData()
            ViewController.isDashboardVC = true

        }
        let pi = tableviewArray[indexPath.row].profile
        print("pipipi\(pi)")
                        let photoUrl = NSURL(string: pi!)
                       
                        let imgUrl = pi!.removingPercentEncoding
                        let options = ImageLoadingOptions(
                            placeholder: UIImage(named: "placeholder"),
                            transition: .fadeIn(duration: 0.33)
                        )
        Nuke.loadImage(with: URL(string: imgUrl!)!, options: options, into: navBarProfile)
                    
     //   TopRightPI.image = UIImage(named: pi!)
    //    cell.TickImg.image = UIImage(named: "check")
        self.ListOfRolesLbl.isHidden = true
        self.subTableView.isHidden = true
         self.imgViewTransparent.isHidden = true
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = self.tableView.cellForRow(at: indexPath) as! SubTableViewCell
        var color = UIColor(hexString: "#1862C6")
       cell.tintColor = color
        cell.accessoryType = .none
}
    func gettableviewdata2()
    {
        var url = "http://intellischools.com/schoolAdminApi/public/api/login"
        let mobilenum = UserDefaults.standard.value(forKey: "userId") as! String
        let password = UserDefaults.standard.value(forKey: "password") as! String
        print("mobilenummobilenum\(mobilenum)")
        print("passwordpassword\(password)")
            let parameters = [
               
                "mobile": mobilenum,
                "password": password,
                "schoolcode" : "INTELL"
                ] as [String : Any]
            if Util.validateNetworkConnection(self){
                
                Alamofire.request(url,method: .post , parameters: parameters).responseJSON {[weak self] (response) in
                    print("parametersparametersparametersTABLEVIEW\(parameters)")
                    print("loginresponse#######TABLEVIEW>>>>\(response.result)")
                    print("loginresponse#######VALUETABLEVIEW>>>>\(response.result.value)")
                    //  self?.stopActivityIndicator()
                    if response.result.error != nil {
                    }
                    print("loginresponse1TABLEVIEW>>>>\(response.result)")
                    switch response.result {
                    case .success:
//                        UserDefaults.standard.set(userId, forKey: "userId")
//                        UserDefaults.standard.set(password, forKey: "password")
                        
                        if let jsonresponse = response.result.value {
                            let originalResponse = JSON(jsonresponse)
                           let sessionToken = originalResponse["sessionToken"]
                            print("sessionTokensessionToken\(sessionToken)")

                            let EmpSuccess = originalResponse["EmpSuccess"]
                            let EmpMessage = originalResponse["EmpMessage"]

                            let InstDetails: [NSDictionary]  = originalResponse["InstDetails"].arrayObject as! [NSDictionary]
                            print("InstDetailsInstDetails\(InstDetails)")
                            for item1 in InstDetails {

                                var code = item1.value(forKey: "code") as! String
                                print("code : \(code)")
                                var accademicYear = item1.value(forKey: "accademicYear") as! String
                                print("accademicYear : \(accademicYear)")

                            }
                            
                            print("EmpMessageEmpMessageEmpMessageEmpMessageTABLEVIEW\(EmpMessage)")
                            print("EmpSuccessEmpSuccess\(EmpSuccess)")
 
                            let ParentSuccess = originalResponse["ParentSuccess"]
                            print("ParentSuccessParentSuccessTABLEVIEW\(ParentSuccess)")
                            let EmpDetails: [NSDictionary]  = originalResponse["EmpDetails"].arrayObject as! [NSDictionary]
                            print("EmpDetailsEmpDetailsEmpDetailsTABLEVIEW\(EmpDetails)")
                            let school_employee_class_setting_array:[NSDictionary]  = originalResponse["school_employee_class_setting_array"].arrayObject as! [NSDictionary]
                            print("school_employee_class_setting_array>>>>>\(school_employee_class_setting_array)")
                            for item2 in EmpDetails {
                                self!.name1 = item2.value(forKey: "name") as! String
                               UserDefaults.standard.set(self!.name1, forKey: "EMPNAME")
                                print("namenamenameEmpDetailsEmpDetails\(self!.name1)")
                                print("profile >> \(self!.profile1)")
                               // UserDefaults.standard.set(self!.name, forKey: "loginprofilename")
                                var spec_role = item2.value(forKey: "spec_role") as! String
                                print("spec_rolespec_role\(spec_role)")
                                self!.profile1 = item2.value(forKey: "profile") as! String
                                var TeacherProfile = item2.value(forKey: "profile") as! String
                                var intellinectsId = item2.value(forKey: "intellinectsId") as! String
                                print("intellinectsId : \(intellinectsId)")
                                UserDefaults.standard.set(TeacherProfile, forKey: "TeacherProfile")
                                UserDefaults.standard.set(intellinectsId, forKey: "INTELLINECTS_ID")
                                UserDefaults.standard.set(intellinectsId, forKey: "EMPLOYEE_ID")

                                var employee_role_id = item2.value(forKey: "employee_role_id") //as! String
                                print("employee_role_id : (employee_role_id)")
                                UserDefaults.standard.set(employee_role_id, forKey: "EMPLOYEE_ROLE_ID")

                               if EmpDetails.isEmpty == false
                               {
                                let tableviewarraytemp1 = ListOfRoles(employeeName: self!.name1, profile: self!.profile1, spec_role: spec_role)
                                self!.tableviewArray.append(tableviewarraytemp1)
                                print("tableviewarraytemp1 \(tableviewarraytemp1)")
                                }
                            }
                            let ParentDetails: [NSDictionary]  = originalResponse["ParentDetails"].arrayObject as! [NSDictionary]
                            print("ParentDetailsParentDetailsParentDetailsEMPPP\(ParentDetails)")
                                print("print statments")
                              
                            for item3 in ParentDetails {
                                self!.name2  = item3.value(forKey: "name") as! String
                                print("namenamenameParentDetails\(self!.name2 )")
                                print("inside parentdetails")
                                self!.profile2 = (item3.value(forKey: "profile") as! String)

                                let className = item3.value(forKey: "className") as! String
                                let classdivId = item3.value(forKey: "classdivId") as! String
                                let intellinectsId = item3.value(forKey: "intellinectsId")as! String
                                let isirsClassName = item3.value(forKey: "isirsClassName")as! String
                                var isirsdivisionName = item3.value(forKey: "isirsdivisionName")as! String
                                var profile = item3.value(forKey: "profile")as! String
                              
                                print("profile parent : \(profile)")
                                print("intellinectsId parent : \(intellinectsId)")
                                print("viewcontrollclassdivId>>> \(classdivId)")

                                var divisionName = item3.value(forKey: "divisionName") as! String
                                print("classNameclassName>>\(className)")
                                
                                var combinedClassNameId = "\(className)" + " " + "\(divisionName)"
                                print("combinedcombined\(combinedClassNameId)")
                                
                                UserDefaults.standard.set(intellinectsId, forKey: "intellinectsId_Parent")
                                UserDefaults.standard.set(className, forKey: "className")
                                UserDefaults.standard.set(classdivId, forKey: "classdivId")
                                UserDefaults.standard.set(isirsClassName, forKey: "isirsClassName")
                                UserDefaults.standard.set(isirsdivisionName, forKey: "isirsdivisionName")

                                var viewcontrolerusrdeut = UserDefaults.standard.integer(forKey: "classdivId")
                                print("viewcontrolerusrdeutviewcontrolerusrdeut\(viewcontrolerusrdeut)")
                                
                                var viewcontrolerclassName = UserDefaults.standard.string(forKey: "className")
                                print("viewcontrolerclassName\(UserDefaults.standard.string(forKey: "className"))")

                                
                                print("profile1111\(self!.profile1)")
                                print("profile2222\(self!.profile1)")
                                if ParentDetails.isEmpty == false
                                {
                                    let tableviewarraytemp2 = ListOfRoles(employeeName: self!.name2, profile: self!.profile2, spec_role: combinedClassNameId)
                                    self!.tableviewArray.append(tableviewarraytemp2)
                                    print("tableviewarraytemp2 \(tableviewarraytemp2)")
                                }
                            }
                            let ParentDetails1: [NSDictionary]  = [ParentDetails[0]]
                            for item4 in ParentDetails1 {
                            let profile = item4.value(forKey: "profile") as! String
                            UserDefaults.standard.set(profile, forKey: "profile")

                            }
                            
                            print(" ParentDetails[1] ParentDetails[1]\( ParentDetails[0])")
                             for item4 in school_employee_class_setting_array {
                                
                                var board = item4.value(forKey: "board") as! String
                                var division = item4.value(forKey: "division") as! String
                                var class1 = item4.value(forKey: "class") as! String
                                UserDefaults.standard.set(board, forKey: "board")
                                UserDefaults.standard.set(division, forKey: "division")
                               // UserDefaults.standard.set(className, forKey: "className")


                            }
                            
                            print("tableviewarraytemptableviewarraytemptableviewarraytemp\(self!.tableviewArray)")
                            //}
                            self!.tableView.reloadData()
                        self!.subTableView.frame = CGRect(x: self!.subTableView.frame.origin.x, y: self!.subTableView.frame.origin.y, width: self!.subTableView.frame.size.width, height: self!.subTableView.contentSize.height)
                            self!.subTableView.reloadData()
                                }
                       // self!.tableView.reloadData()

                    case .failure(_):
                        
                        print("switch errror TABLEVIEW")
                    }
                }
                
            }
        }
    @IBAction func LogoutBtnTapped(_ sender: Any) {
        let prefs = UserDefaults.standard
        prefs.removeObject(forKey: "userId")
        prefs.removeObject(forKey: "password")
        
        prefs.removeObject(forKey: "sessionToken")

        tableView.reloadData()

        customAlert(title: "Alert", description: "Are you sure you want to logout?")
    }
    @objc func a()
    {
        self.imgViewTransparent.isHidden = true

    }
    func dismissView() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("a"))
    }
    @IBAction func profileBtnTapped(_ sender: Any) {
     let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "a")
        imgViewTransparent.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
        print("btn tappeddd")
        self.imgViewTransparent.frame = CGRect(x: 0, y: 0, width: ViewController.screenSize.width, height: ViewController.screenSize.height)
                        
                        self.imgViewTransparent.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                        
                        self.view.addSubview(self.imgViewTransparent)
                        self.imgViewTransparent.isHidden = false
        
                        self.ListOfRolesLbl.isHidden = false
                        self.subTableView.isHidden = false
                        self.imgViewTransparent.addSubview(self.subTableView)

}
    func customAlert(title: String, description: String) {
        let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)
        //    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        //   present(alert, animated: true, completion: nil)
        
        let CancelAction = UIAlertAction(title: "cancel", style: .cancel) { (action) in
            
        }
        
       
        let action = UIAlertAction(title: "OK", style: .default) { (action) -> Void in
//            let viewControllerYouWantToPresent = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationVC")
//            self.present(viewControllerYouWantToPresent!, animated: true, completion: nil)
//            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//            let destination = self.storyboard!.instantiateViewController(withIdentifier: "RegistrationVC") as! RegistrationVC
//            self.navigationController?.popToRootViewController(animated: true)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "RegistrationVC")
            self.present(controller, animated: true, completion: nil)
        }
        alert.addAction(CancelAction)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }

    
    @IBAction func Footer_Homebtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "vc")
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func Footer_ProfileBtn(_ sender: Any) {
    }
    
    
    @IBAction func Footer_ComposeBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Compose_HomeworkVC")
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func Footer_SupportBtn(_ sender: Any) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let controller = storyboard.instantiateViewController(withIdentifier: "sliderVC")
////        self.present(controller, animated: true, completion: nil)
////        let controller = storyboard.instantiateViewController(withIdentifier: "scene storyboard id")
//        addChild(controller)
//        controller.view.frame =  CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: 100, height: 0))
//        view.addSubview(controller.view)
//        controller.didMove(toParent: self)
        
        let controller = storyboard!.instantiateViewController(withIdentifier: "sliderVC")
        addChild(controller)
//controller.view.frame = CGRect(x:0 , y: 0,width: self.view.frame.width,height: 0)
            //CGRect(x: 0 , y: 0, width: UIScreen.main.bounds.size.width,
                                      //  height: UIScreen.main.bounds.size.height-600) // or, better, turn off `translatesAutoresizingMaskIntoConstraints` and then define constraints for this subview
        controller.view.widthAnchor.constraint(equalToConstant: 100).isActive = true
            // flowHeightConstraint = controller.heightAnchor.constraint(equalToConstant: 30)
            // flowHeightConstraint?.isActive = true
        collectionView.addSubview(controller.view)
        controller.didMove(toParent: self)
        
    }
    @objc func functionToCallWhenUserTapsOutsideOfTableView() {

        self.imgViewTransparent.isHidden = true

        print("user tapped outside table view")
    }
    private func displayNoPermissionAlert() {
        let alert = UIAlertController(title: "Message", message: "No permission given for this functionality.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler:nil))
        present(alert, animated: true, completion: nil)
    }
}


extension Array {
  mutating func remove(at indexes: [Int]) {
    for index in indexes.sorted(by: >) {
      remove(at: index)
    }
  }
}
extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()

        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }

    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
}


