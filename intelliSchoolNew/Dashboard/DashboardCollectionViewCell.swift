//
//  DashboardCollectionViewCell.swift
//  Intellinects
//
//  Created by rupali  on 01/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class DashboardCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var btnTapped: UIButton!
}
