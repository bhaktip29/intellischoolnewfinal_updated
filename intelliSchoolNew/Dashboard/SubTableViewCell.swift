//
//  SubTableViewCell.swift
//  Intellinects
//
//  Created by rupali  on 04/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class SubTableViewCell: UITableViewCell {

    @IBOutlet weak var roleLbl: UILabel!
    @IBOutlet weak var lbl: UILabel!
   
    @IBOutlet weak var Profileimage: UIImageView!
    @IBOutlet weak var TickImg: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        Profileimage.layer.cornerRadius = Profileimage.frame.size.width / 2;
        Profileimage.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
