//
//  ExpandableStyle.swift
//  intelliSchoolNew
//
//  Created by rupali  on 11/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//



import Foundation

public enum ExpandableStyle {
    case normal
    case closeAndOpen
}
