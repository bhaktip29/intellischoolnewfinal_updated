//
//  AppDelegate.swift
//  Intellinects
//
//  Created by rupali  on 01/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Firebase
//import FirebaseInstanceID
//import FirebaseMessaging
import SwiftyJSON
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
//,UNUserNotificationCenterDelegate, MessagingDelegate
{
    
    var userInfoObject =  [AnyHashable : Any]()
    var window: UIWindow?
   // let gcmMessageIDKey = "gcm.message_id"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //    FirebaseApp.configure()
      splashscreen()
        Thread.sleep(forTimeInterval: 2.0)
        
        
        
        return true
        
    }
    
    private func splashscreen()
    {
        if UserDefaults.standard.string(forKey: "userId") != nil && UserDefaults.standard.string(forKey: "password") != nil
        {
            //        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            //        let resultViewController =
            //            storyBoard.instantiateViewController(withIdentifier:
            //                "vc") as! ViewController
            //        self.navigationController?.pushViewController(resultViewController,
            //                                                      animated: true)
            
            
            let launchscreenVC = UIStoryboard.init(name : "Main", bundle: nil)
            let rootVC = launchscreenVC.instantiateViewController(withIdentifier: "vc")
            self.window?.rootViewController = rootVC
            self.window?.makeKeyAndVisible()
        }
        func applicationWillResignActive(_ application: UIApplication) {
            // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
            // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        }
        
        func applicationDidEnterBackground(_ application: UIApplication) {
            // Messaging.messaging().shouldEstablishDirectChannel = false
        }
        
        func applicationWillEnterForeground(_ application: UIApplication) {
            // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        }
        
        func applicationDidBecomeActive(_ application: UIApplication) {
            UIApplication.shared.applicationIconBadgeNumber = 0
            connectToFcm()
        }
        
        func applicationWillTerminate(_ application: UIApplication) {
            // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        }
        //    public func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String){
        //        print("FCMTOKEN ",fcmToken)
        //        connectToFcm()
        //        UserDefaults.standard.set(fcmToken, forKey: "SCHOOL_DEVICETOKEN")
        //      //  UtilTeacher.registerDeviceIDonServer(fcmToken)
        //    }
        //    private func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        //        Messaging.messaging().appDidReceiveMessage(userInfo)
        //    }
        //
        //    private func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        //        print(userInfo)
        //        Messaging.messaging().appDidReceiveMessage(userInfo)
        //        completionHandler(UIBackgroundFetchResult.newData)
        //    }
        //
        //    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        //
        //Tricky line
        //        Messaging.messaging().setAPNSToken(deviceToken as Data, type: MessagingAPNSTokenType.unknown)
        //        InstanceID.instanceID().instanceID { (result, error) in
        //            if let error = error{
        //                print("Error fetching remote instance ID: \(error)")
        //            }else if let result = result{
        //                print("Remote instance ID token: \(result.token)")
        //                UserDefaults.standard.set(result.token, forKey: "SCHOOL_DEVICETOKEN")
        //              //  UtilTeacher.registerDeviceIDonServer(result.token)
        //            }
        //        }
    }
    
    func connectToFcm() {
        InstanceID.instanceID().instanceID { (result, error) in
            guard result?.token != nil else {
                return
            }
        }
      //  Messaging.messaging().shouldEstablishDirectChannel = true
}
//}
}
