//
//  AttachementVC.swift
//  SchoolProtoTypeApp
//
//  Created by Intellinects on 22/09/17.
//  Copyright © 2017 Intellinects Ventures. All rights reserved.
//

import UIKit
import QuickLook
import SwiftyJSON
import Alamofire

class AttachementVC: BaseViewController,UITableViewDelegate,UITableViewDataSource,QLPreviewControllerDataSource {
    
    let cellId = "DownloadCell"
    var dataItem = DataItem() 
    let quickLookController = QLPreviewController()
    var fileURLs = [NSURL]()
    var heightConstraint: NSLayoutConstraint?
    
    let tableView : UITableView = {
        let tblView = UITableView()
        tblView.translatesAutoresizingMaskIntoConstraints = false
        tblView.layer.cornerRadius = 5
        tblView.layer.masksToBounds = true
        tblView.backgroundColor = UIColor.white
        tblView.separatorInset = UIEdgeInsets.zero
        return tblView
    }()
    
    let btnCloseOverlay: UIButton = {
        let btn = UIButton(type: .system)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(onCloseClick), for: .touchUpInside)
        return btn
    }()
    
    private func setUpViews() {
    
        view.addSubview(btnCloseOverlay)
        // needed constarunts x,y,w,h
        btnCloseOverlay.topAnchor.constraint(equalTo:view.topAnchor).isActive = true
        btnCloseOverlay.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        btnCloseOverlay.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        btnCloseOverlay.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        setUpTableView()
    }
    
    private func setUpTableView() {
        
        tableView.delegate = self
        tableView.dataSource = self

        view.addSubview(tableView)
        //needed constraints x,y,w,h
        tableView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        tableView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        tableView.widthAnchor.constraint(equalTo: view.widthAnchor,multiplier:0.85).isActive = true
        heightConstraint = tableView.heightAnchor.constraint(equalToConstant:0)
        heightConstraint?.isActive = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(DownloadeAbleCell.self, forCellReuseIdentifier: cellId)
        setUpViews()
        quickLookController.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewDidLayoutSubviews()
    }
    
    override func viewDidLayoutSubviews()
    {
        heightConstraint?.constant =  tableView.contentSize.height > 450 ? 450 : tableView.contentSize.height
    }
    
    @objc func onCloseClick() {
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.alpha = 0
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParent()
        })
    }
    
    public func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return fileURLs.count
    }
    
    public func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem
    {
        return fileURLs[index]
    }
    
    //MARK:- TableView Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataItem.postAttachment.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! DownloadeAbleCell
        cell.cImageView.image = UIImage(named: "ic_attachment")
        let attachment = dataItem.postAttachment
        let str = attachment[indexPath.row].removingPercentEncoding!
        let name :[String] = str.components(separatedBy: "/")
        if name.count > 6 {
            cell.nameLabel.text = name[6]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        download(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let lbl = UILabel()
        lbl.backgroundColor = GlobleConstants.homeScreenIconColor
        lbl.textColor = GlobleConstants.navigationBarColorTint
        lbl.text = "Attachment"
        lbl.textAlignment = .center
        return lbl
    }
    
    func download (_ row : Int) {
        
        let attachment = dataItem.postAttachment
        let str = attachment[row].removingPercentEncoding!
        let name :[String] = str.components(separatedBy: "/")

        guard name.count > 6 else {
            return
        }
        let docpath = name[6]
        let fileManager = FileManager.default
        let rootPath = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let pdfPathInDocument = rootPath.appendingPathComponent(docpath)
        
        if fileManager.fileExists(atPath: pdfPathInDocument.path){
            
            fileURLs.removeAll()
            fileURLs.append(pdfPathInDocument as NSURL)
            quickLookController.reloadData()
            if QLPreviewController.canPreview((fileURLs[0])) {
                quickLookController.currentPreviewItemIndex = 0
                navigationController?.pushViewController(quickLookController, animated: true)
            }
        }
        else
        {
            if Util.validateNetworkConnection(self) {
                
                let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                    var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                    documentsURL.appendPathComponent(docpath)
                    return (documentsURL, [.removePreviousFile])
                }
                showActivityIndicatory(uiView: view)
                self.view.isUserInteractionEnabled = false
                Alamofire.download(str, to: destination).responseData { [weak self]response in
                    if let destinationUrl = response.destinationURL {
                        self?.view.isUserInteractionEnabled = true
                        self?.stopActivityIndicator()
                        self?.fileURLs.removeAll()
                        self?.fileURLs.append(destinationUrl as NSURL)
                        self?.quickLookController.reloadData() 
                        guard let file = self?.fileURLs[0] else {
                            return
                        }
                        if QLPreviewController.canPreview(file) {
                            self?.quickLookController.currentPreviewItemIndex = 0
                            self?.navigationController?.pushViewController((self?.quickLookController)!, animated: true)
                        }
                    }
                }
            }
        }
    }
}

class DownloadeAbleCell:UITableViewCell {
    
    let nameLabel:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.numberOfLines = 0
        lbl.sizeToFit()
        return lbl
    }()
    
    let cImageView:UIImageView = {
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.contentMode = .scaleToFill
        return imgView
    }()
    
    private func setUpViews() {
        
        contentView.addSubview(cImageView)
        //needed constarints x,y,w,h
        cImageView.rightAnchor.constraint(equalTo: contentView.rightAnchor,constant:-8).isActive = true
        cImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        cImageView.widthAnchor.constraint(equalToConstant:30).isActive = true
        cImageView.heightAnchor.constraint(equalToConstant:30).isActive = true
        
        contentView.addSubview(nameLabel)
        //needed constarints x,y,w,h
        nameLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor,constant:8).isActive = true
        nameLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        nameLabel.rightAnchor.constraint(equalTo: cImageView.leftAnchor).isActive = true
        nameLabel.heightAnchor.constraint(equalTo: contentView.heightAnchor,constant:-8).isActive = true
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        setUpViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
