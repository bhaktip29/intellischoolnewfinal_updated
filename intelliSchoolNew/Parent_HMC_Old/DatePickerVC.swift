//
//  DatePickerVC.swift
//  CampionSchool
//
//  Created by Intellinects on 12/11/18.
//  Copyright © 2018 Intellinects Ventures. All rights reserved.
//

import UIKit

protocol DatePikerDelegate {
    func handleWithSelectedDate(datePickerSelectedDate date: Date)
    func dismissDatePicker(_ isPresent: Bool)
}

class DatePickerVC: UIViewController {
  
    var delegate:DatePikerDelegate?
    var isRestrictPriviousDate = false
    let datePicker:UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = UIDatePicker.Mode.date
        picker.maximumDate = picker.date
        picker.backgroundColor = UIColor.white
        picker.layer.cornerRadius = 5
        picker.layer.masksToBounds = true
        picker.translatesAutoresizingMaskIntoConstraints = false
        return picker
    }()
    
    let cancelBtn:UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Cancel", for: .normal)
        btn.layer.borderColor = UIColor.blue.cgColor
        btn.layer.borderWidth = 0.3
        btn.layer.cornerRadius = 5
        btn.layer.masksToBounds = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(handleCancel), for: .touchUpInside)
        return btn
    }()
    
    let OkBtn:UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Ok", for: .normal)
        btn.layer.borderColor = UIColor.blue.cgColor
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.borderWidth = 0.3
        btn.layer.cornerRadius = 5
        btn.layer.masksToBounds = true
        btn.addTarget(self, action: #selector(handleOk), for: .touchUpInside)
        return btn
    }()
    
    //MARK:- Custom Views Setup Methods
    private func setUpViews() {
        
        let vw  = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = UIColor.white
        vw.layer.cornerRadius = 5
        vw.layer.masksToBounds = true
        view.addSubview(vw)
        // needed constraints x,y,w,h
        vw.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        vw.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        vw.widthAnchor.constraint(equalToConstant: 300).isActive = true
        vw.heightAnchor.constraint(equalToConstant: 250).isActive = true
        
        vw.addSubview(datePicker)
        // needed constraints x,y,w,h
        datePicker.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        datePicker.topAnchor.constraint(equalTo: vw.topAnchor).isActive = true
        datePicker.widthAnchor.constraint(equalTo:vw.widthAnchor).isActive = true
        datePicker.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        vw.addSubview(cancelBtn)
        // needed constraints x,y,w,h
        cancelBtn.topAnchor.constraint(equalTo: datePicker.bottomAnchor,constant:4).isActive = true
        cancelBtn.leftAnchor.constraint(equalTo: vw.leftAnchor,constant:4).isActive = true
        cancelBtn.widthAnchor.constraint(equalTo:vw.widthAnchor,multiplier:0.5,constant:-8).isActive = true
        cancelBtn.heightAnchor.constraint(equalToConstant:40).isActive = true
        
        vw.addSubview(OkBtn)
        // needed constraints x,y,w,h
        OkBtn.topAnchor.constraint(equalTo: datePicker.bottomAnchor,constant:4).isActive = true
        OkBtn.rightAnchor.constraint(equalTo: vw.rightAnchor,constant:-4).isActive = true
        OkBtn.widthAnchor.constraint(equalTo:vw.widthAnchor,multiplier:0.5,constant:-8).isActive = true
        OkBtn.heightAnchor.constraint(equalToConstant:40).isActive = true
    }
    
    //MARK:- View Controller Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        if isRestrictPriviousDate {
            var calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
            calendar.timeZone = TimeZone(identifier: "UTC")!
            var components: DateComponents = DateComponents()
            components.calendar = calendar as Calendar
            components.month = -1
            let minDate: Date = calendar.date(byAdding: components, to: Date())!
            datePicker.minimumDate = minDate as Date
        }
        setUpViews()
    }
    
    //MARK:- Custom Action Methods
    
    @objc func handleCancel() {
        self.removeFromParent()
        self.view.removeFromSuperview()
        self.delegate?.dismissDatePicker(false)
    }
    
   @objc func handleOk() {
        delegate?.handleWithSelectedDate(datePickerSelectedDate: datePicker.date)
        self.removeFromParent()
        self.view.removeFromSuperview()
    }
    func getYYYYMMDDFormattedDate(_ dateString: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-M-dd"
        return formatter.string(from: dateString)
    }
}
