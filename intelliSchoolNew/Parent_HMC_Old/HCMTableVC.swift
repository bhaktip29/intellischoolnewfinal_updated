//
//  HCMTableVC.swift
//  SchoolProtoTypeApp
//
//  Created by Intellinects on 13/09/17.
//  Copyright © 2017 Intellinects Ventures. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import QuickLook

class HCMTableVC: BaseViewController, UITableViewDelegate, UITableViewDataSource, QLPreviewControllerDataSource {
    
    let cellId = "CellId"
    var intellinectsId = ""
    
    var selectedIndex = String()
    var dataItemList = [DataItem]()
    var dataItemListTemp = [DataItem]()
    let quickLookController = QLPreviewController()
    var fileURLs = [NSURL]()
    var heightConstraint: NSLayoutConstraint?
    
    let tableView: UITableView = {
        let tbl = UITableView()
        tbl.translatesAutoresizingMaskIntoConstraints = false
        return tbl
    }()
    
    let refreshControl: UIRefreshControl = {
        let rf = UIRefreshControl()
        rf.tintColor = UIColor.black
        rf.attributedTitle = NSAttributedString(string: "Pull to refresh")
        rf.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        return rf
    }()
    
    let childName : UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.boldSystemFont(ofSize: 14)
        lbl.textColor = UIColor.black
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    //MARK:- Custom views Setup Methods
    
    private func setUpLabel() {

        let model = UIDevice.current.modelName
        var constant: CGFloat = 0.0
        if model == "iPhone10,6" || model == "iPhone10,3" || (model == "x86_64" && UIDevice.current.model == "iPhone") {
            constant = 86.0
        }else {
            constant = 64.0
        }
        view.addSubview(childName)
        // needed constraints x,y,w,h
        childName.topAnchor.constraint(equalTo: view.topAnchor,constant:constant).isActive = true
        childName.leftAnchor.constraint(equalTo: view.leftAnchor,constant:8).isActive = true
        childName.widthAnchor.constraint(equalTo: view.widthAnchor,constant:-16).isActive = true
        childName.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }

    private func setUpTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        tableView.backgroundColor = UIColor.clear
        view.addSubview(tableView)
        // needed constraints
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: childName.bottomAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.addSubview(refreshControl)
        setUpErrorLbl()
    }
    
    //MARK:- View Controller Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        if(UserDefaults.standard.string(forKey: "HMC") == "1")
        {
         selectedIndex = "Homework" //String()
        }
        else if(UserDefaults.standard.string(forKey: "HMC") == "2")
        {
         selectedIndex = "Message" //String()
        }
        else if(UserDefaults.standard.string(forKey: "HMC") == "3")
        {
         selectedIndex = "Circular" //String()
        }
        setUpLabel()
        setUpTableView()
        childName.text = getChildName()
        tableView.isHidden = true
        tableView.register(TblCell.self, forCellReuseIdentifier: cellId)
        quickLookController.dataSource = self
    }
    
    var vLoaded = false
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
    
     //  navigationItem.leftBarButtonItem = UIBarButtonItem(image:defaultMenuImage(), style: .plain, target: self, action: #selector(handleSlide))
      //  navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"backarrow"), style: .plain, target: self, action: #selector(handleSlide))
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 25.0/255.0, green: 89.0/255.0, blue: 194.0/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        if(UserDefaults.standard.string(forKey: "HMC") == "1")
        {
         self.navigationItem.title = "Homework"
        }
        else if(UserDefaults.standard.string(forKey: "HMC") == "2")
        {
            self.navigationItem.title = "Message"
        }
        else if(UserDefaults.standard.string(forKey: "HMC") == "3")
        {
            self.navigationItem.title = "Circular"
        }
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(handleSlide))
        navigationItem.leftBarButtonItem?.tintColor = .white //GlobleConstants.navigationBarColorTint
      //  navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(handleSlide1))
      //  navigationItem.rightBarButtonItem?.tintColor = .white
        
        
        if !vLoaded {
            getData()
            vLoaded = true
        }
        NotificationCenter.default.addObserver(self, selector: #selector(pushNotification), name: NSNotification.Name(rawValue: "refreshNotification"), object: nil)
    }
   
    @objc func handleSlide() {
        print("back button tapped..")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let controller = storyboard.instantiateViewController(withIdentifier: "vc")
               self.present(controller, animated: true, completion: nil)
    }
    @objc func handleSlide1() {
           print("right button tapped..")
          
               let logoutAlert = UIAlertController(title: nil, message: "Are you sure you want to logout?", preferredStyle: UIAlertController.Style.actionSheet)
               
               logoutAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
               
               logoutAlert.addAction(UIAlertAction(title: "Logout", style: .destructive, handler: { [weak self] (action) in
                   UserDefaults.standard.set(false, forKey: isLogged)
                   UserDefaults.standard.synchronize()
                 
                 self?.resetDefaults()
                self?.customAlert(title: "Alert", description: "Are you sure you want to logout?")
                   
               }))
               addActionSheetForiPad(actionSheet: logoutAlert)
               present(logoutAlert, animated: true, completion: nil)
           }
    
    func customAlert(title: String, description: String) {
            let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)
            let CancelAction = UIAlertAction(title: "cancel", style: .cancel) { (action) in
                
            }
            let action = UIAlertAction(title: "OK", style: .default) { (action) -> Void in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "RegistrationVC")
                self.present(controller, animated: true, completion: nil)
            }
            alert.addAction(CancelAction)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
    
    func defaultMenuImage() -> UIImage {
        var defaultMenuImage = UIImage()
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 25, height: 22), false, 0.0)
        UIColor.black.setFill()
        UIBezierPath(rect: CGRect(x:0, y:3, width:25, height:1)).fill()
        UIBezierPath(rect: CGRect(x:0, y:10,width:25, height:1)).fill()
        UIBezierPath(rect: CGRect(x:0, y:17, width:25, height:1)).fill()
        
        UIColor.white.setFill()
        UIBezierPath(rect: CGRect(x:0, y:4, width:25, height:0.7)).fill()
        UIBezierPath(rect: CGRect(x:0, y:11, width:25, height:0.7)).fill()
        UIBezierPath(rect: CGRect(x:0, y:18, width:25, height:0.7)).fill()
        
        defaultMenuImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return defaultMenuImage;
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func handleRefresh() {
        getData()
        refreshControl.endRefreshing()
    }
    
    @objc func pushNotification(notification: NSNotification) {
        
        var result: String = ""
        guard let userInfo = notification.userInfo else {
            return
        }
        
        for item in userInfo {
            if (item.key as? String) == "result" {
                result = item.value as! String
            }
        }
        
        let json = JSON(result.parseJSONString!).dictionaryValue
        guard let tempNavigation = json["alert"]?.stringValue else {
            return
        }
        print("tempNavigation :\(tempNavigation)")
        if (selectedIndex == "Message" && tempNavigation == "Message") || (selectedIndex == "Homework" && tempNavigation == "Homework") || (selectedIndex == "Circular" && tempNavigation == "Circular") {
            getData()
        }
    }
    
    //MARK- Quick Look Controller Methods
    public func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return fileURLs.count
    }
    
    public func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem
    {
        return fileURLs[index]
    }
    
    // MARK:- TableView Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataItemList.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let dataItem = dataItemList[indexPath.row]
//        let url = URL(string: dataItem)!
//        UIApplication.shared.open(url,options: [:])
//        let input = DataItem.postContent!
//                let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
//                let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))
//
//                for match in matches {
//                    guard let range = Range(match.range, in: input) else { continue }
//                    let url = input[range]
//                    print(url)
//                    cell.descriptionLbl.textColor =  UIColor.blue
//
//                    let url1 = NSURL (string: String(url))
//                    UIApplication.shared.openURL(url1! as URL)
//    }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! TblCell
        let dataItem = dataItemList[indexPath.row]
        print("dataItem : \(dataItem)")
        cell.dateLbl.text = convertDate(dataItem.postDateGmt ?? "")
        switch selectedIndex {
        case "Message":
            print("@")
            cell.nameLbl.text = dataItem.teacher
            break
        case "Homework":
            print("@@")
            cell.nameLbl.text = dataItem.subject
            break
        case "Circular":
            print("@@@")
            cell.nameLbl.text = dataItem.postTitle
            break
        default:
            break
        }
        
        cell.descriptionLbl.text =  dataItem.postContent
        
        // get url string
        
//        let input = dataItem.postContent!
//        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
//        let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))
//
//        for match in matches {
//            guard let range = Range(match.range, in: input) else { continue }
//            let url = input[range]
//            print(url)
//            cell.descriptionLbl.textColor =  UIColor.blue
          
//            let url1 = NSURL (string: String(url))
//            UIApplication.shared.openURL(url1! as URL)
//        }
        
        
//        let text = dataItem.postContent!
//        let types: NSTextCheckingResult.CheckingType = .link
//        var URLStrings = [NSURL]()
//        let detector = try? NSDataDetector(types: types.rawValue)
//        let matches = detector!.matches(in: text, options: .reportCompletion, range: NSMakeRange(0, text.count))
//        for match in matches {
//            print(match.url!)
//            URLStrings.append(match.url! as NSURL)
//        }
//        print(URLStrings)
//
//        let urlstring1 = URLStrings
//        let url = NSURL (string: URLStrings);
//
//        print(url)
//        let requestObj = NSURLRequest(URL: url!);
//        cell.descriptionLbl.textColor =  UIColor.blue
//        UIApplication.shared.openURL(URLStrings)
//        UIApplication.shared.openURL(URLStrings)
        
// get url string
//        // underline text
//        // let attributedString = NSMutableAttributedString(string: URLStrings.path)
//        let attributedString =  NSMutableAttributedString(string: dataItem.postContent!)
//        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: attributedString.length))
//        //Underline effect here
//        cell.descriptionLbl.attributedText = attributedString
//        // underline text

        
        if dataItem.postAttachment.count < 1 {
            cell.dImageView.isHidden = true
        } else {
            cell.dImageView.isHidden = false
            cell.dImageView.tag = indexPath.row
            cell.dImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(download)))
            cell.dImageView.isUserInteractionEnabled = true
        }
        return cell
    }
    
    func convertDate(_ inputDate:String)-> String {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd"
        guard let showDate = inputFormatter.date(from: inputDate) else { return "" }
        inputFormatter.dateFormat = "dd MMM yyyy"
        let resultString = inputFormatter.string(from: showDate)
        return resultString
    }
    
    @objc func download(_ sender:UIGestureRecognizer) {
        guard let rowId = sender.view?.tag else {
            return
        }
        let vc = AttachementVC()
        vc.dataItem = dataItemList[rowId]
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChild(vc)
        self.view.addSubview(vc.view)
        vc.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            vc.view.alpha = 1
        }, completion:nil)
    }
    
    //MARK:- Server Methods
    func getData(){
        //if Util.validateNetworkConnection(self) {
            showActivityIndicatory(uiView: view)
            var actionName = ""
        print("selectedIndex :\(selectedIndex)")
        switch selectedIndex {
        case "Message":
            print("Message *")
            if let data = UserDefaults.standard.value(forKey:"GetMessage") as? Data {
                 print("Message **")
                self.stopActivityIndicator()
                dataItemList = try! PropertyListDecoder().decode(Array<DataItem>.self, from: data)
                dataItemList.sort(by: {$0.date.timeIntervalSinceNow > $1.date.timeIntervalSinceNow})
                if dataItemList.count > 0 {
                    print("Message ***")
                    tableView.isHidden = false
                    tableView.reloadData()
                    tableView.animateTable()
                    lbl.isHidden = true
                }
                else
                {
                    print("Message ****")
                    lbl.isHidden = false
                }
                print("Message *****")
                actionName = "GetMessage_Mayuri"
            }
            else{
                 print("Message ******")
                actionName = "GetMessage_Mayuri"
            }
            break
        case "Homework":
            if let data = UserDefaults.standard.value(forKey:"GetHomework") as? Data {
                self.stopActivityIndicator()
                dataItemList = try! PropertyListDecoder().decode(Array<DataItem>.self, from: data)
                dataItemList.sort(by: {$0.date.timeIntervalSinceNow > $1.date.timeIntervalSinceNow})
                // let dta = songs2.sorted(by: {$0.date.timeIntervalSinceNow > $1.date.timeIntervalSinceNow})
                 print("dat", dataItemList)
                if dataItemList.count > 0 {
                    tableView.isHidden = false
                    tableView.reloadData()
                    tableView.animateTable()
                    lbl.isHidden = true
                }
                else{
                    lbl.isHidden = false
                }
                actionName = "GetHomework"
            }
            else{
                actionName = "GetHomework"
            }
            break
        case "Circular":
            if let data = UserDefaults.standard.value(forKey:"GetCircular") as? Data {
                self.stopActivityIndicator()
                dataItemList = try! PropertyListDecoder().decode(Array<DataItem>.self, from: data)
                dataItemList.sort(by: {$0.date.timeIntervalSinceNow > $1.date.timeIntervalSinceNow})
                // let dta = songs2.sorted(by: {$0.date.timeIntervalSinceNow > $1.date.timeIntervalSinceNow})
//                print("circular", dataItemList)
                if dataItemList.count > 0 {
                    tableView.isHidden = false
                    tableView.reloadData()
                    tableView.animateTable()
                    lbl.isHidden = true
                }
                else{
                    lbl.isHidden = false
                }
                actionName = "GetCircular"
            }
            else{
                actionName = "GetCircular"
            }
            break
        default:
            break
        }
        
            let parameters = [
                "action": actionName,
                "SCHOOL_ID": "8", //GlobleConstants.schoolID,
                "deviceId":"".getUDIDCode(),
                "userId": "402", //UserDefaults.standard.string(forKey: "\(SDefaultKeys.studentId)\(UserDefaults.standard.integer(forKey: SDefaultKeys.selectedStudent))")?.capitalized ?? "",
                "role": "Father" //UserDefaults.standard.string(forKey: SDefaultKeys.role) ?? ""
            ] as [String : Any]
            print("HMC params:>>",parameters)
            Alamofire.request(ParentsWebUrls.baseUrl,method: .get,parameters: parameters)
                .responseJSON { [weak self] (response) in
                    self?.stopActivityIndicator()
                    print("HMC url:>>",ParentsWebUrls.baseUrl)
                    print("HMC Response:>>> ",response.result.value as Any)
                
                if let jsonresponse = response.result.value {
                    self?.lbl.isHidden = true
                    let originalResponse = JSON(jsonresponse)
                     print("originalResponse : ",originalResponse)
                    self?.getSuccessResponse(originalResponse: originalResponse)
                }
                else{
                    self?.tableView.reloadData()
                    self?.tableView.animateTable()
                    self?.lbl.isHidden = false
                }
            }
    
   
        //}
    }
    
    func getSuccessResponse(originalResponse : JSON)
    {
        if(originalResponse.count > 0){
            dataItemList.removeAll()
            let sClass = UserDefaults.standard.string(forKey: "\(SDefaultKeys.schoolClass)\(UserDefaults.standard.integer(forKey: SDefaultKeys.selectedStudent))")?.uppercased() ?? ""
            let div = UserDefaults.standard.string(forKey: "\(SDefaultKeys.schoolDiv)\(UserDefaults.standard.integer(forKey: SDefaultKeys.selectedStudent))")?.uppercased() ?? ""
            switch selectedIndex {
            case "Message":
                print("#")
                for dic in originalResponse.arrayValue {
                    print("##")
                //    if sClass == dic["standard"].stringValue.uppercased() && div == dic["division"].stringValue.uppercased() {
                        print("###")
                        var dItem = DataItem()
                        let tempArr = dic["postAttachment"].arrayValue
                        if tempArr.count > 0 {
                            dItem.postAttachment = tempArr.map{ String(describing: $0) }
                        }
                        dItem.id = dic["id"].stringValue
                        dItem.postDateGmt = dic["postDateGmt"].stringValue
                        dItem.standard = dic["standard"].stringValue
                        dItem.division = dic["division"].string
                        dItem.postContent = dic["postContent"].stringValue
                        dItem.postTitle = dic["postTitle"].stringValue
                        dItem.teacher = dic["teacher"].string
                        dItem.schoolId = dic["school_id"].stringValue
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd"
                        dItem.date = formatter.date(from: dic["postDateGmt"].stringValue) ?? Date()
                        dataItemList.append(dItem)
                 //   }
                }
                    print("####")
                
                localData(forKey: "GetMessage")
                break
            case "Homework":
                for dic in originalResponse.arrayValue {
                    if sClass == dic["standard"].stringValue.uppercased() && div == dic["division"].stringValue.uppercased() {
                        var dItem = DataItem()
                        let tempArr = dic["image"].arrayValue
                        if tempArr.count > 0 {
                            dItem.postAttachment = tempArr.map{ String(describing: $0) }
                        }
                        dItem.postDateGmt = dic["postDate"].stringValue
                        dItem.standard = dic["standard"].stringValue
                        dItem.division = dic["division"].stringValue
                        dItem.postContent = dic["postContent"].stringValue
                        dItem.postTitle = dic["postTitle"].stringValue
                        dItem.subject = dic["subject"].stringValue
                        dItem.schoolId = dic["school_id"].stringValue
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd"
                        dItem.date = formatter.date(from: dic["postDate"].stringValue) ?? Date()
                        dataItemList.append(dItem)
                    }
                }
                localData(forKey: "GetHomework")
                break
            case "Circular":
                for dic in originalResponse.arrayValue {
               //     if sClass == dic["classId"].stringValue.uppercased() {
                        var dItem = DataItem()
                        let tempArr = dic["postAttachment"].arrayValue
                        if tempArr.count > 0 {
                            dItem.postAttachment = tempArr.map{ String(describing: $0) }
                        }
                        dItem.id = dic["id"].stringValue
                        dItem.postDateGmt = dic["postDate"].stringValue
                        dItem.standard = dic["classId"].stringValue
                        dItem.division = dic["class"].stringValue
                        dItem.postContent = dic["postContent"].stringValue
                        dItem.postTitle = dic["postTitle"].stringValue
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd"
                        dItem.date = formatter.date(from: dic["postDate"].stringValue) ?? Date()
                        dataItemList.append(dItem)
                 //   }
                }
                localData(forKey : "GetCircular")
                break
            default:
                break
            }
        }
        else{
            self.tableView.reloadData()
            self.tableView.animateTable()
            self.lbl.isHidden = false
        }
    }
    func localData(forKey : String) {
        if let data = UserDefaults.standard.value(forKey:"\(forKey)") as? Data {
            dataItemListTemp = try! PropertyListDecoder().decode(Array<DataItem>.self, from: data)
            dataItemListTemp.sort(by: {$0.date.timeIntervalSinceNow > $1.date.timeIntervalSinceNow})
            dataItemList.sort(by: {$0.date.timeIntervalSinceNow > $1.date.timeIntervalSinceNow})
            if dataItemList.count > 0 && dataItemList.count >= dataItemListTemp.count {
                tableView.isHidden = false
                tableView.reloadData()
                tableView.animateTable()
                lbl.isHidden = true
            }
            else {
                 lbl.isHidden = false
            }
        }
        else{
            dataItemList.sort(by: {$0.date.timeIntervalSinceNow > $1.date.timeIntervalSinceNow})
            if dataItemList.count > 0 {
                tableView.isHidden = false
                tableView.reloadData()
                tableView.animateTable()
                lbl.isHidden = true
            }
            else {
                lbl.isHidden = false
            }
            UserDefaults.standard.set(try? PropertyListEncoder().encode(dataItemList), forKey:"\(forKey)")
        }
    }
}

extension UIViewController {
    
    func getChildName() -> String {
       /* let name = UserDefaults.standard.string(forKey: "\(SDefaultKeys.name)\(UserDefaults.standard.integer(forKey: SDefaultKeys.selectedStudent))")?.capitalized ?? ""
        let sClass = UserDefaults.standard.string(forKey: "\(SDefaultKeys.schoolClass)\(UserDefaults.standard.integer(forKey: SDefaultKeys.selectedStudent))")?.capitalized ?? ""
        let div = UserDefaults.standard.string(forKey: "\(SDefaultKeys.schoolDiv)\(UserDefaults.standard.integer(forKey: SDefaultKeys.selectedStudent))")?.capitalized ?? ""
         return  "\(name) (\(sClass.uppercased())-\(div))"
         */
        let classdiv = UserDefaults.standard.string(forKey: "SELECTED_CLASS_DIV")!
        let name = UserDefaults.standard.string(forKey: "SELECTED_STUDENTNAME")!
         return  "\(name) (\(classdiv))"
    }
}

struct DataItem : Codable{
    var id: String?
    var postDateGmt: String?
    var standard: String?
    var division: String?
    var postContent: String?
    var postTitle: String?
    var schoolId: String?
    var postAttachment: [String] = []
    var teacher: String?
    var date = Date()
    var subject: String?
}

class TblCell: UITableViewCell {
    
    let dateLbl: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .justified
        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let nameLbl:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.black
        lbl.textAlignment = .justified
        lbl.numberOfLines = 0
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        return lbl
    }()
    
    let descriptionLbl:UITextView = {
           let lbl = UITextView() //UILabel()
         //  lbl.enabledTypes = [.mention, .hashtag, .url]
           lbl.textAlignment = .justified
           let font = UIFont.systemFont(ofSize: 14)
           lbl.font = font
        //   lbl.adjustsFontSizeToFitWidth = true
           lbl.translatesAutoresizingMaskIntoConstraints = false
        //   lbl.numberOfLines = 0
           lbl.accessibilityTraits = .link
           lbl.backgroundColor = UIColor.clear
           lbl.textColor = UIColor.black
           lbl.isUserInteractionEnabled = true
           let text = lbl.text ?? "" as String
           let size = text.size(withAttributes: [NSAttributedString.Key.font:font])
       //    lbl.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height + 120)
           lbl.frame = CGRect(x: 0, y: 0, width: 120, height: 1120)
           lbl.isEditable = false
           lbl.dataDetectorTypes = .link
        //   lbl.translatesAutoresizingMaskIntoConstraints = true
           lbl.isScrollEnabled = false
           lbl.sizeToFit()
           return lbl
       }()
       
    
    let dImageView: UIImageView = {
        let imgView = UIImageView()
        imgView.layer.cornerRadius = 3
        imgView.layer.masksToBounds = true
        imgView.contentMode = .scaleAspectFit
        imgView.image = UIImage(named: "ic_attachment")
        imgView.translatesAutoresizingMaskIntoConstraints  = false
        return imgView
    }()
    
    let containerView:UIView = {
        let v = CardView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = UIColor.white
        v.layer.cornerRadius = 3
        v.layer.masksToBounds = true
        return v
    }()
    
    let descView:UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = UIColor.lightGray.withAlphaComponent(0.4)
        v.layer.cornerRadius = 3
        v.layer.masksToBounds = true
        v.layer.cornerRadius = 3
        v.layer.masksToBounds = true
        return v
    }()
    func setUpViews() {
        
        self.addSubview(containerView)
        // needed constraints
        containerView.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 8).isActive = true
        containerView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        containerView.widthAnchor.constraint(equalTo:self.widthAnchor,constant: -16).isActive = true
        containerView.heightAnchor.constraint(equalTo: self.heightAnchor,constant: -8).isActive = true

        containerView.addSubview(dImageView)
        // needed constraints
        dImageView.topAnchor.constraint(equalTo: containerView.topAnchor,constant:10).isActive = true
        dImageView.rightAnchor.constraint(equalTo: containerView.rightAnchor,constant:-8).isActive = true
        dImageView.widthAnchor.constraint(equalToConstant:40).isActive = true
        dImageView.heightAnchor.constraint(equalToConstant:40).isActive = true
        
        containerView.addSubview(nameLbl)
        // needed constraints
        nameLbl.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        nameLbl.leftAnchor.constraint(equalTo: containerView.leftAnchor,constant: 8).isActive = true
        nameLbl.rightAnchor.constraint(equalTo: dImageView.leftAnchor).isActive = true
        nameLbl.heightAnchor.constraint(equalToConstant:40).isActive = true
        
        
        containerView.addSubview(dateLbl)
        // needed constraints
        dateLbl.leftAnchor.constraint(equalTo: containerView.leftAnchor,constant: 8).isActive = true
        dateLbl.topAnchor.constraint(equalTo: nameLbl.bottomAnchor).isActive = true
        dateLbl.rightAnchor.constraint(equalTo:dImageView.leftAnchor).isActive = true
        dateLbl.heightAnchor.constraint(equalToConstant:20).isActive = true
        
        containerView.addSubview(descView)
        // needed constraints
        descView.topAnchor.constraint(equalTo: dateLbl.bottomAnchor,constant: 8).isActive = true
        descView.rightAnchor.constraint(equalTo: containerView.rightAnchor,constant: -8).isActive = true
        descView.widthAnchor.constraint(equalTo: containerView.widthAnchor,constant: -16).isActive = true
        descView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor,constant: -8).isActive = true
        
        descView.addSubview(descriptionLbl)
        // needed constraints
        descriptionLbl.topAnchor.constraint(equalTo: descView.topAnchor,constant: 8).isActive = true
        descriptionLbl.rightAnchor.constraint(equalTo: descView.rightAnchor,constant: -8).isActive = true
        descriptionLbl.widthAnchor.constraint(equalTo: descView.widthAnchor,constant: -16).isActive = true
        descriptionLbl.heightAnchor.constraint(equalTo: descView.heightAnchor,constant: -16).isActive = true
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        setUpViews()
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension UIDevice {
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        return identifier
    }
    
    
}
