//
//  DailyAttachmentVC.swift
//  CampionSchool
//
//  Created by Intellinects on 14/11/18.
//  Copyright © 2018 Intellinects Ventures. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import QuickLook
class DailyAttachmentVC: BaseViewController, QLPreviewControllerDataSource {
    
    let screenheight = UIScreen.main.bounds.size.height
    var list = [String]()
    var attachmentList = [JSON]()
    let quickLookController = QLPreviewController()
    var fileURLs = [NSURL]()
    var heightConstraint: NSLayoutConstraint?
    
    let tableView : UITableView = {
        let tblView = UITableView()
        tblView.translatesAutoresizingMaskIntoConstraints = false
        tblView.layer.cornerRadius = 5
        tblView.layer.masksToBounds = true
        tblView.backgroundColor = UIColor.white
        tblView.bounces = false
        tblView.separatorInset = UIEdgeInsets.zero
        return tblView
    }()
    
    let btnCloseOverlay: UIButton = {
        let btn = UIButton(type: .system)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(onCloseClick), for: .touchUpInside)
        return btn
    }()
    
    private func setUpViews() {
        view.addSubview(btnCloseOverlay)
        // needed constarunts x,y,w,h
        btnCloseOverlay.topAnchor.constraint(equalTo:view.topAnchor).isActive = true
        btnCloseOverlay.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        btnCloseOverlay.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        btnCloseOverlay.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        setUpTableView()
    }
    private func setUpTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        view.addSubview(tableView)
        //needed constraints x,y,w,h
        tableView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        tableView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        tableView.widthAnchor.constraint(equalTo: view.widthAnchor,multiplier:0.85).isActive = true
        heightConstraint = tableView.heightAnchor.constraint(equalToConstant:0)
        heightConstraint?.isActive = true
    }
    //MARK :- View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        for data in self.attachmentList{
            self.list.append(data.stringValue)
        }
        tableView.register(downloadCell.self, forCellReuseIdentifier: downloadCell.cellId)
        setUpViews()
        quickLookController.dataSource = self
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableView.automaticDimension
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewDidLayoutSubviews()
    }
    
    override func viewDidLayoutSubviews(){
        heightConstraint?.constant =  tableView.contentSize.height > 450 ? 450 : tableView.contentSize.height
    }
    
    @objc func onCloseClick() {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.alpha = 0
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParent()
        })
    }
    
    public func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return fileURLs.count
    }
    public func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem{
        return fileURLs[index]
    }
    func download (_ row : Int) {
        let name :[String] = list[row].components(separatedBy: "/")
        let docpath = name.last
        let fileManager = FileManager.default
        let rootPath = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let pdfPathInDocument = rootPath.appendingPathComponent(docpath!)
        if fileManager.fileExists(atPath: pdfPathInDocument.path){
            fileURLs.removeAll()
            fileURLs.append(pdfPathInDocument as NSURL)
            quickLookController.reloadData()
            if QLPreviewController.canPreview((fileURLs[0])) {
                quickLookController.currentPreviewItemIndex = 0
                navigationController?.pushViewController(quickLookController, animated: true)
            }
        }else{
            if Util.validateNetworkConnection(self) {
                let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                    var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                    documentsURL.appendPathComponent(docpath!)
                    return (documentsURL, [.removePreviousFile])
                }
                showActivityIndicatory(uiView: view)
                self.view.isUserInteractionEnabled = false
                Alamofire.download(list[row], to: destination).responseData { [weak self]response in
                    switch response.result{
                    case .failure(let error):
                        self?.view.isUserInteractionEnabled = true
                        if #available(iOS 10.0, *) {
                            Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false, block: { (_) in
                                self?.stopActivityIndicator()
                                self?.onCloseClick()
                                //Toast(text: "Failed to download due to server error. please try again later", delay: 1, duration: 5).show()
                            })
                        } else {
                            // Fallback on earlier versions
                        }
                        print(error.localizedDescription)
                        return
                    case .success(_):
                         self?.view.isUserInteractionEnabled = true
                        if let destinationUrl = response.destinationURL {
                            self?.stopActivityIndicator()
                            self?.fileURLs.removeAll()
                            self?.fileURLs.append(destinationUrl as NSURL)
                            self?.quickLookController.reloadData()
                            guard let file = self?.fileURLs[0] else {
                                return
                            }
                            if QLPreviewController.canPreview(file) {
                                self?.quickLookController.currentPreviewItemIndex = 0
                                self?.navigationController?.pushViewController((self?.quickLookController)!, animated: true)
                            }
                        }
                    }
                }
            }
        }
    }
}

//MARK:- TableView Methods

extension DailyAttachmentVC: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: downloadCell.cellId, for: indexPath) as! downloadCell
        cell.cImageView.tintColor = GlobleConstants.baseColor
        cell.cImageView.image = UIImage(named: "ic_attachment")?.withRenderingMode(.alwaysTemplate)
        let str = list[indexPath.item]
        let name: [String] = str.components(separatedBy: "/")
        //if name.count > 8 {
        // print("name>>>>>>>>>>>>>>>",name.last)
        cell.nameLabel.text = name.last
        //}
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        download(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let lbl = UILabel()
        lbl.backgroundColor = GlobleConstants.baseColor
        #if STANDREWS
        lbl.textColor = GlobleConstants.navigationBarColorTint
        #else
        lbl.textColor = UIColor.white
        #endif
        lbl.text = "Attachment"
        lbl.textAlignment = .center
        return lbl
    }
}

class downloadCell:UITableViewCell {
    
    static let cellId = "DownloadCell"
    let nameLabel:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.numberOfLines = 0
        lbl.sizeToFit()
        return lbl
    }()
    
    let cImageView:UIImageView = {
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.contentMode = .scaleToFill
        return imgView
    }()
    
    private func setUpViews() {
        contentView.addSubview(nameLabel)
        nameLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor,constant: -10).isActive = true
        nameLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive =   true
        contentView.addSubview(cImageView)
        //needed constarints x,y,w,h
        cImageView.leftAnchor.constraint(equalTo: contentView.leftAnchor,constant:8).isActive = true
        cImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        cImageView.rightAnchor.constraint(equalTo: nameLabel.leftAnchor,constant: -10).isActive = true
        cImageView.heightAnchor.constraint(equalToConstant:30).isActive = true
        cImageView.widthAnchor.constraint(equalToConstant:30).isActive = true
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        setUpViews()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
