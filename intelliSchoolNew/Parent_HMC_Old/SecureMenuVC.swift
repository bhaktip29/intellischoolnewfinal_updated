//
//  SecureMenuVC.swift
//  SchoolProtoTypeApp
//
//  Created by Intellinects on 06/09/17.
//  Copyright © 2017 Intellinects Ventures. All rights reserved.
//

import UIKit
//import AMXFontAutoScale

class SecureMenuVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    let indexPath = UserDefaults.standard.value(forKey: SDefaultKeys.selectedStudent)
    var div = String()
    
    let cellId = "MenuCell"
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    let profileImageView = UIImageView()
    let nameLabel = UILabel()
    let classLabel = UILabel()
    var isdissmis = false
    
    let collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        let clView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        clView.backgroundColor = UIColor.white
        clView.translatesAutoresizingMaskIntoConstraints = false
        return clView
    }()
    
    //MARK:- Custom Views Setup Methods
    
    private func setUpCollectionView() {
        
        view.addSubview(collectionView)
        // needed constarunts x,y,w,h
        collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
    }
    
    private func setUpLogOutBtn(){
        navigationItem.rightBarButtonItem = UIBarButtonItem(image:UIImage(named:"LogoutRounded"), style: .plain, target: self, action: #selector(handleLogout))
        #if DONBOSCO
        navigationItem.rightBarButtonItem?.tintColor = GlobleConstants.homeScreenIconColor
        #else
        navigationItem.rightBarButtonItem?.tintColor = GlobleConstants.navigationBarColorTint
        #endif
    }
    
    private func setUpHomeBtn(){
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image:UIImage(named:"icon-Home"), style: .plain, target: self, action: #selector(handleHome))
        #if DONBOSCO
        navigationItem.leftBarButtonItem?.tintColor = GlobleConstants.homeScreenIconColor
        #else
        navigationItem.leftBarButtonItem?.tintColor = GlobleConstants.navigationBarColorTint
        #endif
    }
    
    func setupNavBar() {
        navigationController?.navigationBar.barTintColor = GlobleConstants.baseColor
        navigationController?.navigationBar.barStyle = .black
        let titleView = UIView()
        titleView.frame = CGRect(x: 0, y: 0, width: (view.frame.size.width / 2) + 30, height: 40)
        titleView.backgroundColor = UIColor.clear
        let profileImageView = UIImageView()
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        profileImageView.contentMode = .scaleAspectFit
        profileImageView.image = UIImage(named: GlobleConstants.menuHeaderImageName)
        profileImageView.isUserInteractionEnabled = true
        titleView.addSubview(profileImageView)
        
        //ios 9 constraint anchors
        //need x,y,width,height anchors
        profileImageView.leftAnchor.constraint(equalTo: titleView.leftAnchor).isActive = true
        profileImageView.centerYAnchor.constraint(equalTo: titleView.centerYAnchor).isActive = true
        profileImageView.widthAnchor.constraint(equalTo:titleView.widthAnchor).isActive = true
        profileImageView.heightAnchor.constraint(equalTo: titleView.heightAnchor).isActive = true
        navigationController?.navigationBar.topItem?.titleView = titleView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserDefaults.standard.set(true, forKey: isOnSecureMenu)
        UserDefaults.standard.synchronize()
        if !UserDefaults.standard.bool(forKey: isLogged) {
            handleHome()
        }
    }
    @objc func handleHome() {
        UserDefaults.standard.set(false, forKey: isOnSecureMenu)
        UserDefaults.standard.removeObject(forKey: "GetHomework")
        UserDefaults.standard.removeObject(forKey: "GetMessage")
        UserDefaults.standard.removeObject(forKey: "GetCircular")

        UserDefaults.standard.synchronize()
     //   let vc = UINavigationController(rootViewController: MenuViewController())
    //    present(vc, animated: true, completion: nil)
//        dismiss(animated: true, completion: nil)
    }
    
    //MARK:- View Controller Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.string(forKey: "\(SDefaultKeys.schoolClass)\(indexPath ?? "0")") != nil
        {
            div = UserDefaults.standard.string(forKey: "\(SDefaultKeys.schoolClass)\(indexPath ?? 0)")!
            if div == ""
            {
                commonAlert(title: "Sorry", message: "Your Division Is Missing. Please Try To Login Again", alertStyle: .alert, okButtonAction: UIAlertAction(title: "Logout", style: .default, handler: { (logoutAction) in
                    self.handleLogout()
                }))
            }
            else
            {
                collectionView.dataSource = self
                collectionView.delegate = self
                collectionView.register(SecureMenuItemCell.self, forCellWithReuseIdentifier: cellId)
                setUpCollectionView()
                setUpLogOutBtn()
                setUpHomeBtn()
                setupNavBar()
            }
        }
        else
        {
            collectionView.dataSource = self
            collectionView.delegate = self
            collectionView.register(SecureMenuItemCell.self, forCellWithReuseIdentifier: cellId)
            setUpCollectionView()
            setUpLogOutBtn()
            setUpHomeBtn()
            setupNavBar()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
        NotificationCenter.default.removeObserver(self)
    }
    
    func showActivityIndicatory(uiView: UIView) {
        
        let loadingView: UIView = UIView()
        loadingView.frame =  CGRect(x: 0, y: 0, width: 80.0, height: 80.0)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor(red: 44/255, green: 44/255, blue: 44/255, alpha: 0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        actInd.frame = CGRect(x: 0, y: 0, width: 40.0, height: 40.0)
        actInd.style =
            UIActivityIndicatorView.Style.whiteLarge
        actInd.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        loadingView.addSubview(actInd)
        uiView.addSubview(loadingView)
        actInd.startAnimating()
    }
    
    func stopActivityIndicator() {
        actInd.stopAnimating()
        let view = actInd.superview
        view?.removeFromSuperview()
    }
    
    //MARK:- CollectionView Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return SecureMenu.menu.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SecureMenuItemCell
        cell.menuNameLbl.text = SecureMenu.menu[indexPath.row]
        cell.imgView.image = UIImage(named: SecureMenu.menuIcons[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/2, height: 100)
     //   return CGSize(width: (collectionView.frame.width-0.2) / 2, height: (collectionView.frame.height - 64) / CGFloat(SecureMenu.menu.count / 2))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){}
}

class SecureMenuItemCell : UICollectionViewCell {
    
    let imgView: UIImageView = {
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.layer.masksToBounds = true
        imgView.contentMode = .scaleAspectFit
        return imgView
    }()
    
    let menuNameLbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
 //       lbl.amx_autoScaleFont(forReferenceScreenSize: .size4Inch)
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpViews()
        self.layer.borderWidth = 0.7
        self.layer.borderColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 0.8).cgColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpViews() {
        
        self.addSubview(imgView)
        
        // Needed constarints x,y,w,h
        imgView.leftAnchor.constraint(equalTo: self.leftAnchor,constant:8).isActive = true
        imgView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        imgView.widthAnchor.constraint(equalTo:self.widthAnchor,multiplier:0.2,constant:4).isActive = true
        imgView.heightAnchor.constraint(equalTo: self.heightAnchor,multiplier: 1/2,constant:4).isActive = true
        
        self.addSubview(menuNameLbl)
        // Needed constarints x,y,w,h
        menuNameLbl.leftAnchor.constraint(equalTo: imgView.rightAnchor,constant:8).isActive = true
        menuNameLbl.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        menuNameLbl.rightAnchor.constraint(equalTo:self.rightAnchor).isActive = true
        menuNameLbl.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
    }
}
