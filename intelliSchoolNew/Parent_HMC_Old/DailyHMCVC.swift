//
//  DailyHMCVC.swift
//  CampionSchool
//
//  Created by Intellinects on 12/11/18.
//  Copyright © 2018 Intellinects Ventures. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
struct circularDetails {
    var id: String?
    var title: String?
    var cDetails: String?
    var cDate: String?
    var attachment = [JSON]()
}
struct messageDetails {
    var id: String?
    var title: String?
    var msgContent: String?
    var date: String?
    var attachment = [JSON]()
    var employee_name: String?
}
struct homeworkDetails {
    var id: String?
    var title: String?
    var hDetails: String?
    var hDate: String?
    var attachment = [JSON]()
    var subject: String?
    var subjectName: String?
    var subjectDisplayName: String?
}
class DailyHMCVC: BaseViewController, DatePikerDelegate{
    
    var barBtnTitle = ""
    var tapGestureRecognizer : UITapGestureRecognizer!
    var circularDetailsList = [circularDetails]()
    var messageDetailsList = [messageDetails]()
    var homeworkDetailsList = [homeworkDetails]()
    let sectionTitle = ["Home work", "Message", "Circular"]
    let sectionIcons = [#imageLiteral(resourceName: "Sports"), #imageLiteral(resourceName: "Feepayment"), #imageLiteral(resourceName: "Inbox-1")]
    let sectionBorderColor = [#colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1), #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1), #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)]
    var studDOB: String?
    var studRollNo : String?
    var studBloodGr: String?
    var isCustomPopup: Bool = true
    var isDatePicker: Bool = false
    private var customView =  UIView()
    var btnDate = UIBarButtonItem()
    var btnDashboard = UIBarButtonItem()
    let tableView : UITableView = {
        let tblView = UITableView()
        tblView.translatesAutoresizingMaskIntoConstraints = false
        tblView.separatorStyle = .singleLine
        tblView.tableFooterView = UIView()
        tblView.rowHeight = UITableView.automaticDimension
        tblView.estimatedRowHeight = 50.0
        tblView.showsVerticalScrollIndicator = false
        tblView.showsHorizontalScrollIndicator = false
        return tblView
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.getProfileData()
        self.view.backgroundColor = .white
        self.title = UserDefaults.standard.string(forKey: "\(SDefaultKeys.name)\(UserDefaults.standard.integer(forKey: SDefaultKeys.selectedStudent))") ?? ""
        self.btnDashboard =  UIBarButtonItem(image: #imageLiteral(resourceName: "assess"), style: .plain, target: self, action: #selector(showDashboard))
        self.btnDate = UIBarButtonItem(title: getFormattedDate(dateString: Date()), style: .plain, target: self, action: #selector(handleDisplayDate))
        navigationItem.rightBarButtonItems = [self.btnDashboard, self.btnDate]
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.setupTableView()
        self.tableView.register(DailyHMCCell.self, forCellReuseIdentifier: DailyHMCCell.cellIdentifire)
        self.getStudentActivity(getYYYYMMDDFormattedDate(dateString: Date()))
//        self.floatingButton()
        customView.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        tapGestureRecognizer = UITapGestureRecognizer(target:self, action: #selector(self.navBarTapped(_:)))
        self.navigationController?.navigationBar.addGestureRecognizer(tapGestureRecognizer)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.removeGestureRecognizer(tapGestureRecognizer)
    }
    @objc func navBarTapped(_ theObject: AnyObject){
//        let vc = StudentInfoPopup()
//        vc.studRollNo = self.studRollNo
//        vc.studDOB = self.studDOB
//        vc.studBloodGr = self.studBloodGr
//        if self.isCustomPopup{
//            self.isCustomPopup = vc.view.isDescendant(of: self.view)
//            vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
//            self.addChildViewController(vc)
//            self.view.addSubview(vc.view)
//            vc.view.layoutIfNeeded()
//            UIView.animate(withDuration: 0.3, animations: { () -> Void in
//                vc.view.alpha = 1
//            }, completion:nil)
//        }else{
//            self.isCustomPopup = true
//            UIView.animate(withDuration: 0.3, animations: { () -> Void in
//            }, completion: { (finished) -> Void in
//                if self.childViewControllers.count > 0{
//                    let viewControllers:[UIViewController] = self.childViewControllers
//                    for viewContoller in viewControllers{
//                        viewContoller.willMove(toParentViewController: nil)
//                        viewContoller.view.removeFromSuperview()
//                        viewContoller.removeFromParentViewController()
//                    }
//                }
//            })
//        }
    }
    @objc func handleDisplayDate(sender: UIBarButtonItem) {
        if self.isDatePicker == false{
            self.isDatePicker = true
            let vc = DatePickerVC()
            vc.delegate = self
            vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            self.addChild(vc)
            view.addSubview(vc.view)
        }
    }
    func handleWithSelectedDate(datePickerSelectedDate date: Date) {
        self.isDatePicker = false
        self.circularDetailsList.removeAll()
        self.homeworkDetailsList.removeAll()
        self.messageDetailsList.removeAll()
        self.btnDate.title = getFormattedDate(dateString: date)
        self.getStudentActivity(getYYYYMMDDFormattedDate(dateString: date))
        navigationItem.rightBarButtonItems = [self.btnDashboard, self.btnDate]
    }
    func dismissDatePicker(_ isPresent: Bool) {
        self.isDatePicker = isPresent
    }
    func setupTableView(){
        self.view.addSubview(self.tableView)
        if #available(iOS 11.0, *){
            self.tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
            self.tableView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor).isActive = true
            self.tableView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor).isActive = true
            self.tableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        }else{
            self.tableView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
            self.tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
            self.tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
            self.tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        }
    }
    @objc func showDashboard(){
     //   let vc = UINavigationController(rootViewController: SecureVC2())
    //    present(vc, animated: true, completion: nil)
    }
    func getStudentActivity(_ date: String){
        self.showActivityIndicatory(uiView: self.view)
        let parameters = [
            "school_id":"\(GlobleConstants.schoolID)",
            "intellinects_id":"\(UserDefaults.standard.string(forKey: "\(SDefaultKeys.intellinectid)\(UserDefaults.standard.integer(forKey: SDefaultKeys.selectedStudent))") ?? "")",
            "date": date
        ]
        Alamofire.request(ParentsWebUrls.studentActivityUrl, method: .post, parameters: parameters)
            .responseJSON { (response) in
                switch response.result{
                case .failure(let error):
                    print("Error occur while processing Data" , error.localizedDescription)
                    self.stopActivityIndicator()
                    return
                case .success(let value):
                    let json = JSON(value)
                    for(key, value) in json{
                        if key == "attendance"{
                            let studname =  UserDefaults.standard.string(forKey: "\(SDefaultKeys.name)\(UserDefaults.standard.integer(forKey: SDefaultKeys.selectedStudent))") ?? ""
                            let attendance = value.stringValue == "" ? "NA" :  value.stringValue
                            self.title = "\(studname) (\(attendance))"
                        }
                        if key == "messages"{
                            for (_, data) in value{
                                self.messageDetailsList.append(messageDetails(id: data["id"].stringValue, title: data["title"].stringValue, msgContent: data["msgContent"].stringValue, date: data["date"].stringValue, attachment: data["attachment"].arrayValue, employee_name: data["teacher_name"].stringValue))
                            }
                        }
                        if key == "homeworks"{
                            for (_, data) in value{
                                self.homeworkDetailsList.append(homeworkDetails(id: data["id"].stringValue, title: data["title"].stringValue, hDetails: data["hDetails"].stringValue, hDate: data["hDate"].stringValue, attachment: data["attachment"].arrayValue, subject: data["subject"].stringValue, subjectName: data["subjectName"].stringValue, subjectDisplayName: data["subjectDisplayName"].stringValue))
                            }
                        }
                        if key == "circulars"{
                            for (_, data) in value{
                                self.circularDetailsList.append(circularDetails(id: data["id"].stringValue, title: data["title"].stringValue, cDetails: data["cDetails"].stringValue, cDate: data["cDate"].stringValue, attachment: data["attachment"].arrayValue))
                            }
                        }
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.stopActivityIndicator()
                    }
                }
        }
    }
    func getProfileData(){
        let  parameters = [
            "school_id": GlobleConstants.schoolID,
            "action": "StudentInfo",
            "intellinectid": UserDefaults.standard.string(forKey: "\(SDefaultKeys.intellinectid)\(UserDefaults.standard.integer(forKey: SDefaultKeys.selectedStudent))") ?? ""
            ] as [String : Any]
        
        Alamofire.request(ParentsWebUrls.studentInfo,method: .post,parameters: parameters)
            .responseJSON { [weak self] (response) in
                print(response)
                switch response.result{
                case .failure(let error):
                    print(error.localizedDescription)
                    return
                case .success(let value):
                    let json = JSON(value)
                    self?.studDOB = json["birthDate"].stringValue
                    self?.studRollNo = json["roll_no"].stringValue
                    self?.studBloodGr = json["bloodGroup"].stringValue
                }
        }
    }
}
extension DailyHMCVC: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sectionTitle.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.homeworkDetailsList.count == 0 ? 1 : (self.homeworkDetailsList.count)
        case 1:
            return self.messageDetailsList.count == 0 ? 1 : (self.messageDetailsList.count)
        case 2:
            return self.circularDetailsList.count == 0 ? 1 : (self.circularDetailsList.count)
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell  = DailyHMCCell()
        switch indexPath.section {
        case 0:
            cell = self.tableView.dequeueReusableCell(withIdentifier: DailyHMCCell.cellIdentifire, for: indexPath) as! DailyHMCCell
            if self.homeworkDetailsList.count > 0{
                cell.lblNoData.isHidden = true
                let homework = self.homeworkDetailsList[indexPath.row]
                cell.lblTitle.text  = homework.subjectDisplayName
                cell.lblDescription.text = homework.hDetails
                if homework.attachment.count > 0{
                    cell.btnAttachment.isHidden = false
                    cell.btnAttachment.tag = indexPath.row
                    cell.btnAttachment.addTarget(self, action: #selector(download(_:)), for: .touchUpInside)
                    cell.btnAttachment.setTitle("homework", for: .selected)
                }else{
                    cell.btnAttachment.isHidden = true
                }
                cell.lblNoData.heightAnchor.constraint(equalToConstant: 50.0).isActive = false
            }
            else{
                cell.lblNoData.isHidden = false
                cell.btnAttachment.isHidden = true
                cell.lblTitle.text = ""
                cell.lblDescription.text = ""
            }
            return cell
        case 1:
            cell = self.tableView.dequeueReusableCell(withIdentifier: DailyHMCCell.cellIdentifire, for: indexPath) as! DailyHMCCell
            if self.messageDetailsList.count > 0{
                cell.lblNoData.isHidden = true
                let message = self.messageDetailsList[indexPath.row]
                cell.lblTitle.text  = message.employee_name
                cell.lblDescription.text = message.msgContent
                if message.attachment.count > 0{
                    cell.btnAttachment.isHidden = false
                    cell.btnAttachment.tag = indexPath.row
                    cell.btnAttachment.addTarget(self, action: #selector(download(_:)), for: .touchUpInside)
                    cell.btnAttachment.setTitle("message", for: .selected)
                }else{
                    cell.btnAttachment.isHidden = true
                }
            }
            else{
                cell.lblNoData.isHidden = false
                cell.btnAttachment.isHidden = true
                cell.lblTitle.text = ""
                cell.lblDescription.text = ""
            }
            return cell
        case 2:
            cell = self.tableView.dequeueReusableCell(withIdentifier: DailyHMCCell.cellIdentifire, for: indexPath) as! DailyHMCCell

            if (self.circularDetailsList.count) > 0{
                cell.lblNoData.isHidden = true
                let circular = self.circularDetailsList[indexPath.row]
                cell.lblTitle.text  = circular.title
                cell.lblDescription.text = circular.cDetails
                if circular.attachment.count > 0{
                    cell.btnAttachment.isHidden = false
                    cell.btnAttachment.tag = indexPath.row
                    cell.btnAttachment.addTarget(self, action: #selector(download(_:)), for: .touchUpInside)
                    cell.btnAttachment.setTitle("circular", for: .selected)
                }else{
                    cell.btnAttachment.isHidden = true
                }
            }
            else{
                cell.lblNoData.isHidden = false
                cell.btnAttachment.isHidden = true
                cell.lblTitle.text = ""
                cell.lblDescription.text = ""
            }
            return cell
        default:
            return cell
        }
    }
    @objc func download(_ sender:UIButton) {
        let vc = DailyAttachmentVC()
        if sender.title(for: .selected) == "homework"{
            vc.attachmentList = self.homeworkDetailsList[sender.tag].attachment
        }else if sender.title(for: .selected) == "message"{
             vc.attachmentList = self.messageDetailsList[sender.tag].attachment
        }else if sender.title(for: .selected) == "circular"{
             vc.attachmentList = self.circularDetailsList[sender.tag].attachment
        }
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChild(vc)
        self.view.addSubview(vc.view)
        vc.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            vc.view.alpha = 1
        }, completion:nil)
    }
}
extension DailyHMCVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DailyHMCDetails()
        let studClass = UserDefaults.standard.string(forKey: "\(SDefaultKeys.schoolClass)\(UserDefaults.standard.integer(forKey: SDefaultKeys.selectedStudent))") ?? ""
        let div = UserDefaults.standard.string(forKey: "\(SDefaultKeys.schoolDiv)\(UserDefaults.standard.integer(forKey: SDefaultKeys.selectedStudent))") ?? ""
        if indexPath.section == 0{
            vc.title = "Homework Details"
            vc.nameLabel = "Homework for \(studClass) - \(div)"
            if self.homeworkDetailsList.count > 0{
                let homework = self.homeworkDetailsList[indexPath.row]
                vc.dateLabel = homework.hDate
                vc.detailLabel = homework.hDetails
                vc.subjectName = homework.subjectDisplayName
                vc.attachment = homework.attachment
                navigationController?.pushViewController(vc, animated: true)
            }
        }else if indexPath.section == 1{
            vc.title = "Message Details"
            vc.nameLabel = "Message for \(studClass) - \(div)"
            if self.messageDetailsList.count > 0{
                let message = self.messageDetailsList[indexPath.row]
                vc.dateLabel = message.date
                vc.detailLabel = message.msgContent
                vc.attachment = message.attachment
                navigationController?.pushViewController(vc, animated: true)
            }
        }else if indexPath.section == 2{
            vc.title = "Circular Details"
            vc.nameLabel = "Circular for \(studClass) - \(div)"
            if self.circularDetailsList.count > 0{
                let circular = self.circularDetailsList[indexPath.row]
                vc.dateLabel = circular.cDate
                vc.detailLabel = circular.cDetails
                vc.subjectName = circular.title
                vc.attachment = circular.attachment
                navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let uiView = UIView()
        var imageView = UIImageView()
        let label = UILabel()
        uiView.layer.borderColor = self.sectionBorderColor[section].cgColor
        imageView = UIImageView(image: self.sectionIcons[section])
        label.text = self.sectionTitle[section]
        uiView.layer.borderWidth = 0.5
        uiView.backgroundColor = UIColor.groupTableViewBackground
        imageView.frame = CGRect(x: 8, y: 5, width: 40, height: 40)
        label.frame = CGRect(x: 56, y: 5, width: 100, height: 40)
        uiView.addSubview(imageView)
        uiView.addSubview(label)
        return uiView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
}

class DailyHMCCell: UITableViewCell {
    static let cellIdentifire = "Cell"
    var lblTitle: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.boldSystemFont(ofSize: 14.0)
        lbl.textColor = .black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 2
        lbl.sizeToFit()
        return lbl
    }()
    var lblDescription: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 13.0)
        lbl.textColor = .darkGray
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 2
        lbl.sizeToFit()
        return lbl
    }()
    var lblNoData: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 13.0)
        lbl.textColor = .darkGray
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 1
        lbl.sizeToFit()
        lbl.text = "There are no updates today."
        return lbl
    }()
    var btnAttachment : UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setImage(#imageLiteral(resourceName: "home-color"), for: .normal)
        return btn
    }()
    func setUpViews(){
        self.addSubview(self.btnAttachment)
        self.btnAttachment.topAnchor.constraint(equalTo: self.topAnchor, constant: 8.0).isActive = true
        self.btnAttachment.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8.0).isActive = true
        self.btnAttachment.widthAnchor.constraint(equalToConstant: 30.0).isActive = true
        self.btnAttachment.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        self.addSubview(self.lblTitle)
        self.lblTitle.topAnchor.constraint(equalTo: self.topAnchor, constant: 8.0).isActive = true
        self.lblTitle.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8.0).isActive = true
        self.lblTitle.rightAnchor.constraint(equalTo: self.btnAttachment.leftAnchor, constant: -8.0).isActive = true
        self.addSubview(lblDescription)
        self.lblDescription.topAnchor.constraint(equalTo: self.lblTitle.bottomAnchor, constant: 5.0).isActive = true
        self.lblDescription.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8.0).isActive = true
        self.lblDescription.rightAnchor.constraint(equalTo: self.btnAttachment.leftAnchor, constant: -8.0).isActive = true
        self.lblDescription.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8.0).isActive = true
        
        self.addSubview(self.lblNoData)
        self.lblNoData.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        self.lblNoData.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        self.lblNoData.topAnchor.constraint(equalTo: self.topAnchor, constant: 16.0).isActive = true
        self.lblNoData.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8.0).isActive = true
        self.lblNoData.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8.0).isActive = true
        self.lblNoData.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -16.0).isActive = true
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        self.setUpViews()
        self.lblNoData.isHidden = true
        self.btnAttachment.isHidden = true
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
