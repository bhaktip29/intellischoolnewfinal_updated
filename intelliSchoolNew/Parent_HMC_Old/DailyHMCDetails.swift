//
//  DailyHMCDetails.swift
//  CampionSchool
//
//  Created by Intellinects on 14/11/18.
//  Copyright © 2018 Intellinects Ventures. All rights reserved.
//

import UIKit
import SwiftyJSON
class DailyHMCDetails:BaseViewController {
    let tableView: UITableView = {
        let tbl = UITableView(frame: CGRect.zero, style: .grouped)
        tbl.translatesAutoresizingMaskIntoConstraints = false
        return tbl
    }()
    var nameLabel : String?
    var dateLabel: String?
    var detailLabel: String?
    var subjectName: String?
    var attachment = [JSON]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.navigationItem.rightBarButtonItem = nil
        self.tableView.register(detailHMCCell.self, forCellReuseIdentifier: detailHMCCell.Identifier)
    }
    func setupTableView(){
        tableView.tableHeaderView = UIView(frame: CGRect(x:0,y:0,width:0,height:CGFloat.leastNormalMagnitude))
        view.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 200
        tableView.backgroundColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.alwaysBounceVertical = false
        view.addSubview(tableView)
        if #available(iOS 11.0, *) {
            tableView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor).isActive = true
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
            tableView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor).isActive = true
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
            tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
    }
    @objc func download() {
        let vc = DailyAttachmentVC()
        vc.attachmentList = self.attachment
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChild(vc)
        self.view.addSubview(vc.view)
        vc.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            vc.view.alpha = 1
        }, completion:nil)
    }
}
extension DailyHMCDetails: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: detailHMCCell.Identifier, for: indexPath) as! detailHMCCell
        let attributedText = NSMutableAttributedString(string: self.nameLabel ?? "", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16)])
        cell.nameLbl.attributedText = attributedText
        cell.dateLabel.text = self.dateLabel ?? ""
        cell.detailLabel.text = self.detailLabel ?? ""
        cell.downloadLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(download)))
        if !(self.title == "Message Details"){
            self.hideNshowSubjectView(cell, 30.0, 16.0)
            cell.subjectLabel.text = self.subjectName ?? ""
        }else{
            self.hideNshowSubjectView(cell, 0.0, 0.0)
        }
        if self.attachment.count > 0{
            self.hideNshowDownloadView(cell, 30.0, 16.0)
        }else{
            self.hideNshowDownloadView(cell, 0.0, 0.0)
        }
        return cell
    }
    func hideNshowDownloadView(_ cell: detailHMCCell, _ widthHeight: CGFloat, _ topBottom: CGFloat){
        cell.downloadImageView.topAnchor.constraint(equalTo:cell.detailLabel.bottomAnchor, constant: topBottom).isActive = true
        cell.downloadImageView.widthAnchor.constraint(equalToConstant: widthHeight).isActive = true
        cell.downloadImageView.heightAnchor.constraint(equalToConstant: widthHeight).isActive = true
        cell.downloadLabel.topAnchor.constraint(equalTo: cell.detailLabel.bottomAnchor, constant: topBottom).isActive = true
        cell.downloadLabel.heightAnchor.constraint(equalToConstant: widthHeight).isActive = true
    }
    func hideNshowSubjectView(_ cell: detailHMCCell, _ widthHeight: CGFloat, _ topBottom: CGFloat){
        cell.subjectImageView.topAnchor.constraint(equalTo:cell.dateLabel.bottomAnchor, constant: topBottom).isActive = true
        cell.subjectImageView.widthAnchor.constraint(equalToConstant: widthHeight).isActive = true
        cell.subjectImageView.heightAnchor.constraint(equalToConstant: widthHeight).isActive = true
        cell.subjectLabel.topAnchor.constraint(equalTo: cell.dateLabel.bottomAnchor, constant: topBottom).isActive = true
        cell.subjectLabel.heightAnchor.constraint(equalToConstant: widthHeight).isActive = true
    }
}
extension DailyHMCDetails: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
class detailHMCCell: UITableViewCell{
    static let Identifier = "Cell"
    let screenheight = UIScreen.main.bounds.size.height
    let containerView:UIView = {
        let v = CardView()
        v.backgroundColor = UIColor.white
        v.translatesAutoresizingMaskIntoConstraints = false
        v.layer.masksToBounds = true
        return v
    }()
    let downloadLabel:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.numberOfLines = 0
        lbl.sizeToFit()
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
        let underlineAttributedString = NSAttributedString(string: "Download Attachments", attributes: underlineAttribute)
        lbl.attributedText = underlineAttributedString
        lbl.isUserInteractionEnabled = true
        lbl.textColor = GlobleConstants.baseColor
        lbl.font = UIFont.boldSystemFont(ofSize: 14)
        return lbl
    }()
    let downloadImageView:UIImageView = {
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.contentMode = .scaleToFill
        imgView.image = UIImage(named: "ic_attachment")?.withRenderingMode(.alwaysTemplate)
        imgView.tintColor = GlobleConstants.baseColor
        return imgView
    }()
    
    let detailLabel:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.numberOfLines = 0
        lbl.clipsToBounds = true
        lbl.sizeToFit()
        return lbl
    }()
    let detailImageView:UIImageView = {
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.contentMode = .scaleToFill
        imgView.image = UIImage(named: "ic_description")?.withRenderingMode(.alwaysTemplate)
        imgView.tintColor = GlobleConstants.baseColor
        return imgView
    }()
    
    let dateLabel:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.numberOfLines = 0
        lbl.sizeToFit()
        return lbl
    }()
    
    let cImageView:UIImageView = {
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.contentMode = .scaleToFill
        imgView.image = UIImage(named: "ic_month")?.withRenderingMode(.alwaysTemplate)
        imgView.tintColor = GlobleConstants.baseColor
        return imgView
    }()
    let subjectView: UIView = {
        let subjectName = UIView()
        subjectName.backgroundColor = GlobleConstants.baseColor
        subjectName.translatesAutoresizingMaskIntoConstraints = false
        subjectName.layer.masksToBounds = true
        return subjectName
    }()
    let nameLbl:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        #if DONBOSCO
         lbl.textColor = GlobleConstants.homeScreenIconColor
        #else
         lbl.textColor = GlobleConstants.navigationBarColorTint
        #endif
       
        lbl.textAlignment = .justified
        lbl.numberOfLines = 0
        lbl.font = UIFont.systemFont(ofSize: 14)
        return lbl
    }()
    let subjectLabel:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.numberOfLines = 0
        lbl.clipsToBounds = true
        lbl.sizeToFit()
        return lbl
    }()
    let subjectImageView:UIImageView = {
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.contentMode = .scaleToFill
        imgView.image = UIImage(named: "ic_subject")?.withRenderingMode(.alwaysTemplate)
        imgView.tintColor = GlobleConstants.baseColor
        return imgView
    }()
    func setAnchorToViews(){
        ///Mark:- ContainerView
        self.addSubview(containerView)
        self.containerView.topAnchor.constraint(equalTo: self.topAnchor,constant: 5.0).isActive = true
        self.containerView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 5.0).isActive = true
        self.containerView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -5.0).isActive = true
        self.containerView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5.0).isActive = true
        
        ///Mark:- SubjectView
        self.containerView.addSubview(self.subjectView)
        self.subjectView.topAnchor.constraint(equalTo: self.containerView.topAnchor, constant: 5.0).isActive = true
        self.subjectView.leftAnchor.constraint(equalTo: self.containerView.leftAnchor, constant: 5.0).isActive = true
        self.subjectView.rightAnchor.constraint(equalTo: self.containerView.rightAnchor, constant: -5.0).isActive = true
        self.subjectView.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        
        ///Mark:- Subject Label
        self.subjectView.addSubview(self.nameLbl)
        self.nameLbl.topAnchor.constraint(equalTo: self.subjectView.topAnchor, constant: 5.0).isActive = true
        self.nameLbl.leftAnchor.constraint(equalTo: self.subjectView.leftAnchor, constant: 11.0).isActive = true
        self.nameLbl.rightAnchor.constraint(equalTo: self.subjectView.rightAnchor, constant: -0.11).isActive = true
        self.nameLbl.bottomAnchor.constraint(equalTo: self.subjectView.bottomAnchor, constant: -0.5).isActive = true
        
        ///Mark:- Calendar ImageView
        self.containerView.addSubview(self.cImageView)
        self.cImageView.topAnchor.constraint(equalTo: self.subjectView.bottomAnchor, constant: 16.0).isActive = true
        self.cImageView.leftAnchor.constraint(lessThanOrEqualTo: self.containerView.leftAnchor, constant: 16.0).isActive = true
        self.cImageView.widthAnchor.constraint(equalToConstant: 30.0).isActive = true
        self.cImageView.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        
        ///MARK:- Date Label
        self.containerView.addSubview(self.dateLabel)
        self.dateLabel.topAnchor.constraint(equalTo: self.subjectView.bottomAnchor, constant: 16.0).isActive = true
        self.dateLabel.leftAnchor.constraint(equalTo: self.cImageView.rightAnchor, constant: 16.0).isActive = true
        self.dateLabel.rightAnchor.constraint(equalTo: self.containerView.rightAnchor, constant: -16.0).isActive = true
        self.dateLabel.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        
        ///MARK:- Detail ImageView
        self.containerView.addSubview(self.subjectImageView)
        self.subjectImageView.topAnchor.constraint(equalTo:self.cImageView.bottomAnchor, constant: 16.0).isActive = true
        self.subjectImageView.leftAnchor.constraint(equalTo: self.containerView.leftAnchor, constant: 16.0).isActive = true
        self.subjectImageView.widthAnchor.constraint(equalToConstant: 30.0).isActive = true
        self.subjectImageView.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        
        ///MARK:- Detail Label
        self.containerView.addSubview(self.subjectLabel)
        self.subjectLabel.topAnchor.constraint(equalTo: self.dateLabel.bottomAnchor, constant: 16.0).isActive = true
        self.subjectLabel.leftAnchor.constraint(equalTo: self.subjectImageView.rightAnchor, constant: 16.0).isActive = true
        self.subjectLabel.rightAnchor.constraint(equalTo: self.containerView.rightAnchor, constant: -16.0).isActive = true
        self.subjectLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 30.0).isActive = true
        
        ///MARK:- Detail ImageView
        self.containerView.addSubview(self.detailImageView)
        self.detailImageView.topAnchor.constraint(equalTo:self.subjectImageView.bottomAnchor, constant: 16.0).isActive = true
        self.detailImageView.leftAnchor.constraint(equalTo: self.containerView.leftAnchor, constant: 16.0).isActive = true
        self.detailImageView.widthAnchor.constraint(equalToConstant: 30.0).isActive = true
        self.detailImageView.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        
        ///MARK:- Detail Label
        self.containerView.addSubview(self.detailLabel)
        self.detailLabel.topAnchor.constraint(equalTo: self.subjectLabel.bottomAnchor, constant: 16.0).isActive = true
        self.detailLabel.leftAnchor.constraint(equalTo: self.detailImageView.rightAnchor, constant: 16.0).isActive = true
        self.detailLabel.rightAnchor.constraint(equalTo: self.containerView.rightAnchor, constant: -16.0).isActive = true
        self.detailLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 30.0).isActive = true
        
        ///MARK:- download ImageView
        self.containerView.addSubview(self.downloadImageView)
        self.downloadImageView.leftAnchor.constraint(equalTo: self.containerView.leftAnchor, constant: 16.0).isActive = true
        
        ///MARK:- Download Label
        self.containerView.addSubview(self.downloadLabel)
        self.downloadLabel.leftAnchor.constraint(lessThanOrEqualTo: self.downloadImageView.rightAnchor, constant: 16.0).isActive = true
        self.downloadLabel.rightAnchor.constraint(lessThanOrEqualTo: self.containerView.rightAnchor, constant: -16.0).isActive = true
        self.downloadLabel.bottomAnchor.constraint(equalTo: self.containerView.bottomAnchor, constant: -16.0).isActive = true
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        self.setAnchorToViews()
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
@IBDesignable class CardView: UIView {
    @IBInspectable var cornerRadius: CGFloat = 2
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 1
    @IBInspectable var shadowColor: UIColor? = UIColor.gray
    @IBInspectable var shadowOpacity: Float = 0.5
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }
}
//@IBDesignable class CardView: UIView {
//    @IBInspectable var cornerRadius: CGFloat = 2
//    @IBInspectable var shadowOffsetWidth: Int = 0
//    @IBInspectable var shadowOffsetHeight: Int = 1
//    @IBInspectable var shadowColor: UIColor? = UIColor.gray
//    @IBInspectable var shadowOpacity: Float = 0.5
//    override func layoutSubviews() {
//        layer.cornerRadius = cornerRadius
//        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
//        layer.masksToBounds = false
//        layer.shadowColor = shadowColor?.cgColor
//        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
//        layer.shadowOpacity = shadowOpacity
//        layer.shadowPath = shadowPath.cgPath
//    }
//}

