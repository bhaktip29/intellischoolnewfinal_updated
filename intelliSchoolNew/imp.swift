//
//  imp.swift
//  intelliSchoolNew
//
//  Created by rupali  on 04/11/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import Foundation
import UIKit
//tab bar font size
//let fontAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18.0)]
//UITabBarItem.appearance().setTitleTextAttributes(fontAttributes, for: .normal)
//
//corner circular
//btn_send.layer.cornerRadius = 10.0

//
//Cell.layer.cornerRadius = 10.0
//
//Cell.clipsToBounds = true

import UIKit

class SelfSizedTableView: UITableView {
    var maxHeight: CGFloat = UIScreen.main.bounds.size.height
    
    override func reloadData() {
        super.reloadData()
        self.invalidateIntrinsicContentSize()
        self.layoutIfNeeded()
    }
    
    override var intrinsicContentSize: CGSize {
        let height = min(contentSize.height, maxHeight)
        return CGSize(width: contentSize.width, height: height)
    }
}

