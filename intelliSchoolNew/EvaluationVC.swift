//
//  EvaluationVC.swift
//  intelliSchoolNew
//
//  Created by Intellinects Venture on 13/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import QuickLook

class EvaluationVC: UIViewController {
        let quickLookController = QLPreviewController()
        var fileURLs = [NSURL]()
        var reportBeeDetails = [ReportBeeModal]()
        var fileName : String?
        
        let tableView: UITableView = {
        let tbl = UITableView()
        tbl.translatesAutoresizingMaskIntoConstraints =  false
        return tbl
        }()
        override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = UIColor(red: 25.0/255.0, green:89.0/255.0, blue: 194.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain,target: self, action: #selector(handleSlide))
        navigationItem.leftBarButtonItem?.tintColor = .white//GlobleConstants.navigationBarColorTint
            //  navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain,target: self, action: #selector(handleSlide1))
        navigationItem.rightBarButtonItem?.tintColor = .white
        
        self.view.backgroundColor = .white
        self.tableView.register(ReportBeeTableViewCell.self, forCellReuseIdentifier: ReportBeeTableViewCell.cellId)
        self.tableView.estimatedRowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 80.0
        self.tableView.delegate = self as! UITableViewDelegate
        self.tableView.dataSource = self as! UITableViewDataSource
        self.quickLookController.dataSource = self as! QLPreviewControllerDataSource
            
        self.setupTableView()
        self.getReportBeeDetails()
            
        }
        @objc func handleSlide() {
           print("back button tapped..")
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
                  let controller = storyboard.instantiateViewController(withIdentifier: "vc")
                  self.present(controller, animated: true, completion: nil)
       }
        func setupTableView(){
        self.view.addSubview(self.tableView)
        self.tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        self.tableView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        self.tableView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        }
        @objc func tapOnAttachment(_ sender: UIButton){
//          print("Tapped with tag : ", sender.tag)
//          print("RC_URL: ", sender.title(for: .selected) ?? "")
            let attachment =   sender.title(for: .selected) ?? ""
            let str = attachment.removingPercentEncoding!
            let name :[String] = str.components(separatedBy: "/")
            
//            guard name.count > 6 else {
//                return
//            }
//            let docpath = name[5]
            
            var docpath = fileName
            docpath?.append("\(UserDefaults.standard.string(forKey: "\(SDefaultKeys.intellinectid)\(UserDefaults.standard.integer(forKey: SDefaultKeys.selectedStudent))") ?? "")")
            docpath?.append(".pdf")
          
            let fileManager = FileManager.default
            let rootPath = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let pdfPathInDocument = rootPath.appendingPathComponent(docpath!)
            
            if fileManager.fileExists(atPath: pdfPathInDocument.path){
                sender.setTitle("Open", for: UIControl.State.normal)
                fileURLs.removeAll()
                fileURLs.append(pdfPathInDocument as NSURL)
                quickLookController.reloadData()
                if QLPreviewController.canPreview((fileURLs[0])) {
                    quickLookController.currentPreviewItemIndex = 0
                  navigationController?.pushViewController(quickLookController, animated: true)
                }
            }
            else
            {
                if Util.validateNetworkConnection(self) {
                    let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                        var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                        documentsURL.appendPathComponent(docpath!)
                        return (documentsURL, [.removePreviousFile])
                    }
                  //  showActivityIndicatory(uiView: view)
                    self.view.isUserInteractionEnabled = false
                    Alamofire.download(str, to: destination).responseData { [weak self]response in
                         if let destinationUrl = response.destinationURL {
                            self?.view.isUserInteractionEnabled = true
                         //   self?.stopActivityIndicator()
                            self?.fileURLs.removeAll()
                            self?.fileURLs.append(destinationUrl as NSURL)
                            self?.quickLookController.reloadData()
//                            guard let file = self?.fileURLs[0] else {
//                                return
//                            }
                            if QLPreviewController.canPreview((self?.fileURLs[0])!) {
                                self?.quickLookController.currentPreviewItemIndex = 0
                                self?.navigationController?.pushViewController((self?.quickLookController)!, animated: true)
                            }
                            
                        }
                    }
                }
            }
        }
        private func getReportBeeDetails(){
       //      self.showActivityIndicatory(uiView: view)
            let parameters = [
                            "school_id": GlobleConstants.schoolID,
                            "third_party_student_id": "\(UserDefaults.standard.string(forKey: "\(SDefaultKeys.intellinectid)\(UserDefaults.standard.integer(forKey: SDefaultKeys.selectedStudent))") ?? "")"
                            ] as [String : Any]
            
        print(parameters)
        Alamofire.request(ParentsWebUrls.reportbee, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            print(response.result)
        switch response.result {
            
        case .failure(let error):
        print(error)
        break
        case .success(let value):
          //  self.stopActivityIndicator()
        self.reportBeeDetails.removeAll()
        let json = JSON(value)
        if json["status"].boolValue{
        let rcURLs = json["all_rc_urls"].arrayValue
        if rcURLs.count > 0{
        for i in 0..<rcURLs.count{
        self.reportBeeDetails.append(ReportBeeModal(report_id: rcURLs[i]["report_id"].stringValue, name: rcURLs[i]["name"].stringValue, rc_pdf_url: rcURLs[i]["rc_pdf_url"].stringValue))
        }
        }
        DispatchQueue.main.async {
        self.tableView.reloadData()
         self.tableView.animateTable()
        }
        }else{
        print(json["message"].stringValue)
        }
        break
        }
        }
        }
        }
        extension EvaluationVC: UITableViewDelegate, UITableViewDataSource{
            func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                return self.reportBeeDetails.count
            }
            func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                let cell = self.tableView.dequeueReusableCell(withIdentifier: ReportBeeTableViewCell.cellId, for: indexPath) as! ReportBeeTableViewCell
                let reportBee = self.reportBeeDetails[indexPath.row]
                cell.lblReportBeeTitle.text = reportBee.name
                cell.btnAttachment.tag = indexPath.row
                cell.btnAttachment.setTitle(reportBee.rc_pdf_url, for: .selected)
                cell.btnAttachment.addTarget(self, action: #selector(self.tapOnAttachment(_:)), for: .touchUpInside)
                fileName=reportBee.name
              
                let attachment =   reportBee.rc_pdf_url
                let str = attachment?.removingPercentEncoding!
                let name :[String] = str!.components(separatedBy: "/")
                var docpath = fileName
                docpath?.append("\(UserDefaults.standard.string(forKey: "\(SDefaultKeys.intellinectid)\(UserDefaults.standard.integer(forKey: SDefaultKeys.selectedStudent))") ?? "")")
                docpath?.append(".pdf")
                
                let fileManager = FileManager.default
                let rootPath = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
                let pdfPathInDocument = rootPath.appendingPathComponent(docpath!)
                
                if fileManager.fileExists(atPath: pdfPathInDocument.path){
                     cell.btnAttachment.setTitle("Open", for: UIControl.State.normal)
                }else{
                     cell.btnAttachment.setTitle("Download", for: UIControl.State.normal)
                }

                return cell
            }
            func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
                return UITableView.automaticDimension
            }
            
        }

extension EvaluationVC: QLPreviewControllerDataSource{
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return fileURLs.count
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
         return fileURLs[index]
    }
}
        class ReportBeeTableViewCell: UITableViewCell{
            static let cellId = "reportBeeCell"
            let lblReportBeeTitle : UILabel = {
                let lbl = UILabel()
                lbl.translatesAutoresizingMaskIntoConstraints = false
                lbl.numberOfLines = 0
                return lbl
            }()
            
            let btnAttachment : UIButton = {
                let btn =  UIButton()
                btn.translatesAutoresizingMaskIntoConstraints = false
                btn.setTitle("Download", for: .normal)
                btn.setTitleColor(.blue, for: .normal)
                btn.titleLabel?.textAlignment = .center
                return btn
            }()
            
            override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
                super.init(style: style, reuseIdentifier: reuseIdentifier)
                self.selectionStyle = .none
                self.setupView()
            }
            required init?(coder aDecoder: NSCoder) {
                fatalError("init(coder:) has not been implemented")
            }
            func setupView(){
                self.addSubview(self.btnAttachment)
                self.btnAttachment.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8.0).isActive = true
                self.btnAttachment.topAnchor.constraint(equalTo: self.topAnchor, constant: 8.0).isActive = true
                self.btnAttachment.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8.0).isActive = true
                self.btnAttachment.widthAnchor.constraint(equalToConstant: 100.0).isActive = true
                
                self.addSubview(self.lblReportBeeTitle)
                self.lblReportBeeTitle.topAnchor.constraint(equalTo: self.topAnchor, constant: 8.0).isActive = true
                self.lblReportBeeTitle.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 8.0).isActive = true
                self.lblReportBeeTitle.trailingAnchor.constraint(equalTo: self.btnAttachment.leadingAnchor, constant: -8.0).isActive = true
                self.lblReportBeeTitle.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8.0).isActive = true
            }
        }
        struct ReportBeeModal {
            let report_id : String?
            let name: String?
            let rc_pdf_url: String?
}
