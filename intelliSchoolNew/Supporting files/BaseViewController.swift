//
//  BaseViewController.swift
//  Intellinects
//
//  Created by rupali  on 03/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    var isFromMainMenu = false
    let lbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "No result found...!!!"
        lbl.textColor = .black
        lbl.textAlignment = .center
        return lbl
        
    }()
    
    func setUpErrorLbl() {
        view.addSubview(lbl)
        // needed constarunts x,y,w,h
        lbl.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        lbl.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        lbl.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        lbl.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbl.isHidden = true
        view.backgroundColor = UIColor.white
        let nav = navigationController?.navigationBar
//        nav?.barTintColor = GlobleConstants.baseColor
        #if DONBOSCO
        nav?.tintColor = GlobleConstants.homeScreenIconColor
        nav?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: GlobleConstants.homeScreenIconColor]
        #else
//        nav?.tintColor = GlobleConstants.navigationBarColorTint
//        nav?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: GlobleConstants.navigationBarColorTint]
        #endif
        if UserDefaults.standard.bool(forKey: isLogged) {
          //  setUpLogOutBtn()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !UserDefaults.standard.bool(forKey: isLogged) {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
       // NotificationCenter.default.addObserver(self, selector: //#selector(pushNotificationReceived), name: NSNotification.Name(rawValue: "redirectNotification"), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
//    @objc func pushNotificationReceived(notification: NSNotification) {
//
//        if navigationController?.viewControllers[0] is SecureVC {
//            let vc = navigationController?.viewControllers[0] as! SecureVC
//            vc.isdissmis = true
//        }
//        navigationController?.popToRootViewController(animated: true)
//    }
    
//    func setUpLogOutBtn(){
//        navigationItem.rightBarButtonItem = UIBarButtonItem(image:UIImage(named:"LogoutRounded"), style: .plain, target: self, action: #selector(handleLogout))
//        navigationItem.rightBarButtonItem?.tintColor = GlobleConstants.navigationBarColorTint
//    }
    
    
    func getFormattedDate(dateString: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "d-M-yyyy"
        return formatter.string(from: dateString)
    }
    
    func getYYYYMMDDFormattedDate(dateString: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: dateString)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showActivityIndicatory(uiView: UIView) {
        
        let loadingView: UIView = UIView()
        loadingView.frame =  CGRect(x: 0, y: 0, width: 80.0, height: 80.0)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor(red: 44/255, green: 44/255, blue: 44/255, alpha: 0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        actInd.frame = CGRect(x: 0, y: 0, width: 40.0, height: 40.0)
        actInd.style =
            UIActivityIndicatorView.Style.whiteLarge
        actInd.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        loadingView.addSubview(actInd)
        uiView.addSubview(loadingView)
        actInd.startAnimating()
    }
    
    func stopActivityIndicator() {
        actInd.stopAnimating()
        let view = actInd.superview
        view?.removeFromSuperview()
    }
}



