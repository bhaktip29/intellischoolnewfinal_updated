//
//  floating.swift
//  intelliSchoolNew
//
//  Created by rupali  on 04/11/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import Foundation
import UIKit
class FloatingLabeledTextField: UITextField {
    
    var floatingLabel: UILabel!
    var placeHolderText: String?
    
    var floatingLabelColor: UIColor = UIColor(hexString: "#1862C6") {
        didSet {
            self.floatingLabel.textColor = floatingLabelColor
        }
        
//        var floatingLabelFont: UIFont = UIFont.systemFont(ofSize: 15) {
        
//        didSet {
//            self.floatingLabel.font = floatingLabelHeight
//        }
        }
        
        var floatingLabelHeight: CGFloat = 30
        
        override init(frame: CGRect) {
        super.init(frame: frame)
        }
        
        required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        let flotingLabelFrame = CGRect(x: 0, y: 0, width: frame.width, height: 0)
        
        floatingLabel = UILabel(frame: flotingLabelFrame)
        floatingLabel.textColor = floatingLabelColor
    //    floatingLabel.font = floatingLabelFont
        floatingLabel.text = self.placeholder
        
        self.addSubview(floatingLabel)
        placeHolderText = placeholder
        
        NotificationCenter.default.addObserver(self, selector: #selector(textFieldDidBeginEditing), name: UITextField.textDidBeginEditingNotification, object: self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(textFieldDidEndEditing), name: UITextField.textDidEndEditingNotification, object: self)
            self.addTarget(self, action: #selector(self.textFieldShouldBeginEditing(_:)), for: UIControl.Event.editingChanged)
    self.addTarget(self, action: #selector(self.textFieldShouldClear(_:)), for: UIControl.Event.editingChanged)
            self.addTarget(self, action: #selector(self.textFieldShouldEndEditing(_:)), for: UIControl.Event.editingChanged)
     //   self.addTarget(self, action: #selector(self.textField(_:)), for: UIControl.Event.editingChanged)
  //      self.addTarget(self, action: #selector(self.textFieldShouldReturn(_:)), for: UIControl.Event.editingChanged)
        }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.setBottomBorder()

    }
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        print("TextField did begin editing method called")
//    }
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        print("TextField did end editing method called\(textField.text!)")
//    }
  @objc  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
    self.setBottomBorderHighlighted()

        return true;
    }
   @objc func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
    self.setBottomBorderHighlighted()

        return true;
    }
   @objc func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
    self.setBottomBorder()
        return true;
    }
   @objc func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
    self.setBottomBorderHighlighted()

        return true;
    }
   @objc func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
    self.setBottomBorder()

        return true;
    }

        @objc func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if self.text == "" {
        UIView.animate(withDuration: 0.3) {
        self.floatingLabel.frame = CGRect(x: 0, y: -self.floatingLabelHeight, width: self.frame.width, height: self.floatingLabelHeight)
        }
        self.placeholder = ""
            self.setBottomBorderHighlighted()
        }
        }
        
        @objc func textFieldDidEndEditing(_ textField: UITextField) {
        
        if self.text == "" {
        UIView.animate(withDuration: 0.1) {
        self.floatingLabel.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 0)
        }
        self.placeholder = placeHolderText
            self.setBottomBorder()
        }
        }
    
        
        deinit {
        
        NotificationCenter.default.removeObserver(self)
        
        }
}

