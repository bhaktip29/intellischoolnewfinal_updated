//
//  Util.swift
//  SchoolProtoTypeApp
//
//  Created by Intellinects on 01/09/17.
//  Copyright © 2017 Intellinects Ventures. All rights reserved.
//

import Foundation
import Alamofire
import UIKit
import Contacts
import ContactsUI
import SwiftyJSON

class Util {
    
    class func nsDateFromTS(_ forString:String,format2:String)-> String
    {
        let date = Date(timeIntervalSince1970: (Double(forString)!))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format2
        let str = dateFormatter.string(from: date)
        let date2 = dateFormatter.date(from: str)!.addingTimeInterval(60*60*24*1)
        let str2 = dateFormatter.string(from: date2)
        return str2
    }
    
    class func registerDeviceIDonServer(_ deviceID : String)
    {
        //call web service to check login valid start
        let parameters = [
            "action": "registrationservice_save" as AnyObject,
            "registrationId":deviceID as AnyObject,
            "mobilePlatformId":"2" as AnyObject,
            "SCHOOL_ID": "306" as AnyObject,
            "projectAppNumber": ""
            ] as [String: AnyObject]
        Alamofire.request(ParentsWebUrls.baseUrl,method: .get, parameters: parameters )
            .responseString(completionHandler: { (responseData) in
                print(responseData)
            })
    }
    
    class func alert(_ title: String, message: String, controller : UIViewController ) {
        
        let alert = UIAlertController(title: title, message:message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
        
        let popOver = alert.popoverPresentationController
        popOver?.sourceView  = controller.view
        popOver?.permittedArrowDirections = UIPopoverArrowDirection.any
        
        controller.present(alert, animated: true){}
    }
    
    class func checkConnection()->Bool {
        //        let status = ReachTeacher().connectionStatus()
        //        switch status {
        //        case .unknown, .offline:
        //            return false
        //        case .online(.wwan):
        //            return true
        //        case .online(.wiFi):
        //            return true
        //        }
        return true
    }
    
    class func validateNetworkConnection(_ with: AnyObject) -> Bool {
        
        if checkConnection() {
            return true
        } else
        {
            let alertController = UIAlertController(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
            {
                (result : UIAlertAction) -> Void in
            }
            alertController.addAction(okAction)
            let controller = with as? UIViewController
            controller?.present(alertController, animated: true, completion: nil)
        }
        return false
    }
    
    
    class func UIColorFromRGB(_ rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    class func dateConvert(_ format1:String,format2:String,datetoconvert:String)->String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format1
        let date = dateFormatter.date(from: datetoconvert)! as Date
        dateFormatter.dateFormat = format2
        return dateFormatter.string(from: date)
        
    }
    
    class func getNsDefaultValue(_ forKey:String) -> String
    {
        var NsValue = String()
        if UserDefaults.standard.string(forKey: forKey) != nil
        {
            NsValue = UserDefaults.standard.string(forKey: forKey)!
        }
        return NsValue
    }
    
    class func dateFromTS(_ forString:String,format2:String)->String
    {
        let date = Date(timeIntervalSince1970: (Double(forString)!))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format2
        return dateFormatter.string(from: date)
    }
}
