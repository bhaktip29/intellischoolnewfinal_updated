//
//  GlobleConstants.swift
//  Intellinects
//
//  Created by rupali  on 03/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//
 
import UIKit

let isLogged = "kLoggedIn"
let isOnSecureMenu = "kSecureMenu"
let profile = "Child Profile"

struct GlobleConstants {
    // change the image name of login, register and password screen
    static let logoImageNameforLRP = "LOGO"
    static let SWITCH_NOTIFY = "SWITCH_NOTIFY"
    
    /////////////////////////////////////////////////////////////////////////////////
    
    static let baseColor = UIColor(red: 24/255, green: 98/255, blue: 198/255, alpha: 0)
       static let statusBarColor = UIColor(red: 0/255, green: 118.0/255, blue: 142.0/255, alpha: 1.0)
       static let noteTxt = "Note:- Please enter the number registered with the school."
       static let noteTxtColor = UIColor.black
       static let homeScreenIconColor = baseColor
       static let homeScreenTextColor = UIColor.black
       static let homeScreenBGColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
       static let navigationBarColorTint = UIColor.white
       static let menuHeaderImageName = "header"
       static let menuHeaderForTeacher = "teacherheader"
       static let schoolname = "iSchool"
       static let schoolID = "327"
       static let schoolCode = "INTELL"
       static let appno = ""
       static let appUrl = "itms-apps://itunes.apple.com/us/app/ischool1-5/id1462264659?ls=1&mt=8"
    
    ////////////////////////////////////////////////////////////////////////////////
    #if HOLYCROSS
   // change basecolor here
    static let baseColor = UIColor(red: 200.0/255, green: 164.0/255, blue: 82.0/255, alpha: 1.0)
    static let statusBarColor = UIColor(red: 25.0/255, green: 99.0/255, blue: 82.0/255, alpha: 1.0)
    // Note text
    static let noteTxt = "Note:- Please enter the number registered with the school."
    static let noteTxtColor = UIColor.black
    static let homeScreenIconColor = baseColor
    static let homeScreenTextColor = UIColor.black
    static let homeScreenBGColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
    static let navigationBarColorTint = UIColor.white
    // chnage the header image name here
    static let menuHeaderImageName = "header"
    static let menuHeaderForTeacher = "teacherheader"
    static let schoolname = "Holy Cross Convent High School"
    static let schoolID = "306"
    static let schoolCode = "HCCHMR"
    static let appno = ""
    static let appUrl = "itms-apps://"

    #elseif INTELLISCHOOLNEW
    static let baseColor = UIColor(red: 0/255, green: 164.0/255, blue: 191.0/255, alpha: 1.0)
    static let statusBarColor = UIColor(red: 0/255, green: 118.0/255, blue: 142.0/255, alpha: 1.0)
    static let noteTxt = "Note:- Please enter the number registered with the school."
    static let noteTxtColor = UIColor.black
    static let homeScreenIconColor = baseColor
    static let homeScreenTextColor = UIColor.black
    static let homeScreenBGColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
    static let navigationBarColorTint = UIColor.white
    static let menuHeaderImageName = "header"
    static let menuHeaderForTeacher = "teacherheader"
    static let schoolname = "iSchool"
    static let schoolID = "327"
    static let schoolCode = "INTELL"
    static let appno = ""
    static let appUrl = "itms-apps://itunes.apple.com/us/app/ischool1-5/id1462264659?ls=1&mt=8"
    
    #elseif ISCHOOL
    static let baseColor = UIColor(red: 0/255, green: 164.0/255, blue: 191.0/255, alpha: 1.0)
    static let statusBarColor = UIColor(red: 0/255, green: 118.0/255, blue: 142.0/255, alpha: 1.0)
    static let noteTxt = "Note:- Please enter the number registered with the school."
    static let noteTxtColor = UIColor.black
    static let homeScreenIconColor = baseColor
    static let homeScreenTextColor = UIColor.black
    static let homeScreenBGColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
    static let navigationBarColorTint = UIColor.white
    static let menuHeaderImageName = "header"
    static let menuHeaderForTeacher = "teacherheader"
    static let schoolname = "iSchool"
    static let schoolID = "327"
    static let schoolCode = "INTELL"
    static let appno = ""
    static let appUrl = "itms-apps://itunes.apple.com/us/app/ischool1-5/id1462264659?ls=1&mt=8"
    
    #elseif STMARY
    static let baseColor = UIColor(red:0.04, green:0.16, blue:0.85, alpha:1.0)
    static let noteTxt = "Note:- Please enter the number registered with the school."
    static let noteTxtColor = UIColor.black
    static let homeScreenIconColor = baseColor
    static let homeScreenTextColor = UIColor.black
    static let homeScreenBGColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
    static let navigationBarColorTint = UIColor.white
    static let menuHeaderImageName = "header"
    static let menuHeaderForTeacher = "teacherheader"
    static let schoolname = "St Mary's ICSE"
    static let schoolID = "8"
    static let schoolCode = "STMARY"
    static let appno = "309445275136"
    static let appUrl = "itms-apps://itunes.apple.com/us/app/st-marys-icse/id1082319704?ls=1&mt=8"
    
    #elseif CAMPION
    static let baseColor = UIColor(red:0.20, green:0.30, blue:0.56, alpha:1.0)
    static let noteTxt = "Note:- Please enter the number registered with the school."
    static let noteTxtColor = UIColor.black
    static let homeScreenIconColor = baseColor
    static let homeScreenTextColor = UIColor.black
    static let homeScreenBGColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
    static let navigationBarColorTint = UIColor.white
    static let menuHeaderImageName = "header"
    static let menuHeaderForTeacher = "teacherheader"
    static let schoolname = "Campion School"
    static let schoolID = "15"
    static let schoolCode = "CAMPMU"
    static let appno = "309445275136"
    static let appUrl = "itms-apps://itunes.apple.com/us/app/campion-school/id1018731703?ls=1&mt=8"
    
    #elseif BEACONHIGH
    static let baseColor = UIColor(red: 63/255, green: 81/255, blue: 181/255, alpha: 1)
    static let noteTxt = "Note:- Please enter the number registered with the school."
    static let noteTxtColor = UIColor.black
    static let homeScreenIconColor = baseColor
    static let homeScreenTextColor = UIColor.black
    static let homeScreenBGColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
    static let navigationBarColorTint = UIColor.white
    static let menuHeaderImageName = "header"
    static let menuHeaderForTeacher = "teacherheader"
    static let schoolname = "Beacon High"
    static let schoolID = "151"
    static let schoolCode = "BEACON"
    static let appno = "309445275136"
    static let appUrl = "itms-apps://itunes.apple.com/us/app/beacon-high/id1213798753?ls=1&mt=8"
    
    #elseif STANDREWS
    static let baseColor = UIColor(red: 255/255, green: 251/255, blue: 0/255, alpha: 1)
    static let noteTxt = "Note:- Please enter the number registered with the school."
    static let noteTxtColor = UIColor.black
    static let homeScreenIconColor = baseColor
    static let homeScreenTextColor = UIColor.black
    static let homeScreenBGColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
    static let navigationBarColorTint = UIColor.black
    static let menuHeaderImageName = "header"
    static let menuHeaderForTeacher = "teacherheader"
    static let schoolname = "St. Andrews High School"
    static let schoolID = "32"
    static let schoolCode = "SASBDR"
    static let appno = "309445275136"
    static let appUrl = "itms-apps://itunes.apple.com/us/app/st-andrews-high-school-bandra/id1001512463?ls=1"
    
    #elseif DONBOSCOINT
    static let baseColor = UIColor(red: 15/255, green: 106/255, blue: 178/255, alpha: 1)
    static let noteTxt = "Note:- Please enter the number registered with the school."
    static let noteTxtColor = UIColor.black
    static let homeScreenIconColor = baseColor
    static let homeScreenTextColor = UIColor.black
    static let homeScreenBGColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
    static let navigationBarColorTint = UIColor.white
    static let menuHeaderImageName = "header"
    static let menuHeaderForTeacher = "teacherheader"
    static let schoolname = "Don Bosco International School"
    static let schoolID = "290"
    static let schoolCode = "DBSINT"
    static let appno = "309445275136"
    static let appUrl = "itms-apps://itunes.apple.com/us/app/don-bosco-international/id1449460021?ls=1&mt=8"
    
    #elseif SACREDHEARTINT
    static let baseColor = UIColor(red: 41/255, green: 22/255, blue: 111/255, alpha: 1)
    static let noteTxt = "Note:- Please enter the number registered with the school."
    static let noteTxtColor = UIColor.black
    static let homeScreenIconColor = baseColor
    static let homeScreenTextColor = UIColor.black
    static let homeScreenBGColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
    static let navigationBarColorTint = UIColor.white
    static let menuHeaderImageName = "header"
    static let menuHeaderForTeacher = "teacherheader"
    static let schoolname = "Sacred Heart International"
    static let schoolID = "333"
    static let schoolCode = "SHINTL"
    static let appno = "309445275136"
    static let appUrl = "itms-apps://itunes.apple.com/us/app/sacred-heart-international/id1448865978?ls=1&mt=8"
    
    #elseif DONBOSCO
    static let baseColor = UIColor(red:1.00, green:0.97, blue:0.90, alpha:1.0)
    static let noteTxt = "Note:- Please enter the number registered with the school."
    static let noteTxtColor = UIColor.black
    static let homeScreenIconColor = UIColor(red:0.24, green:0.41, blue:0.62, alpha:1.0)
    static let homeScreenTextColor = UIColor.black
    static let homeScreenBGColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
    static let navigationBarColorTint = UIColor.white
    static let menuHeaderImageName = "header"
    static let menuHeaderForTeacher = "teacherheader"
    static let schoolname = "Don Bosco High School"
    static let schoolID = "17"
    static let schoolCode = "DONBSH"
    static let appno = "309445275136"
    static let appUrl = "itms-apps://itunes.apple.com/us/app/don-bosco-high-school-matunga/id1253356706?ls=1"
    
    #elseif SMALLWONDERS
    static let baseColor = UIColor(red: 0/255, green: 91/255, blue: 174/255, alpha: 1)
    static let noteTxt = "Note:- Please enter the number registered with the school."
    static let noteTxtColor = UIColor.black
    static let homeScreenIconColor = baseColor
    static let homeScreenTextColor = UIColor.black
    static let homeScreenBGColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
    static let navigationBarColorTint = UIColor.white
    static let menuHeaderImageName = "header"
    static let menuHeaderForTeacher = "teacherheader"
    static let schoolname = "Small Wonders"
    static let schoolID = "152"
    static let schoolCode = "SMLWDR"
    static let appno = "309445275136"
    static let appUrl = "itms-apps://itunes.apple.com/us/app/small-wonders/id1217722526?ls=1"
    
    #elseif SHARDAMANDIR
    static let baseColor = UIColor(red:0.93, green:0.93, blue:0.63, alpha:1.0)
    static let noteTxt = "Note:- Please enter the number registered with the school."
    static let noteTxtColor = UIColor.black
    static let homeScreenIconColor = UIColor(red:0.00, green:0.28, blue:0.51, alpha:1.0)
    static let homeScreenTextColor = UIColor.black
    static let homeScreenBGColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
    static let navigationBarColorTint = UIColor(red:0.00, green:0.28, blue:0.51, alpha:1.0)
    static let menuHeaderImageName = "header"
    static let menuHeaderForTeacher = "teacherheader"
    static let schoolname = "Sharda Mandir High School"
    static let schoolID = "91"
    static let schoolCode = "SHARDA"
    static let appno = ""
    static let appUrl = "itms-apps://itunes.apple.com/us/app/sharada-mandir-high-school/id1148837324?ls=1"
    
    #elseif APOSTOLIC
    static let baseColor = UIColor(red: 191/255, green: 30/255, blue: 45/255, alpha: 1)
    static let noteTxt = "Note:- Please enter the number registered with the school."
    static let noteTxtColor = UIColor.black
    static let homeScreenIconColor = baseColor
    static let homeScreenTextColor = UIColor.black
    static let homeScreenBGColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
    static let navigationBarColorTint = UIColor.white
    static let menuHeaderImageName = "header"
    static let menuHeaderForTeacher = "teacherheader"
    static let schoolname = "Apostolic Carmel ICSE"
    static let schoolID = "252"
    static let schoolCode = "ACICSE"
    static let appno = "309445275136"
    static let appUrl = "itms-apps://itunes.apple.com/us/app/apostolic-carmel-icse/id1448865821?ls=1"
    #elseif DRANTONIODA
    static let baseColor = UIColor(red: 254/255, green: 225/255, blue: 121/255, alpha: 1)
    static let noteTxt = "Note:- Please enter the number registered with the school."
    static let noteTxtColor = UIColor.black
    static let homeScreenIconColor = UIColor(red: 26/255, green: 19/255, blue: 163/255, alpha: 1)
    static let homeScreenTextColor = UIColor.black
    static let homeScreenBGColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
    static let navigationBarColorTint = UIColor(red: 26/255, green: 19/255, blue: 163/255, alpha: 1)
    static let menuHeaderImageName = "header"
    static let menuHeaderForTeacher = "teacherheader"
    static let schoolname = "Dr. Antonio da Silva High School & Jr College"
    static let schoolID = "102"
    static let schoolCode = "DRADHS"
    static let appno = "309445275136"
    static let appUrl = "itms-apps://itunes.apple.com/us/app/dr-antonio-da-silva-educational-trust/id1172786398?ls=1"
#elseif DRANTONIOTECHNICAL
static let baseColor = UIColor(red: 254/255, green: 225/255, blue: 121/255, alpha: 1)
static let noteTxt = "Note:- Please enter the number registered with the school."
static let noteTxtColor = UIColor.black
static let homeScreenIconColor = UIColor(red: 26/255, green: 19/255, blue: 163/255, alpha: 1)
static let homeScreenTextColor = UIColor.black
static let homeScreenBGColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
static let navigationBarColorTint = UIColor(red: 26/255, green: 19/255, blue: 163/255, alpha: 1)
static let menuHeaderImageName = "header"
static let menuHeaderForTeacher = "teacherheader"
static let schoolname = "Dr Antonio da Silva Technical School & Jr College"
static let schoolID = "105"
static let schoolCode = "DRADTS"
static let appno = ""
static let appUrl = ""
#endif
}

struct ParentsWebUrls {
    static let baseUrl = "https://isirs.org/intellinect_dashboard/WS/index_main.php?"
    static let newsUrl = "\(baseUrl)action=news"
    static let eventsUrl = "\(baseUrl)action=events"
    static let loginAPI = "https://isirs.org/api/v1.5/login.php?"
    static let getChildrenInfo = "https://isirs.org/api/v1.0/my-childrens.php?"
    static let terms = "http://test.intellischools.org/privacy_policy.html"
    static let studentActivityUrl = "http://isirs.org/intellinect_dashboard/WS/student_activity.php"
    static let paymentUrl = "http://isirs.org/intellinect_dashboard/feepay/getAllPaymentPlans"
    static let studentInfo = "https://isirs.org/quiz_app/Student_api.php?"
    static let faq = "http://faq.isirs.org/public/"
    static let updateFCM = "https://isirs.org/intellinect_dashboard/WS/Student_api.php"
     static let reportbee = "https://evaluation.intellischools.org/superadmin/evaluationPHPAPIForRB/reportCardDownloadApi.php"
    static let moodleLink = ""

    
    ////////////////////////////////////////////////////////////////////////////////
    static let basePath1 = "https://intellischools.org/"
    static let facilities = "https://intellischools.org/"
    static let contactus = "https://intellischools.org/"
    static let aboutus = "https://intellischools.org/"
    static let principal_message = "https://intellischools.org/"
    static let morenews = "\(basePath1)news"
    static let facebook = "https://www.facebook.com/Intellischools-547368815465267/"
    static let campustour = ""
    static let youtubeAPI = ""
    static let youtubeCL = ""
    static let LMS_URL = "https://lms.intellischools.org/Parent/login/\(GlobleConstants.schoolCode)/"
    static let HEALTHCARE_URL = "https://healthcare.intellischools.org/HealthcareParent/login"
    static let roleofhonar = ""
    static let curriculum = ""
    static let library = ""
    
    ////////////////////////////////////////////////////////////////////////////////
    
    
    #if HOLYCROSS
    static let basePath = "https://holycrossconventschoolmiraroad.org/"
    static let facilities = "https://holycrossconventschoolmiraroad.org/"
    static let contactus = "https://holycrossconventschoolmiraroad.org/"
    static let aboutus = "https://holycrossconventschoolmiraroad.org/"
    static let principal_message = "https://holycrossconventschoolmiraroad.org/"
    static let morenews = "\(basePath)news"
    static let facebook = "https://www.facebook.com/pages/category/School/Holy-Cross-Convent-High-School-Mira-Road-Mumbai-190025884355695/"
    static let campustour = ""
    static let youtubeAPI = ""
    static let youtubeCL = ""
    static let LMS_URL = "https://lms.intellischools.org/Parent/login/\(GlobleConstants.schoolCode)/"
    static let HEALTHCARE_URL = "https://healthcare.intellischools.org/HealthcareParent/login"
    static let roleofhonar = ""
    static let curriculum = ""
    static let library = ""
    
    #elseif INTELLISCHOOL
       static let basePath = "https://intellischools.org/"
       static let facilities = "https://intellischools.org/"
       static let contactus = "https://intellischools.org/"
       static let aboutus = "https://intellischools.org/"
       static let principal_message = "https://intellischools.org/"
       static let morenews = "\(basePath)news"
       static let facebook = "https://www.facebook.com/Intellischools-547368815465267/"
       static let campustour = ""
       static let youtubeAPI = ""
       static let youtubeCL = ""
       static let LMS_URL = "https://lms.intellischools.org/Parent/login/\(GlobleConstants.schoolCode)/"
       static let HEALTHCARE_URL = "https://healthcare.intellischools.org/HealthcareParent/login"
       static let roleofhonar = ""
       static let curriculum = ""
       static let library = ""
    
    #elseif ISCHOOL
    static let basePath = "https://intellischools.org/"
    static let facilities = "https://intellischools.org/"
    static let contactus = "https://intellischools.org/"
    static let aboutus = "https://intellischools.org/"
    static let principal_message = "https://intellischools.org/"
    static let morenews = "\(basePath)news"
    static let facebook = "https://www.facebook.com/Intellischools-547368815465267/"
    static let campustour = ""
    static let youtubeAPI = ""
    static let youtubeCL = ""
    static let LMS_URL = "https://lms.intellischools.org/Parent/login/\(GlobleConstants.schoolCode)/"
    static let HEALTHCARE_URL = "https://healthcare.intellischools.org/HealthcareParent/login"
    static let roleofhonar = ""
    static let curriculum = ""
    static let library = ""
    #elseif STMARY
    static let basePath = "https://stmarysicse.com/"
    static let facilities = "\(basePath)facilities/"
    static let contactus = "\(basePath)contact-us/"
    static let aboutus = "\(basePath)about-us/"
    static let principal_message = "\(basePath)about-us/principals-desk/"
    static let morenews = "\(basePath)news"
    static let facebook = "https://www.facebook.com/stmarysicsemazgaon/"
    static let campustour = ""
    static let youtubeAPI = "AIzaSyDIFx_lGF7o8m-ereSV-P6BMsof0gMtXCY"
    static let youtubeCL = "UCgGWlHURR4vJlJJkAY25GOw"
    static let LMS_URL = "https://lms.intellischools.org/Parent/login/\(GlobleConstants.schoolCode)/"
    static let HEALTHCARE_URL = "https://healthcare.intellischools.org/HealthcareParent/login"
    static let roleofhonar = ""
    static let curriculum = ""
    static let library = ""
    #elseif CAMPION
    static let basePath = "https://campionschool.in/"
    static let facilities = "\(basePath)about-us/campus-tour/"
    static let contactus = "\(basePath)contact-us/"
    static let aboutus = "\(basePath)about-us/"
    static let morenews = "\(basePath)news"
    static let facebook = "https://www.facebook.com/CampionSchoolMumbai/"
    static let campustour = "\(basePath)about-us/campus-tour/"
    static let youtubeAPI = "AIzaSyC5AojbORg3QEPmjdQBmnd2VKyxABvxpXo"
    static let youtubeCL = "UCbooNSHrg0AoDA9j4DNIONA"
    static let principal_message = "\(basePath)about-us/principals-desk/"
    static let LMS_URL = "https://lms.intellischools.org/Parent/login/\(GlobleConstants.schoolCode)/"
    static let HEALTHCARE_URL = "https://healthcare.intellischools.org/HealthcareParent/login"
    static let roleofhonar = ""
    static let curriculum = ""
    static let library = ""
    #elseif BEACONHIGH
    static let basePath = "http://www.beaconhigh.in/"
    static let facilities = "\(basePath)highlights/"
    static let contactus = "\(basePath)contact-us/"
    static let aboutus = "\(basePath)about-us/"
    static let morenews = "\(basePath)news"
    static let facebook = "https://www.facebook.com/beaconhighkhar/"
    static let campustour = ""
    static let youtubeAPI = "AIzaSyDlnwPBBJDb_jTIbW-oLUPNjdW9GQMUmaQ"
    static let youtubeCL = "UCU5j1JwguiK3M1nFPlqXAOg"
    static let principal_message = "\(basePath)about-us/principals-message/"
    static let LMS_URL = "https://lms.intellischools.org/Parent/login/\(GlobleConstants.schoolCode)/"
    static let HEALTHCARE_URL = "https://healthcare.intellischools.org/HealthcareParent/login"
    static let roleofhonar = "\(basePath)roll-of-honor/"
    static let curriculum = ""
    static let library = ""
    #elseif STANDREWS
    static let basePath = "https://saintandrewschoolbandra.in/"
    static let facilities = "\(basePath)facilities/"
    static let contactus = "\(basePath)contact-us/"
    static let aboutus = "\(basePath)about-us/"
    static let morenews = "\(basePath)news/"
    static let facebook = "https://www.facebook.com/PROUDTOBEANDREAN/"
    static let campustour = ""
    static let youtubeAPI = "AIzaSyCgEPYZ84MOLeu6cvCYUwS6it_39hedNpo"
    static let youtubeCL = "UCfdf7kpBXPMRSq-uCBuALBw"
    static let principal_message = "\(basePath)about-us/principals-message/"
    static let LMS_URL = "https://lms.intellischools.org/Parent/login/\(GlobleConstants.schoolCode)/"
    static let HEALTHCARE_URL = "https://healthcare.intellischools.org/HealthcareParent/login"
    static let roleofhonar = ""
    static let curriculum = ""
    static let library = ""
    #elseif DONBOSCOINT
    static let basePath = "https://dbis.in/"
    static let facilities = "\(basePath)about-us/facilities/"
    static let contactus = "\(basePath)contact-us/"
    static let aboutus = "\(basePath)about-us/"
    static let morenews = "\(basePath)news/"
    static let facebook = "https://www.facebook.com/dbismatunga/"
    static let campustour = ""
    static let youtubeAPI = "AIzaSyC5AojbORg3QEPmjdQBmnd2VKyxABvxpXo"
    static let youtubeCL = "UCn9RDu0z6DUSv3P0f8tFLvw"
    static let principal_message = "\(basePath)/about-us/from-the-principals-desk/"
    static let LMS_URL = "https://lms.intellischools.org/Parent/login/\(GlobleConstants.schoolCode)/"
    static let HEALTHCARE_URL = "https://healthcare.intellischools.org/HealthcareParent/login"
    static let roleofhonar = ""
    static let curriculum = ""
    static let library = ""
    #elseif SACREDHEARTINT
    static let basePath = "https://sacredheartintl.org/"
    static let facilities = "\(basePath)daily-activities/"
    static let contactus = "\(basePath)contact-us/"
    static let aboutus = "\(basePath)about-us/"
    static let morenews = "\(basePath)news/"
    static let facebook = "https://www.facebook.com/sacredheartintlsantacruz/"
    static let campustour = ""
    static let youtubeAPI = ""
    static let youtubeCL = ""
    static let principal_message = "\(basePath)"
    static let LMS_URL = "https://lms.intellischools.org/Parent/login/\(GlobleConstants.schoolCode)/"
    static let HEALTHCARE_URL = "https://healthcare.intellischools.org/HealthcareParent/login"
    static let roleofhonar = ""
    static let curriculum = ""
    static let library = ""
    #elseif DONBOSCO
    static let basePath = "https://donboscomatunga.com/"
    static let facilities = "\(basePath)facilities/"
    static let contactus = "\(basePath)contact-us/"
    static let aboutus = "\(basePath)about-us/"
    static let morenews = "\(basePath)news/"
    static let facebook = "https://www.facebook.com/pages/category/School/Don-Bosco-Matunga-516863601814593"
    static let campustour = ""
    static let youtubeAPI = "AIzaSyCgEPYZ84MOLeu6cvCYUwS6it_39hedNpo"
    static let youtubeCL = "UCpCgRmBElnaiqmsbQcY3Sjg"
    static let principal_message = "\(basePath)about-us/principals-desk/"
    static let LMS_URL = "https://lms.intellischools.org/Parent/login/\(GlobleConstants.schoolCode)/"
    static let HEALTHCARE_URL = "https://healthcare.intellischools.org/HealthcareParent/login"
    static let roleofhonar = ""
    static let curriculum = ""
    static let library = ""
    #elseif SMALLWONDERS
    static let basePath = "http://small-wonders.in/"
    static let facilities = "\(basePath)facilities/"
    static let contactus = "\(basePath)contact-us/"
    static let aboutus = "\(basePath)about-us/"
    static let morenews = "\(basePath)gallery/"
    static let facebook = "https://www.facebook.com/smallwonderskhar/?nr"
    static let campustour = ""
    static let youtubeAPI = "AIzaSyCgEPYZ84MOLeu6cvCYUwS6it_39hedNpo"
    static let youtubeCL = "UC0rWGeWtbHUQ76OxVGtqrWg"
    static let principal_message = "\(basePath)about-us/principals-desk/"
    static let LMS_URL = "https://lms.intellischools.org/Parent/login/\(GlobleConstants.schoolCode)/"
    static let HEALTHCARE_URL = "https://healthcare.intellischools.org/HealthcareParent/login"
    static let roleofhonar = ""
    static let curriculum = "\(basePath)curriculum/"
    static let library = ""
    #elseif SHARDAMANDIR
    static let basePath = "http://smhighschool.edu.in/"
    static let facilities = "\(basePath)facilities/"
    static let contactus = "\(basePath)contact-us/"
    static let aboutus = "\(basePath)about-us/"
    static let morenews = "\(basePath)news"
    static let facebook = "https://www.facebook.com/smhighschoolbylodhafoundation/"
    static let campustour = ""
    static let youtubeAPI = "AIzaSyD_qIX6V2ShgRwalgWCk_FlllmUrcEAnE0"
    static let youtubeCL = "UCBgr0KhrKz-5UaUoKgjJ1OQ"
    static let principal_message = "\(basePath)about-us/chairpersons-message/"
    static let LMS_URL = "https://lms.intellischools.org/Parent/login/\(GlobleConstants.schoolCode)/"
    static let HEALTHCARE_URL = "https://healthcare.intellischools.org/HealthcareParent/login"
    static let roleofhonar = ""
    static let curriculum = ""
    static let library = ""
    #elseif APOSTOLIC
    static let basePath = "https://apostoliccarmelicse.org/"
    static let facilities = "\(basePath)extra-curricular/"
    static let contactus = "\(basePath)contact-us/"
    static let aboutus = "\(basePath)about-us/our-vision/"
    static let morenews = "\(basePath)school-news/"
    static let facebook = "https://www.facebook.com/sacredheartintlsantacruz/"
    static let campustour = ""
    static let youtubeAPI = ""
    static let youtubeCL = ""
    static let principal_message = "\(basePath)about-us/principals-message/"
    static let LMS_URL = "https://lms.intellischools.org/Parent/login/\(GlobleConstants.schoolCode)/"
    static let HEALTHCARE_URL = "https://healthcare.intellischools.org/HealthcareParent/login"
    static let roleofhonar = "\(basePath)faculty-staff/"
    static let curriculum = ""
    static let library = ""
    #elseif DRANTONIODA
    static let basePath = "https://drantoniodasilva.com/"
    static let facilities = "\(basePath)facilities/"
    static let contactus = "\(basePath)contact-us/"
    static let aboutus = "\(basePath)about-us/"
    static let morenews = "\(basePath)news/"
    static let facebook = "https://www.facebook.com/pages/Dr-Antonio-Da-Silva-High-School-and-Junior-College-of-Commerce/551992711487884"
    static let campustour = ""
    static let youtubeAPI = "AIzaSyAxhh_CN63BKZ-Es1gwyBQ_e-4FaV2kzYk"
    static let youtubeCL = ""
    static let principal_message = "\(basePath)about-us/chairpersons-message/"
    static let LMS_URL = "https://lms.intellischools.org/Parent/login/\(GlobleConstants.schoolCode)/"
    static let HEALTHCARE_URL = "https://healthcare.intellischools.org/HealthcareParent/login"
    static let roleofhonar = ""
    static let curriculum = ""
    static let library = "http://drantonioelibrary.org/"
    #elseif DRANTONIOTECHNICAL
    static let basePath = "https://drantoniodasilva.com/"
    static let facilities = "\(basePath)facilities/"
    static let contactus = "\(basePath)contact-us/"
    static let aboutus = "\(basePath)about-us/"
    static let morenews = "\(basePath)news/"
    static let facebook = "https://www.facebook.com/pages/Dr-Antonio-Da-Silva-High-School-and-Junior-College-of-Commerce/551992711487884"
    static let campustour = ""
    static let youtubeAPI = "AIzaSyAxhh_CN63BKZ-Es1gwyBQ_e-4FaV2kzYk"
    static let youtubeCL = ""
    static let principal_message = "\(basePath)about-us/chairpersons-message/"
    static let LMS_URL = "https://lms.intellischools.org/Parent/login/\(GlobleConstants.schoolCode)/"
    static let HEALTHCARE_URL = "https://healthcare.intellischools.org/HealthcareParent/login"
    static let roleofhonar = ""
    static let curriculum = ""
    static let library = "http://drantonioelibrary.org/"
    #endif
}
struct TeachersWebUrls {
    static let baseUrl = "https://isirs.org/intellischools_teachers_app/"
    // employee registration check
    static let registrationUrl = "\(baseUrl)ws_nec/employee_device_registration.php"
    static let faqsUrl = "http://faq.isirs.org/public/"
    static let supportUrl = "http://faq.isirs.org/public/userRaiseTicket?"
}
struct EventGrid {
    static let start1 : UInt = 0x322435
    static let middle1: UInt = 0x023a47
    static let end1 : UInt = 0x4c3000
    static let start : UInt = 0x8a638f
    static let middle: UInt = 0x0bc1f0
    static let end : UInt = 0xffa200
}
struct WebserviceConstantResponse {
    static let AlreadyRegister = "UserIsAlreadyRegister"
    static let RegisteredSuccess = "UserregisterAndSMSSend"
    static let RegisteredFail = "UserNotregister"
    static let RegisteredWithOtherNo = "DeviceRegisterWithAnotherNumber"
    static let MobileNoNotRegistered = "Mobile number is not register"
}

struct SecureMenu {
    static var menu = ["Messages","Homework","Circulars","Child Profile","Class TimeTable","Exam TimeTable","Attendance","Evaluation","Learning Management","Healthcare","Bus Tracking","Help Ticket","FAQ", "Fee Payment"]
    static var menuIcons = ["ic_messages","ic_homework","ic_circular","ic_student_profile","ic_class_timetable","ic_exam_timetable","ic_attendance","ic_evaluationold","ic_learning","ic_healthcare","ic_bus","ic_ticket","ic_faq", "ic_FeePayment",]
    
    static var classesWiseFeatures = [Int: [String : Array<Any>]]()
    static var classesWiseFeaturesTemp = Array<Any>()
}

struct SDefaultKeys {
    static let loginUsername = "LoginUsername"
    static let loginPassword = "LoginPassword"
    static let news = "NewsLastSync"
    static let events = "EventsLastSync"
    static let userId = "user_id"
    static let intellinectid = "intellinectid"
    static let studentProfile = "studentProfile"
    static let rollNumber = "RoleNumber"
    static let name = "name"
    static let schoolClass = "schoolClass"
    static let schoolDiv = "schoolDiv"
    static let studentId = "studentId"
    static let schoolBoard = "schoolBoard"
    static let studentPassword = "studentPassword"
    static let role = "role"
    static let gaurdian = "gaurdian"
    static let evaluation = "Evaluation"
    static let mobileNumber = "mobileNumber"
    static let deviceId = "device_id"
    static let selectedStudent = "selectedStudent"
    static let learnningManagement = "learnningManagement"
    static let isActiveEvaluation = "isActiveEvaluation"
}

struct ViewControllerTitles {
    static let newsVC = "News"
    static let newsDetailVC = "News Details"
    static let eventVC = "Events"
    static let attendenceVC = "Attendance"
    static let timetableVC = "Timetable"
}

