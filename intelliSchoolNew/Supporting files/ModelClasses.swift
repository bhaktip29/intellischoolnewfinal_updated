//
//  ModelClasses.swift
//  Intellinects
//
//  Created by rupali  on 03/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import Foundation
import UIKit

//import UIKit
class Event{

    var eventName: String?
    var startDate: String?
   
    var startTime : String?
    var endTime : String?
    var allDay : String?
    var postdateMonth : String?
    var postDateInt : String?
    
    init(eventName: String? , startDate : String? ,startTime : String? , endTime : String?  ,allDay : String?, postdateMonth : String? , postDateInt : String? ) {
        self.eventName = eventName!
        self.startDate = startDate!
        self.startTime = startTime!
        self.endTime = endTime!
        self.allDay = allDay!
        self.postdateMonth = postdateMonth!
        self.postDateInt = postDateInt!
    }
    func geteventName() -> String! {
        return self.eventName
    }
    func getstartDate() -> String! {
        return self.startDate
    }
    func getstartTime() -> String! {
        return self.startTime
    }
    func getendTime() -> String! {
        return self.endTime
    }
    func getallDay() -> String! {
        return self.allDay
    }
    func getpostdateMonth() -> String! {
        return self.postdateMonth!
    }
    func getpostDateInt() -> String! {
        return self.postDateInt!
    }
    

}

class Event1 {
   
    var postDate: String?
    var postTitle: String?
   
    var postContent : String?
    var postdateMonth : String?
    var postDateInt : String?
    var endDate : String?
    
    var allDay : String?
    init(postTitle: String? , postDate : String? ,postContent : String? , postdateMonth : String? , postDateInt : String? , endDate : String? ,allDay : String? ) {
        self.postTitle = postTitle!
        self.postDate = postDate!
        self.postContent = postContent!
        self.postdateMonth = postdateMonth!
        self.postDateInt = postDateInt!
        self.endDate = endDate!
        self.allDay = allDay!
    }
    func getpostDate() -> String! {
        return self.postDate
    }
    func getpostTitle() -> String! {
        return self.postTitle!
    }
    func getpostContent() -> String! {
        return self.postContent!
    }
    func getpostdateMonth() -> String! {
        return self.postdateMonth!
    }
    func getpostDateInt() -> String! {
        return self.postDateInt!
    }
    func getendDate() -> String! {
        return self.endDate!
    }
    func getallDay() -> String! {
        return self.allDay!
    }
    
}
class ListOfRoles{
    var childname:String?
    var employeeName:String?
    var profile:String?
    var spec_role:String?

    init( employeeName : String? ,profile : String?, spec_role : String? )
    {
      //  self.childname = childname!
       self.profile = profile!
        self.employeeName = employeeName!
        self.spec_role = spec_role!
    }
   // func getchildname() -> String! {
     //   return self.childname
   // }
    func getspec_role() -> String! {
        return self.spec_role
    }
    func getemployeeName() -> String! {
        return self.employeeName
    }
    func getProfile() -> String! {
        return self.profile
    }
}

class ListOfRolesProfie{
   
    var profile:String?
    init(  profile : String? )
    {
        //  self.childname = childname!
        self.profile = profile!
      
    }
    // func getchildname() -> String! {
    //   return self.childname
    // }
   
    func getProfilw() -> String! {
        return self.profile
    }
}
class dashboard : NSObject  {
    var serviceName:String?
    var icon:String?
    init(serviceName: String? , icon : String?) {
        self.serviceName = serviceName!
        self.icon = icon
        // self.Post_date = Post_date!
    }
    func getserviceName() -> String! {
        return self.serviceName
    }
    func geticon() -> String! {
        return self.icon!
    }
}
class dashboard2 : NSObject {
    var serviceName:String?
   
    init(serviceName: String?) {
        self.serviceName = serviceName!
      
        // self.Post_date = Post_date!
    }
    func getserviceName() -> String! {
        return self.serviceName
    }
   
}
class PostInfo {
//    var ID: Int = 0
//    var sectionId: String = String()
//    var Post_Title: String = String()
//    var Post_Status: String = String()
   var Post_date: String?
   var postTitle: String?
//    var Post_Type: String = String()
    var date : String?
    var postContent : String?
    var featuredImage : String?
//    var Event_StartDate: String = String()
//    var Event_EndDate: String = String()
//    var News_LastModifiedDate: String = String()
//    var venue: String = String()
//    var contactno: String = String()
//    var contactPerson: String = String()
//    var allDay :Int = 0
//    var postArr : [String] = []
//    var PostType: String = String()
//    var PostRepeatDate: String = String()
//    var SchoolID: String = String()
    
    init(postTitle: String? , date : String? , postContent : String? , featuredImage : String?) {
        self.postTitle = postTitle!
        self.date = date!
        self.postContent = postContent!
        self.featuredImage = featuredImage!
       // self.Post_date = Post_date!
    }
    func getfeaturedImage() -> String! {
        return self.featuredImage
    }
    func getdate() -> String! {
        return self.date
    }
    func getpostTitle() -> String! {
        return self.postTitle!
    }
    func getpostContent() -> String! {
    return self.postContent!
    }
}

class studentsProfile {
    //    var ID: Int = 0
    //    var sectionId: String = String()
    //    var Post_Title: String = String()
    //    var Post_Status: String = String()
    var classId: String?
    var mother_name: String?
    var bloodGroup: String?
    var father_name: String?
    var aadharCard: String?
    var father_mobile: String?
    var birthDate: String?
    var allergy: String?
    var gr_no: String?
    var mother_mobile: String?
    var roll_no: String?
    var address: String?
    var board: String?
    var intellinectId: String?
    var photo_url : String?

    
    init(classId: String?, bloodGroup : String? ,father_name : String?  ,father_mobile : String? ,birthDate : String? , gr_no : String? ,mother_mobile : String? , roll_no : String? ,address : String? , board : String? , intellinectId : String? ,mother_name : String? , photo_url : String?) {
        self.classId = classId
        self.mother_name = mother_name
        self.bloodGroup = bloodGroup
        self.father_name = father_name
       // self.photo_url = photo_url!
      //  self.aadharCard = aadharCard!
        self.father_mobile = father_mobile
        self.birthDate = birthDate
       // self.allergy = allergy!
        self.gr_no = gr_no
        self.mother_mobile = mother_mobile
        self.roll_no = roll_no
        self.address = address
        self.board = board
        self.intellinectId = intellinectId
        self.photo_url = photo_url
        // self.Post_date = Post_date!
    }
    func getPhoto_Url() -> String {
        return self.photo_url!
    }
    func getfather_name() -> String {
        return self.father_name!
    }
    func getclassId() -> String {
        return self.classId!
    }
    func getmother_name() -> String! {
        return self.mother_name
    }
    func getbloodGroup() -> String! {
        return self.bloodGroup
    }
//    func getphoto_url() -> String! {
//        return self.photo_url!
//    }
//    func getaadharCard() -> String! {
//        return self.aadharCard!
//    }
    func getfather_mobile() -> String! {
        return self.father_mobile
    }
    func getbirthDate() -> String! {
        return self.birthDate
    }
    func getallergy() -> String! {
        return self.allergy
    }
    func getgr_no() -> String! {
        return self.gr_no
    }
    func getmother_mobile() -> String! {
        return self.mother_mobile
    }
//    func getroll_no() -> Int! {
//        return self.roll_no!
//    }
    func getaddress() -> String! {
        return self.address
    }
    func getboard() -> String! {
        return self.board
    }
    func getintellinectId() -> String! {
        return self.intellinectId
    }
    
}





class studentsProfile2 {
    //    var ID: Int = 0
    //    var sectionId: String = String()
    //    var Post_Title: String = String()
    //    var Post_Status: String = String()
    var classId: Int?
    var mother_name: String?
    var bloodGroup: String?
    var photo_url: String?
    var father_name: String?
  
    
    
    init(father_name : String? ) {
        
        self.father_name = father_name
        
        
 
    
}
    func getfather_name() -> String {
        return self.father_name!
    }
}























class Student5 {
   
    var father_name:String?
    init(father_name: String? ) {
        self.father_name = father_name!
      //  self.postContent = postContent!
        // self.image = image
        //home self.postContent = postDate!
        //1st = var postcontend second is init postcontent
        // self.Post_date = Post_date!
    }
    func getfather_name() -> String! {
        return self.father_name
    }
}

class SchoolClass: NSObject {
    
    var classID: Int = 0
    var divisionID: Int = 0
    var boardID: Int = 0
    var className: String?
    var divisionName: String?
    var isChecked: Bool = false
}

class ClassWeekDaysTimeTable: NSObject {
    
    var period: String?
    var time : String?
    var totime : String?
    var monday : String?
    var tuesday : String?
    var wednesday : String?
    var thursday : String?
    var friday : String?
    var saturday : String?
    var sunday : String?
}
class teacher{
 
    var postDate: String?
    var msgContent: String?
    var subjectName : String?
     var fname : String?
    var lname : String?
    var title : String?
    var attachments : String
    var file_original_name : String
    //var image : String?
    
    init(postDate: String? , msgContent : String? , fname : String? , lname : String?, title : String? , subjectName : String? , attachments : String , file_original_name : String) {
        self.postDate = postDate!
        self.msgContent = msgContent!

        self.fname = fname!
        self.lname = lname!
        self.title = title!
        self.subjectName = subjectName
        self.attachments = attachments
        self.file_original_name = file_original_name
       // self.image = image
        //home self.postContent = postDate!
        //1st = var postcontend second is init postcontent
        // self.Post_date = Post_date!
    }
    func getfile_original_name() -> String {
        return self.file_original_name
    }
    func getattachments() -> String {
        return self.attachments
    }
    func getfname() -> String! {
        return self.fname
    }
    func getlname() -> String! {
        return self.lname
    }
    func gettitle() -> String! {
        return self.title
    }
    func getpostDate() -> String! {
        return self.postDate
    }
    func getmsgContent() -> String! {
        return self.msgContent!
    }
    func getsubjectName() -> String! {
    return self.subjectName
    }
//    func getimage() -> String!{
//        return self.image
//    }
}

//struct StudentInfo {
//    
//    var rollNo: String?
//    var fatherName: String?
//    var motherName: String?
//    var intellinectsID: String?
//    var bloodGroup: String?
//    var imageUrl: String?
//    var address: String?
//    var classId: String?
//    var allergy: String?
//    var fatherMobile: String?
//    var motherMobile: String?
//    var board: String?
//    var birthDate: String?
//    var adharNumber: String?
//    var udiseNo: String?
//    var grNumber: String?
//}

class circular {
    var postTitle:String?
    var postDate:String?
    var postContent:String?
    var fName:String?
    var lName:String?

    init(postDate: String?, postContent:String? ,postTitle:String?, fName:String? , lName:String? ) {
        self.postDate = postDate!
        self.postContent = postContent!
        self.postTitle = postTitle!
        self.fName = fName!
        self.lName = lName!
        //  self.postContent = postContent!
        // self.image = image
        //home self.postContent = postDate!
        //1st = var postcontend second is init postcontent
        // self.Post_date = Post_date!
    }
    func getfName() -> String! {
        return self.fName
    }
    func getlName() -> String! {
        return self.lName
    }
    func getpostDate() -> String! {
        return self.postDate
    }
    func getpostContent() -> String! {
        return self.postContent
    }
    func getpostTitle() -> String! {
        return self.postTitle
    }
    
}
class Message1 {
    var postTitle:String?
     var postDate:String?
     var postContent:String?
     var fName:String?
     var lName:String?

     init(postDate: String?, postContent:String? ,postTitle:String?, fName:String? , lName:String? ) {
         self.postDate = postDate!
         self.postContent = postContent!
         self.postTitle = postTitle!
         self.fName = fName!
         self.lName = lName!
         //  self.postContent = postContent!
         // self.image = image
         //home self.postContent = postDate!
         //1st = var postcontend second is init postcontent
         // self.Post_date = Post_date!
     }
     func getfName() -> String! {
         return self.fName
     }
     func getlName() -> String! {
         return self.lName
     }
     func getpostDate() -> String! {
         return self.postDate
     }
     func getpostContent() -> String! {
         return self.postContent
     }
     func getpostTitle() -> String! {
         return self.postTitle
     }
}
class SMS {
    var postTitle:String?
     var postDate:String?
     var postContent:String?
     var fName:String?
     var lName:String?
    var receiver_group_type:String?

    init(postDate: String?, postContent:String? ,postTitle:String?, fName:String? , lName:String?, receiver_group_type:String ) {
         self.postDate = postDate!
         self.postContent = postContent!
         self.postTitle = postTitle!
         self.fName = fName!
         self.lName = lName!
        self.receiver_group_type = receiver_group_type
         //  self.postContent = postContent!
         // self.image = image
         //home self.postContent = postDate!
         //1st = var postcontend second is init postcontent
         // self.Post_date = Post_date!
     }
    func getreceiver_group_type() -> String! {
        return self.receiver_group_type
    }
    
     func getfName() -> String! {
         return self.fName
     }
     func getlName() -> String! {
         return self.lName
     }
     func getpostDate() -> String! {
         return self.postDate
     }
     func getpostContent() -> String! {
         return self.postContent
     }
     func getpostTitle() -> String! {
         return self.postTitle
     }
}
class ClassGroup_DataModel {
    
    var First_Lable: String?
    var Second_Lable: Int
    
    init(First_Lable: String?, Second_Lable: Int) {
        self.First_Lable = First_Lable
        self.Second_Lable = Second_Lable
    }
    
    func getName() -> String! {
        return self.First_Lable!
    }
    func getId() -> Int {
        return self.Second_Lable
    }
}
class Class_DataModel {
    
    var First_Lable: String?
    var Second_Lable: Int
    
    init(First_Lable: String?, Second_Lable: Int) {
        self.First_Lable = First_Lable
        self.Second_Lable = Second_Lable
    }
    
    func getName() -> String! {
        return self.First_Lable!
    }
    func getId() -> Int {
        return self.Second_Lable
    }
}
class Division_DataModel {
    
   /*                                           let classId = item.value(forKey: "classId")
                                               print("classId >>: \(classId!)")
                                               let classDivisionId = item.value(forKey: "classDivisionId")
                                               print("classDivisionId >>: \(classDivisionId!)")
                                               let className = item.value(forKey: "className")
                                               print("className >>: \(className!)")
                                               let classGroupId = item.value(forKey: "classGroupId")
                                               print("classGroupId >>: \(classGroupId!)")
                                               let classGroupName = item.value(forKey: "classGroupName")
                                               print("classGroupName >>: \(classGroupName!)")
                                               let classAliase = item.value(forKey: "classAliase")
                                               print("classAliase >>: \(classAliase!)") */
    
    var First_Lable: String?
    var Second_Lable: Int
    var Third_Lable: String?
    var Fourth_Lable: Int
    var Fifth_Lable: String?
   var Sixth_Lable: String?
    
    init(First_Lable: String?, Second_Lable: Int, Third_Lable: String?, Fourth_Lable: Int, Fifth_Lable: String?, Sixth_Lable: String?) {
        self.First_Lable = First_Lable
        self.Second_Lable = Second_Lable
        self.Third_Lable = Third_Lable
        self.Fourth_Lable = Fourth_Lable
        self.Fifth_Lable = Fifth_Lable
        self.Sixth_Lable = Sixth_Lable
    }
    
    func classDivisionId() -> String! {
        return self.First_Lable!
    }
    func classId() -> Int {
        return self.Second_Lable
    }
    func className() -> String! {
        return self.Third_Lable!
    }
    func classGroupId() -> Int {
        return self.Fourth_Lable
    }
    func classGroupName() -> String! {
        return self.Fifth_Lable!
    }
    func classAliase() -> String! {
        return self.Sixth_Lable!
    }
}
class Subject_DataModel {
    
    var First_Lable: String?
    var Second_Lable: Int
    
    init(First_Lable: String?, Second_Lable: Int) {
        self.First_Lable = First_Lable
        self.Second_Lable = Second_Lable
    }
    
    func getName() -> String! {
        return self.First_Lable!
    }
    func getId() -> Int {
        return self.Second_Lable
    }
}
class attachment1
{
    var attachments : String
init(attachments: String?)
{
    self.attachments = attachments!
    }
    
    func getattachments() -> String! {
        return self.attachments
    }
}
class Student_DataModel {
    
    var First_Lable: String?
    var Second_Lable: String?
    var Third_Lable: String?
    
    init(First_Lable: String?, Second_Lable: String?, Third_Lable: String?) {
        self.First_Lable = First_Lable
        self.Second_Lable = Second_Lable
        self.Third_Lable = Third_Lable
    }
    
    func getFName() -> String! {
        return self.First_Lable!
    }
    func getSName() -> String! {
        return self.Second_Lable!
    }
    func getintellinectsid() -> String! {
        return self.Third_Lable!
    }
}
class Teacher_DataModel {
    
    var First_Lable: String?
   var Second_Lable: String?
    
    init(First_Lable: String?, Second_Lable: String?) {
        self.First_Lable = First_Lable
        self.Second_Lable = Second_Lable
    }
    func getName() -> String! {
        return self.First_Lable!
    }
    func getintellinectsid() -> String! {
        return self.Second_Lable!
    }
}
class Message: NSObject {
    var messageID: NSInteger = NSInteger()
    var messageDate: String = String()
    var messageClass: String = String()
    var messageDiv: String = String()
    var messageSubject: String = String()
    var message: String = String()
    var attachmentUrl = [String]()
}
/*class SchoolClassTeacher: NSObject {
    
    var classID: Int = 0
    var divisionID: Int = 0
    var boardID: Int = 0
    var className: String?
    var divisionName: String?
    var isChecked: Bool = false
}

class SubjectList: NSObject {
    var subjectName: String = String()
    var subjectId: String = String()
}

class MessageList: NSObject {
    var messageID: NSInteger = NSInteger()
    var messageDate: String = String()
    var messageClass: String = String()
    var messageDiv: String = String()
    var messageSubject: String = String()
    var message: String = String()
    var attachmentUrl: String = String()
}
class Staff: NSObject {
    var firstName: String?
    var lastName: String?
    var divisionTitle: String?
    var className: String?
    var employeeId: String?
}
class StudentTeacher: NSObject {
    
    var studentName:String?
    var studentId: Int = 0
    var studentRollNo: Int = 0
    var intellinectId:String?
    var status: String?
    var statusColor: String?
    var isChecked: Bool = false
}
*/
