//
//  ListEmployee.swift
//  intelliSchoolNew
//
//  Created by Intellinects Venture on 10/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class ListEmployee: NSObject {
    
    static let shared = ListEmployee()
    
    var arrid = [String]()
    var arrentryBy = [String]()
    var arrmsgContent = [String]()
    var arremployeeIntellinectId = [String]()
    var arrtitle = [String]()
    var arrsubjectCode = [String]()
    var arrsubjectName = [String]()
    var arrmessageDate = [String]()
    var arrnotificationStatus = [String]()
    var arrclassDivisionId = [String]()
    var arrclassId = [String]()
    var arrclassName = [String]()
    var arrclassAlias = [String]()
    var arrgroupId = [String]()
    var arrgroupName = [String]()
    var arrgroupType = [String]()
    var arrnotificationType = [String]()
    var arracademicYear = [String]()
    var arrschoolCode = [String]()
    var arripAddress = [String]()
    var arrcreatedDate = [String]()
    var arrintellinect_id = [String]()
    var arrfname = [String]()
    var arrmname = [String]()
    var arrlname = [String]()
    var arrAttachments = [[AttachmentsModel]]()

    
    func parseData(_ dic: [String: Any]) {
        
        
        arrid.append(dic["id"] as? String ?? "")
        arrentryBy.append(dic["entryBy"] as? String ?? "")
        arrmsgContent.append(dic["msgContent"] as? String ?? "")
        arremployeeIntellinectId.append(dic["employeeIntellinectId"] as? String ?? "")
        arrtitle.append(dic["title"] as? String ?? "")
        arrsubjectCode.append(dic["subjectCode"] as? String ?? "")
        arrsubjectName.append(dic["subjectName"] as? String ?? "")
        arrmessageDate.append(dic["messageDate"] as? String ?? "")
        arrnotificationStatus.append(dic["notificationStatus"] as? String ?? "")
        arrclassDivisionId.append(dic["classDivisionId"] as? String ?? "")
        arrclassId.append(dic["classId"] as? String ?? "")
        arrclassName.append(dic["className"] as? String ?? "")
        arrclassAlias.append(dic["classAlias"] as? String ?? "")
        arrgroupId.append(dic["groupId"] as? String ?? "")
        arrgroupName.append(dic["groupName"] as? String ?? "")
        arrgroupType.append(dic["groupType"] as? String ?? "")
        arrnotificationType.append(dic["notificationType"] as? String ?? "")
        arracademicYear.append(dic["academicYear"] as? String ?? "")
        arrschoolCode.append(dic["schoolCode"] as? String ?? "")
        arripAddress.append(dic["ipAddress"] as? String ?? "")
        arrcreatedDate.append(dic["createdDate"] as? String ?? "")
        arrintellinect_id.append(dic["intellinect_id"] as? String ?? "")
        arrfname.append(dic["fname"] as? String ?? "")
        arrmname.append(dic["mname"] as? String ?? "")
        arrlname.append(dic["lname"] as? String ?? "")
        
        if let attachments = dic["attachments"] as? [[String: Any]] {
            var attachmentAr = [AttachmentsModel]()
            for val in attachments {
                let attachment = AttachmentsModel()
                attachment.fileOriginalName = val["file_original_name"] as? String ?? ""
                attachment.fileSize = val["file_size"] as? Int ?? 0
                attachment.filepath = val["filepath"] as? String ?? ""
                attachmentAr.append(attachment)
            }
            arrAttachments.append(attachmentAr)
        } else {
            arrAttachments.append([AttachmentsModel]())
        }
        
    }
    
    
    
    func clear() {
        
        arrid.removeAll()
        arrentryBy.removeAll()
        arrmsgContent.removeAll()
        arremployeeIntellinectId.removeAll()
        arrtitle.removeAll()
        arrsubjectCode.removeAll()
        arrsubjectName.removeAll()
        arrmessageDate.removeAll()
        arrnotificationStatus.removeAll()
        arrclassDivisionId.removeAll()
        arrclassId.removeAll()
        arrclassName.removeAll()
        arrclassAlias.removeAll()
        arrgroupId.removeAll()
        arrgroupName.removeAll()
        arrgroupType.removeAll()
        arrnotificationType.removeAll()
        arracademicYear.removeAll()
        arrschoolCode.removeAll()
        arripAddress.removeAll()
        arrcreatedDate.removeAll()
        arrintellinect_id.removeAll()
        arrfname.removeAll()
        arrmname.removeAll()
        arrlname.removeAll()
        arrAttachments.removeAll()
    }
}

class AttachmentsModel: NSObject {

    var fileOriginalName = ""
    var fileSize = 0
    var filepath = ""
}
