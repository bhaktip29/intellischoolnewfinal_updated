//
//  sliderVC.swift
//  intelliSchoolNew
//
//  Created by Intellinects Venture on 19/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class sliderVC: UIViewController , UITableViewDelegate , UITableViewDataSource {
    var menu = ["Terms of use","Help ticket","FAQ's","Share App"]
    var menuImgs = ["terms_gray","ticket","faq_gray","faq_gray"]
    
    @IBOutlet weak var tableView: UITableView!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = 60.0

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "SliderTableViewCell") as! SliderTableViewCell
     //   cell.borderlbl.addBottomBorderWithColor(color: .red, width: 1)
      //  cell.addTopBorderWithColor(color: .red, width: 2)
    //    cell.lbl.addBottomBorderWithColor(color: .red, width: 1)
        cell.addBottomBorderWithColor(color: .gray, width: 2)
      //  cell.addBottomBorderWithColor(color: <#T##UIColor#>, width: <#T##CGFloat#>)
        cell.lbl.text = menu[indexPath.row]
        cell.imgMenu.image = UIImage(named: menuImgs[indexPath.row])
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menu = self.menu[indexPath.item]
        
        if menu == "Share App"{
                    
            //                   case "Terms of use" :
            //                    UIApplication.shared.openURL(NSURL(string: "https://test.intellischools.org/privacy_policy.html")! as URL)
            //
            //                    break
            //
            //                    case "Help ticket" :
            //                    UIApplication.shared.openURL(NSURL(string: "https://test.intellischools.org/privacy_policy.html")! as URL)
            //                    break
            //
            //                    case "FAQ's" :
            //                    UIApplication.shared.openURL(NSURL(string: "https://faq.isirs.org/public/")! as URL)
            //                    break

//                                case "Share App" :
                              //  UIApplication.shared.openURL(NSURL(string: "https://test.intellischools.org/privacy_policy.html")! as URL)
                                var url = "https://test.intellischools.org/privacy_policy.html"
                                let vc = UIActivityViewController(activityItems: [url], applicationActivities: [])
                                present(vc, animated: true)
//
//                                break
//
//                                   // Circular:
//                                default:
//                                              break
//                                          }
        }
        else{
            let menu = self.menu[indexPath.item]
            switch (menu) {
          
            case "Terms of use" :
//                    UIApplication.shared.openURL(NSURL(string: "https://test.intellischools.org/privacy_policy.html")! as URL)
                var supportUrl = "https://test.intellischools.org/privacy_policy.html"
                    UserDefaults.standard.set(supportUrl, forKey: "supportUrl")
                        
            break
                        
            case "Help ticket" :
//                    UIApplication.shared.openURL(NSURL(string: "https://test.intellischools.org/privacy_policy.html")! as URL)
            break
                        
            case "FAQ's" :
//                UIApplication.shared.openURL(NSURL(string: "https://faq.isirs.org/public/")! as URL)
                var supportUrl = "https://faq.isirs.org/public/"
                      UserDefaults.standard.set(supportUrl, forKey: "supportUrl")
            break

            case "Share App" :
                UIApplication.shared.openURL(NSURL(string: "https://test.intellischools.org/privacy_policy.html")! as URL)

                default:
                              break
                          }
            
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "supportDetailsVC")
        self.present(controller, animated: true, completion: nil)
          

    }
//  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//  {
//      return 250 //or whatever you need
//  }

}
    
}

