//
//  CircularCollectionViewCell.swift
//  intelliSchoolNew
//
//  Created by rupali  on 18/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class CircularCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var textView: UITextView!
}
