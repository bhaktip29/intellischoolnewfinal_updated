//
//  HomeworkVC.swift
//  Intellinects
//
//  Created by rupali  on 09/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import  Alamofire
import SwiftyJSON
class HomeworkVC:  BaseViewController ,UICollectionViewDataSource , UICollectionViewDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var programVar : Int!
    var datechange : String!
    var selectedProgram1 : Int!
    var newsList = [teacher]()

    let images = ["news","events","inbox","attendance","timetable","lms","evaluation","payment","sports","bus","healthcare","publishing","discover","Assess","examtimetable","faq","terms","ticket"]
    let names = ["Assess","attendance","bus","discover","evaluation","events","examtimetable","faq","healthcare","inbox","lms","news","payment","publishing","sports","terms","ticket","timetable"]
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return newsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homecell", for: indexPath) as! HomeworkCollectionViewCell
        let userObjec = newsList[selectedProgram1]
        Cell.nameLbl.text =  userObjec.getpostContent()
        Cell.dateLbl.text = userObjec.getpostDate()
        Cell.textView.text = userObjec.getpostContent()
        let a = userObjec.getteacher()
        let firstLtter = String(a!.prefix(1))
        print("firstLtterfirstLtter\(firstLtter)")
       
        //
        let color1 = UIColor(hexString: "#8a638f")
        let color2 = UIColor(hexString: "#0bc1f0")
        let color3 = UIColor(hexString: "#ffa200")
        let bgColors = [color1,color2,color3]
        
        
         Cell.firstLetterLbl.backgroundColor = bgColors[indexPath.row % bgColors.count]
        Cell.firstLetterLbl.text = firstLtter
       // Cell.firstLetterLbl.layer.cornerRadius = Cell.firstLetterLbl.frame.width/2
      Cell.firstLetterLbl.layer.cornerRadius =  Cell.firstLetterLbl.frame.width/2
      Cell.firstLetterLbl.layer.masksToBounds = true
       // Cell.img.image = userObjec.getimage()
        return Cell
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        getNewsData()
       selectedProgram1 = UserDefaults.standard.integer(forKey: "selectedProgram")
        
        print("programVar\(selectedProgram1)")
        
        
        let a = "StackOverFlow"
        //let last4 = a.substringFrom(a.endIndex.advancedBy(-4))

        // Do any additional setup after loading the view.
    }
    
    func getNewsData()
    {
        var url : String = ""
        
        if Util.validateNetworkConnection(self){
            showActivityIndicatory(uiView: view)
            let sendDate = "2015-01-01"
            url =           "https://isirs.org/intellinect_dashboard/WS/index_main.php?action=GetMessage_Mayuri&SCHOOL_ID=8&deviceId=869447038367960&userId=266&role=Father"
            
            Alamofire.request(url,method: .get,parameters: [:])
                .responseJSON {[weak self] (response) in
                    self?.stopActivityIndicator()
                    if response.result.error != nil {
                       
                    }
                    print("response.reslt>>>>\(response.result)")
                    switch response.result {
                    case .success:
                        if let jsonresponse = response.result.value {
                            let originalResponse = JSON(jsonresponse)
                            print("Homeworkresponse1>>>>\(originalResponse)")
                            let recordsArray: [NSDictionary] = originalResponse.arrayObject as! [NSDictionary]
                            
                            
                            print("recordsArray>>>>\(recordsArray)")
                            
                            
                            for item in recordsArray {
                                
                            
                                let postDate = item.value(forKey: "postDate")
                                let postContent = item.value(forKey: "postContent")
                                let image = item.value(forKey: "image")
                                let teacher1 = item.value(forKey: "teacher")
                               // let date = item.value(forKey: "postDate")
                                // var changeddate = self!.changeDate(date as! String)
                                print("postDate>>>>>\(postDate)")
                                print("postContent>>>> \(postContent!)")
                                //print("android_version \(postDate!)")
                                
                                
                                // date formatter
                                let dateFormatterGet = DateFormatter()
                                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                
                                let dateFormatterPrint = DateFormatter()
                                dateFormatterPrint.dateFormat = "dd-MM-yyyy"
                                
                                if let date = dateFormatterGet.date(from: "2016-02-29 12:24:26") {
                                    print("dateformaterrr>>>\(dateFormatterPrint.string(from: date))")
                                    self!.datechange = dateFormatterPrint.string(from: date)
                                }
                                else
                                {
                                    print("There was an error decoding the string")
                                }
                                //
                                let teacherinfo = teacher(postDate: self!.datechange, postContent: postContent as! String, teacher: teacher1 as! String)
                                self!.newsList.append(teacherinfo)
                                print("newsListforhomework>>>>>>>>>>>>>>>\(self!.newsList)")
                            

                            }
                            self?.collectionView.reloadData()
                        }
                    case .failure(_):
                        print("switch case erroe")
                    }
            }
        }
        
        
        
    }

}
