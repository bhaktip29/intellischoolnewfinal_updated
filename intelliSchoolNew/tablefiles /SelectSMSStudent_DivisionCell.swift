//
//  SelectSMSStudent_DivisionCell.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 11/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class SelectSMSStudent_DivisionCell: UITableViewCell {

    @IBOutlet weak var SelectSMS_StudentDivisionLbl: UILabel!
    
    @IBOutlet weak var SelectSMS_StudentDivisionCheck: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.SelectSMS_StudentDivisionCheck.image = UIImage(named: "checkbox-inactive")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }

}
