//
//  SelectClass_CircularCell.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 04/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class SelectClass_CircularCell: UITableViewCell {

    @IBOutlet weak var SelectClass_CircularCell_lbl: UILabel!
    
    @IBOutlet weak var SelectClass_CircularCell_check: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
         self.SelectClass_CircularCell_check.image = UIImage(named: "checkbox-inactive")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }

}
