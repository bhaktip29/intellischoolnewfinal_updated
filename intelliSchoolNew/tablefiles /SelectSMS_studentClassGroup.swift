//
//  SelectSMS_studentClassGroup.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 10/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class SelectSMS_studentClassGroup: UITableViewCell {

    @IBOutlet weak var SelectStudentClassGroupLbl: UILabel!
    
    @IBOutlet weak var SelectStudentClassGroupCheck: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.SelectStudentClassGroupCheck.image = UIImage(named: "checkbox-inactive")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }

}
