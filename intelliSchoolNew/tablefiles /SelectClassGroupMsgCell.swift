//
//  SelectClassGroupMsgCell.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 02/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class SelectClassGroupMsgCell: UITableViewCell {

    @IBOutlet weak var SelectClassGroupLbl: UILabel!
    
    @IBOutlet weak var SelectClassGroupCheck: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.SelectClassGroupCheck.image = UIImage(named: "checkbox-inactive")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
