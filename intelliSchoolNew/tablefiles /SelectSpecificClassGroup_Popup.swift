//
//  SelectSpecificClassGroup_Popup.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 29/11/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class SelectSpecificClassGroup_Popup: UITableViewCell {

    @IBOutlet weak var SpecificClassGroupLbl: UILabel!
    
    @IBOutlet weak var SpecificClassGroupCheck: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.SpecificClassGroupCheck.image = UIImage(named: "checkbox-inactive")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
