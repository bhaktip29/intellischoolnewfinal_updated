//
//  SelectClassGroupCircular_SpecificClassCell.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 04/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class SelectClassGroupCircular_SpecificClassCell: UITableViewCell {

    @IBOutlet weak var SpecificClassGroupLbl: UILabel!
    
    @IBOutlet weak var SpecificCircularClassGroup_check1: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
         self.SpecificCircularClassGroup_check1.image = UIImage(named: "checkbox-inactive")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
      
    }

}
