//
//  SelectClassGroupCircularCell.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 04/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class SelectClassGroupCircularCell: UITableViewCell {

    @IBOutlet weak var SelectClassGroupCircularCell_lbl: UILabel!
    @IBOutlet weak var SelectClassGroupCircular_Check: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
         self.SelectClassGroupCircular_Check.image = UIImage(named: "checkbox-inactive")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
       
    }

}
