//
//  SelectReceiversGroupCell.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 10/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class SelectReceiversGroupCell: UITableViewCell {

    @IBOutlet weak var SelectReceiversLbl: UILabel!
    
    @IBOutlet weak var SelectReceiversCheck: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.SelectReceiversCheck.image = UIImage(named: "checkbox-inactive")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
