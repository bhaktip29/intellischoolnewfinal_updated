//
//  SelectSMS_messageTypeCell.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 10/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class SelectSMS_messageTypeCell: UITableViewCell {

    @IBOutlet weak var SelectMessageType_lbl: UILabel!
    
    @IBOutlet weak var SelectMsgTypeCheck: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.SelectMsgTypeCheck.image = UIImage(named: "checkbox-inactive")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
