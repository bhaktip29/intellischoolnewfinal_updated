//
//  SelectIndividualTeachingStaffCell.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 18/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class SelectIndividualTeachingStaffCell: UITableViewCell {

    @IBOutlet weak var SelectIndividualTeachingStaffLbl: UILabel!
    
    @IBOutlet weak var SelectIndividualTeachingStaffCell: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.SelectIndividualTeachingStaffCell.image = UIImage(named: "checkbox-inactive")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
