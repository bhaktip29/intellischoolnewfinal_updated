//
//  SelectClassGroup.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 26/11/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class SelectClassGroup: UITableViewCell {

    @IBOutlet weak var SelectClassGroupLbl: UILabel!
    
    @IBOutlet weak var SelectClassGroup_img: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.SelectClassGroup_img.image = UIImage(named: "checkbox-inactive")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
