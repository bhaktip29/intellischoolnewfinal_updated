//
//  SelectSMS_IndividualStudentsCell.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 12/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class SelectSMS_IndividualStudentsCell: UITableViewCell {

    @IBOutlet weak var SelectSMS_individualStudentCell: UILabel!
    @IBOutlet weak var SelectSMS_individualStudentCheck: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.SelectSMS_individualStudentCheck.image = UIImage(named: "checkbox-inactive")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
