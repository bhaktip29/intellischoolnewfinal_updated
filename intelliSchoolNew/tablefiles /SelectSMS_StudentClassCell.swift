//
//  SelectSMS_StudentClassCell.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 11/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class SelectSMS_StudentClassCell: UITableViewCell {

    @IBOutlet weak var SelectSMS_StudentClasslbl: UILabel!
    
    @IBOutlet weak var SelectSMS_StudentClassCheck: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.SelectSMS_StudentClassCheck.image = UIImage(named: "checkbox-inactive")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }

}
