//
//  SelectDivisionCell.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 27/11/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class SelectDivisionCell: UITableViewCell {

    @IBOutlet weak var DivisionLable: UILabel!
    @IBOutlet weak var DivisionCheckBox: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.DivisionCheckBox.image = UIImage(named: "checkbox-inactive")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
