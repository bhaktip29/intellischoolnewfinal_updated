//
//  SelectSpecificMsgClassCell.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 03/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class SelectSpecificMsgClassCell: UITableViewCell {

    @IBOutlet weak var SpecificClassMsgLbl: UILabel!
    
    @IBOutlet weak var SelectClassMsgCheck: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.SelectClassMsgCheck.image = UIImage(named: "checkbox-inactive")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

}
