//
//  SelectSebjectCell.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 28/11/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class SelectSebjectCell: UITableViewCell {

    @IBOutlet weak var Subject_lbl: UILabel!
    
    @IBOutlet weak var Subject_check:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.Subject_check.image = UIImage(named: "checkbox-inactive")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
