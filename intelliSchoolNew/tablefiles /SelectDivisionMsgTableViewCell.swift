//
//  SelectDivisionMsgTableViewCell.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 03/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class SelectDivisionMsgTableViewCell: UITableViewCell {

    @IBOutlet weak var SelectDivisionMsgLbl: UILabel!
    
    @IBOutlet weak var SelectDivisionMsgCheck: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.SelectDivisionMsgCheck.image = UIImage(named: "checkbox-inactive")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
         
        
    }

}
