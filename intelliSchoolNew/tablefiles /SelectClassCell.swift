//
//  SelectClassCell.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 26/11/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class SelectClassCell: UITableViewCell {

    @IBOutlet weak var SelectClass_Label: UILabel!
    
    @IBOutlet weak var SelectClass_checkbox: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.SelectClass_checkbox.image = UIImage(named: "checkbox-inactive")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
