//
//  SelectClass_specificGroupCell.swift
//  intelliSchoolNew
//
//  Created by Intellinects Ventures on 28/11/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class SelectClass_specificGroupCell: UITableViewCell {

    @IBOutlet weak var SpecificClass_lbl: UILabel!
    
    @IBOutlet weak var SpecificClass_Check: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
         self.SpecificClass_Check.image = UIImage(named: "checkbox-inactive")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
