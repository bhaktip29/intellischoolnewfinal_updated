//
//  WeeklyTTViewController.swift
//  intelliSchoolNew
//
//  Created by Intellinects Venture on 08/01/20.
//  Copyright © 2020 rupali . All rights reserved.
//

import UIKit
  import Alamofire
  import SwiftyJSON
class WeeklyTTViewController: BaseViewControllerTeacher, UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    var days = ["mon","tues","wed"]
    var mon = ["mon1","mon2"]
    var count = 0
var timetableClassdiv1 = [timetableClassdiv]()
var tue = ["tue1","tue2"]
   // var wed = [String]()
   var wed = ["wed1","wed2"]
    var thurs = ["thurs1","thurs2"]
        let cellId = "TimeCell"
        let headerCellId  = "HeaderCell"
        var classTimeTable = [ClassWeekDaysTimeTableTeacher]()
        var schoolClass = SchoolClassTeacher()
        
        let collectionView : UICollectionView = {
            let layout = UICollectionViewFlowLayout()
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
            let clView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
            clView.backgroundColor = UIColor.groupTableViewBackground
            clView.translatesAutoresizingMaskIntoConstraints = false
            return clView
        }()
        
        let errorLbl: UILabel = {
            let lbl = UILabel()
            lbl.translatesAutoresizingMaskIntoConstraints = false
            lbl.textAlignment = .center
            lbl.text = "Ooops....TimeTable details not availables!!"
            return lbl
        }()
        
        // MARK:- Custom Views Setup Methods
        
        private func setUpCollectionView() {
            
            view.addSubview(collectionView)
            collectionView.dataSource = self
            collectionView.delegate = self
            
            // needed constarunts x,y,w,h
            if #available(iOS 11.0, *) {
                collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
            } else {
                collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
            }
            collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
            collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
        
        private func setUpErrorLabel() {
        
            view.addSubview(errorLbl)
            //needed constraints
            errorLbl.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
            if #available(iOS 11.0, *) {
                errorLbl.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant:8).isActive = true
            } else {
                errorLbl.topAnchor.constraint(equalTo: view.topAnchor,constant:72).isActive = true
            }
            errorLbl.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
            errorLbl.heightAnchor.constraint(equalToConstant: 50).isActive = true
        }
        
        //MARK:- View Controllers Methods
        
        override func viewDidLoad() {
            super.viewDidLoad()
          //  getTimeTableDta()
            getTimeTableDetails()
            getClassesAndDivisions()
            view.backgroundColor = UIColor.white
           // collectionView.isHidden = true
           // errorLbl.isHidden = true
            collectionView.register(ClassTimeTableCVCellTeacher1.self, forCellWithReuseIdentifier: cellId)
            collectionView.register(ClassTimeTableHeaderTeacher1.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader , withReuseIdentifier: headerCellId)
            if let className = schoolClass.className, let division = schoolClass.divisionName {
                self.title = "Class \(className) \(division)"
            }
            setUpCollectionView()
        //    getTimeTableDetails()
            setUpErrorLabel()
        }
        
        //MARK:- CollectionView Methods
        
        func numberOfSections(in collectionView: UICollectionView) -> Int {
            return 1
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return timetableClassdiv1.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
 
        {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ClassTimeTableCVCellTeacher1
            var userList = timetableClassdiv1[indexPath.row]
            print("userrrrr>>>\(userList.subjectName)")
        //    let weekTime = classTimeTable[indexPath.row] as ClassWeekDaysTimeTableTeacher
            cell.timeLbl.text = mon[indexPath.row]
            if indexPath.section == 0 {
                cell.timeLbl1.text = mon[indexPath.row]
                cell.timeLbl2.text = tue[indexPath.row]
               // cell.t
                cell.timeLbl4.text = thurs[indexPath.row]
                if wed.isEmpty == true
                {
                    cell.timeLbl3.isHidden = true
                }

                else{
                cell.timeLbl3.text = wed[indexPath.row]
                }
            }
            
            return cell
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: view.frame.width, height: 35)
        }
        
        func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerCellId, for: indexPath as IndexPath) as! ClassTimeTableHeaderTeacher1
            headerView.headerLbl.text = "Time"
            var array = [headerView.headerLbl1,headerView.headerLbl2,headerView.headerLbl3]
            for n in 1...days.count {
                var label = "headerLbl[count]"
                array[count].text = days[n-1]
                count =  count+1
print("countcountcount\(count)")
            }

//            if indexPath.section == 0 {
//                headerView.headerLbl1.text = "Monday"
//                }
//                if wed.isEmpty == true {
////                    headerView.headerLbl3.text = "Wednesday"
//                    headerView.headerLbl3.isHidden = true
//                }
//                else{
//                    headerView.headerLbl3.text = "wed"
//
//                }
//                //else{
//                headerView.headerLbl2.text = "Tuesday"
//
//               // }
//            }else {
//                headerView.headerLbl1.text = "Thursday"
//                headerView.headerLbl2.text = "Friday"
//                headerView.headerLbl3.text = "Saturday"
//            }
            return headerView
        }

        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
        {
            return CGSize(width: collectionView.bounds.width, height: 30)
        }
        
        //MARK: -  Custom Methods
        
        private func getTimeTableDetails() {
            
            if UtilTeacher.validateNetworkConnection(self) {
                let userDefault = UserDefaults.standard
                let parameters =
                    [
                    "action":"getTimeTable",
                    "SCHOOL_ID": GlobleConstants.schoolID,
                    "classId": schoolClass.classID.description,
                    "divId": schoolClass.divisionID.description,
                    "boardId": schoolClass.boardID.description
                    ] as [String : Any]//division board
    //            ["classId": "9",
    //             "action": "getTimeTable",
    //             "SCHOOL_ID": 8,
    //             "divId": "22",
    //             "boardId": "1"]
    //                as [String : Any]
                
                showActivityIndicatory(uiView: view)
                Alamofire.request(WebServiceUrls.timeTableService,method:.get,parameters: parameters)
                    
                    .responseJSON { [weak self](response) in
                        print("ClassTimeTableVCTeacher parameters>\(parameters)")

                        self?.stopActivityIndicator()
                        guard response.result.error == nil else {
                            return
                        }
                        // make sure we got JSON and it's a dictionary
                        guard let json1 = response.result.value as? [Any] else {
                            print("didn't get todo object as JSON from API")
                            return
                        }
                        print("response.result.value>>\(response.result.value)")
                        if json1.count > 0 {
                            guard let  jsondic = json1[0] as? [String: Any] else {
                                return
                            }
                            guard let ttdetails = jsondic["timtabArray"] as? NSArray else {
                                return
                            }
                            let ttdet = JSON(ttdetails).rawString()
                            let json: AnyObject? = ttdet?.parseJSONString
                            
                            guard let tt   =  json  as? [[String:AnyObject]] else {
                                return
                            }
                            for details in tt  {
                                let weekTimeTable = ClassWeekDaysTimeTableTeacher()
                                weekTimeTable.time = details["subjectName"] as? String
                                print(" weekTimeTable.time >>>\( weekTimeTable.time )")
//                                weekTimeTable.totime = details["totime"] as? String
//                                weekTimeTable.period = details["period"] as? String
//                                weekTimeTable.monday = details["mon"] as? String
//                                weekTimeTable.tuesday = details["tue"] as? String
//                                weekTimeTable.wednesday = details["wed"] as? String
//                                weekTimeTable.thursday = details["thur"] as? String
//                                weekTimeTable.friday = details["fri"] as? String
//                                weekTimeTable.saturday = details["sat"] as? String
//                                weekTimeTable.sunday = details["sun"] as? String
//                                self?.classTimeTable.append(weekTimeTable)
                            }
                            self?.collectionView.reloadData()
                            self?.collectionView.isHidden = false
                        }
                        else{
                            self?.errorLbl.isHidden = false
                        }
                }
            }
        }
    func getClassesAndDivisions()
    {
        if UtilTeacher.validateNetworkConnection(self){
            let userDefaults = UserDefaults.standard
            
           // let parameters =
//                [
//                "token": UserDefaults.standard.value(forKey: "sessionToken") as! String,
//                "schoolcode":"INTELL",
//                "intellinectsId":"INTELL19E00SCH002",
//                "ac_yr":"2019-2020",
//                "role":"School admin"] as [String : Any]
                let parameters =
                    [
                        "token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9pbnRlbGxpc2Nob29scy5jb21cL3NjaG9vbEFkbWluQXBpXC9wdWJsaWNcL2FwaVwvbG9naW4iLCJpYXQiOjE1NzgzOTI0NjEsImV4cCI6MTU3OTYwMjA2MSwibmJmIjoxNTc4MzkyNDYxLCJqdGkiOiI4VENteVlCbHIxWHpRYVBoIiwic3ViIjoxLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.batORio9r1aOV3Z5h8epf33IToFu9R33lvXe_CQ9oWQ",
                        "schoolcode":"INTELL",
                        "intellinectsId":"INTELL19E00SCH002",
                        "ac_yr":"2019-2020",
                        "classId":"4",
                        "divisionId":"INTELL4A"
//                    "action":"getTimeTable",
//                    "SCHOOL_ID": GlobleConstants.schoolID,
//                    "classId": schoolClass.classID.description,
//                    "divId": schoolClass.divisionID.description,
//                    "boardId": schoolClass.boardID.description
                    ] as [String : Any]//division board
            showActivityIndicatory(uiView: view)
            Alamofire.request("https://intellischools.com/schoolAdminApi/public/api/timetableClassdiv",method:.post,parameters: parameters)
                .responseJSON{ [weak self] (response)  in
                    self?.stopActivityIndicator()
                    print("teacherClassDivVC response\(response.result)")

                    print("teacherClassDivVC response\(response.result.value)")
                    if response.result.error != nil {
                        UtilTeacher.invokeAlertMethod("Failed!", strBody: "Error Occured While Processing Data ", delegate: nil,vcobj : self!)
                    }
                    if let jsonresponse = response.result.value {
                        
                        let originalResponse = JSON(jsonresponse)
                        UtilTeacher.LoaderEnd()
                        self?.getSuccessResponse(originalResponse: originalResponse)
                    }
            }
        }
    }
    
    func getSuccessResponse(originalResponse : JSON)
    {
         if(originalResponse.count > 0)
         {
             for (key,subJson):(String, JSON) in originalResponse {
                     
                            if key == "Monday" {
                                for item in subJson.arrayValue {
                                    let student = timetableClassdiv()
                                    student.subjectName = item["subjectName"].stringValue
                                    print("student.classNamestudent.className\(student.className)")
                               //     student.classDiv = item["classDiv"].stringValue

                                    timetableClassdiv1.append(student)
                                }
                            }
                        
            //}
          //  print("timetableListtimetableListtimetableList\(timetableList)")

           // myTableView.reloadData()
     //       tableView.animateTable()
            }
        }
    }
    
//   func getTimeTableDta()
//    {
//        var url : String = ""
//
//        if Util.validateNetworkConnection(self){
//            showActivityIndicatory(uiView: view)
//            let sendDate = "2015-01-01"
//            //            url =           "https://isirs.org/intellinect_dashboard/WS/index_main.php?action=GetCircular&SCHOOL_ID=8&deviceId=869447038367960&userId=266&role=Father"
//            url = "https://intellischools.com/schoolAdminApi/public/api/timetableClassdiv"
//            let parameters =
//            [
//        "token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9pbnRlbGxpc2Nob29scy5jb21cL3NjaG9vbEFkbWluQXBpXC9wdWJsaWNcL2FwaVwvbG9naW4iLCJpYXQiOjE1NzgzOTI0NjEsImV4cCI6MTU3OTYwMjA2MSwibmJmIjoxNTc4MzkyNDYxLCJqdGkiOiI4VENteVlCbHIxWHpRYVBoIiwic3ViIjoxLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.batORio9r1aOV3Z5h8epf33IToFu9R33lvXe_CQ9oWQ",
//            "schoolcode":"INTELL",
//            "intellinectsId":"INTELL19E00SCH002",
//            "ac_yr":"2019-2020",
//            "classId":"4",
//            "divisionId":"INTELL4A"
//            //                    "action":"getTimeTable",
//            //                    "SCHOOL_ID": GlobleConstants.schoolID,
//            //                    "classId": schoolClass.classID.description,
//            //                    "divId": schoolClass.divisionID.description,
//            //                    "boardId": schoolClass.boardID.description
//                                ] as [String : Any]//division board
//            print("parametersparameters\(parameters)")
//            Alamofire.request(url,method: .post,parameters: parameters)
//                .responseJSON {[weak self] (response) in
//                    self?.stopActivityIndicator()
//                    if response.result.error != nil {
//
//                    }
//                    print("response.reslt>>>>\(response.result)")
//                    switch response.result {
//                    case .success:
//                        if let jsonresponse = response.result.value {
//                            let originalResponse = JSON(jsonresponse)
//                            print("Circularresponse1>>>>\(originalResponse)")
//                            let data = originalResponse["timtabArray"]
//                            print("datadatadatadatadatav\(data)")
//                        //    let originalResponse = JSON(jsonresponse)
//                            let data2 = JSON(data)
//                            print("data2data2data2\(data2)")
//
//
//                            let recordsArray: [NSDictionary] = data.arrayObject as! [NSDictionary]
//
//                                     for item in recordsArray {
//
//                            let dayId = item.value(forKey: "dayId")
//                            print("dayIdday\(dayId)")
//
//                            let monday = item.value(forKey: "Monday")
//                          print("mondaymondaymondaymondaymonday11\(monday)")
//
////                            let data1 = JSON(data)
////                                                      // print("recordsArrayrecordsArrayrecordsArray\(recordsArray)")
////                                                 print("data1data1data1\(data1)")
//                            let recordsArrayofdata = JSON(recordsArray)
//                            let Monday = recordsArrayofdata["Monday"]
//print("MondayMondayMonday>>>\(Monday)")
//                            let recordsArray5: [NSDictionary] = monday as! [NSDictionary]
//
//                            for item2 in recordsArray5 {
//                                let subjectName = item2.value(forKey: "subjectName")
//
//
//                                        }}
//
////                            let Monday1: [NSDictionary]  = monday1.. as! [NSDictionary]
////                                             //  var Monday = data1["Monday"]
////                                                                      print("MondayMondayMonday\(Monday1)")
////                            for item in monday! {
////                                let Monday = item.value(forKey: "Monday")
//////                            let recordsArray1: [NSDictionary] = Monday.arrayObject as! [NSDictionary]
////                                print("recordsArray11111112233333>>>>\(Monday)")
////
////
////
////
////                            for item in recordsArray {
//
//
//                              //  let subjectName = item.value(forKey: "subjectName")
////                                let fName = item.value(forKey: "fname")
////                                let lName = item.value(forKey: "lname")
////                                let title = item.value(forKey: "title")
////                                let postContent = item.value(forKey: "msgContent")
////                                let postTitle = item.value(forKey: "title")
////                                let teacher1 = item.value(forKey: "teacher")
////                                let attachment = item.value(forKey: "attachments")
//                                // let date = item.value(forKey: "postDate")
//                                // var changeddate = self!.changeDate(date as! String)
////                                print("subjectName>>>>>\(subjectName)")
////
//////                                let Info = circular(postDate: self!.datechange as! String, postContent: postContent as! String , postTitle: postTitle as! String, fName: fName as! String, lName: lName as! String)
//////                                self!.InfoList.append(Info)
//////                                print("CirculrListforhomework>>>>>>>>>>>>>>>\(self!.InfoList)")
////
////
////                                }}
//                        }
//                           // self?.collectionView.reloadData()
//
//                    case .failure(_):
//                        print("switch case erroe")
//                    }
//            }
//        }
//
//
//
//    }
    }

    extension String {
        
    //    var parseJSONString: AnyObject? {
    //
    //        let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false)
    //
    //        if let jsonData = data {
    //            // Will return an object or nil if JSON decoding fails
    //            return try! JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
    //        } else {
    //            // Lossless conversion of the string was not possible
    //            return nil
    //        }
    //    }
    }

    class ClassTimeTableCVCellTeacher1 :UICollectionViewCell {
        
        var timeLbl = UILabel()
        var timeLbl1 = UILabel()
        var timeLbl2 = UILabel()
        var timeLbl3 = UILabel()
        var timeLbl4 = UILabel()

        private func setUpLbels() {
            
            var lbls = [UILabel]()
            
            func getLabel() -> UILabel {
                let lbl = UILabel()
                lbl.textColor = UIColor.black
                lbl.font = UIFont(name: "Helvetica Neue", size: 12)
                lbl.textAlignment = .center
                lbl.numberOfLines = 0
                lbl.layer.borderColor = UIColor.lightGray.cgColor
                lbl.layer.borderWidth = 0.3
                lbl.sizeToFit()
                return lbl
            }
            
            timeLbl = getLabel()
            lbls.append(timeLbl)
            timeLbl1 = getLabel()
            lbls.append(timeLbl1)
            timeLbl2 = getLabel()
            lbls.append(timeLbl2)
            timeLbl3 = getLabel()
            lbls.append(timeLbl3)
            timeLbl4 = getLabel()
            lbls.append(timeLbl4)
            
            let stackView = UIStackView(arrangedSubviews: lbls)
            stackView.alignment = .fill
            stackView.axis = .horizontal
            stackView.distribution = .fillEqually
            stackView.translatesAutoresizingMaskIntoConstraints = false
            
            self.addSubview(stackView)
            // needed constarints
            stackView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
            stackView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
            stackView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
            stackView.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.backgroundColor = UIColor.white
            setUpLbels()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }

    class ClassTimeTableHeaderTeacher1: UICollectionReusableView {
        
        var headerLbl = UILabel()
        var headerLbl1 = UILabel()
        var headerLbl2 = UILabel()
        var headerLbl3 = UILabel()
        
        private func setUpLbels() {
            
            var lbls = [UILabel]()
            
            func getLabel() -> UILabel {
                let lbl = UILabel()
                lbl.textColor = UIColor.black
                lbl.font = UIFont.boldSystemFont(ofSize: 13) //UIFont(name: "Helvetica Neue", size: 13)
                lbl.textAlignment = .center
                lbl.layer.borderColor = UIColor.darkGray.cgColor
                lbl.layer.borderWidth = 0.3
                return lbl
            }
            
            headerLbl = getLabel()
            lbls.append(headerLbl)
            headerLbl1 = getLabel()
            lbls.append(headerLbl1)
            headerLbl2 = getLabel()
            lbls.append(headerLbl2)
            headerLbl3 = getLabel()
            lbls.append(headerLbl3)
            
            
            let stackView = UIStackView(arrangedSubviews: lbls)
            stackView.alignment = .fill
            stackView.axis = .horizontal
            stackView.distribution = .fillEqually
            stackView.translatesAutoresizingMaskIntoConstraints = false
            
            self.addSubview(stackView)
            // needed constarints
            stackView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
            stackView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
            stackView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
            stackView.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.backgroundColor = UIColor.lightGray
            setUpLbels()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }



