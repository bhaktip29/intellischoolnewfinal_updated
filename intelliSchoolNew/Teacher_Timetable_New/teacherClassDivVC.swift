//
//  teacherClassDivVC.swift
//  intelliSchoolNew
//
//  Created by Intellinects Venture on 07/01/20.
//  Copyright © 2020 rupali . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class teacherClassDivVC:BaseViewControllerTeacher,UITableViewDelegate,UITableViewDataSource {

    
//var classList = ["nsdmsa","absnbasmn","bsbs"]
//    let cellId = "CellIdentifire"
   var timetableList = [timetable]()
//    let bottomLabel : UILabel = {
//        let lbl = UILabel()
//        lbl.text = DisplayTextMsgs.calssDivisionSelectionText
//        lbl.font = UIFont.boldSystemFont(ofSize: 13)
//        lbl.backgroundColor = UIColor.lightGray
//        lbl.translatesAutoresizingMaskIntoConstraints = false
//        lbl.textColor = UIColor.white
//        return lbl
//    }()
//
//    let tableView : UITableView = {
//
//        let tblView = UITableView()
//        tblView.tableFooterView = UIView()
//        tblView.translatesAutoresizingMaskIntoConstraints = false
//        tblView.separatorInset = UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 4)
//        tblView.separatorInset = UIEdgeInsets.zero
//        return tblView
//    }()
//    private func setUpTableView() {
//
//        view.addSubview(tableView)
//        tableView.dataSource = self
//        tableView.delegate = self
//        tableView.tableFooterView = UIView()
//        // needed constarunts x,y,w,h
//        tableView.topAnchor.constraint(equalTo:view.topAnchor, constant : 64).isActive = true
//        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
//        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
//        tableView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
//        tableView.bottomAnchor.constraint(equalTo: bottomLabel.topAnchor).isActive = true
//    }
//    private func setUpBottomLabel() {
//
//        view.addSubview(bottomLabel)
//        // needed constarunts x,y,w,h
//        bottomLabel.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
//        if #available(iOS 11.0, *) {
//            bottomLabel.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
//        } else {
//            bottomLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
//        }
//        bottomLabel.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
//        bottomLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
//    }
//    override func viewDidLoad() {
//        super.viewDidLoad()
//         getClassesAndDivisions()
//       navigationItem.title = ViewControllerTitlesTeacher.classOrDivison
//       tableView.cellLayoutMarginsFollowReadableWidth = false
//       tableView.register(ClassCell.self, forCellReuseIdentifier: "cellId")
//      //  self.tableView.register(UINib(nibName: String(describing: ClassCell.self), bundle: nil), forCellReuseIdentifier: "cellId")
//
//       setUpBottomLabel()
//       setUpTableView()
//      // getClassesAndDivisions()
//    }
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return timetableList.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! ClassCell
//
//      //  let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ClassCell
//       let timetableclass = timetableList[indexPath.row]
//       // cell.textLabel?.text = classList[indexPath.row]
//        cell.textLabel?.text = "Class \(timetableclass.classDivisionId)"
//   //     print("timetableclass.classDivisionId>>\(timetableclass.classDivisionId)")
//
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 50
//    }
    private var myTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
getClassesAndDivisions()
        let barHeight: CGFloat = UIApplication.shared.statusBarFrame.size.height
        let displayWidth: CGFloat = self.view.frame.width
        let displayHeight: CGFloat = self.view.frame.height

        myTableView = UITableView(frame: CGRect(x: 0, y: barHeight, width: displayWidth, height: displayHeight - barHeight))
        myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        myTableView.dataSource = self
        myTableView.delegate = self
        self.view.addSubview(myTableView)
    }

//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print("Num: \(indexPath.row)")
//        print("Value: \(myArray[indexPath.row])")
//    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return timetableList.count
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
              let timetableclass = timetableList[indexPath.row]

        cell.textLabel!.text = "\(timetableclass.className)\(timetableclass.classDiv)"
       // cell.textLabel?.text = "Class \(timetableclass.classDiv)"
        return cell
    }
    func getClassesAndDivisions()
    {
        if UtilTeacher.validateNetworkConnection(self){
            let userDefaults = UserDefaults.standard
            
            let parameters = [
                "token": UserDefaults.standard.value(forKey: "sessionToken") as! String,
                "schoolcode":"INTELL",
                "intellinectsId":"INTELL19E00SCH002",
                "ac_yr":"2019-2020",
                "role":"School admin"] as [String : Any]
            showActivityIndicatory(uiView: view)
            Alamofire.request("https://intellischools.com/schoolAdminApi/public/api/teacherClassDiv",method:.post,parameters: parameters)
                .responseJSON{ [weak self] (response)  in
                    self?.stopActivityIndicator()
                    print("teacherClassDivVC response\(response.result)")

                    print("teacherClassDivVC response\(response.result.value)")
                    if response.result.error != nil {
                        UtilTeacher.invokeAlertMethod("Failed!", strBody: "Error Occured While Processing Data ", delegate: nil,vcobj : self!)
                    }
                    if let jsonresponse = response.result.value {
                        
                        let originalResponse = JSON(jsonresponse)
                        UtilTeacher.LoaderEnd()
                        self?.getSuccessResponse(originalResponse: originalResponse)
                    }
            }
        }
    }
    func getSuccessResponse(originalResponse : JSON)
    {
         if(originalResponse.count > 0)
         {
             for (key,subJson):(String, JSON) in originalResponse {
                      //  for (key,subJson):(String, JSON) in originalResponse {
                            if key == "TeachersSubjects" {
                                for item in subJson.arrayValue {
                                    let student = timetable()
                                    student.className = item["className"].stringValue

                                    student.classDiv = item["classDiv"].stringValue

                                    timetableList.append(student)
                                //}
                            }
                        }

            //}
            print("timetableListtimetableListtimetableList\(timetableList)")

            myTableView.reloadData()
     //       tableView.animateTable()
            }
        }
    }
//    func getSuccessResponse(originalResponse : JSON)
//    {
//        if(originalResponse.count > 0)
//        {
//            for (key,subJson):(String, JSON) in originalResponse {
//                if key == "TeachersSubjects" {
//                    for item in subJson.arrayValue {
//                        let student = timetable()
//                        student.classDivisionId = item["classDivisionId"].stringValue.capitalized
////                        student.studentId = Int(item["student_id"].stringValue)!
////                        student.studentRollNo = Int(item["student_rollno"].stringValue)!
////                        student.intellinectId = item["intellinectid"].stringValue
//                        timetableList.append(student)
//                    }
//
//                }
//            }
//            myTableView.reloadData()
//        }
//    }
    
}
