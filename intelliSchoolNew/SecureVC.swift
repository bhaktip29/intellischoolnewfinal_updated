////
////  SecureVC.swift
////  SchoolProtoTypeApp
////
////  Created by Intellinects on 06/09/17.
////  Copyright © 2017 Intellinects Ventures. All rights reserved.
////
//
//import UIKit
//import Alamofire
//import SwiftyJSON
//import MessageUI
//
//class SecureVC: SecureMenuVC, MFMailComposeViewControllerDelegate {
//
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        if UserDefaults.standard.value(forKey: "Menu") != nil
//        {
//            let decoded  = UserDefaults.standard.object(forKey: "Menu") as! Data
//            let decodeItems = NSKeyedUnarchiver.unarchiveObject(with: decoded)
//            SecureMenu.classesWiseFeatures = decodeItems as! [Int : [String : Array<Any>]]
//            getMenuClasswise()
//        }
//        else
//        {
//            let encodedData = NSKeyedArchiver.archivedData(withRootObject: SecureMenu.classesWiseFeatures)
//            let userDefaults = UserDefaults.standard
//            userDefaults.set(encodedData, forKey: "Menu")
//        }
//    }
//    func getMenuClasswise()
//    {
//        SecureMenu.menu.removeAll()
//        SecureMenu.menuIcons.removeAll()
//        var menuDict = SecureMenu.classesWiseFeatures.sorted() { $0.key < $1.key }
//        #if ISCHOOL
//        menuDict.insert((key: menuDict.count, value: ["Learning Management" : [""]]), at: menuDict.count)
//        #elseif HOLYCROSS
//        menuDict.insert((key: menuDict.count, value: ["Learning Management" : [""]]), at: menuDict.count)
//        #endif
//        //        menuDict.insert((key: menuDict.count, value: ["Healthcare" : [""]]), at: menuDict.count)
//        #if DONBOSCOINT
//        menuDict.insert((key: menuDict.count, value: ["Reportbee" : [""]]), at: menuDict.count)
//        #endif
//        menuDict.insert((key: menuDict.count, value: ["Help-Ticket" : [""]]), at: menuDict.count)
//        for (_,value) in menuDict
//        {
//            for(key,value) in value
//            {
//                switch key
//                {
//                case "Message":
//                    let arr = value as! [String]
//                    if arr.contains(div){
//                        SecureMenu.menuIcons.append("ic_messages")
//                        SecureMenu.menu.append(key)
//                    }else{
//                        print("False")
//                    }
//                    break
//                case "Circular":
//                    let arr = value as! [String]
//                    if arr.contains(div){
//                        SecureMenu.menuIcons.append("ic_circular")
//                        SecureMenu.menu.append(key)
//                    }else{
//                        print("False")
//                    }
//                    break
//                case "Homework":
//                    let arr = value as! [String]
//                    if arr.contains(div){
//                        SecureMenu.menuIcons.append("ic_homework")
//                        SecureMenu.menu.append(key)
//                    }else{
//                        print("False")
//                    }
//                    break
//                case "Class Timetable":
//                    let arr = value as! [String]
//                    if arr.contains(div){
//                        SecureMenu.menuIcons.append("ic_class_timetable")
//                        SecureMenu.menu.append(key)
//                    }else{
//                        print("False")
//                    }
//                    break
//                    //                case "Exam Timetable":
//                    //                    let arr = value as! [String]
//                    //                    if arr.contains(div){
//                    //                        SecureMenu.menuIcons.append("ic_exam_timetable")
//                    //                        SecureMenu.menu.append(key)
//                    //                    }else{
//                    //                        print("False")
//                    //                    }
//                //                    break
//                case "Attendance":
//                    let arr = value as! [String]
//                    if arr.contains(div){
//                        SecureMenu.menuIcons.append("ic_attendance")
//                        SecureMenu.menu.append(key)
//                    }else{
//                        print("False")
//                    }
//                    break
//                    //                case "Evaluation":
//                    //                    let arr = value as! [String]
//                    //                    if arr.contains(div){
//                    //                        SecureMenu.menuIcons.append("ic_evaluationold")
//                    //                        SecureMenu.menu.append(key)
//                    //                    }else{
//                    //                        print("False")
//                    //                    }
//                //                    break
//                case "Fee Payment System":
//                    let arr = value as! [String]
//                    if arr.contains(div){
//                        SecureMenu.menuIcons.append("ic_FeePayment")
//                        SecureMenu.menu.append("Fee Payment")
//                    }else{
//                        print("False")
//                    }
//                    break
//
//                    //                case "Library Management":
//                    //                    let arr = value as! [String]
//                    //                    if arr.contains(div){
//                    //                        SecureMenu.menuIcons.append("ic_library_icon")
//                    //                        SecureMenu.menu.append(key)
//                    //                    }else{
//                    //                        print("False")
//                    //                    }
//                //                    break
//                case "Learning Management":
//                    SecureMenu.menuIcons.append("ic_learning")
//                    SecureMenu.menu.append(key)
//                    //                    let arr = value as! [String]
//                    //                    if arr.contains(div){
//                    //                        SecureMenu.menuIcons.append("ic_learning")
//                    //                        SecureMenu.menu.append(key)
//                    //                    }else{
//                    //                        print("False")
//                    //                    }
//                    break
//                    //                case "Healthcare" :
//                    //                    SecureMenu.menuIcons.append("ic_healthcare")
//                    //                    SecureMenu.menu.append(key)
//                //                    break
//                case "Student Profile":
//                    let arr = value as! [String]
//                    if arr.contains(div){
//                        SecureMenu.menuIcons.append("ic_student_profile")
//                        SecureMenu.menu.append(key)
//                    }else{
//                        print("False")
//                    }
//                    break
//                    //                case "Quiz":
//                    //                    let arr = value as! [String]
//                    //                    if arr.contains(div){
//                    //                        SecureMenu.menuIcons.append("ic_quiz")
//                    //                        SecureMenu.menu.append(key)
//                    //                    }else{
//                    //                        print("False")
//                    //                    }
//                    //                    break
//                    //                case "Feedback":
//                    //                    let arr = value as! [String]
//                    //                    if arr.contains(div){
//                    //                        SecureMenu.menuIcons.append("ic_FeePayment")
//                    //                        SecureMenu.menu.append(key)
//                    //                    }else{
//                    //                        print("False")
//                    //                    }
//                //                    break
//                case "Bus Tracking":
//                    let arr = value as! [String]
//                    if arr.contains(div){
//                        SecureMenu.menuIcons.append("ic_bus")
//                        SecureMenu.menu.append(key)
//                    }else{
//                        print("False")
//                    }
//                    break
//                    //                case "FAQ" :
//                    //                    let arr = value as! [String]
//                    //                    if arr.contains(div){
//                    //                        SecureMenu.menuIcons.append("ic_faq")
//                    //                        SecureMenu.menu.append(key)
//                    //                    }else{
//                    //                        print("False")
//                    //                    }
//                //                    break
//                case "Help-Ticket" :
//                    SecureMenu.menuIcons.append("ic_ticket")
//                    SecureMenu.menu.append(key)
//                    break
//                case "Reportbee":
//                    SecureMenu.menuIcons.append("ic_evaluationold")
//                    SecureMenu.menu.append("Reportbee")
//                    break
//                    //                case "LMS" :
//                    //                    SecureMenu.menuIcons.append("ic_LMS")
//                //                    SecureMenu.menu.append(key)
//                default:
//                    break
//                }
//            }
//        }
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        if isdissmis {
//            dismiss(animated: true, completion: nil)
//        }
//        NotificationCenter.default.addObserver(self, selector: #selector(pushNotificationReceived), name: NSNotification.Name(rawValue: "pushNotification"), object: nil)
//    }
//
//    @objc func pushNotificationReceived(notification: NSNotification) {
//        dismiss(animated: true, completion: nil)
//    }
//
//    private func displayNoPermissionAlert() {
//        let alert = UIAlertController(title: "Message", message: "No permission given for this functionality.", preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "ok", style: .default, handler:nil))
//        present(alert, animated: true, completion: nil)
//    }
//
//    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//        if Util.validateNetworkConnection(self) {
//            selectViewControllerAt(indexPath.row + 1)
//        }
//    }
//
//    @objc override func handleLogout() {
//
//        let logoutAlert = UIAlertController(title: nil, message: "Are you sure you want to logout?", preferredStyle: UIAlertController.Style.actionSheet)
//
//        logoutAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//
//        logoutAlert.addAction(UIAlertAction(title: "Logout", style: .destructive, handler: { [weak self] (action: UIAlertAction!) in
//            UserDefaults.standard.set(false, forKey: isLogged)
//            self?.resetDefaults()
//            self?.viewWillAppear(true)
//        }))
//        addActionSheetForiPad(actionSheet: logoutAlert)
//        present(logoutAlert, animated: true, completion: nil)
//    }
//}
//
//extension UIViewController {
//
//    func commonAlert(title : String, message : String , alertStyle : UIAlertController.Style , okButtonAction : UIAlertAction){
//        let alertController = UIAlertController(title: title, message: message, preferredStyle: alertStyle)
//        let okButton = okButtonAction
//        alertController.addAction(okButton)
//        present(alertController, animated: true, completion: nil)
//
//    }
//
//    func selectViewControllerAt(_ row : Int) {
//
//        switch(SecureMenu.menu[row - 1])
//        {
//        case "Message","Circular","Homework" :
//            let vc = HCMTableVC()
//            vc.selectedIndex = SecureMenu.menu[row - 1]
//            vc.isFromMainMenu = true
//            vc.title = SecureMenu.menu[row - 1]
//            navigationController?.pushViewController(vc, animated: true)
//            break
//
//        case "Student Profile":
//            let vc = StudentProfileVC()
//            vc.isFromMainMenu = true
//            navigationController?.pushViewController(vc, animated: true)
//            break
//
//        case "Class Timetable":
//            let vc = ClassTimeTableVC()
//            vc.isFromMainMenu = true
//            vc.title = SecureMenu.menu[row - 1]
//            navigationController?.pushViewController(vc, animated: true)
//            break
//
//        case "Exam Timetable":
//            let vc = ExamTimeTableListVC()
//            vc.isFromMainMenu = true
//            vc.title = SecureMenu.menu[row - 1]
//            navigationController?.pushViewController(vc, animated: true)
//            break
//
//        case "Attendance" :
//            let vc = AttendanceVC()
//            vc.isFromMainMenu = true
//            vc.title = SecureMenu.menu[row - 1]
//            navigationController?.pushViewController(vc, animated: true)
//            break
//
//        case "Evaluation" :
//            let vc = WebVC()
//            vc.isFromMainMenu = true
//            vc.selectedTag = 8
//            vc.title = SecureMenu.menu[row - 1]
//            navigationController?.pushViewController(vc, animated: true)
//            //            if UserDefaults.standard.bool(forKey: SDefaultKeys.isActiveEvaluation) {
//            //
//            //            }else
//            //            {
//            //                displayComingSoonAlert()
//            //            }
//            break
//        case "Fee Payment" :
//            let vc = FeePaymentVC()
//            vc.isFromMainMenu = true
//            vc.title = SecureMenu.menu[row - 1]
//            navigationController?.pushViewController(vc, animated: true)
//            break
//        case "Learning Management" :
//            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewBrowser") as! WebViewBrowser
//            vc.title = "Learning Management"
//            var cls = ""
//            var index = UserDefaults.standard.integer(forKey: SDefaultKeys.selectedStudent)
//            #if STMARY
//            cls = UserDefaults.standard.string(forKey: "\(SDefaultKeys.schoolClass)\(index)")?.numberValue?.romanNumeral() ?? ""
//            #else
//            cls = UserDefaults.standard.string(forKey: "\(SDefaultKeys.schoolClass)\(index)") ?? ""
//            #endif
//            //            let studCount = UserDefaults.standard.integer(forKey: "studentCount")
//            //            for i in 0..<studCount{
//            //                #if STMARY
//            //                cls += UserDefaults.standard.string(forKey: "\(SDefaultKeys.schoolClass)\(i)")?.numberValue?.romanNumeral() ?? ""
//            //                #else
//            //                cls += UserDefaults.standard.string(forKey: "\(SDefaultKeys.schoolClass)\(i)") ?? ""
//            //                #endif
//            //                if i != studCount-1{
//            //                    cls += ","
//            //                }
//            //            }
//            vc.urlString = "\(ParentsWebUrls.LMS_URL)\(cls)"
//            print("\(ParentsWebUrls.LMS_URL)\(cls)")
//            navigationController?.pushViewController(vc, animated: true)
//            //            let cls = UserDefaults.standard.string(forKey: "\(SDefaultKeys.schoolClass)\(0)")
//            //            print(UserDefaults.standard.integer(forKey: "studentCount"))
//            //            print(ParentsWebUrls.LMS_URL)
//            //
//
//            //            let vc = WebVC()
//            //            vc.isFromMainMenu = true
//            //            vc.selectedTag = 9
//            //            vc.title = SecureMenu.menu[row - 1]
//            //            navigationController?.pushViewController(vc, animated: true)
//            break
//
//        case "Healthcare" :
//            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewBrowser") as! WebViewBrowser
//            vc.title = "Healthcare"
//            vc.urlString = ParentsWebUrls.HEALTHCARE_URL
//            navigationController?.pushViewController(vc, animated: true)
//            break
//
//        case "Bus Tracking": //11 :
//            let vc = BusTrackingVC()
//            vc.isFromMainMenu = true
//            vc.title = SecureMenu.menu[row - 1]
//            navigationController?.pushViewController(vc, animated: true)
//            break
//
//        case "Help-Ticket":
//            if !MFMailComposeViewController.canSendMail() {
//                print("Mail services are not available")
//                commonAlert(title: "Email Services Are Not Available", message: "Please check mail account and retry again", alertStyle: UIAlertController.Style.alert, okButtonAction: UIAlertAction(title: "Ok", style: .default, handler: nil))
//                return
//            }
//            else{
//                let index = UserDefaults.standard.integer(forKey: SDefaultKeys.selectedStudent)
//                let name = UserDefaults.standard.string(forKey: "\(SDefaultKeys.name)\(index)")?.capitalized ?? ""
//                let roll = UserDefaults.standard.string(forKey: "\(SDefaultKeys.rollNumber)\(index)")
//                let className = UserDefaults.standard.string(forKey: "\(SDefaultKeys.schoolClass)\(index)")
//                let division = UserDefaults.standard.string(forKey: "\(SDefaultKeys.schoolDiv)\(index)")
//                let gaurdian =  UserDefaults.standard.string(forKey: "\(SDefaultKeys.gaurdian)")?.capitalized ?? ""
//                print(gaurdian)
//                var mobileNumber = ""
//                if let index = (gaurdian.range(of: "**")?.upperBound){
//                    mobileNumber = String(gaurdian.suffix(from: index))
//                }
//                let composeVC = MFMailComposeViewController()
//                composeVC.mailComposeDelegate = self as? MFMailComposeViewControllerDelegate
//
//                // Configure the fields of the interface.
//                composeVC.setToRecipients(["\(GlobleConstants.schoolCode)@intellischools.org".lowercased()])
//                composeVC.setSubject("Ticket from \(name)")
//                composeVC.setMessageBody("\n Description :\n\n\n\n\nPlease do not delete the account details below :\nSchool Name : \(GlobleConstants.schoolname) \n SchoolId : \(GlobleConstants.schoolID)\n Class : \(className ?? "") \(division ?? "")\n Roll No. : \(roll ?? "")\n Guardian Mobile No.: \(mobileNumber) \n Model Name : \(getAppInfo()) \n", isHTML: false)
//
//                // Present the view controller modally.
//                self.present(composeVC, animated: true, completion: nil)
//            }
//            break
//        case "Reportbee" :
//            let vc = ReportbeeViewController()
//            vc.isFromMainMenu = true
//            vc.title = SecureMenu.menu[row - 1]
//            navigationController?.pushViewController(vc, animated: true)
//            break
//        case "FAQ" :
//            let vc = WebVC()
//            vc.isFromMainMenu = true
//            vc.selectedTag = 1
//            vc.title = SecureMenu.menu[row - 1]
//            vc.url = URL(string: ParentsWebUrls.faq)
//            navigationController?.pushViewController(vc, animated: true)
//            break
//
//            //        case 10,12,16 :
//            //            let vc = FeedbackVC()
//            //            vc.isFromMainMenu = true
//            //            vc.title = SecureMenu.menu[row - 1]
//            //            if row == 10 {
//            //                vc.messageTxtView.text = "A student interest can be gauged by not only his/her academic performance but also their reading ahbits,extra-curricular activities, etc. With library records integrated our system, you can now get an insight inot the types and the names of the books the students read. This information will help you to understand your stuadent better and guide them in their career choice"
//            //            } else if row == 12 {
//            //                vc.messageTxtView.text = "This feature is under development and will be coming soon. You wold be able to play quiz based on your class and choice of subject."
//            //            } else {
//            //                vc.messageTxtView.text = "This feature is available via the login on school website only. It provides and opportunity for the Principal and management to get feedback from parents on various initiatives/ideas. It is also used to obtain feedback on programmes currently in place."
//            //            }
//            //            navigationController?.pushViewController(vc, animated: true)
//        //            break
//        default:
//            break
//        }
//    }
//    @objc(mailComposeController:didFinishWithResult:error:) func mailComposeController(_ controller: MFMailComposeViewController,didFinishWith result: MFMailComposeResult, error: Error?) {
//        // Check the result or perform other tasks.
//
//        // Dismiss the mail compose view controller.
//        controller.dismiss(animated: true, completion: nil)
//    }
//    func getAppInfo()->String {
//        return UIDevice.current.name + "\nVersion : iOS " + UIDevice.current.systemVersion
//    }
//    func displayComingSoonAlert() {
//        let alert = UIAlertController(title: "Message", message: "Coming soon...", preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "ok", style: .default, handler:nil))
//        present(alert, animated: true, completion: nil)
//    }
//
//    @objc func handleLogout() {
//
//        let logoutAlert = UIAlertController(title: nil, message: "Are you sure you want to logout?", preferredStyle: UIAlertController.Style.actionSheet)
//
//        logoutAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//
//        logoutAlert.addAction(UIAlertAction(title: "Logout", style: .destructive, handler: { [weak self] (action) in
//            UserDefaults.standard.set(false, forKey: isLogged)
//            UserDefaults.standard.synchronize()
//            if UserDefaults.standard.bool(forKey: isOnSecureMenu) {
//                self?.resetDefaults()
//                self?.navigationController?.popToRootViewController(animated: true)
//            }else {
//                self?.resetDefaults()
//                if let sl = self as? BaseViewController,sl.isFromMainMenu == true  {
//                    self?.navigationController?.popToRootViewController(animated: true)
//                }else {
//                    self?.viewWillAppear(true)
//                }
//            }
//
//        }))
//        addActionSheetForiPad(actionSheet: logoutAlert)
//        present(logoutAlert, animated: true, completion: nil)
//    }
//
//    func resetDefaults() {
//        let defaults = UserDefaults.standard
//        defaults.removeObject(forKey: "GetHomework")
//        defaults.removeObject(forKey: "GetMessage")
//        defaults.removeObject(forKey: "GetCircular")
//        defaults.removeObject(forKey: "News")
//        defaults.removeObject(forKey: "Menu")
//        let dictionary = defaults.dictionaryRepresentation()
//        dictionary.keys.forEach { key in
//            defaults.removeObject(forKey: key)
//        }
//        UserDefaults.standard.set(1, forKey: "TERMSANDCONDITION")
//        defaults.synchronize()
//    }
//}
