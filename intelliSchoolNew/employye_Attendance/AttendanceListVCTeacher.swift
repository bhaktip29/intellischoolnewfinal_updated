//
//  AttendanceListVCTeacher.swift
//  intelliSchoolNew
//
//  Created by Intellinects Venture on 26/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AttendanceListVCTeacher: BaseViewControllerTeacher,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,DatePikerDelegate
{
    var selectedDate = Date()
    let cellId = "TimeCell"
    let headerCellId  = "HeaderCell"
    var attendanceList = [AttendanceClassList]()
    var attendanceGroup = [AttendanceGroup]()
    var schoolClass = SchoolClassTeacher()
    
    let collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        let clView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        clView.backgroundColor = UIColor.groupTableViewBackground
        clView.translatesAutoresizingMaskIntoConstraints = false
        return clView
    }()

    // MARK:- Custom Views Setup Methods
    
    private func setUpCollectionView() {
        
        view.addSubview(collectionView)
        collectionView.dataSource = self
        collectionView.delegate = self
        
        // needed constarunts x,y,w,h
        collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    //MARK:- View Controllers Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        collectionView.register(AttendanceCVCellTeacher.self, forCellWithReuseIdentifier: cellId)
        collectionView.register(AttendanceHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader , withReuseIdentifier: headerCellId)
        self.title = ViewControllerTitles.attendenceVC
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: getFormattedDate(dateString: selectedDate), style: .plain, target: self, action: #selector(handleDisplayDate))
        setUpCollectionView()
        getAttendanceByDate(selectedDate)
    }
    func dismissDatePicker(_ isPresent: Bool) {}
    @objc func handleDisplayDate(sender: UIBarButtonItem) {
        let vc = DatePickerVC()
        vc.delegate = self
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChild(vc)
        view.addSubview(vc.view)
    }
    
    func handleWithSelectedDate(datePickerSelectedDate date: Date) {
        attendanceList.removeAll()
        attendanceGroup.removeAll()
        navigationItem.rightBarButtonItem?.title = getFormattedDate(dateString: date)
        getAttendanceByDate(date)
    }

    //MARK:- CollectionView Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return attendanceGroup.count
        }else{
            return attendanceList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! AttendanceCVCellTeacher
        
        if indexPath.section == 0 {
            let group = attendanceGroup[indexPath.row] as AttendanceGroup
            cell.timeLbl.text = group.classGroupName
            cell.timeLbl1.text = group.groupTotalStudent
            cell.timeLbl2.text = group.groupPresentStudent
            cell.timeLbl3.text = group.groupAbsentStudent
        }else {
            let cList = attendanceList[indexPath.row]
            cell.timeLbl.text = "\(cList.className!)-\(cList.division_name!)"
            cell.timeLbl1.text = cList.totalStudent
            cell.timeLbl2.text = cList.presentStudent
            cell.timeLbl3.text = cList.absentStudent
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 35)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerCellId, for: indexPath as IndexPath) as! AttendanceHeader
        
        headerView.headerLbl.text = indexPath.section == 0 ? "Group" : "Class"
        headerView.headerLbl1.text = "Total Student"
        headerView.headerLbl2.text = "Present"
        headerView.headerLbl3.text = "Absent"
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
    {
        return CGSize(width: collectionView.bounds.width, height: 30)
    }
    
    
    //MARK:- Server methods
    
    func getAttendanceByDate(_ date: Date)
    {
        if UtilTeacher.validateNetworkConnection(self){
            let userDefaults = UserDefaults.standard
            let parameters = [
                "school_db_settings_array": userDefaults.object(forKey: "SCHOOL_DB_SETTINGS") ?? "school_db_settings_array",
                "action":"AttendanceList",
                "employee_id":  userDefaults.string(forKey: "EMPLOYEE_ID"),
                "attenddate": "2019-12-30",
                "role": userDefaults.string(forKey: "EMPLOYEE_ROLE_ID") ?? "employee_role_id"] as [String : Any]
            showActivityIndicatory(uiView: view)
            Alamofire.request(WebServiceUrls.attendanceService,method:.post,parameters: parameters)
                .responseJSON{ [weak self] (response)  in
                     self?.stopActivityIndicator()
                    print("parameter AttendanceListVCTeacher\(parameters)")
                    print("response AttendanceListVCTeacher \response ")
                    if response.result.error != nil {
                        UtilTeacher.invokeAlertMethod("Failed!", strBody: "Error Occured While Processing Data ", delegate: nil,vcobj : self!)
                    }
                    if let jsonresponse = response.result.value {
                        let originalResponse = JSON(jsonresponse)
                        self?.getSuccessResponse(originalResponse: originalResponse)
                    }
            }
        }
    }
    
    
    func getSuccessResponse(originalResponse : JSON)
    {
        if(originalResponse.count > 0)
        {
            for (key,subJson):(String, JSON) in originalResponse {
                if key == "classGroupList" {
                    for item in subJson.arrayValue {
                        let attGroup = AttendanceGroup()
                        attGroup.classGroupId = item["classGroupId"].stringValue
                        attGroup.classGroupName = item["classGroupName"].stringValue
                        attGroup.groupAbsentStudent = item["groupAbsentStudent"].stringValue
                        attGroup.groupTotalStudent = item["groupTotalStudent"].stringValue
                        attGroup.groupPresentStudent = item["groupPresentStudent"].stringValue
                        attendanceGroup.append(attGroup)
                    }
                }
                if key  == "classList" {
                    for item in subJson.arrayValue {
                        let attList = AttendanceClassList()
                        attList.presentStudent = item["presentStudent"].stringValue
                        attList.absentStudent = item["absentStudent"].stringValue
                        attList.totalStudent = item["totalStudent"].stringValue
                        attList.className = item["class_name"].stringValue
                        attList.classid = item["classid"].stringValue
                        attList.division_id = item["division_id"].stringValue
                        attList.board = item["board"].stringValue
                        attList.division_name = item["division_name"].stringValue
                        attendanceList.append(attList)
                    }
                }
             }
            collectionView.reloadData()
        }
    }
    
    func getYYYYMDDFormattedDate(_ dateString: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-M-dd"
        return formatter.string(from: dateString)
    }
}


class AttendanceCVCellTeacher :UICollectionViewCell {
    
    var timeLbl = UILabel()
    var timeLbl1 = UILabel()
    var timeLbl2 = UILabel()
    var timeLbl3 = UILabel()
    
    private func setUpLbels() {
        
        var lbls = [UILabel]()
        
        func getLabel() -> UILabel {
            let lbl = UILabel()
            lbl.textColor = UIColor.black
            lbl.font = UIFont(name: "Helvetica Neue", size: 13)
            lbl.textAlignment = .center
            lbl.numberOfLines = 0
            lbl.layer.borderColor = UIColor.lightGray.cgColor
            lbl.layer.borderWidth = 0.3
            lbl.sizeToFit()
            return lbl
        }
        
        timeLbl = getLabel()
        lbls.append(timeLbl)
        timeLbl1 = getLabel()
        lbls.append(timeLbl1)
        timeLbl2 = getLabel()
        lbls.append(timeLbl2)
        timeLbl3 = getLabel()
        lbls.append(timeLbl3)
        
        let stackView = UIStackView(arrangedSubviews: lbls)
        stackView.alignment = .fill
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(stackView)
        // needed constarints
        stackView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        stackView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        stackView.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        setUpLbels()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class AttendanceHeader: UICollectionReusableView {
    
    var headerLbl = UILabel()
    var headerLbl1 = UILabel()
    var headerLbl2 = UILabel()
    var headerLbl3 = UILabel()
    
    private func setUpLbels() {
        
        var lbls = [UILabel]()
        
        func getLabel() -> UILabel {
            let lbl = UILabel()
            lbl.textColor = UIColor.black
            lbl.font = UIFont.boldSystemFont(ofSize: 10) //UIFont(name: "Helvetica Neue", size: 13)
            lbl.textAlignment = .center
            lbl.layer.borderColor = UIColor.darkGray.cgColor
            lbl.layer.borderWidth = 0.3
            return lbl
        }
        
        headerLbl = getLabel()
        lbls.append(headerLbl)
        headerLbl1 = getLabel()
        lbls.append(headerLbl1)
        headerLbl2 = getLabel()
        lbls.append(headerLbl2)
        headerLbl3 = getLabel()
        lbls.append(headerLbl3)
        
        
        let stackView = UIStackView(arrangedSubviews: lbls)
        stackView.alignment = .fill
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(stackView)
        // needed constarints
        stackView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        stackView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        stackView.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.lightGray
        setUpLbels()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
