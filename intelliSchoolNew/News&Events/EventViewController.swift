//
//  EventViewController.swift
//  Intellinects
//
//  Created by rupali  on 04/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import  Alamofire
import SwiftyJSON
class EventViewController: UIViewController , UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout  {
    var Events = [Event]()
    var datechange : String!
    var date : String!
    var dateMonth : String!
    var dateIntValue : String!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var eventNotFound: UILabel!
    @IBOutlet weak var ImgViewEmptyData: UIImageView!
    var name = ["lorem ipsum is simply dummy","lorem ipsum is simply dummy","lorem ipsum is simply dummy","lorem ipsum is simply dummy","lorem ipsum is simply dummy","lorem ipsum is simply dummy","lorem ipsum is simply dummy","lorem ipsum is simply dummy","lorem ipsum is simply dummy","lorem ipsum is simply dummy",]
//    var date = ["12:00AMto 23:00PM","12:00AMto 23:00PM","12:00AMto 23:00PM","12:00AMto 23:00PM","12:00AMto 23:00PM","12:00AMto 23:00PM","12:00AMto 23:00PM","12:00AMto 23:00PM","12:00AMto 23:00PM","12:00AMto 23:00PM",]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
     getEventdata()

    }
   func getEventdata()
        {
        var url : String = ""
        
        if Util.validateNetworkConnection(self){
     //   showActivityIndicatory(uiView: view)
        let sendDate = "2019-08-01"
            url =  "http://intellischools.com/schoolAdminApi/public/api/event/eventListApi"
            let params = [
            "token" :UserDefaults.standard.value(forKey: "sessionToken") as! String ,
            "schoolcode" : "INTELL",
            "academicYear" : "2019-2020"
            ] as [String : Any]
        Alamofire.request(url,method: .post,parameters: params)
        .responseJSON {[weak self] (response) in
        //self?.stopActivityIndicator()
        if response.result.error != nil {

        }
        print("Eventsresponse1>>>>\(response.result)")
        switch response.result {
        case .success:
        if let jsonresponse = response.result.value {
        let originalResponse = JSON(jsonresponse)
        print("response2>>>>\(originalResponse)")
       // var data = originalResponse["data"]
        let recordsArray: [NSDictionary] = originalResponse["EventList"].arrayObject as! [NSDictionary]
        print("eventrecordsArrayrecordsArray\(recordsArray)")
        for item in recordsArray {
            var eventName = item.value(forKey: "eventName")
            var startDate = item.value(forKey: "startDate")
            var startTime = item.value(forKey: "startTime")
            var endTime = item.value(forKey: "endTime")
            var allDay = item.value(forKey: "allDay")
            
            print("eventName :: \(eventName)")
            print("startDate :: \(startDate)")
            print("startTime :: \(startTime)")
            print("endTime :: \(endTime)")
            print("allDay :: \(allDay)")


            
            // date formatter
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd"
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "MMM d"
            
            let dateFormatterPrintMonth = DateFormatter()

           dateFormatterPrintMonth.dateFormat = "MMM"
            
            let dateFormatterPrintDate = DateFormatter()

            dateFormatterPrintDate.dateFormat = "d"

            self!.date = (startDate as! String)
            if let date = dateFormatterGet.date(from: self!.date) {
                print("dateformaterrr>>>\(dateFormatterPrint.string(from: date))")
                self!.datechange = dateFormatterPrint.string(from: date)
                self!.dateMonth = dateFormatterPrintMonth.string(from: date)
                print("dateformaterrr2>>>\(dateFormatterPrintMonth.string(from: date))")
                print("dateformaterrr3>>>\(dateFormatterPrintDate.string(from: date))")

                self!.dateIntValue = dateFormatterPrintDate.string(from: date)

            }
            else
            {
                print("There was an error decoding the string")
            }
          
            print("c\(self!.datechange)")
            print("datechangedatechange\(self!.datechange)")
            var event2 = Event(eventName: eventName as! String, startDate: self?.datechange, startTime: startTime  as! String, endTime: endTime  as! String, allDay: "allday", postdateMonth: self!.dateMonth, postDateInt: self!.dateIntValue  )
            self!.Events.append(event2)
            
        }
            if recordsArray.isEmpty
            {
              
                self!.collectionView.isHidden = true
    //            self!.ImgViewEmptyData.image = UIImage(named: "Screen Shot 2013-04-03 at 11.30.55 AM")
                self!.eventNotFound.text = "Events Not Found"
            }
        self?.collectionView.reloadData()
        }
        case .failure(_):
        self!.collectionView.isHidden = true
        self!.eventNotFound.text = "Events Not Found"

        print("switch case erroe")
        }
        }
        }
        }
        

    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Events.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EventCell", for: indexPath) as! EventsCollectionViewCell
        let userList = Events[indexPath.row]
        
        
        cell.layer.cornerRadius = 10.0
        cell.clipsToBounds = true
     
        let color1 = UIColor(hexString: "#8a638f")
        let color2 = UIColor(hexString: "#0bc1f0")
        let color3 = UIColor(hexString: "#ffa200")
        let bgColors = [color1,color2,color3]
        
       
        cell.RandomColorLbl.backgroundColor = bgColors[indexPath.row % bgColors.count]
        cell.PostTitleLbl.text = userList.geteventName()
     //   cell.postContentLbl.text = userList.getpostContent()
        cell.MonthLbl.text = userList.getpostdateMonth()
        cell.dateIntLbl.text = userList.getpostDateInt()
        cell.allday.text = (userList.getstartTime() + (" to ") + userList.getendTime())

//        if userList.getallDay() == true as? String
//        {
//        cell.allday.text = userList.getallDay()
//
//        }
        
        return cell
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
                 return CGSize(width: self.collectionView.frame.width-20, height: self.collectionView.frame.width/4)
//        let bounds = collectionView.bounds
//
//        return CGSize(width: bounds.width-60, height: self.collectionView.frame.width/4)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        //horizontal spacing
        return 15
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var cell = collectionView.cellForItem(at: indexPath) as! EventsCollectionViewCell
        var userList = Events[indexPath.row]
        var index = indexPath.row
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
        controller.index3 = indexPath.row
        controller.eventName = cell.PostTitleLbl.text
        controller.allday =  cell.allday.text
        controller.dateandmonth = (userList.getpostdateMonth() + userList.getpostDateInt())
        
        
       
        self.present(controller, animated: true, completion: nil)
     //   UserDefaults.standard.set(index, forKey: "NewsIndex")

        var EventPostTitle = cell.PostTitleLbl.text
          UserDefaults.standard.set(EventPostTitle, forKey: "EventPostTitle")
        var EventPostContent = cell.postContentLbl.text
        UserDefaults.standard.set(EventPostContent, forKey: "EventPostContent")
        
    }

    @IBAction func backBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let controller = storyboard.instantiateViewController(withIdentifier: "vc")
               self.present(controller, animated: true, completion: nil)
        
    }
    
}
