//
//  EventsCollectionViewCell.swift
//  intelliSchoolNew
//
//  Created by rupali  on 18/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class EventsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var dateeee: UILabel!
    
    @IBOutlet weak var dateIntLbl: UILabel!
    @IBOutlet weak var MonthLbl: UILabel!
    @IBOutlet weak var postContentLbl: UILabel!
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var PostTitleLbl: UILabel!
    @IBOutlet weak var allday: UILabel!
    
    @IBOutlet weak var RandomColorLbl: UILabel!
}
