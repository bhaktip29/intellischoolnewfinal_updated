//
//  NewsVC.swift
//  Intellinects
//
//  Created by rupali  on 03/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Nuke
class NewsVC:  BaseViewController {
  let array = ["1","2"]
    var datechange : String!

    
    @IBOutlet weak var collectionView: UICollectionView!
    var newsList = [PostInfo]()
    override func viewDidLoad() {
    super.viewDidLoad()
    getNewsData()
    }
   func getdata()
   {
    var url =           "https://apps.intellinects.com/TestApi/public/api/wpNews"
    var para =
        [ "SCHOOL_ID" : 327
        ]
        Alamofire.request(url,method: .post,parameters: para).responseJSON {
        response in
        
        //  print("Parameters :\(parameters)")
        print("Response ::::\(response)")
        
        guard let value = response.result.value as? [String: Any] else {
            return
        }
        print("response.result\(response.result)")
        print("value>>>>>>>>>\(value)")
        let status = value["result"] as! String
        print("status>>>>>>\(status)")
        if status != "success"
        {
        return
        }
        guard let data = value["postStatus"] as? [String: Any] else
        {
        return
                
        }
        print("data>>>>>>>>\(data)")
           
            
         
       
    }

    }
    
    func getNewsData()
    {
            var url : String = ""
            
            if Util.validateNetworkConnection(self){
                showActivityIndicatory(uiView: view)
                let sendDate = "2015-01-01"
            url = "https://apps.intellinects.com/TestApi/public/api/wpNews"
                var param =
                    [ "SCHOOL_ID" : 383
                ]
            Alamofire.request(url,method: .post,parameters: param)
                    .responseJSON {[weak self] (response) in
            self?.stopActivityIndicator()
            if response.result.error != nil {
                            // print("response>>>>\(response)")
            }
            print("response.result>>>>\(response.result.value)")
            switch response.result {
            case .success:
            if let jsonresponse = response.result.value {
            let originalResponse = JSON(jsonresponse)
                print("originalResponse\(originalResponse)")
                var recordsArray: [NSDictionary] = originalResponse.arrayObject as! [NSDictionary]
                        
            print("responseNewss>>>>\(originalResponse)")
            print("recordsArrayNewss>>>>\(recordsArray)")

                for item in recordsArray {
       //     var ready = item.sorted(by: { $0.compare($1) == .orderedDescending })

            let postTitle1 = item.value(forKey: "postTitle")
            let postDate = item.value(forKey: "postDate")
            let postContent = item.value(forKey: "postContent")
            let featuredImage = item.value(forKey: "featuredImage")
            let postStatus = item.value(forKey: "postStatus")
            // var changeddate = self!.changeDate(date as! String)
          //  print("changeddate>>>>>\(date)")
            print("postTitle1>>>> \(postTitle1!)")
            print("postDate \(postDate!)")
            print("postContent \(postContent!)")
            print("featuredImage>>> \(featuredImage!)")

               // changeDate(_, mydate : postDate)
            // date formatter
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MM-yyyy"
                                
            if let date = dateFormatterGet.date(from: "2016-oct-29 12:24:26") {
            print("dateformaterrr>>>\(dateFormatterPrint.string(from: date))")
            self!.datechange = dateFormatterPrint.string(from: date)
            }
            else
            {
            print("There was an error decoding the string")
            }
                
                
                ////SECOND DATE FORMATTER
    //            if let timeResult = postDate {
                    let date = Date(timeIntervalSince1970: postDate as! TimeInterval)
            //    print("\()")
                    let dateFormatter = DateFormatter()
                  //  dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
                    dateFormatter.dateFormat = "dd MMM YYYY"
     //Set date style
                   // dateFormatter.timeZone = .current
                    let localDate = dateFormatter.string(from: date)
                
                print("localDatelocalDatelocalDate\(localDate)")
               // }
                    
           if postStatus as! String == "publish"
            {
                print("postStatuspostStatus\(postStatus)")
                let postinfo = PostInfo(postTitle: postTitle1 as? String, date: localDate , postContent : postContent as? String , featuredImage : featuredImage as? String)
            self!.newsList.append(postinfo)
               // var customObjects = [obj1, obj2, obj3, obj4, obj5]
           
                
            print("newsList>>>>>>>>>>>>>>>\(self!.newsList)")
                               
                }}
                self?.newsList.reverse()
            self?.collectionView.reloadData()
            }
            case .failure(_):
            print("switch case erroe")
                }
            }
        }
        

        
    }
    func changeDate(_ mydate:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let convertedDate = dateFormatter.date(from: mydate)
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let date = dateFormatter.string(from: convertedDate!)
        return date
    }
    @IBAction func backBtn(_ sender: Any) {
//        navigationController?.popViewController(animated: true)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let controller = storyboard.instantiateViewController(withIdentifier: "vc")
               self.present(controller, animated: true, completion: nil)
        

    }
    
}



extension NewsVC : UICollectionViewDataSource , UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return newsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath) as! NewsCollectionViewCell
        cell.layer.cornerRadius = 5.0
        cell.clipsToBounds = true
     //   let userObjec = Array(reversed1.sorted().reversed())

    let userObjec = newsList[indexPath.row]
      //  userObjec.so
     //   userObjec.getpostTitle()?.({ $0.fileID > $1.fileID })
        //cell.lbl.text = array[indexPath.row]
        cell.lbl.text = userObjec.getpostTitle()
         cell.dateLbl.text = userObjec.getdate()
        cell.postContentLbl.text = userObjec.getpostContent()
       // cell.lbl.text = userObjec.getPost_Content()
        print("inside collection")
        print("userObjec>>>\(userObjec)")
        cell.addBottomBorderWithColor(color: .gray, width: 1)
            //Nuke image adding
                
                            let photoUrl = NSURL(string: userObjec.getfeaturedImage())
                           
                            let imgUrl = userObjec.getfeaturedImage().removingPercentEncoding
                            let options = ImageLoadingOptions(
                                placeholder: UIImage(named: "placeholder"),
                                transition: .fadeIn(duration: 0.33)
                            )
            Nuke.loadImage(with: URL(string: imgUrl!)!, options: options, into: cell.ImageView)
        
    
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var cell = collectionView.cellForItem(at: indexPath) as! NewsCollectionViewCell
        var index = indexPath.row

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "NewsDetailsViewController")
        self.present(controller, animated: true, completion: nil)
        UserDefaults.standard.set(index, forKey: "NewsIndex")

        var NewsPostTitle = cell.lbl.text
          UserDefaults.standard.set(NewsPostTitle, forKey: "NewsPostTitle")
        var NewsPostContent = cell.postContentLbl.text
        UserDefaults.standard.set(NewsPostContent, forKey: "NewsPostContent")
        
    }
    
}
extension String {
   
}
extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}
