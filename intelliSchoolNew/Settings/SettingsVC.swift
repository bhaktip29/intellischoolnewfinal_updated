//
//  SettingsVC.swift
//  Intellinects
//
//  Created by rupali  on 07/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import DLRadioButton
class SettingsVC: UIViewController{
   
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var label5: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        label1.addBottomBorderWithColor(color: .gray, width: 1)
        label2.addBottomBorderWithColor(color: .gray, width: 1)
        label3.addBottomBorderWithColor(color: .gray, width: 1)
        label4.addBottomBorderWithColor(color: .gray, width: 1)
        label5.addBottomBorderWithColor(color: .gray, width: 1)
        // Do any additional setup after loading the view.
        
//        if let myImage = UIImage(named: "bus") {
//            let tintableImage = myImage.withRenderingMode(.alwaysTemplate)
//            imageView.image = tintableImage
//        }
//        imageView.tintColor = UIColor.red
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UIView {
    func addTopBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x:0,y: 0, width:self.frame.size.width, height:width)
        self.layer.addSublayer(border)
    }
    
    func addRightBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: self.frame.size.width - width,y: 0, width:width, height:self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x:0, y:self.frame.size.height - width, width:self.frame.size.width, height:width)
        self.layer.addSublayer(border)
    }
    
    func addLeftBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x:0, y:0, width:width, height:self.frame.size.height)
        self.layer.addSublayer(border)
    }
}
