//
//  BusTrackingVC.swift
//  intelliSchoolNew
//
//  Created by Intellinects Venture on 13/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON
import WebKit
class BusTrackingVC: BaseViewController, UIWebViewDelegate,CLLocationManagerDelegate {

    let navBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))

        let webView: WKWebView = {
            let wbView = WKWebView()
            wbView.translatesAutoresizingMaskIntoConstraints = false
           // wbView.scalesPageToFit = true
            wbView.scrollView.isScrollEnabled = false
            wbView.scrollView.bounces  = false
            return wbView
        }()
        
        // MARK:- Custom Views Setup Methods
        private func setUpViews() {
            view.addSubview(webView)
            // needed constraints
            if #available(iOS 11.0, *) {
                navigationController?.navigationBar.barTintColor = UIColor(red: 25.0/255.0, green: 89.0/255.0, blue: 194.0/255.0, alpha: 1.0)

                       self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
                navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(handleSlide))
                navigationItem.leftBarButtonItem?.tintColor = .white //GlobleConstants.navigationBarColorTint
              //  navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(handleSlide1))
                navigationItem.rightBarButtonItem?.tintColor = .white
                
                      //  let navItem = UINavigationItem(title: "SomeTitle")
                webView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor).isActive = true
                webView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
            } else {
                webView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
                webView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
            }
          //  webView.topAnchor.constraint(equalTo: navBar.topAnchor, constant: 15)

            webView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
            webView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
    //        view.bringSubview(toFront: actInd)
        }
    @objc func handleSlide() {
           print("back button tapped..")
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
                  let controller = storyboard.instantiateViewController(withIdentifier: "vc")
                  self.present(controller, animated: true, completion: nil)
       }
        //MARK:- View Controller Methods
        override func viewDidLoad() {
            super.viewDidLoad()
        
//            let width = self.view.frame.width
//            let navigationBar: UINavigationBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: width, height: 44))
//            self.view.addSubview(navigationBar)
            

            
            STMARYGetDrivoolCredentails()
            view.backgroundColor = UIColor.white
            setUpViews()
         //   webView.delegate = self
            #if STMARY
            STMARYGetDrivoolCredentails()
            #endif
            

        }

        func webViewDidStartLoad(_ webView: UIWebView) {
        }
        func webViewDidFinishLoad(_ webView: UIWebView) {
            self.stopActivityIndicator()
        }
        func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
            self.stopActivityIndicator()
            Util.alert("Connection Interrupted", message:  "could not connect to the Server. Make sure your network connection is active and try again",controller:self)
        }
        func STMARYGetDrivoolCredentails() {
            let url = "http://isirs.org/bus_tracking_ws/login_drivool.php"
            let params = [
                "action":"login_drivool",
                "intellinectsid":"STMARY1605SCH4058"
                ] as [String : Any]
            print(params)
            if Util.validateNetworkConnection(self) {
                showActivityIndicatory(uiView: view)
                Alamofire.request(url, method: .post, parameters: params)
                    .responseJSON { [weak self](response) in
                        print("bustracking>>>",response.result.value as Any)
                        if response.result.error != nil {
                            Util.alert("Failed!", message: "Error Occured While Processing Data ",controller:self!)
                        }
                        if let jsonresponse = response.result.value {
                            var gapn = ""
                            var ZOOMTO = ""
                            var SHOWONLYArray  = [String]()
                            let originalResponse = JSON(jsonresponse)
                            for (key,subJson):(String, JSON) in originalResponse {
                                if(key == "gapn"){
                                    gapn = subJson.stringValue
                                }
                                if(key == "ZOOMTO"){
                                    ZOOMTO = subJson.stringValue
                                }
                                if (key == "PICKUPDROPPOINTS"){
                                    for (key1,subJson1):(String, JSON) in subJson{
                                        if( key == "delgsi03"){
                                            
                                        }
                                        for(key, subjson2): (String, JSON) in subJson1{
                                            if(key == "p"){
                                                if subjson2.stringValue != ""{
                                                    SHOWONLYArray.append("\(key1).\(subjson2.stringValue)")
                                                }
                                            }
                                            else if (key == "d"){
                                                if subjson2.stringValue != ""{
                                                    SHOWONLYArray.append("\(key1).\(subjson2.stringValue)")
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if ZOOMTO == "delgsi03"{
                                self?.commonAlert(title: "No Routes Found", message: "Bus tracking is not available currently", alertStyle: .alert, okButtonAction: UIAlertAction(title: "Ok", style: .default, handler: { (okAction) in
                                    self?.navigationController?.popViewController(animated: true)
                                    self?.dismiss(animated: true, completion: nil)
                                }))
                            }else if(gapn  != ""){
                                let showOnly = SHOWONLYArray.joined(separator: ",")
                                let keyOfGapn = "-LAb-KHCL1dUmPOkhzdS"
                                let urlString = "https://www.drivool.com/track/vtschool.html?g=\(gapn)&k=\(keyOfGapn)&showonly=\(showOnly)&z=\(ZOOMTO)"
                                let url = URL(string: urlString)
                                var request = URLRequest(url: url!)
                                request.httpMethod = "GET"
                                self?.webView.load(request)
                            }
                        }
                }
            }
        }
    }
//extension UIViewController {
//
//  func commonAlert(title : String, message : String , alertStyle : UIAlertController.Style , okButtonAction : UIAlertAction){
//      let alertController = UIAlertController(title: title, message: message, preferredStyle: alertStyle)
//      let okButton = okButtonAction
//      alertController.addAction(okButton)
//      present(alertController, animated: true, completion: nil)
//      
//  }
//}
