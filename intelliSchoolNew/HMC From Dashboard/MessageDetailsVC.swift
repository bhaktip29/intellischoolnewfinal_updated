//
//  MessageDetailsVC.swift
//  intelliSchoolNew
//
//  Created by Intellinects Venture on 26/11/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class MessageDetailsVC: UIViewController {

    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var lblFirstLetter: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var tittle: UILabel!
    @IBOutlet weak var content: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblFirstLetter.layer.cornerRadius =  lblFirstLetter.frame.width/2
        lblFirstLetter.layer.masksToBounds = true
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let color1 = UIColor(hexString: "#1862C6")
        statusBarView.backgroundColor = color1
        view.addSubview(statusBarView)
        var MessageTitle = UserDefaults.standard.string(forKey: "MessageTitle")
        var MessagePostContent = UserDefaults.standard.string( forKey: "MessagePostContent")
        
        var nameLbl = UserDefaults.standard.string(forKey: "nameLbl")
        var dateLbl = UserDefaults.standard.string(forKey: "dateLbl")
        var colorfullLbl = UserDefaults.standard.string(forKey: "colorfullLbl")
        tittle.text = MessageTitle
        content.text = MessagePostContent
        lbldate.text = dateLbl
        lblFirstLetter.text = colorfullLbl
        name.text = nameLbl
        
    }
    
    @IBAction func backBtn(_ sender: Any) {
        if ViewController.isDashboardVC == true {

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "MessageDashboardViewController")
        self.present(controller, animated: true, completion: nil)
    }
    else{
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "InboxVC")
        self.present(controller, animated: true, completion: nil)
    }
    



}
}
