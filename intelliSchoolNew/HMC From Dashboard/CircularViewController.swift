//
//  CircularViewController.swift
//  intelliSchoolNew
//
//  Created by rupali  on 25/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class CircularViewController:  BaseViewController , UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    var datechange : String!
    var InfoList = [circular]()
    var date : String?
    var parameters : [String:Any]!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var collectionView: UICollectionView!
    var names = ["label","label","label","label","label","label","label","label","label","label","label","label","label","label","label","label","label",]
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return InfoList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "circular", for: indexPath) as! NewsCollectionViewCell
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: "circularDashboardCollectionViewCell", for: indexPath) as! circularDashboardCollectionViewCell
        cell.layer.cornerRadius = 5.0
        cell.clipsToBounds = true
        var UserObj = InfoList[indexPath.row]
        cell.nameLbl.text = ((UserObj.getfName()) + (" ") + (UserObj.getlName()))
        cell.dateLbl.text = UserObj.getpostDate()
        cell.postContentLbl.text = UserObj.getpostContent()
        cell.titleLbl.text = UserObj.getpostTitle()
        let a = UserObj.getfName()
        let firstLtter = String(a!.prefix(1))
        print("firstLtterfirstLtter\(firstLtter)")
        
        //
        cell.colorfullLbl.text = firstLtter
        // Cell.firstLetterLbl.layer.cornerRadius = Cell.firstLetterLbl.frame.width/2
        cell.colorfullLbl.layer.cornerRadius =  cell.colorfullLbl.frame.width/2
        cell.colorfullLbl.layer.masksToBounds = true
        
        //
        let color1 = UIColor(hexString: "#f6bf26")
        let color2 = UIColor(hexString: "#ba68c8")
        let color3 = UIColor(hexString: "#5e97f6")
        let color4 = UIColor(hexString: "#57bb8a")

        let bgColors = [color1,color2,color3,color4]
        
        
        cell.colorfullLbl.backgroundColor = bgColors[indexPath.row % bgColors.count]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        //         return CGSize(width: self.collectionView.frame.width/2, height: self.collectionView.frame.width/4)
        let bounds = collectionView.bounds
        
        return CGSize(width: bounds.width-30, height: self.collectionView.frame.width/3)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var cell = collectionView.cellForItem(at: indexPath) as! circularDashboardCollectionViewCell


        var CircularTitle = cell.titleLbl.text
          UserDefaults.standard.set(CircularTitle, forKey: "CircularTitle")
        var CircularPostContent = cell.postContentLbl.text
        UserDefaults.standard.set(CircularPostContent, forKey: "CircularPostContent")
        
        
        var nameLbl = cell.nameLbl.text
         UserDefaults.standard.set(nameLbl, forKey: "nameLbl")
        var dateLbl = cell.dateLbl.text
         UserDefaults.standard.set(dateLbl, forKey: "dateLbl")
        var colorfullLbl = cell.colorfullLbl.text
         UserDefaults.standard.set(colorfullLbl, forKey: "colorfullLbl")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CircularDetailsVC")
        self.present(controller, animated: true, completion: nil)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        getNewsData()
        
        
        let flow = UICollectionViewFlowLayout()
        if ViewController.isDashboardVC == true {
        flow.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
         let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
         let color1 = UIColor(hexString: "#1862C6")
         statusBarView.backgroundColor = color1
         view.addSubview(statusBarView)
       }
      
        else
        {
            let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
            let color1 = UIColor(hexString: "#F7F7F7")
            statusBarView.backgroundColor = color1
            view.addSubview(statusBarView)
            
           // flow.minimumInteritemSpacing = 50
            flow.minimumLineSpacing = 10

            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
            navigationBar.isHidden = true
        flow.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 150, right: 0)
        }
        collectionView?.setCollectionViewLayout(flow, animated: false)

        // Do any additional setup after loading the view.
    }
    
    func getNewsData()
    {
        var url : String = ""
        
        if Util.validateNetworkConnection(self){
            showActivityIndicatory(uiView: view)
            let sendDate = "2015-01-01"
            //            url =           "https://isirs.org/intellinect_dashboard/WS/index_main.php?action=GetCircular&SCHOOL_ID=8&deviceId=869447038367960&userId=266&role=Father"
            url = "http://commn.intellischools.com/hmcs_angular_api/public/api/circularList"
            if(UserDefaults.standard.string(forKey: "parent") == "0")
            {
            parameters = ["schoolCode" :  UserDefaults.standard.value(forKey: "code")as! String ,
                          "token" :  UserDefaults.standard.value(forKey: "sessionToken") as! String,
                          "is_academicYear" :  UserDefaults.standard.value(forKey: "accademicYear") as! String
                
                            ] as [String:Any]
            }
            else
            {
                
                var viewcontrolerclassName = UserDefaults.standard.string(forKey: "className")

                parameters = ["schoolCode" : UserDefaults.standard.value(forKey: "code")as! String,
                              "token" : UserDefaults.standard.value(forKey: "sessionToken") as! String,
                              "is_academicYear" : UserDefaults.standard.value(forKey: "accademicYear") as! String,
                              "className" : UserDefaults.standard.string(forKey: "className") as! String
                    ] as [String:Any]
            }
            print("parametersparameters\(parameters)")
            Alamofire.request(url,method: .post,parameters: parameters)
                .responseJSON {[weak self] (response) in
                    self?.stopActivityIndicator()
                    if response.result.error != nil {
                        
                    }
                    print("response.reslt>>>>\(response.result)")
                    switch response.result {
                    case .success:
                        if let jsonresponse = response.result.value {
                            let originalResponse = JSON(jsonresponse)
                            print("Circularresponse1>>>>\(originalResponse)")
                            let data = originalResponse["data"]
                            let recordsArray: [NSDictionary] = data.arrayObject as! [NSDictionary]
                            
                            
                            print("recordsArray>>>>\(recordsArray)")
                            
                            
                            for item in recordsArray {
                                
                                
                                let postDate = item.value(forKey: "createdDate")
                                let fName = item.value(forKey: "fname")
                                let lName = item.value(forKey: "lname")
                                let title = item.value(forKey: "title")
                                let postContent = item.value(forKey: "msgContent")
                                let postTitle = item.value(forKey: "title")
                                let teacher1 = item.value(forKey: "teacher")
                                let attachment = item.value(forKey: "attachments")
                                // let date = item.value(forKey: "postDate")
                                // var changeddate = self!.changeDate(date as! String)
                                print("postDate>>>>>\(postDate)")
                                print("postContent>>>> \(postContent!)")
                                print("title>>>>>\(title)")
                                print("fName>>>>>\(fName)")
                                print("attachment >> \(attachment)")
                                
                                //print("android_version \(postDate!)")
                                
                                
                                // date formatter
                                let dateFormatterGet = DateFormatter()
                                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                
                                let dateFormatterPrint = DateFormatter()
                                dateFormatterPrint.dateFormat = "dd MMM YYYY"
                                self!.date = (postDate as! String)
                                if let date = dateFormatterGet.date(from: self!.date!)
                                {
                        print("dateformaterrr>>>\(dateFormatterPrint.string(from: date))")
                                    self!.datechange = dateFormatterPrint.string(from: date)
                                }
                                else
                                {
                                    print("There was an error decoding the string")
                                }
                                
                                let Info = circular(postDate: self!.datechange as! String, postContent: postContent as! String , postTitle: postTitle as! String, fName: fName as! String, lName: lName as! String)
                                self!.InfoList.append(Info)
                                print("CirculrListforhomework>>>>>>>>>>>>>>>\(self!.InfoList)")
                                
                                
                            }
                            self?.collectionView.reloadData()
                        }
                    case .failure(_):
                        print("switch case erroe")
                    }
            }
        }
        
        
        
    }

    
    
    
}
