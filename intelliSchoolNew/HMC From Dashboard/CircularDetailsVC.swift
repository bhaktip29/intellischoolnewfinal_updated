//
//  CircularDetailsVC.swift
//  intelliSchoolNew
//
//  Created by Intellinects Venture on 26/11/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class CircularDetailsVC: UIViewController {

    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var lblfirstlettwr: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var textview: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblfirstlettwr.layer.cornerRadius =  lblfirstlettwr.frame.width/2
        lblfirstlettwr.layer.masksToBounds = true

        
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let color1 = UIColor(hexString: "#1862C6")
        statusBarView.backgroundColor = color1
        view.addSubview(statusBarView)
        var CircularTitle =
          UserDefaults.standard.string( forKey: "CircularTitle")
        var CircularPostContent =
        UserDefaults.standard.string(forKey: "CircularPostContent")
        // Do any additional setup after loading the view.
        titleLbl.text = CircularTitle
        textview.text = CircularPostContent
        
        
        var nameLbl = UserDefaults.standard.string(forKey: "nameLbl")
        var dateLbl = UserDefaults.standard.string(forKey: "dateLbl")
        var colorfullLbl = UserDefaults.standard.string(forKey: "colorfullLbl")
      //  tittle.text = MessageTitle
     //   content.text = MessagePostContent
        lbldate.text = dateLbl
        lblfirstlettwr.text = colorfullLbl
        name.text = nameLbl
    }
    

    @IBAction func backBtn(_ sender: Any) {
        if ViewController.isDashboardVC == true {

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CircularViewController")
        self.present(controller, animated: true, completion: nil)
    }
    

    else{
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "InboxVC")
        self.present(controller, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
}
