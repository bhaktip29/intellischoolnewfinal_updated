//
//  homeworkAttachmentViewController.swift
//  intelliSchoolNew
//
//  Created by Intellinects Venture on 10/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Nuke
class homeworkAttachmentViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    
    @IBOutlet weak var firstleterr: UILabel!
    @IBOutlet weak var postContentTxtView: UITextView!
    @IBOutlet weak var titleLbl: UILabel!
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    //  var params : [String : Any]!
    var programVar : Int!
    var datechange : String!
    var params : [String : Any]!
    var combinedstring : String!
    var myarrayofattachment = [String]()
    //   var selectedProgram1 : Int!
    var file_original_name : String!
    var newsList = [teacher]()
    var attachmentList = [attachment1]()
    var date : String!
    var urlString : URL!
    var imageUrlfordoc : URL!
    let images = ["news","events","inbox","attendance","timetable","lms","evaluation","payment","sports","bus","healthcare","publishing","discover","Assess","examtimetable","faq","terms","ticket"]
    let names = ["Assess","attendance","bus","discover","evaluation","events","examtimetable","faq","healthcare","inbox","lms","news","payment","publishing","sports","terms","ticket","timetable"]
    
    var index = 0
        override func viewDidLoad() {
            super.viewDidLoad()
            //  getEventData()
       //
            
            
             firstleterr.layer.cornerRadius =  firstleterr.frame.width/2
             firstleterr.layer.masksToBounds = true
             let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
             let color1 = UIColor(hexString: "#1862C6")
             statusBarView.backgroundColor = color1
             view.addSubview(statusBarView)
             
             var HomeworkTitle =
               UserDefaults.standard.string(forKey: "HomeworkTitle")
             print("HomeworkTitleindetails\(HomeworkTitle)")
             var HomeworkPostContent =
             UserDefaults.standard.string(forKey: "HomeworkPostContent")
             
            var index = UserDefaults.standard.integer(forKey: "indexfromhomework")
             print("indexindexdetails\(index)")
            var msgcontent = HomeworkPostContent
            
            
            print("msgcontentmsgcontent\(msgcontent)")
            var title = HomeworkTitle

             
             var homworkname =
             UserDefaults.standard.string(forKey: "homworkname")
             var Homeworkdate =
             UserDefaults.standard.string(forKey: "Homeworkdate")
             var Homeworkfirstletter =
             UserDefaults.standard.string(forKey: "Homeworkfirstletter")
             self.titleLbl.text = title
             self.postContentTxtView.text = msgcontent
             self.lbldate.text = Homeworkdate
             self.firstleterr.text = Homeworkfirstletter

             self.name.text = homworkname
            //
            
            
    //            if ViewController.isDashboardVC == true {
    //
    //            let storyboard = UIStoryboard(name: "Main", bundle: nil)
    //            let controller = storyboard.instantiateViewController(withIdentifier: "homewrokDashboardViewController")
    //            self.present(controller, animated: true, completion: nil)
    //        }
    //        else{
    //            let storyboard = UIStoryboard(name: "Main", bundle: nil)
    //            let controller = storyboard.instantiateViewController(withIdentifier: "InboxVC")
    //            self.present(controller, animated: true, completion: nil)
    //        }
            
            let flow = UICollectionViewFlowLayout()
            collectionView?.setCollectionViewLayout(flow, animated: false)

            collectionView.register(HomeWorkDetaCell.self, forCellWithReuseIdentifier: "HomeWorkDetaCell")
            
        //    getNewsData()
            //  self.title = "Homework"
            //     selectedProgram1 = UserDefaults.standard.integer(forKey: "selectedProgram")
            
            //      print("programVar\(selectedProgram1)")
            
            
            let a = "StackOverFlow"
            //let last4 = a.substringFrom(a.endIndex.advancedBy(-4))
            
            // Do any additional setup after loading the view.
        }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ListEmployee.shared.arrAttachments[index].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeWorkDetaCell", for: indexPath) as! HomeWorkDetaCell
        //        let userObjec = newsList[indexPath.row]
        


        Cell.layer.cornerRadius = 5.0
        Cell.clipsToBounds = true
        
        let obj = ListEmployee.shared.arrAttachments[index][indexPath.row]
        Cell.lblName.text = obj.fileOriginalName
        print("filenameee>>\(obj.fileOriginalName)")
        
        let urlString = obj.filepath + "/" + obj.fileOriginalName
        
        
        
        ///pdff button and download button disable
         var imageUrlfordoc = URL(string: obj.filepath + "/" + obj.fileOriginalName )
        print("clforrpwat \(imageUrlfordoc)")

//        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageUrlfordoc!.lastPathComponent)
//                     let checkValidation = FileManager.default
//
//                     if (checkValidation.fileExists(atPath: paths))
//                     {
//                        Cell.btnDownload.isEnabled = false
//                        Cell.btnDownload.alpha = 0.50
//                     }
//         else
//         {
//             print("noteefileexistt")
//
//         }
        
        
        //Nuke image adding
//                        let photoUrl = NSURL(string: userObjec.getfeaturedImage())
        
        
       let urlStringforimg =
                URL(string: obj.filepath + "/" + obj.fileOriginalName)
        let fileManager = FileManager.default
        //let url = urlString
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(urlStringforimg!.lastPathComponent)
            var extensionOfUrl = urlStringforimg!.pathExtension
            
                if extensionOfUrl == "img" || extensionOfUrl == "png" || extensionOfUrl == "jpg"
                {

                        let imgUrl = urlString.removingPercentEncoding
                        let options = ImageLoadingOptions(
                            placeholder: UIImage(named: "placeholder"),
                            transition: .fadeIn(duration: 0.33)
                        )
        Nuke.loadImage(with: URL(string: imgUrl!)!, options: options, into: Cell.imageview)

        }
        else
                {
                    Cell.imageview.isHidden = true
                    
                    
                    
                      Cell.imageview.translatesAutoresizingMaskIntoConstraints = false
                               
                                
                                
                                NSLayoutConstraint.activate([
                                    Cell.imageview.topAnchor.constraint(equalTo: Cell.topAnchor, constant: 10),
                                    Cell.lblName.topAnchor.constraint(equalTo: Cell.imageview.topAnchor, constant: 10),
                                    Cell.heightAnchor.constraint(equalToConstant: 120)
//                                    button.leadingAnchor.constraint(equalTo: Cell.viewBack.leadingAnchor, constant: 15),
//                                    button.trailingAnchor.constraint(equalTo: Cell.viewBack.trailingAnchor, constant: -15),
//                                    button.heightAnchor.constraint(equalToConstant: 30)
                                    
                                ])
        }
        //    if ListEmployee.shared.arrAttachments[53].count != 0 {
        var tempBtn = UIButton()
        
        Cell.btnOpen.tag = indexPath.row
        Cell.btnOpen.addTarget(self, action: #selector(open(sender:)), for: .touchUpInside)
        Cell.btnDownload.tag = indexPath.row

        Cell.btnDownload.addTarget(self, action: #selector(download(sender:)), for: .touchUpInside)

   

        return Cell
    }
    
    @objc func download(sender: UIButton){
        let buttonTag = sender.tag
        print("buttonTagbuttonTagdownload\(buttonTag)")
        let obj = ListEmployee.shared.arrAttachments[index][buttonTag]
        urlString = URL(string: obj.filepath + "/" + obj.fileOriginalName)

        guard let url = URL(string: obj.filepath + "/" + obj.fileOriginalName) else { return }
        
        let urlSession = URLSession(configuration: .default, delegate: self as! URLSessionDelegate, delegateQueue: OperationQueue())
        
        let downloadTask = urlSession.downloadTask(with: url)
        downloadTask.resume()
    }
    @objc func open(sender: UIButton){
        let buttonTag = sender.tag
        print("buttonTagbuttonTag\(buttonTag)")
        let obj = ListEmployee.shared.arrAttachments[index][buttonTag]
        
        urlString =
         URL(string: obj.filepath + "/" + obj.fileOriginalName)
   //     UserDefaults.standard.set(urlString, forKey: "urlString")
        
              let fileManager = FileManager.default
            let url = urlString
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(urlString.lastPathComponent)
        print("lastpathcomponenet\((url!.pathExtension))")
        var extensionOfUrl = url!.pathExtension
        let HomeworkImgViewController1 = HomeworkImgViewController()

            if extensionOfUrl == "pdf" || extensionOfUrl == "docx" || extensionOfUrl == "img" || extensionOfUrl == "png" || extensionOfUrl == "jpg" || extensionOfUrl == "jpeg" {
//            let checkValidation = FileManager.default
//
//         //   let destinationURL = documentsPath.appendingPathComponent(url.lastPathComponent)
//
//            if (checkValidation.fileExists(atPath: paths))
//            {
//            let pdfViewController = PDFViewController()
//            pdfViewController.pdfURL = self.urlString
//            present(pdfViewController, animated: false, completion: nil)
//        }
//            else{
//                print("please downloaddd file")
//            }
//        }
//
//                else if extensionOfUrl == "docx" {
                //Assuming there is a text file in the project named "home.txt".

                var imageUrlfordoc = obj.filepath + "/" + obj.fileOriginalName
                
                print("imageUrlfordocimageUrlfordoc1\(imageUrlfordoc)")
            //    HomeworkImgViewController1.docURL = self.imageUrlfordoc


               UserDefaults.standard.set(imageUrlfordoc, forKey: "imageUrlfordoc")
             //   var urluserdefault = UserDefaults.standard.value(forKey: "imageUrlfordoc")
              //  print("urluserdefault>>\(urluserdefault)")
             //   HomeworkImgViewController1.img.isHidden = true
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "PdfVC")
                self.present(controller, animated: true, completion: nil)
            }
            else
              {
             //   HomeworkImgViewController1.webview.isHidden = true
//               var imageUrl = obj.filepath + "/" + obj.fileOriginalName
//                UserDefaults.standard.set(imageUrl, forKey: "urlString")
//
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let controller = storyboard.instantiateViewController(withIdentifier: "HomeworkImgViewController")
//                self.present(controller, animated: true, completion: nil)
//
//                print("urlofImage\(urlString?.path)")
              }
        


    }
        
        //                if ListEmployee.shared.arrAttachments[buttonTag].count != 0 {
        //                    var tempBtn = UIButton()
        //                    for idx in 0..<ListEmployee.shared.arrAttachments[buttonTag].count {
        //
        //
        //                        // var ary = ListEmployee.shared.arrAttachments[indexPath.row]
        //                        var ary = ListEmployee.shared.arrAttachments[buttonTag][idx].fileOriginalName
        //                        print("aryary>>\(ary)")
        //                        myarrayofattachment.append(ary)
        //                        print("myarrayofattachmentmyarrayofattachment>>\(myarrayofattachment)")
        //                    }}
        //        let attachment = newsList[sender.tag].attachments
        //        print("attachment>>> \(attachment)")
        //        UserDefaults.standard.set(attachment, forKey: "attachment")
        //        print("buttonTagbuttonTag")
        //        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //        let controller = storyboard.instantiateViewController(withIdentifier: "HomeworkImgViewController")
        //        self.present(controller, animated: true, completion: nil)
        
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //         return CGSize(width: self.collectionView.frame.width/2, height: self.collectionView.frame.width/4)
        let bounds = collectionView.bounds
        
       return CGSize(width: bounds.width-30, height: 220)//
      //  return UICollectionViewFlowLayout.automaticSize //
        
    }
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        var cell = collectionView.cellForItem(at: indexPath) as! homeworkAttachmentCollectionViewCell
//
//        print("didselectmethod\(indexPath.row)")
//        // cell.pdfopen.tag = indexPath.row
//
//        //        var HomeworkTitle = cell.title.text
//        //          UserDefaults.standard.set(HomeworkTitle, forKey: "HomeworkTitle")
//        //        print("HomeworkTitle>>>>\(HomeworkTitle)")
//        //        var HomeworkPostContent = newsList[indexPath.row].msgContent
//        //        print("HomeworkPostContentHomeworkPostContent>>\(HomeworkPostContent)")
//        //        UserDefaults.standard.set(HomeworkPostContent, forKey: "HomeworkPostContent")
//        //        let indexfromhomework = indexPath.row
//        //        UserDefaults.standard.set(indexfromhomework, forKey: "indexfromhomework")
//        //        print("indexinhomeworkdashboard>>\(indexfromhomework)")
//        //
//        //
//        //        print("Cell.aatachmentsLbl.textCell.aatachmentsLbl.text\(newsList[indexPath.row].attachments)")
//        //        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        //        let controller = storyboard.instantiateViewController(withIdentifier: "HomeworkDetailsVC")
//        //        self.present(controller, animated: true, completion: nil)
//
//        //        if ListEmployee.shared.arrAttachments[indexPath.row].count != 0 {
//        //            var tempBtn = UIButton()
//        //            for idx in 0..<ListEmployee.shared.arrAttachments[indexPath.row].count {
//        //
//        //
//        //                // var ary = ListEmployee.shared.arrAttachments[indexPath.row]
//        //                var ary = ListEmployee.shared.arrAttachments[indexPath.row][idx].fileOriginalName
//        //                print("aryary>>\(ary)")
//        //                myarrayofattachment.append(ary)
//        //                print("myarrayofattachmentmyarrayofattachment>>\(myarrayofattachment)")
//
//        ///myarrayofattachment.insert(ary, at: 0)
//        print("")
//
//        //  let button2 = UIButton()
//
//
//
//
//
//        //}
//        //        }
//    }
    func collectionView(_ collectionView: UICollectionView, didDeSelectItemAt indexPath: IndexPath) {
        myarrayofattachment.removeAll()
        
    }
    
    

    
   // func getNewsData()
//    {
//        var url : String = ""
//
//        if Util.validateNetworkConnection(self){
//            // showActivityIndicatory(uiView: view)
//            let sendDate = "2015-01-01"
//            //            url =           "https://isirs.org/intellinect_dashboard/WS/index_main.php?action=GetMessage_Mayuri&SCHOOL_ID=8&deviceId=869447038367960&userId=266&role=Father"
//
//            url = "http://commn.intellischools.com/hmcs_angular_api/public/api/homeWorkList"
//            print("trueeee>>>\(ViewController.isParent)")
//            //                 if(UserDefaults.standard.string(forKey: "parent") == "0")
//            //
//            //                        {
//            //vikas employee
//            params = [
//                "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MTA1MTI2NywiZXhwIjoxNTcxMDU0ODY3LCJuYmYiOjE1NzEwNTEyNjcsImp0aSI6Im83SjNZR29VcWVwUzAzOEQiLCJzdWIiOjUyLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.z0C28PpiK0P4_3u8sexIU1nLJ6afwi5PNYqPmsPMREU" ,
//                "schoolCode" : "INTELL",
//                "is_academicYear" : "2019-2020",
//                ] as [String : Any]
//            //                        }
//            //
//            //                       else
//            //                    {
//            //
//            //
//            //                        let classdivid = UserDefaults.standard.value(forKey: "classdivId") as! String
//            //
//            //                        print("classdividclassdividclassdivid\(classdivid)")
//            //                        params = [
//            //                                     "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MTA1MTI2NywiZXhwIjoxNTcxMDU0ODY3LCJuYmYiOjE1NzEwNTEyNjcsImp0aSI6Im83SjNZR29VcWVwUzAzOEQiLCJzdWIiOjUyLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.z0C28PpiK0P4_3u8sexIU1nLJ6afwi5PNYqPmsPMREU" ,
//            //                                     "schoolCode" : "INTELL",
//            //                                     "is_academicYear" : "2019-2020",
//            //                                     "classDivId" : classdivid
//            //
//            //                                     ] as [String : Any]
//            //                    }
//            //
//
//            Alamofire.request(url,method: .post,parameters: params)
//                .responseJSON {[weak self] (response) in
//                    // self?.stopActivityIndicator()
//                    if response.result.error != nil {
//
//                    }
//                    print("response.reslt>>>>\(response.result)")
//                    switch response.result {
//                    case .success:
//                        if let jsonresponse = response.result.value {
//                            //                            let originalResponse = JSON(jsonresponse)
//                            //                            print("HomeworkresponseFromInbox>>>>\(originalResponse)")
//
//                            let data = jsonresponse as? [String: Any] ?? [:]
//                            let status = data["success"] as? Bool ?? false
//                            ListEmployee.shared.clear()
//                            if status {
//                                if let dataArr = data["data"] as? [[String: Any]] {
//                                    for val in dataArr {
//                                        ListEmployee.shared.parseData(val)
//                                    }
//                                }
//
//                            }
//                            //                            print("data>>>>\(data)")
//                            //                            let dataValues : [NSDictionary] = data.arrayObject as! [NSDictionary]
//                            //
//                            //                            print("dataValues>>>>\(dataValues)")
//                            //
//                            //
//                            //                            for item in dataValues {
//                            //
//                            //                                let fName = item.value(forKey: "fname")
//                            //                                let lname = item.value(forKey: "lname")
//                            //                                 let title = item.value(forKey: "title")
//                            //                                let createdDate = item.value(forKey: "createdDate")
//                            //                                let msgContent = item.value(forKey: "msgContent")
//                            //                                let image = item.value(forKey: "image")
//                            //                                var subjectName = item.value(forKey: "subjectName")
//                            //                                let attachments = item.value(forKey: "attachments")
//                            //                                // let date = item.value(forKey: "postDate")
//                            //                                // var changeddate = self!.changeDate(date as! String)
//                            //                                print("createdDate>>>>>\(createdDate)")
//                            //                                print("msgContent>>>> \(msgContent!)")
//                            //                                print("subjectName>>>> \(subjectName!)")
//                            //                                print("msgContent>>>> \(msgContent!)")
//                            //                                print("createdDate>>>>\(createdDate)")
//                            //                                print("attachments>>\(attachments)")
//                            //                                //print("android_version \(postDate!)")
//                            //
//                            //                                if subjectName is NSNull
//                            //                                {
//                            //                                    subjectName = ""
//                            //                                }
//                            //                             //   let filepathe22 = attachments!["filepath"].string
//                            //                             //   print("filepathe22filepathe22\(filepathe22)")
//                            //                             //   let data = originalResponse[attachments]
//                            //
//                            //                                let data1 = JSON(attachments)
//                            ////                                if attachments is NSNull
//                            //                                print("data1data1\(data1)")
//                            //                                if data1.isEmpty
//                            //                                {
//                            //                                   self!.combinedstring = ""
//                            //                                    self!.file_original_name = ""
//                            //                                    var atachmnetList2 = attachment1(attachments : self!.combinedstring as String)
//                            //                                    self?.attachmentList.append(atachmnetList2)
//                            //                                }
//                            //                                else
//                            //                                {
//                            //                                  //  print("data222555>>>>\(data1)")
//                            ////                                 let dataValues1 : [NSDictionary] = data1.arrayObject as! [NSDictionary]
//                            //
//                            //
//                            //                                let dataValues1: [NSDictionary]  = data1.arrayObject as! [NSDictionary]
//                            //                                   print("dataValues1dataValues1\(dataValues1)")
//                            //                                    ///here we get2
//                            //
//                            //                                    for item2 in dataValues1 {
//                            //
//                            //                                        let filepath5 = item2.value(forKey: "filepath") as! String
//                            //                                    print("filepathfilepath2222>>>>>>\(filepath5)")
//                            //                                        var filename = item2.value(forKey: "file_original_name") as! String
//                            //                                    print("file_original_namefile_original_name\(filename)")
//                            //                                        self!.combinedstring = "\(filepath5)//" + "\(filename)"
//                            //                                        self!.file_original_name = "\(filename)"
//                            //                                        print("combinedstringcombinedstring\(self!.combinedstring)")
//                            //                                        print("file_original_namefile_original_name\(self!.file_original_name)")
//                            //
//                            //                                        var atachmnetList2 = attachment1(attachments : self!.combinedstring as String)
//                            //                                        self?.attachmentList.append(atachmnetList2)
//                            //                                        print("attachmentList>>\(self!.attachmentList)")
//                            //                                    }}
//                            //
//                            //                                //self!.collectionView.reloadData()
//                            //                                //attachments
//                            //                                // date formatter
//                            //                                let dateFormatterGet = DateFormatter()
//                            //                                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
//                            //
//                            //                                let dateFormatterPrint = DateFormatter()
//                            //                                dateFormatterPrint.dateFormat = "dd MMM YYYY"
//                            //                                self!.date = (createdDate as! String)
//                            //                                if let date = dateFormatterGet.date(from: self!.date) {
//                            //                                    print("dateformaterrr>>>\(dateFormatterPrint.string(from: date))")
//                            //                                    self!.datechange = dateFormatterPrint.string(from: date)
//                            //                                    print("datechange\(self!.datechange)")
//                            //                                }
//                            //                                else
//                            //                                {
//                            //                                    print("There was an error decoding the string")
//                            //                                }
//                            //                                //  print("MMMMMMMMM\(MMM)") print("datechangedatechange\(self!.datechange)")
//                            //                                //
//                            //
//                            //                                //
//                            //                                //                                let formatter = ISO8601DateFormatter()
//                            //                                //                                let trimmedString = self!.datechange.replacingOccurrences(of: "\\s?UTC", with: "Z", options: .regularExpression)
//                            //                                //                                formatter.formatOptions = [.withFullDate, .withFullTime, .withSpaceBetweenDateAndTime]
//                            //                                //                                if let date = formatter.date(from: trimmedString) {
//                            //                                //                                    let components = Calendar.current.dateComponents([.day, .month], from: date)
//                            //                                //                                    let day = components.day!
//                            //                                //                                    let month = components.month!
//                            //                                //                                    print("day, month>>>\(day)\(month)")
//                            //                                //                                }
//                            //                                //
//                            //                                let teacherinfo = teacher(postDate:self?.datechange as! String?, msgContent:msgContent as! String?, fname:fName as! String?, lname:lname as! String?, title:title as! String? , subjectName: subjectName as? String ,attachments: (self!.combinedstring as String) , file_original_name : self!.file_original_name)
//                            //                                self!.newsList.append(teacherinfo)
//                            //                                print("newsListforhomework>>>>>>>>>>>>>>>\(self!.newsList)")
//                            //
//                            //
//                            //                                    }
//                            self?.collectionView.reloadData()
//                        }
//                    case .failure(_):
//                        print("switch case erroe")
//                    }
//            }
//        }
//
//
//
//    }
    
    @IBAction func backBtn(_ sender: Any) {
   
        if ViewController.isDashboardVC == true {

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "homewrokDashboardViewController")
        self.present(controller, animated: true, completion: nil)
    }
    else{
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "InboxVC")
        self.present(controller, animated: true, completion: nil)
    }
}
}
//extension UIButton {
//   func selectedButton(title:String, iconName: String, widthConstraints: NSLayoutConstraint){
//   self.backgroundColor = UIColor(red: 0, green: 118/255, blue: 254/255, alpha: 1)
//   self.setTitle(title, for: .normal)
//   self.setTitle(title, for: .highlighted)
//   self.setTitleColor(UIColor.white, for: .normal)
//   self.setTitleColor(UIColor.white, for: .highlighted)
//   self.setImage(UIImage(named: iconName), for: .normal)
//   self.setImage(UIImage(named: iconName), for: .highlighted)
//   let imageWidth = self.imageView!.frame.width
//    let textWidth = (title as NSString).size(withAttributes:[NSAttributedString.Key.font:self.titleLabel!.font!]).width
//   let width = textWidth + imageWidth + 24
//   //24 - the sum of your insets from left and right
//   widthConstraints.constant = width
//   self.layoutIfNeeded()
//   }
//}
extension homeworkAttachmentViewController:  URLSessionDownloadDelegate {
           func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
               print("downloadLocation:", location)
               // create destination URL with the original pdf name
               guard let url = downloadTask.originalRequest?.url else { return }
               let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
               let destinationURL = documentsPath.appendingPathComponent(url.lastPathComponent)
               print("documentsPath : \(documentsPath)")
               // delete original copy
               try? FileManager.default.removeItem(at: destinationURL)
               // copy from temp to Document
               do {
                   try FileManager.default.copyItem(at: location, to: destinationURL)
                   self.urlString = destinationURL
               } catch let error {
                   print("Copy Error: \(error.localizedDescription)")
               }
           }
       }
