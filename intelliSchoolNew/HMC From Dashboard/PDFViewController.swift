//
//  PDFViewController.swift
//  intelliSchoolNew
//
//  Created by Intellinects Venture on 16/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//


import UIKit
import PDFKit

class PDFViewController: UIViewController {

   var pdfView = PDFView()
        var pdfURL: URL!

        override func viewDidLoad() {
            super.viewDidLoad()
            let navBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
            view.addSubview(navBar)

            let navItem = UINavigationItem(title: "SomeTitle")
         //   let doneItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: nil, action: #selector(selectorName:))
      //      navItem.rightBarButtonItem = doneItem

            navBar.setItems([navItem], animated: false)
            view.addSubview(pdfView)
            
            if let document = PDFDocument(url: pdfURL) {
                pdfView.document = document
                print("document url : \(document)")
                print("document url2 : \(pdfURL)")

            }
            
            DispatchQueue.main.asyncAfter(deadline: .now()+100) {
                self.dismiss(animated: true, completion: nil)
            }
        }
        
        override func viewDidLayoutSubviews() {
            pdfView.frame = view.frame
            let newView = UIView()
            //2
             // let newView = UIView(frame:CGRect(x: 0, y: 0, width: 100, height: 500))
            //                     view.addSubview(newView)
            //                     pdfView.frame = newView.frame
            
            
           // view.addSubview(newView)
           // newView.addSubview(pdfView)
        //    pdfView.translatesAutoresizingMaskIntoConstraints = false

                    
                    NSLayoutConstraint.activate([
//                        newView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
//                        newView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
//                        newView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
//                        newView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
                        
                    
                        
                        
                       
                     //   pdfView.topAnchor.constraint(equalTo: newView.bottomAnchor, constant: 2),
                      //  pdfView.leadingAnchor.constraint(equalTo: newView.leadingAnchor, constant: 15),
                      //  btnDownload.trailingAnchor.constraint(equalTo: viewBack.trailingAnchor, constant: -15),
                    

                      //  pdfView.heightAnchor.constraint(equalToConstant: 30),
                      //  pdfView.widthAnchor.constraint(equalToConstant: 100),
                        
                        pdfView.topAnchor.constraint(equalTo: view.bottomAnchor, constant: 100),
                       // pdfView.leadingAnchor.constraint(equalTo: newView.leadingAnchor, constant: 200),
            //            btnOpen.trailingAnchor.constraint(equalTo: viewBack.trailingAnchor, constant: -15),
                        

                       // pdfView.heightAnchor.constraint(equalToConstant: 1000),
                       // pdfView.widthAnchor.constraint(equalToConstant: 500)


            //            lblDate.topAnchor.constraint(equalTo: lblName.bottomAnchor, constant: 0),
            //            lblDate.leadingAnchor.constraint(equalTo: dateCircleLbl.trailingAnchor, constant: 10),
            //            lblDate.trailingAnchor.constraint(equalTo: viewBack.trailingAnchor, constant: -15),
            //            lblDate.heightAnchor.constraint(equalToConstant: 20),
            //
            //            lblTitle.topAnchor.constraint(equalTo: dateCircleLbl.bottomAnchor, constant: 10),
            //            lblTitle.leadingAnchor.constraint(equalTo: viewBack.leadingAnchor, constant: 15),
            //            lblTitle.trailingAnchor.constraint(equalTo: viewBack.trailingAnchor, constant: -15),
            //            lblTitle.heightAnchor.constraint(equalToConstant: 20),
                    ])
            
        }
    
    @IBAction func backBtn(_ sender: Any) {
        if ViewController.isDashboardVC == true {
          let storyboard = UIStoryboard(name: "Main", bundle: nil)
          let controller = storyboard.instantiateViewController(withIdentifier: "homewrokDashboardViewController")
          self.present(controller, animated: true, completion: nil)
          }
          else{
              let storyboard = UIStoryboard(name: "Main", bundle: nil)
              let controller = storyboard.instantiateViewController(withIdentifier: "InboxVC")
              self.present(controller, animated: true, completion: nil)
          }
    }
    }
