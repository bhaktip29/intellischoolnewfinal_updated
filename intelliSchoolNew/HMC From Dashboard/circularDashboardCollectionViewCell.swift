//
//  circularDashboardCollectionViewCell.swift
//  intelliSchoolNew
//
//  Created by rupali  on 25/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class circularDashboardCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var postContentLbl: UILabel!
    @IBOutlet weak var colorfullLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
}
