//
//  SMSCollectionViewCell.swift
//  intelliSchoolNew
//
//  Created by Intellinects Venture on 26/11/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class SMSCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var randomColorsLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var contentLbl: UILabel!
}
