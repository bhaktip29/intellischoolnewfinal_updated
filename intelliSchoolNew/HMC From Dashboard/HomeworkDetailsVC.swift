//
//  HomeworkDetailsVC.swift
//  intelliSchoolNew
//
//  Created by Intellinects Venture on 26/11/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class HomeworkDetailsVC: UIViewController {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    
    @IBOutlet weak var firstleterr: UILabel!
    @IBOutlet weak var backBtn: UIBarButtonItem!
    @IBOutlet weak var postContentTxtView: UITextView!
    @IBOutlet weak var titleLbl: UILabel!
  
       var programVar : Int!
       var datechange : String!
       var params : [String : Any]!
       var combinedstring : String!
       var myarrayofattachment = [String]()
    //   var selectedProgram1 : Int!
       var file_original_name : String!
       var newsList = [teacher]()
       var attachmentList = [attachment1]()
       var date : String!
    override func viewDidLoad() {
        super.viewDidLoad()
         getNewsData()
        
        
        firstleterr.layer.cornerRadius =  firstleterr.frame.width/2
        firstleterr.layer.masksToBounds = true
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let color1 = UIColor(hexString: "#1862C6")
        statusBarView.backgroundColor = color1
        view.addSubview(statusBarView)
        
        var HomeworkTitle =
          UserDefaults.standard.string(forKey: "HomeworkTitle")
        print("HomeworkTitleindetails\(HomeworkTitle)")
        var HomeworkPostContent =
        UserDefaults.standard.string(forKey: "HomeworkPostContent")
        
       var index = UserDefaults.standard.integer(forKey: "indexfromhomework")
        print("indexindexdetails\(index)")
       var msgcontent = HomeworkPostContent
        var title = HomeworkTitle

        
        var homworkname =
        UserDefaults.standard.string(forKey: "homworkname")
        var Homeworkdate =
        UserDefaults.standard.string(forKey: "Homeworkdate")
        var Homeworkfirstletter =
        UserDefaults.standard.string(forKey: "Homeworkfirstletter")
        self.titleLbl.text = title
        self.postContentTxtView.text = msgcontent
        self.lbldate.text = Homeworkdate
        self.firstleterr.text = Homeworkfirstletter

        self.name.text = homworkname

    



    }
    
    @IBAction func backBtn(_ sender: Any) {
        if ViewController.isDashboardVC == true {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "homewrokDashboardViewController")
        self.present(controller, animated: true, completion: nil)
            let prefs = UserDefaults.standard
            prefs.removeObject(forKey: "HomeworkTitle")
            prefs.removeObject(forKey: "indexfromhomework")
        }
        else{
            let prefs = UserDefaults.standard
            prefs.removeObject(forKey: "HomeworkTitle")
            prefs.removeObject(forKey: "indexfromhomework")

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "InboxVC")
            self.present(controller, animated: true, completion: nil)
        }
        
    }
    
      func getNewsData()
        {
            var url : String = ""
            
            if Util.validateNetworkConnection(self){
               // showActivityIndicatory(uiView: view)
                let sendDate = "2015-01-01"
                //            url =           "https://isirs.org/intellinect_dashboard/WS/index_main.php?action=GetMessage_Mayuri&SCHOOL_ID=8&deviceId=869447038367960&userId=266&role=Father"
                
                url = "http://commn.intellischools.com/hmcs_angular_api/public/api/homeWorkList"
             print("trueeee>>>\(ViewController.isParent)")
             if(UserDefaults.standard.string(forKey: "parent") == "0")
              
                    {
                        //vikas employee
                        params = [
                                     "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MTA1MTI2NywiZXhwIjoxNTcxMDU0ODY3LCJuYmYiOjE1NzEwNTEyNjcsImp0aSI6Im83SjNZR29VcWVwUzAzOEQiLCJzdWIiOjUyLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.z0C28PpiK0P4_3u8sexIU1nLJ6afwi5PNYqPmsPMREU" ,
                                     "schoolCode" : "INTELL",
                                     "is_academicYear" : "2019-2020",
                                     ] as [String : Any]
                    }

                   else
                {
                    
                   
                    let classdivid = UserDefaults.standard.value(forKey: "classdivId") as! String

                    print("classdividclassdividclassdivid\(classdivid)")
                    params = [
                                 "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jb21tbi5pbnRlbGxpc2Nob29scy5jb21cL2htY3NfYW5ndWxhcl9hcGlcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU3MTA1MTI2NywiZXhwIjoxNTcxMDU0ODY3LCJuYmYiOjE1NzEwNTEyNjcsImp0aSI6Im83SjNZR29VcWVwUzAzOEQiLCJzdWIiOjUyLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.z0C28PpiK0P4_3u8sexIU1nLJ6afwi5PNYqPmsPMREU" ,
                                 "schoolCode" : "INTELL",
                                 "is_academicYear" : "2019-2020",
                                 "classDivId" : classdivid

                                 ] as [String : Any]
                }
                    
              
                Alamofire.request(url,method: .post,parameters: params)
                    .responseJSON {[weak self] (response) in
                       // self?.stopActivityIndicator()
                        if response.result.error != nil {
                            
                        }
                        print("response.reslt>>>>\(response.result)")
                        switch response.result {
                        case .success:
                            if let jsonresponse = response.result.value {
                                let originalResponse = JSON(jsonresponse)
                                print("HomeworkresponseFromInbox>>>>\(originalResponse)")
                                
                                let data = originalResponse["data"]
                                
                                print("data>>>>\(data)")
                                let dataValues : [NSDictionary] = data.arrayObject as! [NSDictionary]
                                
                                print("dataValues>>>>\(dataValues)")
                                
                                
                                for item in dataValues {
                                    
                                    let fName = item.value(forKey: "fname")
                                    let lname = item.value(forKey: "lname")
                                     let title = item.value(forKey: "title")
                                    let createdDate = item.value(forKey: "createdDate")
                                    let msgContent = item.value(forKey: "msgContent")
                                    let image = item.value(forKey: "image")
                                    var subjectName = item.value(forKey: "subjectName")
                                    let attachments = item.value(forKey: "attachments")
                                    // let date = item.value(forKey: "postDate")
                                    // var changeddate = self!.changeDate(date as! String)
                                    print("createdDate>>>>>\(createdDate)")
                                    print("msgContent>>>> \(msgContent!)")
                                    print("subjectName>>>> \(subjectName!)")
                                    print("msgContent>>>> \(msgContent!)")
                                    print("createdDate>>>>\(createdDate)")
                                    print("attachments>>\(attachments)")
                                    //print("android_version \(postDate!)")
                                    
                                    if subjectName is NSNull
                                    {
                                        subjectName = ""
                                    }
                                 //   let filepathe22 = attachments!["filepath"].string
                                 //   print("filepathe22filepathe22\(filepathe22)")
                                 //   let data = originalResponse[attachments]
                                    
                                    let data1 = JSON(attachments)
    //                                if attachments is NSNull
                                    print("data1data1\(data1)")
                                    if data1.isEmpty
                                    {
                                       self!.combinedstring = ""
                                        self!.file_original_name = ""
                                        var atachmnetList2 = attachment1(attachments : self!.combinedstring as String)
                                        self?.attachmentList.append(atachmnetList2)
                                    }
                                    else
                                    {
                                      //  print("data222555>>>>\(data1)")
    //                                 let dataValues1 : [NSDictionary] = data1.arrayObject as! [NSDictionary]
                                        
                                        
                                    let dataValues1: [NSDictionary]  = data1.arrayObject as! [NSDictionary]
                                       print("dataValues1dataValues1\(dataValues1)")
                                        ///here we get2
                                        
                                        for item2 in dataValues1 {
                                            
                                            let filepath5 = item2.value(forKey: "filepath") as! String
                                        print("filepathfilepath2222>>>>>>\(filepath5)")
                                            var filename = item2.value(forKey: "file_original_name") as! String
                                        print("file_original_namefile_original_name\(filename)")
                                            self!.combinedstring = "\(filepath5)//" + "\(filename)"
                                            self!.file_original_name = "\(filename)"
                                            print("combinedstringcombinedstring\(self!.combinedstring)")
                                            print("file_original_namefile_original_name\(self!.file_original_name)")
                                            
                                            var atachmnetList2 = attachment1(attachments : self!.combinedstring as String)
                                            self?.attachmentList.append(atachmnetList2)
                                            print("attachmentList>>\(self!.attachmentList)")
                                        }}
                                   
                                    //self!.collectionView.reloadData()
                                    //attachments
                                    // date formatter
                                    let dateFormatterGet = DateFormatter()
                                    dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                    
                                    let dateFormatterPrint = DateFormatter()
                                    dateFormatterPrint.dateFormat = "dd MMM YYYY"
                                    self!.date = (createdDate as! String)
                                    if let date = dateFormatterGet.date(from: self!.date) {
                                        print("dateformaterrr>>>\(dateFormatterPrint.string(from: date))")
                                        self!.datechange = dateFormatterPrint.string(from: date)
                                        print("datechange\(self!.datechange)")
                                    }
                                    else
                                    {
                                        print("There was an error decoding the string")
                                    }
                                    //  print("MMMMMMMMM\(MMM)") print("datechangedatechange\(self!.datechange)")
                                    //
                                    
                                    //
                                    //                                let formatter = ISO8601DateFormatter()
                                    //                                let trimmedString = self!.datechange.replacingOccurrences(of: "\\s?UTC", with: "Z", options: .regularExpression)
                                    //                                formatter.formatOptions = [.withFullDate, .withFullTime, .withSpaceBetweenDateAndTime]
                                    //                                if let date = formatter.date(from: trimmedString) {
                                    //                                    let components = Calendar.current.dateComponents([.day, .month], from: date)
                                    //                                    let day = components.day!
                                    //                                    let month = components.month!
                                    //                                    print("day, month>>>\(day)\(month)")
                                    //                                }
                                    //
                                    let teacherinfo = teacher(postDate:self?.datechange as! String?, msgContent:msgContent as! String?, fname:fName as! String?, lname:lname as! String?, title:title as! String? , subjectName: subjectName as? String ,attachments: (self!.combinedstring as String) , file_original_name : self!.file_original_name)
                                    self!.newsList.append(teacherinfo)
                                    print("newsListforhomework>>>>>>>>>>>>>>>\(self!.newsList)")
                                    
                                    
                                        }
                               // self?.collectionView.reloadData()
                            }
                        case .failure(_):
                            print("switch case erroe")
                        }
                }
            }
            
            
            
        }

}
