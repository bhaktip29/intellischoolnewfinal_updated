//
//  HomeworkDashboardCollectionViewCell.swift
//  intelliSchoolNew
//
//  Created by rupali  on 25/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class HomeworkDashboardCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var dateOnRandomColorsLbl: UILabel!
  
    @IBOutlet weak var attachmentBtn: UIButton!
    
    @IBOutlet weak var teacherName: UILabel!
    
    @IBOutlet weak var dateLBL: UILabel!
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var subjectLbl: UILabel!
    @IBOutlet weak var aatachmentsLbl: UILabel!
    @IBOutlet weak var postcontent: UILabel!

}
