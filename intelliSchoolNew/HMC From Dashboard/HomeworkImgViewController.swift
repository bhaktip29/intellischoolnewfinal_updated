//
//  HomeworkImgViewController.swift
//  intelliSchoolNew
//
//  Created by Intellinects Venture on 04/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Nuke
class HomeworkImgViewController: UIViewController {

    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var openImg: UIButton!
    @IBOutlet var downloadImg: UIView!
    override func viewDidLoad() {
        var attachment = UserDefaults.standard.string(forKey: "urlString")

        super.viewDidLoad()
        print("attachmentattachment\(attachment)")
        if attachment!.isEmpty
        {
            
        }else
        {
        let imgUrl = attachment?.removingPercentEncoding
                        let options = ImageLoadingOptions(
                            placeholder: UIImage(named: "placeholder"),
                            transition: .fadeIn(duration: 0.33)
                        )
        Nuke.loadImage(with: URL(string: imgUrl!)!, options: options, into: img)
        // Do any additional setup after loading the view.
    }
    }
    
    @IBAction func backBtn(_ sender: Any) {
        if ViewController.isDashboardVC == true {
          let storyboard = UIStoryboard(name: "Main", bundle: nil)
          let controller = storyboard.instantiateViewController(withIdentifier: "homewrokDashboardViewController")
          self.present(controller, animated: true, completion: nil)
          }
          else{
              let storyboard = UIStoryboard(name: "Main", bundle: nil)
              let controller = storyboard.instantiateViewController(withIdentifier: "InboxVC")
              self.present(controller, animated: true, completion: nil)
          }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
