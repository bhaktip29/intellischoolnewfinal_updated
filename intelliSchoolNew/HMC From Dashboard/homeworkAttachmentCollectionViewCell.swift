//
//  homeworkAttachmentCollectionViewCell.swift
//  intelliSchoolNew
//
//  Created by Intellinects Venture on 10/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class homeworkAttachmentCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lbl: UILabel!
    
    @IBOutlet weak var img: UIImageView!
}
