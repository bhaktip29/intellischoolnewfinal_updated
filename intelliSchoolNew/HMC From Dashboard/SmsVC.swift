//
//  SmsVC.swift
//  Intellinects
//
//  Created by rupali  on 09/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class SmsVC: BaseViewController , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    @IBOutlet weak var navigationBar: UINavigationBar!
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        InfoList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    var cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SMSCollectionViewCell", for: indexPath) as! SMSCollectionViewCell
        var userObjc =  InfoList[indexPath.row]
        cell.layer.cornerRadius = 5.0
        cell.clipsToBounds = true
        cell.nameLbl.text = userObjc.getfName()
        cell.dateLbl.text = ((userObjc.getpostDate()) + ("|") + (userObjc.getreceiver_group_type()))

        cell.contentLbl.text = userObjc.getpostContent()
        
        
        let a = userObjc.getfName()
        let firstLtter = String(a!.prefix(1))
        print("firstLtterfirstLtter\(firstLtter)")
        
        //
        cell.randomColorsLbl.text = firstLtter
        // Cell.firstLetterLbl.layer.cornerRadius = Cell.firstLetterLbl.frame.width/2
        cell.randomColorsLbl.layer.cornerRadius =  cell.randomColorsLbl.frame.width/2
        cell.randomColorsLbl.layer.masksToBounds = true
        
        //
        let color1 = UIColor(hexString: "#f6bf26")
        let color2 = UIColor(hexString: "#ba68c8")
        let color3 = UIColor(hexString: "#5e97f6")
        let color4 = UIColor(hexString: "#57bb8a")

        let bgColors = [color1,color2,color3,color4]
        
        
        cell.randomColorsLbl.backgroundColor = bgColors[indexPath.row % bgColors.count]
        return cell

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        //         return CGSize(width: self.collectionView.frame.width/2, height: self.collectionView.frame.width/4)
        let bounds = collectionView.bounds
        
        return CGSize(width: bounds.width-30, height: self.collectionView.frame.width/3)
        
    }
    var date : String?
    var InfoList = [SMS] ()
    var datechange : String!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
getsmsdata()
        let flow = UICollectionViewFlowLayout()
        if ViewController.isDashboardVC == true {
        flow.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
            let color1 = UIColor(hexString: "#F7F7F7")
            statusBarView.backgroundColor = color1
            view.addSubview(statusBarView)
        }
        else
        {
            let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
            let color1 = UIColor(hexString: "#F7F7F7")
            statusBarView.backgroundColor = color1
            view.addSubview(statusBarView)
            flow.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)

           // flow.minimumInteritemSpacing = 50
            flow.minimumLineSpacing = 10

            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
            navigationBar.isHidden = true
        flow.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 150, right: 0)
        }
        collectionView?.setCollectionViewLayout(flow, animated: false)
        // Do any additional setup after loading the view.
    }
    
func getsmsdata()
    {
        var url : String = ""
        
        if Util.validateNetworkConnection(self){
            showActivityIndicatory(uiView: view)
            let sendDate = "2015-01-01"
            //            url =           "https://isirs.org/intellinect_dashboard/WS/index_main.php?action=GetCircular&SCHOOL_ID=8&deviceId=869447038367960&userId=266&role=Father"
            url = "http://commn.intellischools.com/hmcs_angular_api/public/api/smsMainListing"
            let parameters = ["schoolCode" : UserDefaults.standard.value(forKey: "code")as! String,
                              "token" : UserDefaults.standard.value(forKey: "sessionToken") as! String,
                              "academicYear" :  UserDefaults.standard.value(forKey: "accademicYear") as! String,
      
                
                ] as [String:Any]
            Alamofire.request(url,method: .post,parameters: parameters)
                .responseJSON {[weak self] (response) in
                    self?.stopActivityIndicator()
                    if response.result.error != nil {
                        
                    }
                    print("response.reslt>>>>\(response.result)")
                    switch response.result {
                    case .success:
                        if let jsonresponse = response.result.value {
                            let originalResponse = JSON(jsonresponse)
                            print("SMSRESPONSE>>>>\(originalResponse)")
                            let data = originalResponse["data"]
                            let recordsArray: [NSDictionary] = data.arrayObject as! [NSDictionary]
                            
                            
                            print("recordsArray>>>>\(recordsArray)")
                            
                            
                            for item in recordsArray {
                                
                                
                                let postDate = item.value(forKey: "created_at")
                                let fName = item.value(forKey: "senderName")
                                let lName = item.value(forKey: "lname")
                              //  let title = item.value(forKey: "title")
                                let postContent = item.value(forKey: "message")
                                let postTitle = item.value(forKey: "title")
                                let teacher1 = item.value(forKey: "teacher")
                                let receiver_group_type = item.value(forKey: "receiver_group_type")
                                // let date = item.value(forKey: "postDate")
                                // var changeddate = self!.changeDate(date as! String)
                                print("SMSpostDate>>>>>\(postDate)")
                                print("SMSpostContent>>>> \(postContent!)")
                              //  print("SMStitle>>>>>\(title)")
                                print("SMSfName>>>>>\(fName)")
                                print("receiver_group_type\(receiver_group_type)")
                                
                                
                                //print("android_version \(postDate!)")
                                
                                
                                // date formatter
                                let dateFormatterGet = DateFormatter()
                                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                
                                let dateFormatterPrint = DateFormatter()
                                dateFormatterPrint.dateFormat = "dd MMM YYYY"
                                self!.date = (postDate as! String)
                                if let date = dateFormatterGet.date(from: self!.date!)
                                {
                                    
                                    //                                if let date = dateFormatterGet.date(from: "2016-02-29 12:24:26") {
                                    print("dateformaterrr>>>\(dateFormatterPrint.string(from: date))")
                                    self!.datechange = dateFormatterPrint.string(from: date)
                                }
                                else
                                {
                                    print("There was an error decoding the string")
                                }
                                
                                let Info = SMS(postDate: self!.datechange , postContent: postContent as! String , postTitle: postContent as! String, fName: fName as! String, lName: postContent as! String, receiver_group_type: receiver_group_type as! String)
                                self!.InfoList.append(Info)
                                print("newsListforhomework>>>>>>>>>>>>>>>\(self!.InfoList)")
                                
                                
                            }
                            self?.collectionView.reloadData()
                        }
                    case .failure(_):
                        print("switch case erroe")
                    }
            }
        }
        
        
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
