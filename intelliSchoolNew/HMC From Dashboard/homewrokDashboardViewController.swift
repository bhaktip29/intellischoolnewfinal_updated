//
//  homewrokDashboardViewController.swift
//  intelliSchoolNew
//
//  Created by rupali  on 25/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Nuke
class homewrokDashboardViewController:  BaseViewController,UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var collectionView: UICollectionView!
    var programVar : Int!
    var datechange : String!
    var params : [String : Any]!
    var combinedstring : String!
    var myarrayofattachment = [String]()
 //   var selectedProgram1 : Int!
    var file_original_name : String!
    var newsList = [teacher]()
    var attachmentList = [attachment1]()
    var date : String!
    
    let images = ["news","events","inbox","attendance","timetable","lms","evaluation","payment","sports","bus","healthcare","publishing","discover","Assess","examtimetable","faq","terms","ticket"]
    let names = ["Assess","attendance","bus","discover","evaluation","events","examtimetable","faq","healthcare","inbox","lms","news","payment","publishing","sports","terms","ticket","timetable"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //  getEventData()
        
        let flow = UICollectionViewFlowLayout()
        if ViewController.isDashboardVC == true {
            flow.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
            
            
            let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
            let color1 = UIColor(hexString: "#1862C6")
            statusBarView.backgroundColor = color1
            view.addSubview(statusBarView)
        }
        else
        {
            
            let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
            let color1 = UIColor(hexString: "#F7F7F7")
            statusBarView.backgroundColor = color1
            view.addSubview(statusBarView)
            
            // flow.minimumInteritemSpacing = 50
            flow.minimumLineSpacing = 10
            
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
            navigationBar.isHidden = true
            flow.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 150, right: 0)
        }
        
        collectionView.register(HomeWorkCell.self, forCellWithReuseIdentifier: "cell")
        
        collectionView?.setCollectionViewLayout(flow, animated: false)
//        collectionView?.setCollectionViewLayout(flow, animated: false)
        getNewsData()
        self.title = "Homework"
        //     selectedProgram1 = UserDefaults.standard.integer(forKey: "selectedProgram")
        
        //      print("programVar\(selectedProgram1)")
        
        
        let a = "StackOverFlow"
        //let last4 = a.substringFrom(a.endIndex.advancedBy(-4))
        
        // Do any additional setup after loading the view.
    }
      

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ListEmployee.shared.arrid.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! HomeWorkCell
//        let userObjec = newsList[indexPath.row]
        
       Cell.layer.cornerRadius = 5.0
       Cell.clipsToBounds = true
        Cell.lblTitle.text =  ListEmployee.shared.arrmsgContent[indexPath.row]
//        Cell.title.text = ListEmployee.shared.arrmsgContent[indexPath.row]

       // let string1 = ListEmployee.shared.arrfname[indexPath.row]
       // let string2 = ListEmployee.shared.arrlname[indexPath.row]
        Cell.lblName.text = ((ListEmployee.shared.arrfname[indexPath.row]) + (" ") + (ListEmployee.shared.arrlname[indexPath.row]))
                                       
        // date formatter
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM YYYY"
        self.date = (ListEmployee.shared.arrcreatedDate[indexPath.row] as! String)
        if let date = dateFormatterGet.date(from: self.date)
        {
            print("dateformaterrr>>>\(dateFormatterPrint.string(from: date))")
            self.datechange = dateFormatterPrint.string(from: date)
            print("datechange\(self.datechange)")
        }
        else
        {
            print("There was an error decoding the string")
        }
        
        
        Cell.lblDate.text = ((datechange) + ("|") + (ListEmployee.shared.arrsubjectName[indexPath.row]))
//        Cell.title.text = userObjec.gettitle()
//       // Cell.aatachmentsLbl.text = userObjec.getattachments()
//      //  Cell.pdfopen.place = userObjec.getattachments()
//      //  Cell.pdfopen.selectedButton(title: userObjec.getattachments(), iconName: "LMS", widthConstraints: .init())
//        Cell.attachmentBtn.tag = indexPath.row
//      // var attachment =  userObjec.getattachments() as! String
//      //  print("attachmentattachment\(attachment)")
//
//
//        Cell.attachmentBtn.addTarget(self, action: #selector(connected(sender:)), for: .touchUpInside)
//        let userObjec2 = attachmentList[indexPath.row]
//        var myarrayofattachment1 = userObjec2.getattachments()
//        myarrayofattachment.append(myarrayofattachment1!)
//        print("myarrayofattachment\(myarrayofattachment)")
        
//        if userObjec.getattachments().isEmpty
//        {
//
////        Cell.pdfopen.setTitle("",for: .normal)
////            Cell.pdfopen.setImage(UIImage(named: ""), for: .normal)
//            Cell.attachmentBtn.isHidden = true
//        }
//        else
//        {
//        for item in myarrayofattachment
//            {
//
//
//            let button = UIButton()
//            button.frame = CGRect(x: 20, y: 10, width: 300, height: 20)
//            button.backgroundColor = UIColor.green
//                button.setTitle(item, for: .normal)
//              //  let button2 = UIButton()
//              //  button2.frame = CGRect(x: 40, y: 20, width: 300, height: 20)
//              //  button2.backgroundColor = UIColor.red
//              //  button2.setTitle(item, for: .normal)
//         //   button.addTarget(self, action: "buttonAction:", forControlEvents: UIControl.Event.TouchUpInside)
//
//          //  let button = UIButton()
//          //    button.frame = CGRect(x: 20, y: 10, width: 300, height: 50)
//          //    button.setTitle("CustomButton", for: .normal)
//            Cell.addSubview(button)
//            //    Cell.addSubview(button2)
//
////            Cell.attachmentBtn.isHidden = false
////
////            Cell.attachmentBtn.setTitle(userObjec2.getattachments(),for: .normal)
//
//            }}
        
        Cell.viewBack.subviews.forEach{(aView) in
            if aView.isKind(of: UIButton.self){
                aView.removeFromSuperview()
        }}
        if ListEmployee.shared.arrAttachments[indexPath.row].count != 0 {
            var tempBtn = UIButton()
            for idx in 0..<ListEmployee.shared.arrAttachments[indexPath.row].count {
                let button = UIButton()
                button.tag = 100
                //button.frame = CGRect(x: 20, y: 10, width: 300, height: 20)
                button.addCustomBorder1()
                button.backgroundColor = UIColor.white
                button.contentHorizontalAlignment = .left
                button.translatesAutoresizingMaskIntoConstraints = false
                button.layer.cornerRadius = 8
                button.setImage(UIImage(named: "rsz_attachment_defult"), for: .normal)
                button.setTitle("  " + ListEmployee.shared.arrAttachments[indexPath.row][idx].fileOriginalName, for: .normal)
                button.setTitleColor(.black, for: .normal)
                button.tag = indexPath.row
                button.addTarget(self, action: #selector(connected(sender:)), for: .touchUpInside)
                
                
                // var ary = ListEmployee.shared.arrAttachments[indexPath.row]
                //                var ary = ListEmployee.shared.arrAttachments[indexPath.row][idx].fileOriginalName
                //                print("aryary>>\(ary)")
                //                myarrayofattachment.append(ary)
                //                print("myarrayofattachmentmyarrayofattachment>>\(myarrayofattachment)")
                
                ///myarrayofattachment.insert(ary, at: 0)
                print("")
                
                //  let button2 = UIButton()
                //  button2.frame = CGRect(x: 40, y: 20, width: 300, height: 20)
                //  button2.backgroundColor = UIColor.red
                //  button2.setTitle(item, for: .normal)
                //   button.addTarget(self, action: "buttonAction:", forControlEvents: UIControl.Event.TouchUpInside)
                
                //  let button = UIButton()
                //    button.frame = CGRect(x: 20, y: 10, width: 300, height: 50)
                //    button.setTitle("CustomButton", for: .normal)
                Cell.viewBack.addSubview(button)
                
                NSLayoutConstraint.activate([
                    
                    button.leadingAnchor.constraint(equalTo: Cell.viewBack.leadingAnchor, constant: 15),
                    button.trailingAnchor.constraint(equalTo: Cell.viewBack.trailingAnchor, constant: -15),
                    button.heightAnchor.constraint(equalToConstant: 30)
                    
                ])
                
                if idx == 0 {
                    button.topAnchor.constraint(equalTo: Cell.lblTitle.bottomAnchor, constant: 10).isActive = true
                    
                } else {
                    button.topAnchor.constraint(equalTo: tempBtn.bottomAnchor, constant: 10).isActive = true
                    
                }
                tempBtn = button
                
                if idx == ListEmployee.shared.arrAttachments[indexPath.row].count - 1 {
                    //                    button.bottomAnchor.constraint(equalTo: Cell.postcontent.bottomAnchor, constant:
                    //                        -10).isActive = true
                    
                }
            }
        } else {
            let btn = Cell.viewWithTag(100) as? UIButton
            btn?.isHidden = true
        }
                   
      //  Cell.subjectLbl.text = userObjec.getsubjectName()
      //  Cell.subjectLbl.addLeftBorderWithColor(color: .black, width: 1)
//        let a = userObjec.getfname()
//        let firstLtter = String(a!.prefix(1))
//        print("firstLtterfirstLtter\(firstLtter)")
//
//        //
//        let color1 = UIColor(hexString: "#f6bf26")
//        let color2 = UIColor(hexString: "#ba68c8")
//        let color3 = UIColor(hexString: "#5e97f6")
//        let color4 = UIColor(hexString: "#57bb8a")
//
//        let bgColors = [color1,color2,color3,color4]
//
//
//        Cell.dateOnRandomColorsLbl.backgroundColor = bgColors[indexPath.row % bgColors.count]
        //   Cell.dateOnRandomColorsLbl.text = ListEmployee.shared.arrfname[indexPath.row]//userObjec.getfname()

        let a = ListEmployee.shared.arrfname[indexPath.row]
        let firstLtter = String(a.first!)
        Cell.dateCircleLbl.text = firstLtter
                let color1 = UIColor(hexString: "#f6bf26")
                let color2 = UIColor(hexString: "#ba68c8")
                let color3 = UIColor(hexString: "#5e97f6")
                let color4 = UIColor(hexString: "#57bb8a")
        
                let bgColors = [color1,color2,color3,color4]
        
        
                Cell.dateCircleLbl.backgroundColor = bgColors[indexPath.row % bgColors.count]
//        Cell.dateCircleLbl.layer.cornerRadius =  Cell.dateCircleLbl.frame.width/2
//       Cell.dateCircleLbl.layer.masksToBounds = true
                //   Cell.dateOnRandomColorsLbl.text =
//         Cell.dateOnRandomColorsLbl.layer.cornerRadius = Cell.dateOnRandomColorsLbl.frame.width/2
//        Cell.dateOnRandomColorsLbl.layer.cornerRadius =  Cell.dateOnRandomColorsLbl.frame.width/2
//        Cell.dateOnRandomColorsLbl.layer.masksToBounds = true
//
//                    //    let photoUrl = NSURL(string: attachment)
//        if userObjec.getattachments().isEmpty
//            {
//           //     Cell.pdfopen.setTitle(imgUrl,for: .normal)
//                Cell.img.image = UIImage(named: "")
//
//        }
//        else
//        {
//
//                      //let pi = newsList[indexPath.row].attachments
//
//                        let imgUrl = userObjec.getattachments().removingPercentEncoding
//                        let options = ImageLoadingOptions(
//                            placeholder: UIImage(named: "placeholder"),
//                            transition: .fadeIn(duration: 0.33)
//                        )
//          //  Cell.attachmentBtn.setTitle(imgUrl,for: .normal)
//
//        Nuke.loadImage(with: URL(string: imgUrl!)!, options: options, into: Cell.img)
//        }
      //   Cell.img.image = userObjec.getimage()
        return Cell
    }
    @objc func connected(sender: UIButton){
        let buttonTag = sender.tag
        
//                if ListEmployee.shared.arrAttachments[buttonTag].count != 0 {
//                    var tempBtn = UIButton()
//                    for idx in 0..<ListEmployee.shared.arrAttachments[buttonTag].count {
//
//
//                        // var ary = ListEmployee.shared.arrAttachments[indexPath.row]
//                        var ary = ListEmployee.shared.arrAttachments[buttonTag][idx].fileOriginalName
//                        print("aryary>>\(ary)")
//                        myarrayofattachment.append(ary)
//                        print("myarrayofattachmentmyarrayofattachment>>\(myarrayofattachment)")
//                    }}
//        let attachment = newsList[sender.tag].attachments
//        print("attachment>>> \(attachment)")
//        UserDefaults.standard.set(attachment, forKey: "attachment")
//        print("buttonTagbuttonTag")
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let controller = storyboard.instantiateViewController(withIdentifier: "HomeworkImgViewController")
//        self.present(controller, animated: true, completion: nil)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
              let controller = storyboard.instantiateViewController(withIdentifier: "homeworkAttachmentViewController") as! homeworkAttachmentViewController
              controller.index = buttonTag
              self.present(controller, animated: true, completion: nil)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //         return CGSize(width: self.collectionView.frame.width/2, height: self.collectionView.frame.width/4)
        //          let bounds = collectionView.bounds
        //
        //        return CGSize(width: bounds.width-30, height: self.collectionView.frame.width/2)//UICollectionViewFlowLayout.automaticSize //
        
        if ListEmployee.shared.arrAttachments[indexPath.row].count != 0 {
            let attachMentHeight: CGFloat = CGFloat(ListEmployee.shared.arrAttachments[indexPath.row].count * 50)
            return CGSize(width: screenWidth, height: 125+attachMentHeight)
        } else {
            return CGSize(width: screenWidth, height: 125)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let cell = collectionView.cellForItem(at: indexPath) as! HomeWorkCell

//        vc.index = indexpath.row
        
//        print("didselectmethod\(indexPath.row)")
       // cell.pdfopen.tag = indexPath.row
        
        var HomeworkTitle =  ListEmployee.shared.arrtitle[indexPath.row]
          UserDefaults.standard.set(HomeworkTitle, forKey: "HomeworkTitle")
        print("HomeworkTitle>>>>\(HomeworkTitle)")
        var HomeworkPostContent = ListEmployee.shared.arrmsgContent[indexPath.row]
        print("HomeworkPostContentHomeworkPostContent>>\(HomeworkPostContent)")
        UserDefaults.standard.set(HomeworkPostContent, forKey: "HomeworkPostContent")
        var homworkname = ((ListEmployee.shared.arrfname[indexPath.row]) + (" ") + (ListEmployee.shared.arrlname[indexPath.row]))

        UserDefaults.standard.set(homworkname, forKey: "homworkname")
        
       
        // date formatter
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd MMM YYYY"
        self.date = (ListEmployee.shared.arrcreatedDate[indexPath.row] as! String)
        if let date = dateFormatterGet.date(from: self.date)
        {
            print("dateformaterrr>>>\(dateFormatterPrint.string(from: date))")
            self.datechange = dateFormatterPrint.string(from: date)
            print("datechange\(self.datechange)")
        }
        else
        {
            print("There was an error decoding the string")
        }
        
        
        var hmdate = ((datechange) + ("|") + (ListEmployee.shared.arrsubjectName[indexPath.row]))
        UserDefaults.standard.set(hmdate, forKey: "Homeworkdate")
        
        
        
        let a = ListEmployee.shared.arrfname[indexPath.row]
        let firstLtter = String(a.first!)
        UserDefaults.standard.set(firstLtter, forKey: "Homeworkfirstletter")

        //let indexfromhomework = indexPath.row
       // UserDefaults.standard.set(indexfromhomework, forKey: "indexfromhomework")
        //print("indexinhomeworkdashboard>>\(indexfromhomework)")
//
//
//        print("Cell.aatachmentsLbl.textCell.aatachmentsLbl.text\(newsList[indexPath.row].attachments)")

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "homeworkAttachmentViewController") as! homeworkAttachmentViewController
        controller.index = indexPath.row

     //   controller.index = indexPath.row
        self.present(controller, animated: true, completion: nil)

//        if ListEmployee.shared.arrAttachments[indexPath.row].count != 0 {
//            var tempBtn = UIButton()
//            for idx in 0..<ListEmployee.shared.arrAttachments[indexPath.row].count {
//
//
//                // var ary = ListEmployee.shared.arrAttachments[indexPath.row]
//                var ary = ListEmployee.shared.arrAttachments[indexPath.row][idx].fileOriginalName
//                print("aryary>>\(ary)")
//                myarrayofattachment.append(ary)
//                print("myarrayofattachmentmyarrayofattachment>>\(myarrayofattachment)")
                
                ///myarrayofattachment.insert(ary, at: 0)
                print("")
                
                     //  let button2 = UIButton()
            
                
            
        
        
   //}
//        }
    }
    func collectionView(_ collectionView: UICollectionView, didDeSelectItemAt indexPath: IndexPath) {
//        myarrayofattachment.removeAll()
        
    }

    


    func getNewsData()
    {
        var session =  UserDefaults.standard.value(forKey: "sessionToken")
          print("sesionsesionsesion\(session)")
     //  var code = UserDefaults.standard.value(forKey: "code")
        var code1 = UserDefaults.standard.value(forKey: "code")
                                   print("code1code1\(code1)")
    //    var code = UserDefaults.standard.string(forKey: "code")
      //  print("codecode\(code)")
        var accademicYear = UserDefaults.standard.value(forKey: "accademicYear")
        print("accademicYearaccademicYear\(accademicYear)")
        var url : String = ""
        
        if Util.validateNetworkConnection(self){
            showActivityIndicatory(uiView: view)
            let sendDate = "2015-01-01"
            //            url =           "https://isirs.org/intellinect_dashboard/WS/index_main.php?action=GetMessage_Mayuri&SCHOOL_ID=8&deviceId=869447038367960&userId=266&role=Father"
            
            url = "http://commn.intellischools.com/hmcs_angular_api/public/api/homeWorkList"
         print("trueeee>>>\(ViewController.isParent)")
         if(UserDefaults.standard.string(forKey: "parent") == "0")
          
                {
                    
                    //vikas employee
                    params = [
                                 "token" : UserDefaults.standard.value(forKey: "sessionToken") as! String ,
                                 "schoolCode" : UserDefaults.standard.value(forKey: "code")as! String,
                                 "is_academicYear" : UserDefaults.standard.value(forKey: "accademicYear") as! String,
                                 ] as [String : Any]
                }

               else
            {
                
               
                let classdivid = UserDefaults.standard.value(forKey: "classdivId") as! String

                print("classdividclassdividclassdivid\(classdivid)")
                params = [
                             "token" : UserDefaults.standard.value(forKey: "sessionToken") as! String ,
                             "schoolCode" : UserDefaults.standard.value(forKey: "code")as! String,
                             "is_academicYear" : UserDefaults.standard.value(forKey: "accademicYear") as! String,
                             "classDivId" : classdivid

                             ] as [String : Any]
            }
                
          
            Alamofire.request(url,method: .post,parameters: params)
                .responseJSON {[weak self] (response) in
                    self?.stopActivityIndicator()
                    if response.result.error != nil {
                        
                    }
                    print("response.reslt>>>>\(response.result)")
                    switch response.result {
                    case .success:
                        if let jsonresponse = response.result.value {
//                            let originalResponse = JSON(jsonresponse)
//                            print("HomeworkresponseFromInbox>>>>\(originalResponse)")
                            
                            let data = jsonresponse as? [String: Any] ?? [:]
                            let status = data["success"] as? Bool ?? false
                            ListEmployee.shared.clear()
                            if status {
                                if let dataArr = data["data"] as? [[String: Any]] {
                                    for val in dataArr {
                                        ListEmployee.shared.parseData(val)
                                    }
                                }

                            }
//                            print("data>>>>\(data)")
//                            let dataValues : [NSDictionary] = data.arrayObject as! [NSDictionary]
//
//                            print("dataValues>>>>\(dataValues)")
//
//
//                            for item in dataValues {
//
//                                let fName = item.value(forKey: "fname")
//                                let lname = item.value(forKey: "lname")
//                                 let title = item.value(forKey: "title")
//                                let createdDate = item.value(forKey: "createdDate")
//                                let msgContent = item.value(forKey: "msgContent")
//                                let image = item.value(forKey: "image")
//                                var subjectName = item.value(forKey: "subjectName")
//                                let attachments = item.value(forKey: "attachments")
//                                // let date = item.value(forKey: "postDate")
//                                // var changeddate = self!.changeDate(date as! String)
//                                print("createdDate>>>>>\(createdDate)")
//                                print("msgContent>>>> \(msgContent!)")
//                                print("subjectName>>>> \(subjectName!)")
//                                print("msgContent>>>> \(msgContent!)")
//                                print("createdDate>>>>\(createdDate)")
//                                print("attachments>>\(attachments)")
//                                //print("android_version \(postDate!)")
//
//                                if subjectName is NSNull
//                                {
//                                    subjectName = ""
//                                }
//                             //   let filepathe22 = attachments!["filepath"].string
//                             //   print("filepathe22filepathe22\(filepathe22)")
//                             //   let data = originalResponse[attachments]
//
//                                let data1 = JSON(attachments)
////                                if attachments is NSNull
//                                print("data1data1\(data1)")
//                                if data1.isEmpty
//                                {
//                                   self!.combinedstring = ""
//                                    self!.file_original_name = ""
//                                    var atachmnetList2 = attachment1(attachments : self!.combinedstring as String)
//                                    self?.attachmentList.append(atachmnetList2)
//                                }
//                                else
//                                {
//                                  //  print("data222555>>>>\(data1)")
////                                 let dataValues1 : [NSDictionary] = data1.arrayObject as! [NSDictionary]
//
//
//                                let dataValues1: [NSDictionary]  = data1.arrayObject as! [NSDictionary]
//                                   print("dataValues1dataValues1\(dataValues1)")
//                                    ///here we get2
//
//                                    for item2 in dataValues1 {
//
//                                        let filepath5 = item2.value(forKey: "filepath") as! String
//                                    print("filepathfilepath2222>>>>>>\(filepath5)")
//                                        var filename = item2.value(forKey: "file_original_name") as! String
//                                    print("file_original_namefile_original_name\(filename)")
//                                        self!.combinedstring = "\(filepath5)//" + "\(filename)"
//                                        self!.file_original_name = "\(filename)"
//                                        print("combinedstringcombinedstring\(self!.combinedstring)")
//                                        print("file_original_namefile_original_name\(self!.file_original_name)")
//
//                                        var atachmnetList2 = attachment1(attachments : self!.combinedstring as String)
//                                        self?.attachmentList.append(atachmnetList2)
//                                        print("attachmentList>>\(self!.attachmentList)")
//                                    }}
//
//                                //self!.collectionView.reloadData()
//                                //attachments
//                                // date formatter
//                                let dateFormatterGet = DateFormatter()
//                                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
//
//                                let dateFormatterPrint = DateFormatter()
//                                dateFormatterPrint.dateFormat = "dd MMM YYYY"
//                                self!.date = (createdDate as! String)
//                                if let date = dateFormatterGet.date(from: self!.date) {
//                                    print("dateformaterrr>>>\(dateFormatterPrint.string(from: date))")
//                                    self!.datechange = dateFormatterPrint.string(from: date)
//                                    print("datechange\(self!.datechange)")
//                                }
//                                else
//                                {
//                                    print("There was an error decoding the string")
//                                }
//                                //  print("MMMMMMMMM\(MMM)") print("datechangedatechange\(self!.datechange)")
//                                //
//
//                                //
//                                //                                let formatter = ISO8601DateFormatter()
//                                //                                let trimmedString = self!.datechange.replacingOccurrences(of: "\\s?UTC", with: "Z", options: .regularExpression)
//                                //                                formatter.formatOptions = [.withFullDate, .withFullTime, .withSpaceBetweenDateAndTime]
//                                //                                if let date = formatter.date(from: trimmedString) {
//                                //                                    let components = Calendar.current.dateComponents([.day, .month], from: date)
//                                //                                    let day = components.day!
//                                //                                    let month = components.month!
//                                //                                    print("day, month>>>\(day)\(month)")
//                                //                                }
//                                //
//                                let teacherinfo = teacher(postDate:self?.datechange as! String?, msgContent:msgContent as! String?, fname:fName as! String?, lname:lname as! String?, title:title as! String? , subjectName: subjectName as? String ,attachments: (self!.combinedstring as String) , file_original_name : self!.file_original_name)
//                                self!.newsList.append(teacherinfo)
//                                print("newsListforhomework>>>>>>>>>>>>>>>\(self!.newsList)")
//
//
//                                    }
                            self?.collectionView.reloadData()
                        }
                    case .failure(_):
                        print("switch case erroe")
                    }
            }
        }
        
        
        
    }
    
}
//extension UIButton {
//   func selectedButton(title:String, iconName: String, widthConstraints: NSLayoutConstraint){
//   self.backgroundColor = UIColor(red: 0, green: 118/255, blue: 254/255, alpha: 1)
//   self.setTitle(title, for: .normal)
//   self.setTitle(title, for: .highlighted)
//   self.setTitleColor(UIColor.white, for: .normal)
//   self.setTitleColor(UIColor.white, for: .highlighted)
//   self.setImage(UIImage(named: iconName), for: .normal)
//   self.setImage(UIImage(named: iconName), for: .highlighted)
//   let imageWidth = self.imageView!.frame.width
//    let textWidth = (title as NSString).size(withAttributes:[NSAttributedString.Key.font:self.titleLabel!.font!]).width
//   let width = textWidth + imageWidth + 24
//   //24 - the sum of your insets from left and right
//   widthConstraints.constant = width
//   self.layoutIfNeeded()
//   }
//}

let screenWidth = UIScreen.main.bounds.width
let screenHeight = UIScreen.main.bounds.height
extension UIButton {
    func addCustomBorder1() {
        self.setTitleColor(GlobleConstants.baseColor, for: .normal)
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 0.5
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
        self.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
    }
}
