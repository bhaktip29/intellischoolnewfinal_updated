//
//  DemoCell.swift
//  intelliSchoolNew
//
//  Created by Intellinects Venture on 11/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

class HomeWorkCell: UICollectionViewCell {
    
    let dateCircleLbl = UILabel()
    let lblName = UILabel()
    let lblDate = UILabel()
    let lblTitle = UILabel()
    let viewBack = UIView()

    var cellBottomAnchor: NSLayoutConstraint?

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupCell()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCell() {
        viewBack.backgroundColor = .white
        viewBack.layer.cornerRadius = 5
//        viewBack.layer.shadowOffset = CGSize(width: 0, height: 1.5)
//        viewBack.layer.shadowColor = UIColor.darkGray.cgColor
//        viewBack.layer.shadowOpacity = 0.5
//        viewBack.layer.shadowRadius = 3
        viewBack.layer.masksToBounds = true
        viewBack.layer.cornerRadius = 10

        viewBack.clipsToBounds = false
        viewBack.translatesAutoresizingMaskIntoConstraints = false
        addSubview(viewBack)
        
        
//        dateCircleLbl.backgroundColor = .white
        dateCircleLbl.textAlignment = .center
        dateCircleLbl.textColor = UIColor.black
      //   dateCircleLbl.layer.cornerRadius =  dateCircleLbl.frame.width/2
        dateCircleLbl.layer.masksToBounds = true
        dateCircleLbl.layer.cornerRadius = 25
        dateCircleLbl.translatesAutoresizingMaskIntoConstraints = false
        dateCircleLbl.textColor = UIColor.white

        viewBack.addSubview(dateCircleLbl)


//        lblPlus.font = UIFont.boldSystemFont(ofSize: TextSize.title * 2)
//        lblName.textAlignment = .center
        lblName.font = UIFont.boldSystemFont(ofSize: 15)
        lblName.textColor = UIColor.black
//        lblName.text = "Rupali Banker"
        lblName.translatesAutoresizingMaskIntoConstraints = false
        viewBack.addSubview(lblName)

        //lblDate.font = UIFont.boldSystemFont(ofSize: TextSize.title * 2)
//        lblDate.textAlignment = .center
        lblDate.textColor = UIColor.lightGray
//        lblDate.text = "20/Sep/2018 | English"
        lblDate.translatesAutoresizingMaskIntoConstraints = false
        viewBack.addSubview(lblDate)

        //lblTitle.font = UIFont.boldSystemFont(ofSize: TextSize.title * 2)
//        lblTitle.textAlignment = .center
        lblTitle.textColor = UIColor.black
//        lblTitle.text = "Title text should be here"
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        viewBack.addSubview(lblTitle)

//        cellBottomAnchor = lblTitle.bottomAnchor.constraint(equalTo: viewBack.bottomAnchor, constant: -10)
//        cellBottomAnchor?.isActive = true
        
        NSLayoutConstraint.activate([
            viewBack.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            viewBack.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            viewBack.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            viewBack.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            
            dateCircleLbl.topAnchor.constraint(equalTo: viewBack.topAnchor, constant: 15),
            dateCircleLbl.leadingAnchor.constraint(equalTo: viewBack.leadingAnchor, constant: 15),
            dateCircleLbl.widthAnchor.constraint(equalToConstant: 50),
            dateCircleLbl.heightAnchor.constraint(equalToConstant: 50),
            
            //dateCircleLbl.tintColor
            lblName.topAnchor.constraint(equalTo: viewBack.topAnchor, constant: 15),
            lblName.leadingAnchor.constraint(equalTo: dateCircleLbl.trailingAnchor, constant: 10),
            lblName.trailingAnchor.constraint(equalTo: viewBack.trailingAnchor, constant: -15),
            lblName.heightAnchor.constraint(equalToConstant: 20),

            lblDate.topAnchor.constraint(equalTo: lblName.bottomAnchor, constant: 0),
            lblDate.leadingAnchor.constraint(equalTo: dateCircleLbl.trailingAnchor, constant: 10),
            lblDate.trailingAnchor.constraint(equalTo: viewBack.trailingAnchor, constant: -15),
            lblDate.heightAnchor.constraint(equalToConstant: 20),

            lblTitle.topAnchor.constraint(equalTo: dateCircleLbl.bottomAnchor, constant: 10),
            lblTitle.leadingAnchor.constraint(equalTo: viewBack.leadingAnchor, constant: 15),
            lblTitle.trailingAnchor.constraint(equalTo: viewBack.trailingAnchor, constant: -15),
            lblTitle.heightAnchor.constraint(equalToConstant: 20),
        ])
    }
    
}


class HomeWorkDetaCell: UICollectionViewCell {
    
    let imageview = UIImageView()
    let lblName = UILabel()
    let lblDate = UILabel()
    let lblTitle = UILabel()
    let viewBack = UIView()
    let btnDownload = UIButton()
    let btnOpen = UIButton()

    var cellBottomAnchor: NSLayoutConstraint?

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupCell()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCell() {
        var backgroundColor = UIColor(hexString: "#f7f7f7")

        viewBack.backgroundColor = .white
        viewBack.layer.cornerRadius = 5
        viewBack.layer.shadowOffset = CGSize(width: 0, height: 1.5)
        viewBack.layer.shadowColor = UIColor.darkGray.cgColor
        viewBack.layer.shadowOpacity = 0.5
        viewBack.layer.shadowRadius = 3
        viewBack.layer.masksToBounds = false
        viewBack.clipsToBounds = false
        viewBack.translatesAutoresizingMaskIntoConstraints = false
        addSubview(viewBack)
        
        
//        dateCircleLbl.backgroundColor = .white
        imageview.backgroundColor = .lightGray
//        imageview.layer.cornerRadius = 25
        imageview.translatesAutoresizingMaskIntoConstraints = false
        viewBack.addSubview(imageview)

//        lblPlus.font = UIFont.boldSystemFont(ofSize: TextSize.title * 2)
//        lblName.textAlignment = .center
        lblName.textColor = UIColor.black
//        lblName.text = "Rupali Banker"
        lblName.translatesAutoresizingMaskIntoConstraints = false
        viewBack.addSubview(lblName)
        
      //  btnDownload.backgroundColor = UIColor.lightGray
        btnDownload.addCustomBorder1()

        btnDownload.contentHorizontalAlignment = .left
        btnDownload.layer.cornerRadius = 8
        btnDownload.backgroundColor = backgroundColor

        btnDownload.setTitle("Download", for: .normal)
      //  btnDownload.frame = CGRect(x: 100, y: 100, width: 300, height: 50)
//        button.setTitle("  " + ListEmployee.shared.arrAttachments[indexPath.row][idx].fileOriginalName, for: .normal)
        btnDownload.setTitleColor(.black, for: .normal)
        btnDownload.translatesAutoresizingMaskIntoConstraints = false
        btnDownload.contentHorizontalAlignment = .center
        viewBack.addSubview(btnDownload)

        btnOpen.addCustomBorder1()
        btnOpen.backgroundColor = backgroundColor
        btnOpen.contentHorizontalAlignment = .center
        btnOpen.layer.cornerRadius = 8
        btnOpen.setTitle("Open", for: .normal)
            //  btnDownload.frame = CGRect(x: 100, y: 100, width: 300height: 50)
        //      button.setTitle("  " + LtEmployee.shared.arrAttachments[indexPath.row][x].fileOriginalName, for: .normal)
        btnOpen.setTitleColor(.black, for: .normal)
        btnOpen.translatesAutoresizingMaskIntoConstraints = false
        viewBack.addSubview(btnOpen)
        //lblDate.font = UIFont.boldSystemFont(ofSize: TextSize.title * 2)
//        lblDate.textAlignment = .center
//        lblDate.textColor = UIColor.lightGray
////        lblDate.text = "20/Sep/2018 | English"
//        lblDate.translatesAutoresizingMaskIntoConstraints = false
//        viewBack.addSubview(lblDate)
//
//        //lblTitle.font = UIFont.boldSystemFont(ofSize: TextSize.title * 2)
////        lblTitle.textAlignment = .center
//        lblTitle.textColor = UIColor.lightGray
////        lblTitle.text = "Title text should be here"
//        lblTitle.translatesAutoresizingMaskIntoConstraints = false
//        viewBack.addSubview(lblTitle)

//        cellBottomAnchor = lblTitle.bottomAnchor.constraint(equalTo: viewBack.bottomAnchor, constant: -10)
//        cellBottomAnchor?.isActive = true
        
        NSLayoutConstraint.activate([
            viewBack.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            viewBack.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            viewBack.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            viewBack.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            
            imageview.topAnchor.constraint(equalTo: viewBack.topAnchor, constant: 35),
            imageview.leadingAnchor.constraint(equalTo: viewBack.leadingAnchor, constant: 15),
            imageview.trailingAnchor.constraint(equalTo: viewBack.trailingAnchor, constant: -15),
            imageview.heightAnchor.constraint(equalToConstant: 100),

            lblName.topAnchor.constraint(equalTo: imageview.bottomAnchor, constant: 2),
            lblName.leadingAnchor.constraint(equalTo: viewBack.leadingAnchor, constant: 15),
            lblName.trailingAnchor.constraint(equalTo: viewBack.trailingAnchor, constant: -15),
            lblName.heightAnchor.constraint(equalToConstant: 30),
            
            
           
            btnDownload.topAnchor.constraint(equalTo: lblName.bottomAnchor, constant: 2),
            btnDownload.leadingAnchor.constraint(equalTo: viewBack.leadingAnchor, constant: 15),
          //  btnDownload.trailingAnchor.constraint(equalTo: viewBack.trailingAnchor, constant: -15),
           


            btnDownload.heightAnchor.constraint(equalToConstant: 30),
            btnDownload.widthAnchor.constraint(equalToConstant: 150),
            
            btnOpen.topAnchor.constraint(equalTo: lblName.bottomAnchor, constant: 2),
            btnOpen.leadingAnchor.constraint(equalTo: viewBack.leadingAnchor, constant: 200),
//            btnOpen.trailingAnchor.constraint(equalTo: viewBack.trailingAnchor, constant: -15),
            

            btnOpen.heightAnchor.constraint(equalToConstant: 30),
            btnOpen.widthAnchor.constraint(equalToConstant: 150)


//            lblDate.topAnchor.constraint(equalTo: lblName.bottomAnchor, constant: 0),
//            lblDate.leadingAnchor.constraint(equalTo: dateCircleLbl.trailingAnchor, constant: 10),
//            lblDate.trailingAnchor.constraint(equalTo: viewBack.trailingAnchor, constant: -15),
//            lblDate.heightAnchor.constraint(equalToConstant: 20),
//
//            lblTitle.topAnchor.constraint(equalTo: dateCircleLbl.bottomAnchor, constant: 10),
//            lblTitle.leadingAnchor.constraint(equalTo: viewBack.leadingAnchor, constant: 15),
//            lblTitle.trailingAnchor.constraint(equalTo: viewBack.trailingAnchor, constant: -15),
//            lblTitle.heightAnchor.constraint(equalToConstant: 20),
        ])
    }
    
}

