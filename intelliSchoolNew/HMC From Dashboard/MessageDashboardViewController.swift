//
//  MessageDashboardViewController.swift
//  intelliSchoolNew
//
//  Created by rupali  on 25/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class MessageDashboardViewController: BaseViewController  , UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var collectionView: UICollectionView!
var programVar : Int!
 var datechange : String!
 var date : String!
 var selectedProgram1 : Int!
 var newsList = [Message1]()
    var Parameters : [String : Any]!
 let images = ["news","events","inbox","attendance","timetable","lms","evaluation","payment","sports","bus","healthcare","publishing","discover","Assess","examtimetable","faq","terms","ticket"]
     let names = ["Assess","attendance","bus","discover","evaluation","events","examtimetable","faq","healthcare","inbox","lms","news","payment","publishing","sports","terms","ticket","timetable"]
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return newsList.count
     }

     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MessageDashboardCollectionViewCell", for: indexPath) as! MessageDashboardCollectionViewCell
        
        Cell.layer.cornerRadius = 5.0
        Cell.clipsToBounds = true
        let userObjec = newsList[indexPath.row]
         Cell.nameLbl.text =  ((userObjec.getfName()) + (" ") + (userObjec.getlName()))
         Cell.dateLbl.text = userObjec.getpostDate()
         Cell.tittleLbl.text = userObjec.getpostTitle()
         Cell.postContent.text = userObjec.getpostContent()
         let a = userObjec.getfName()
         let firstLtter = String(a!.prefix(1))
         print("firstLtterfirstLtter\(firstLtter)")

         //
         let color1 = UIColor(hexString: "#f6bf26")
         let color2 = UIColor(hexString: "#ba68c8")
         let color3 = UIColor(hexString: "#5e97f6")
         let color4 = UIColor(hexString: "#57bb8a")

         let bgColors = [color1,color2,color3,color4]


         Cell.colorfullLbl.backgroundColor = bgColors[indexPath.row % bgColors.count]
         Cell.colorfullLbl.text = firstLtter
         // Cell.firstLetterLbl.layer.cornerRadius = Cell.firstLetterLbl.frame.width/2
         Cell.colorfullLbl.layer.cornerRadius =  Cell.colorfullLbl.frame.width/2
         Cell.colorfullLbl.layer.masksToBounds = true
         // Cell.img.image = userObjec.getimage()
         return Cell
     }
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

     //         return CGSize(width: self.collectionView.frame.width/2, height: self.collectionView.frame.width/4)
     let bounds = collectionView.bounds
     
     return CGSize(width: bounds.width-30, height: self.collectionView.frame.width/3)

 }
// 
 func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     var cell = collectionView.cellForItem(at: indexPath) as! MessageDashboardCollectionViewCell



    var MessageTitle = cell.tittleLbl.text
       UserDefaults.standard.set(MessageTitle, forKey: "MessageTitle")
    var MessagePostContent = cell.postContent.text
     UserDefaults.standard.set(MessagePostContent, forKey: "MessagePostContent")
 
    
    var nameLbl = cell.nameLbl.text
     UserDefaults.standard.set(nameLbl, forKey: "nameLbl")
    var dateLbl = cell.dateLbl.text
     UserDefaults.standard.set(dateLbl, forKey: "dateLbl")
    var colorfullLbl = cell.colorfullLbl.text
     UserDefaults.standard.set(colorfullLbl, forKey: "colorfullLbl")
    
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let controller = storyboard.instantiateViewController(withIdentifier: "MessageDetailsVC")
    self.present(controller, animated: true, completion: nil)
     
 }
   

       

     override func viewDidLoad() {
         super.viewDidLoad()
         let flow = UICollectionViewFlowLayout()
         if ViewController.isDashboardVC == true {
         flow.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
           let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
           let color1 = UIColor(hexString: "#1862C6")
           statusBarView.backgroundColor = color1
           view.addSubview(statusBarView)
         }
         
         else
         {
             let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
             let color1 = UIColor(hexString: "#F7F7F7")
             statusBarView.backgroundColor = color1
             view.addSubview(statusBarView)
             
            // flow.minimumInteritemSpacing = 50
             flow.minimumLineSpacing = 10

             collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
             navigationBar.isHidden = true
         flow.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 150, right: 0)
         }
         collectionView?.setCollectionViewLayout(flow, animated: false)

         getNewsData()



         let a = "StackOverFlow"
         //let last4 = a.substringFrom(a.endIndex.advancedBy(-4))

         // Do any additional setup after loading the view.
     }

     func getNewsData()
     {
         var url : String = ""

         if Util.validateNetworkConnection(self){
             showActivityIndicatory(uiView: view)
             let sendDate = "2015-01-01"

 //            url =           "https://isirs.org/intellinect_dashboard/WS/index_main.php?action=GetMessage_Mayuri&SCHOOL_ID=8&deviceId=869447038367960&userId=266&role=Father"
             url = "http://commn.intellischools.com/hmcs_angular_api/public/api/messageList"
            if(UserDefaults.standard.string(forKey: "parent") == "0")
            {
            Parameters = ["token" : UserDefaults.standard.value(forKey: "sessionToken") as! String ,
                         "schoolCode" : UserDefaults.standard.value(forKey: "code")as! String,
                         "is_academicYear" : UserDefaults.standard.value(forKey: "accademicYear") as! String
                         ]as [String : Any]
            }
            else{
                let viewcontrolerclassName = UserDefaults.standard.string(forKey: "className") as! String
                print("mesageviewcontrolerclassName >> \(viewcontrolerclassName)")
            Parameters = ["token" : UserDefaults.standard.value(forKey: "sessionToken") as! String ,
                          "schoolCode" : UserDefaults.standard.value(forKey: "code")as! String,
                          "is_academicYear" : UserDefaults.standard.value(forKey: "accademicYear") as! String,
                          "classDivId" : UserDefaults.standard.value(forKey: "classdivId") as! String
             ]as [String : Any]
                print("parametersparametersmessage\(Parameters)")

            }

             Alamofire.request(url,method: .post,parameters: Parameters )
                 .responseJSON {[weak self] (response) in
                     self?.stopActivityIndicator()
                     if response.result.error != nil {

                     }

                     print("response.reslt>>>>\(response.result)")
                     switch response.result {
                     case .success:
                         if let jsonresponse = response.result.value {
                             let originalResponse = JSON(jsonresponse)
                             print("Homeworkresponse1>>>>\(originalResponse)")
                             let data = originalResponse["data"]
                             let recordsArray: [NSDictionary] = data.arrayObject as! [NSDictionary]


                             print("recordsArray>>>>\(recordsArray)")


                             for item in recordsArray {

                                 let fname = item.value(forKey: "fname")
                                 let lname = item.value(forKey: "lname")

                                 let title = item.value(forKey: "title")
                                 let postDate = item.value(forKey: "createdDate")
                                 let postContent = item.value(forKey: "msgContent")
                                 let image = item.value(forKey: "image")

                                 print("MessagepostDate>>>>>\(postDate)")
                                 print("postContent>>>> \(postContent!)")
                                 print("title>>>> \(title!)")
                               //  print("teacher \(teacher1)")

                                 //print("android_version \(postDate!)")


                                 // date formatter
                                 let dateFormatterGet = DateFormatter()
                                 dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"

                                 let dateFormatterPrint = DateFormatter()
                                 dateFormatterPrint.dateFormat = "dd MMM YYYY"
                                 self!.date = (postDate as! String)
                                 if let date = dateFormatterGet.date(from: self!.date)
                                 {
                                     print("dateformaterrr>>>\(dateFormatterPrint.string(from: date))")
                                     self!.datechange = dateFormatterPrint.string(from: date)
                                     print("self!.datechange \(self!.datechange )")
                                 }
                                 else
                                 {
                                     print("There was an error decoding the string")
                                 }
                                 //
                                let teacherinfo = Message1(postDate: self?.datechange, postContent:postContent as? String, postTitle: title as? String, fName: fname as? String, lName: lname as? String)
                                 self!.newsList.append(teacherinfo)
                                 print("newsListforhomework>>>>>>>>>>>>>>>\(self!.newsList)")


                             }
                             self?.collectionView.reloadData()
                         }
                     case .failure(_):
                         print("switch case erroe")
                     }
             }
         }



     }

 }


