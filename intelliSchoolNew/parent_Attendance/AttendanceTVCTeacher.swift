//
//  AttendanceTVCTeacher.swift
//  intelliSchoolNew
//
//  Created by Intellinects Venture on 26/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AttendanceTVCTeacher: BaseViewControllerTeacher,UITableViewDelegate,UITableViewDataSource,DatePikerDelegate {
    
    var classList = [SchoolClassTeacher]()
    let cellId = "CellIdentifire"
    
    let bottomLabel : UILabel = {
        let lbl = UILabel()
        lbl.text = DisplayTextMsgs.calssDivisionSelectionText
        lbl.font = UIFont.boldSystemFont(ofSize: 13)
        lbl.backgroundColor = UIColor.lightGray
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.white
        return lbl
    }()
    
    let tableView : UITableView = {
        
        let tblView = UITableView()
        tblView.translatesAutoresizingMaskIntoConstraints = false
        tblView.separatorInset = UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 4)
        tblView.separatorInset = UIEdgeInsets.zero
        return tblView
    }()

    //MARK:- Custom SetupView Methods
    
    @objc func handleDisplayDate(sender: UIBarButtonItem) {
        let vc = DatePickerVC()
        vc.delegate = self
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChild(vc)
        view.addSubview(vc.view)
    }
    
    func handleWithSelectedDate(datePickerSelectedDate date: Date) {
        let vc = AttendanceListVCTeacher()
        vc.selectedDate = date
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func setUpTableView() {
        
        view.addSubview(tableView)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        // needed constarunts x,y,w,h
        tableView.topAnchor.constraint(equalTo:view.topAnchor, constant : 64).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: bottomLabel.topAnchor).isActive = true
    }
    
    private func setUpBottomLabel() {
        
        view.addSubview(bottomLabel)
        // needed constarunts x,y,w,h
        bottomLabel.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        if #available(iOS 11.0, *) {
            bottomLabel.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            bottomLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
        bottomLabel.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        bottomLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    //MARK:- View Controllers Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = ViewControllerTitlesTeacher.classOrDivison
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(handleSlide))
        tableView.cellLayoutMarginsFollowReadableWidth = false
        let userDefaults = UserDefaults.standard
        let employee_role_id = Int(userDefaults.string(forKey: "EMPLOYEE_ROLE_ID")!)
        
        if(employee_role_id == 1 || employee_role_id == 2 || employee_role_id == 4){
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: getFormattedDate(dateString: Date()), style: .plain, target: self, action: #selector(handleDisplayDate))
        }
        tableView.register(ClassCell.self, forCellReuseIdentifier: cellId)
        setUpBottomLabel()
        setUpTableView()
        getClassesAndDivisions()
    }
    @objc func handleSlide() {
           print("back button tapped..")
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
                  let controller = storyboard.instantiateViewController(withIdentifier: "vc")
                  self.present(controller, animated: true, completion: nil)
       }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
    }
    func dismissDatePicker(_ isPresent: Bool) {}

    // MARK: - TableView Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return classList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ClassCell
        let schoolClass:SchoolClassTeacher = classList[indexPath.row]
        cell.textLabel?.text = "Class \(schoolClass.className!) \(schoolClass.divisionName!)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let connstatus = UtilTeacher.checkConnection()
        if(!connstatus)
        {
            UtilTeacher.invokeAlertMethod("Network Error", strBody: "No Internet Connectivity , Please check internet connectivity!", delegate: nil,vcobj : self)
        }
        else {
            let studentAttendanceVC = ClassWiseStudentAttendanceVCTeacher()
            studentAttendanceVC.schoolClass = classList[indexPath.row]
            navigationController?.pushViewController(studentAttendanceVC, animated: true)
        }
    }
 
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    //MARK:- Server methods
    
    func getClassesAndDivisions()
    {
        if UtilTeacher.validateNetworkConnection(self){
            let userDefaults = UserDefaults.standard
            
            let parameters =
//                [
//                "school_db_settings_array": "{\n  \"enc_settings\" : \"Ss0Utq4vjomC6yWC7UQl6rmcExwRLuUbTLdm5GO3EthyOkAKJR57W%2B2f%2BiUaDs6Q%2Fe9rb%2Bw5u1ESmrVS4qr4JHzTrTB1yj53lGzCnMldOn4%3D\"\n}",
//                "school_employee_class_setting_array":"[\n\n]",
//                "action":"ClassListIOS",
//                "employee_id" : "INTELN1807SCH7782",
//                "employee_role_id": "5"]as [String : Any]
             
            ["school_db_settings_array": userDefaults.object(forKey: "SCHOOL_DB_SETTINGS") ?? "school_db_settings_array",
             "action": "ClassListIOS",
             "employee_id": userDefaults.string(forKey: "EMPLOYEE_ID"),
             "employee_role_id": UserDefaults.standard.integer(forKey: "EMPLOYEE_ROLE_ID"),
             "school_employee_class_setting_array": userDefaults.string(forKey: "SUBJECT_SETTINGS") ?? "subject_settings"
                ] as [String : Any]
            
            showActivityIndicatory(uiView: view)
            Alamofire.request(WebServiceUrls.attendanceService,method:.post,parameters: parameters)
                .responseJSON{ [weak self] (response)  in
                    print("parametersparametersAttendanceTVCTeacher>>>\(parameters)")
                    self?.stopActivityIndicator()
                    print("response AttendanceTVCTeacher>>\(response)")
                    if response.result.error != nil {
                        UtilTeacher.invokeAlertMethod("Failed!", strBody: "Error Occured While Processing Data ", delegate: nil,vcobj : self!)
                    }
                    if let jsonresponse = response.result.value {
                        
                        let originalResponse = JSON(jsonresponse)
                        self?.getSuccessResponse(originalResponse: originalResponse)
                    }
            }
        }
    }
    
    func getSuccessResponse(originalResponse : JSON)
    {
        if(originalResponse.count > 0)
        {
            for (key,subJson):(String, JSON) in originalResponse {
                
                if key == "response" {
                    UtilTeacher.invokeAlertMethod("Failed!", strBody: "No class details available..!!!", delegate: nil,vcobj : self)
                    break
                }else{
                    let schoolClass : SchoolClassTeacher = SchoolClassTeacher()
                    print("schoolClass.classID\(schoolClass.classID)")
                    schoolClass.classID = Int(subJson["class_id"].stringValue)!
                    schoolClass.boardID = Int(subJson["board_id"].stringValue)!
                    print("schoolClass.boardID\(schoolClass.boardID)")
                    schoolClass.divisionID = Int(subJson["division_id"].stringValue)!
                    print("schoolClass.divisionID\(schoolClass.divisionID)")
                    schoolClass.className = subJson["class_name"].stringValue
                    print(" schoolClass.className \( schoolClass.className )")
                    schoolClass.divisionName = subJson["division_title"].stringValue
                    print("schoolClass.divisionName\(schoolClass.divisionName)")
                    classList.append(schoolClass)
                }
            }
            tableView.reloadData()
            tableView.animateTable()
        }
    }
}
