//
//  GlobalConstantTeacher.swift
//  intelliSchoolNew
//
//  Created by Intellinects Venture on 26/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import Foundation
import UIKit

struct WebServiceUrls {
     static let baseUrl = "https://isirs.org/intellischools_teachers_app/"
    // employee registration check
    static let registrationUrl = "\(baseUrl)ws_nec/employee_device_registration.php"
    static let schoolListUrl  = "\(baseUrl)teachers_script/school_list.php?"
    static let registerUrl = "\(baseUrl)teachers_script/registration_api.php"
    static let loginService = "\(baseUrl)teachers_script/login_api.php?"
    static let logoutService = "\(baseUrl)teachers_script/logout_api.php?"
    static let circularService = "\(baseUrl)teachers_script/circulars_api.php?"
    static let attendanceService = "\(baseUrl)teachers_script/attendance_api.php?"
    static let messagesService = "\(baseUrl)teachers_script/messages_api.php?"
    static let homeworkService = "\(baseUrl)teachers_script/homework_api.php?"
    static let timeTableService = "\(baseUrl)teachers_script/ExamTimetable/index.php?"
    static let studentInfoService = "\(baseUrl)teachers_script/student_info_api.php?"
    static let multiSMSService = "\(baseUrl)teachers_script/sms_view_api.php?"
    static let uploadAttachamentService =  "\(baseUrl)teachers_script/upload_ios.php?"
    static let teacherProfileService = "\(baseUrl)teachers_script/teacher_profile_api.php?"
    static let journalService = "\(baseUrl)teachers_script/journal_view_api.php?"
    static let profileUpload = "\(baseUrl)teachers_script/upload_api.php?"
    static let schoolProfile = "\(baseUrl)teachers_script/school_profile.php?"
    static let admissionService = "\(baseUrl)teachers_script/cap/getAdmissionDetails.php?"
    static let ticketingLoginService = "http://faq.isirs.org/public/ticketingLogin"
    static let digitalSummary = "\(baseUrl)teachers_script/activity.php"
    static let neBaseUrl = "http://isirs.org/intellinect_dashboard/WS/index_main.php?"
    static let newsUrl = "\(neBaseUrl)action=news"
    static let eventsUrl = "\(neBaseUrl)action=events"
    
    static let morenews = "https://ischoolsystem.net/news"
}

struct DisplayTextMsgs {
    static let calssDivisionSelectionText = "   Please select at least one Class/Division above."
}

struct UploadCode {
    
    static let circularUploadCode = "CIRC"
    static let homeworkUploadCode = "HMWK"
    static let messagesUploadCode = "MSG"
}

struct PlaceHolderStrings {
    static let tfPlacehoder = "Enter Subject here..."
    static let tvPlaceHolder = "Enter your message here..."
}

struct BasicDetails {
    static let schoolId = "55"
    static let eventCategories = "8"
    static let newsCategories = "4,22,24,25"
    static let newsTableName = "SchoolNews"
    static let eventsTableName = "SchoolEvents"
}

struct DefaultKey {
    static let news = "NewsLastSync"
    static let events = "EventsLastSync"
}

struct ViewControllerTitlesTeacher {
    
    static let newsVC = "News"
    static let newsDetailVC = "News Details"
    static let classOrDivison = "Classes/Divisions"
    static let eventVC = "Events"
    static let attendenceVC = "Attendance"
    static let supportCenterVC = "Support Center"
}


struct Constants {
    
    struct DB {
        static let name = "intellinectsventuresapp.sqlite"
        static let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
    }
    struct School {
        static let schoolIDNewsCategories = "4,22,24,25"
        static let schoolIDEventCategories = "8"
    }
    
    struct EventGrid {
        static let start1 : UInt = 0x322435
        static let middle1: UInt = 0x023a47
        static let end1 : UInt = 0x4c3000
        static let start : UInt = 0x8a638f
        static let middle: UInt = 0x0bc1f0
        static let end : UInt = 0xffa200
        
    }
}

extension UIApplication {
    class func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
}
