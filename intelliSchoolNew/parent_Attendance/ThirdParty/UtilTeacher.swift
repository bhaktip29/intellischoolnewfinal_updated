//
//  UtilTeacher.swift
//  intelliSchoolNew
//
//  Created by Intellinects Venture on 26/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import Foundation
import Alamofire
import UIKit
import Contacts
import ContactsUI
import SwiftyJSON

class UtilTeacher{
    
    class func nsDateFromTS(_ forString:String,format2:String)-> String
    {
        let date = Date(timeIntervalSince1970: (Double(forString)!))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format2
        let str = dateFormatter.string(from: date)
        let date2 = dateFormatter.date(from: str)!.addingTimeInterval(60*60*24*1)
        let str2 = dateFormatter.string(from: date2)
        return str2
    }
    
    class func registerDeviceIDonServer(_ deviceID : String)
    {
        var DeviceTokenId : String = String()
        if let deviceToken = UserDefaults.standard.string(forKey: "SCHOOL_DEVICETOKEN")
        {
            DeviceTokenId=deviceToken
        }
        
        let parameters = [
            "school_id":UserDefaults.standard.string(forKey: "SCHOOL_ID"),
            "action":"DeviceRegister",
            "mobile_no":UserDefaults.standard.string(forKey: "MOBILE_NO"),
            "username":UserDefaults.standard.string(forKey: "USER_ID"),
            "device_id":UIDevice.current.identifierForVendor!.uuidString,
            "mobile_platform_id":"2",
            "device_token":deviceID
            ] as [String: AnyObject]
        
        Alamofire.request(WebServiceUrls.registrationUrl,method: .post, parameters: parameters )
            .responseJSON { (response) in
                UtilTeacher.LoaderEnd()
                if response.result.value != nil {
                    let defaults = UserDefaults.standard
                    defaults.set(DeviceTokenId, forKey: "SERVER_DEVICETOKEN")
                }
        }
    }

    class func getPath(_ fileName: String) -> String {
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileURL = documentsURL.appendingPathComponent(fileName)
        
        return fileURL.path
    }
    
    class func copyFile(_ fileName: NSString) {
        let dbPath: String = getPath(fileName as String)
        let fileManager = FileManager.default
        if !fileManager.fileExists(atPath: dbPath) {
            
            let documentsURL = Bundle.main.resourceURL
            let fromPath = documentsURL!.appendingPathComponent(fileName as String)
            do {
                try fileManager.copyItem(atPath: fromPath.path, toPath: dbPath)
            } catch let error1 as NSError {
               print(error1)
            }
        }
    }
    
    class func invokeAlertMethod(_ strTitle: NSString, strBody: NSString, delegate: AnyObject? ,vcobj : UIViewController ) {
        let alert = UIAlertController(title: strTitle as String, message:strBody as String, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
        
        let popOver = alert.popoverPresentationController
        popOver?.sourceView  = vcobj.view
        popOver?.permittedArrowDirections = UIPopoverArrowDirection.any
        
        vcobj.present(alert, animated: true){}
    }
    
    class func alert(_ title: NSString, message: NSString, controller : UIViewController ) {
        
        let alert = UIAlertController(title: title as String, message:message as String, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
        
        let popOver = alert.popoverPresentationController
        popOver?.sourceView  = controller.view
        popOver?.permittedArrowDirections = UIPopoverArrowDirection.any
        
        controller.present(alert, animated: true){}
    }
    
    class func checkConnection()->Bool {
       let status = ReachTeacher().connectionStatus()
        switch status {
        case .unknown, .offline:
            return false
        case .online(.wwan):
            return true
        case .online(.wiFi):
            return true
        }
    }
    
    
    class func validateNetworkConnection(_ with: AnyObject) -> Bool {

        if checkConnection() {
            return true
        } else
        {
            UtilTeacher.LoaderEnd()
            let alertController = UIAlertController(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
            {
                (result : UIAlertAction) -> Void in
            }
            alertController.addAction(okAction)
            let controller = with as? UIViewController
            controller?.present(alertController, animated: true, completion: nil)
        }
        return false
    }
    
    
    class func UIColorFromRGB(_ rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
    class func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    class func validatePhone(_ value: String) -> Bool {
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    class func LoaderSettings()
    {
        var config : SwiftLoader.Config = SwiftLoader.Config()
        config.size = 150
        config.backgroundColor = UIColor(red:0.03, green:0.82, blue:0.7, alpha:1)
        config.spinnerColor = UIColor(red:0.88, green:0.26, blue:0.18, alpha:1)
        config.titleTextColor = UIColor(red:0.88, green:0.26, blue:0.18, alpha:1)
        config.spinnerLineWidth = 2.0
        config.foregroundColor = UIColor.black
        config.foregroundAlpha = 0.5
        
        SwiftLoader.setConfig(config: config)
    }
    
    class func LoaderStart()
    {
        SwiftLoader.show(title: "Loading...", animated: true)
    }
    
    class func LoaderEnd()
    {
        SwiftLoader.hide()
    }

    class func dateConvert(_ format1:String,format2:String,datetoconvert:String)->String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format1
        let date = dateFormatter.date(from: datetoconvert)! as Date
        dateFormatter.dateFormat = format2
        return dateFormatter.string(from: date)
        
    }
    
    class func getNsDefaultValue(_ forKey:String)->String
    {
        var NsValue = String()
        if UserDefaults.standard.string(forKey: forKey) != nil
        {
            NsValue = UserDefaults.standard.string(forKey: forKey)!
        }
        return NsValue
    }
    
    class func setNsDefaultValue(_ forKey:String,value:String)
    {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: forKey)
        UserDefaults.standard.synchronize()
    }
    
    class func removeNsDefaultValue(_ forKey:String)
    {
        UserDefaults.standard.removeObject(forKey: forKey)
        UserDefaults.standard.synchronize()
    }
    
    class func dateFromTS(_ forString:String,format2:String)->String
    {
        let date = Date(timeIntervalSince1970: (Double(forString)!))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format2
        return dateFormatter.string(from: date)
    }
}
