//
//  AttendanceVC.swift
//  Intellinects
//
 
import UIKit
import FSCalendar
import SwiftyJSON
import Alamofire

class AttendanceVC: BaseViewController ,FSCalendarDelegate,FSCalendarDataSource,FSCalendarDelegateAppearance {
    
    var cmonth: String?
    var intellinectsId = ""
    var monthAssociatedValues = Dictionary<String, Any>()
    
    let colorCodeRepresentationText = ["Present","Absent","Sick Leave","Half Day","Extra-curricular","Holidays","Other","Weekly off","Atten. not taken"]
    
    let colorCodes = [UIColor.white,UIColor.red,UIColor.orange,UIColor.magenta,UIColor(red:0.12, green:0.44, blue:0.05, alpha:1.0),UIColor.green,UIColor.yellow,UIColor.blue.withAlphaComponent(0.3),UIColor.lightGray]

    let calendar: FSCalendar = {
        let cal = FSCalendar()
        cal.translatesAutoresizingMaskIntoConstraints = false
        cal.allowsSelection = false
        cal.appearance.caseOptions = [.headerUsesUpperCase,.weekdayUsesUpperCase]
        cal.scope = .month
        cal.backgroundColor = UIColor.lightGray.withAlphaComponent(0.3)
        return cal
    }()
    
    let colorView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
     
     
       private func setUpViews() {
            
            calendar.dataSource = self
            calendar.delegate = self
            
    //        let device = UIDevice.current.systemVersion
    //        let constant: CGFloat = device == "11.0" ? 86.0 : 64.0
            
            view.addSubview(calendar)
            // Needed Constraint
            if #available(iOS 11.0, *) {
                calendar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 0).isActive = true
            } else {
                calendar.topAnchor.constraint(equalTo: view.topAnchor,constant: 0).isActive = true
            }
            calendar.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
            calendar.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
            calendar.heightAnchor.constraint(equalTo: view.heightAnchor,multiplier:0.67,constant:-15).isActive = true
        }
        
        private func setUpColorCodeView() {
            
            view.addSubview(colorView)
            // Needed Constraint
            colorView.topAnchor.constraint(equalTo: calendar.bottomAnchor,constant: 8).isActive = true
            colorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            colorView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
            colorView.heightAnchor.constraint(equalTo: view.heightAnchor,multiplier:1/3).isActive = true
            
            var leftHeight: CGFloat = 0
            var rightHeight: CGFloat = 0
            
            for i in 0..<colorCodeRepresentationText.count {
                
                let v = ColorCodeView()
                v.translatesAutoresizingMaskIntoConstraints = false
                v.colorNameLbl.text = colorCodeRepresentationText[i]
                v.colorCodeView.backgroundColor = colorCodes[i]
                colorView.addSubview(v)
                
                if i % 2 == 0 {
                    v.leftAnchor.constraint(equalTo: colorView.leftAnchor).isActive = true
                    v.topAnchor.constraint(equalTo: colorView.topAnchor,constant:leftHeight).isActive = true
                    v.widthAnchor.constraint(equalTo: colorView.widthAnchor,multiplier:1/2).isActive = true
                    v.heightAnchor.constraint(equalToConstant: 20).isActive = true
                    leftHeight = leftHeight + 24
                    
                }else {
                    v.rightAnchor.constraint(equalTo: colorView.rightAnchor).isActive = true
                    v.topAnchor.constraint(equalTo: colorView.topAnchor,constant:rightHeight).isActive = true
                    v.widthAnchor.constraint(equalTo: colorView.widthAnchor,multiplier:1/2).isActive = true
                    v.heightAnchor.constraint(equalToConstant: 20).isActive = true
                    rightHeight = rightHeight + 24
                }
            }
        }
        
    
    
        
        func setUpCalenderView(_ cmonth: String?) {
            let components = Calendar.current.dateComponents([.day , .month , .year], from:Date())
            guard let yr = components.year, let mnth = cmonth else {
                return
            }
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = "yyyy-MMM-dd"
            guard let dd = dateFormatter.date(from: "\(yr)-\(mnth)-02") else {return}
            calendar.setCurrentPage(dd, animated: true)
            calendar.appearance.borderRadius =  20
            calendar.appearance.borderDefaultColor = UIColor.gray
            calendar.appearance.titleTodayColor = UIColor.black
        }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
          navigationController?.navigationBar.barTintColor = UIColor(red: 25.0/255.0, green: 89.0/255.0, blue: 194.0/255.0, alpha: 1.0)

                 self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
          navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(handleSlide))
          navigationItem.leftBarButtonItem?.tintColor = .white //GlobleConstants.navigationBarColorTint
        //  navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(handleSlide1))
          navigationItem.rightBarButtonItem?.tintColor = .white

    
    //    self.navigationItem.title = "Daily"
        
//        self.title = ViewControllerTitles.attendenceVC
        let view = UIView(frame: UIScreen.main.bounds) 
        view.backgroundColor = UIColor.groupTableViewBackground
        self.view = view
        
        setUpViews()
        calendar.register(FSCalendarCell.self, forCellReuseIdentifier: "cell")
        getData()
    }
    @objc func handleSlide() {
               print("back button tapped..")
               let storyboard = UIStoryboard(name: "Main", bundle: nil)
                      let controller = storyboard.instantiateViewController(withIdentifier: "vc")
                      self.present(controller, animated: true, completion: nil)
           }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpColorCodeView()
        setUpCalenderView(cmonth)
        
    }
    
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell = calendar.dequeueReusableCell(withIdentifier: "cell", for: date, at: position)
        if position == .previous || position == .next {
            cell.isHidden = true
        }else {
            cell.isHidden = false
        }
        return cell
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        
        var returnColor = UIColor.clear

        if monthAssociatedValues.count > 0 {
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = "yyyy-M-dd"
            
            let dd = dateFormatter.string(from: date).components(separatedBy: "-")
            var days = [String:Any]()
            for(key,subJson):(String, Any) in monthAssociatedValues {
                if key == dd[1] {
                    days = JSON(subJson).dictionaryValue
                }
            }
            
            var day = dd[2]
            if day.hasPrefix("0") {
                day =  day.replacingOccurrences(of: "0", with: "")
            }
            
            let value = JSON(days)["\(day)"].stringValue
            
            if(value == "1"){
                returnColor = UIColor.lightGray
            }
            else if(value == "W")
            {
                returnColor = UIColor.blue.withAlphaComponent(0.3)
            }
            else if(value=="A")
            {
                returnColor = UIColor.red
            }
            else if(value=="E")
            {
                returnColor = UIColor(red:0.12, green:0.44, blue:0.05, alpha:1.0)
            }
            else if(value=="S")
            {
                returnColor = UIColor.orange
            }
            else if(value=="HD")
            {
                returnColor = UIColor.magenta
            }
            else if(value=="H")
            {
                returnColor = UIColor.green
            }
            else if(value=="O")
            {
                returnColor = UIColor.yellow
            }
            else if(value=="P")
            {
                returnColor = UIColor.white
            }
        }
        return returnColor
    }
    
    func getData(){
        
        if Util.validateNetworkConnection(self) {
            showActivityIndicatory(uiView: view)
            
            let parameters = [
                "action":"getDailyAttendance",
                "userId": UserDefaults.standard.string(forKey: "\(SDefaultKeys.studentId)\(UserDefaults.standard.integer(forKey: SDefaultKeys.selectedStudent))")?.capitalized ?? "",
                "role": UserDefaults.standard.string(forKey: SDefaultKeys.role) ?? "",
                "student_id": UserDefaults.standard.string(forKey: "\(SDefaultKeys.studentId)\(UserDefaults.standard.integer(forKey: SDefaultKeys.selectedStudent))")?.capitalized ?? "",
                "type":"daily",
                "boxcarToken":"".getUDIDCode(),
                "deviceId": "".getUDIDCode(),
                "SCHOOL_ID":  GlobleConstants.schoolID,
                "alias": "\("".getUDIDCode())-\(GlobleConstants.schoolID)"
            ] as [String : Any]
            
//            let parameters = [
//                "action": "getDailyAttendance", "alias": "01FACCC7-FE5C-45DA-B94B-120D99366A34-8", "student_id": "883", "role": "Mother", "userId": "883", "type": "daily", "boxcarToken": "01FACCC7-FE5C-45DA-B94B-120D99366A34", "deviceId": "01FACCC7-FE5C-45DA-B94B-120D99366A34", "SCHOOL_ID": "8"
//            ] as [String : Any]
            
            
            
            Alamofire.request(ParentsWebUrls.baseUrl,method: .get,parameters: parameters)
                .responseJSON { [weak self] (response) in
                self?.stopActivityIndicator()
                    print("attendance res:>>>",response.result.value as Any)
                if let jsonresponse = response.result.value {
                    
                    let originalResponse = JSON(jsonresponse)
                    self?.getSuccessResa(originalResponse: originalResponse)
                    DispatchQueue.main.async {
                        self?.calendar.reloadData()
                        self?.calendar.appearance.titleTodayColor = UIColor.black
                    }
                }
            }
        }
    }
    
    func getSuccessResa(originalResponse : JSON)
    {
        if(originalResponse.count > 0)
        {
            for dic in originalResponse["daily_attendance"].arrayValue {
                let newTempDic = dic["daily_details"].dictionaryValue
                var monthStr = dic["month"].stringValue
                if monthStr.hasPrefix("0") {
                    monthStr =  monthStr.replacingOccurrences(of: "0", with: "")
                }
                monthAssociatedValues[monthStr] = newTempDic
            }
        }
    }
    

}
    
    class ColorCodeView : UIView {
    
    let colorCodeView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.layer.cornerRadius = 10
        v.layer.masksToBounds = true
        return v
    }()
    
    let colorNameLbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpViews() {
        
        self.addSubview(colorCodeView)
        // Needed constarints x,y,w,h
        colorCodeView.leftAnchor.constraint(equalTo: self.leftAnchor,constant:8).isActive = true
        colorCodeView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        colorCodeView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        colorCodeView.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        
        self.addSubview(colorNameLbl)
        // Needed constarints x,y,w,h
        colorNameLbl.leftAnchor.constraint(equalTo: colorCodeView.rightAnchor,constant:8).isActive = true
        colorNameLbl.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    colorNameLbl.rightAnchor.constraint(equalTo:self.rightAnchor,constant:-8).isActive = true
        colorNameLbl.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
    }
}

