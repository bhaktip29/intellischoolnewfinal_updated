//
//  modelClassesTeacher.swift
//  intelliSchoolNew
//
//  Created by Intellinects Venture on 26/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import Foundation
// Not required classes
class StudentList: NSObject {
    
    var studentName: String = String()
    var studentId: Int = 0
    var studentRollNo: Int = 0
    var intellinectId: String = String()
    var status: String = String()
    var statusColor: String = String()
    
}

class MessageList: NSObject {
    var messageID: NSInteger = NSInteger()
    var messageDate: String = String()
    var messageClass: String = String()
    var messageDiv: String = String()
    var messageSubject: String = String()
    var message: String = String()
    var attachmentUrl: String = String()
}

class ClassList: NSObject {
    var classID: Int = 0
    var divID: Int = 0
    var boardID: Int = 0
    var className: String = String()
    var divName: String = String()
    var isChecked: Bool = false
}
/////////////////////////

class DigitalEngmnt
{
    var month:String?
    var homeworkCunt:String?
    var journalCount: String?
    var msgCount: String?
    var smsCount: String?
}

class SchoolClassTeacher: NSObject {
    
    var classID: Int = 0
    var divisionID: Int = 0
    var boardID: Int = 0
    var className: String?
    var divisionName: String?
    var isChecked: Bool = false
}

class SubjectList: NSObject {
    var subjectName: String = String()
    var subjectId: String = String()
}

class ClassWeekDaysTimeTableTeacher: NSObject {
    
    var period: String?
    var time : String?
    var totime : String?
    var monday : String?
    var tuesday : String?
    var wednesday : String?
    var thursday : String?
    var friday : String?
    var saturday : String?
    var sunday : String?
}

class StudentTeacher: NSObject {
    
    var studentName:String?
    var studentId: Int = 0
    var studentRollNo: Int = 0
    var intellinectId:String?
    var status: String?
    var statusColor: String?
    var isChecked: Bool = false
}

class ExamTimeTableTeacher: NSObject {
    
    var date : String?
    var time : String?
    var subject : String?
    var type : String?
    var syllabus : String?
}

class PostInfoTeacher: NSObject {
    var ID: Int = 0
    var sectionId: String = String()
    var Post_Title: String = String()
    var Post_Status: String = String()
    var Post_date: String = String()
    var Post_Content: String = String()
    var Post_Type: String = String()
    var date = Date()
    var Event_StartDate: String = String()
    var Event_EndDate: String = String()
    var News_LastModifiedDate: String = String()
    var venue: String = String()
    var contactno: String = String()
    var contactPerson: String = String()
    var allDay :Int = 0
    var postArr : [String] = []
    var PostType: String = String()
    var PostRepeatDate: String = String()
    var SchoolID: String = String()
}

//MARK:- Attendance Model Classes

class AttendanceClassList:NSObject {
    var presentStudent:String?
    var absentStudent:String?
    var totalStudent:String?
    var className :String?
    var classid:String?
    var division_id:String?
    var board:String?
    var division_name:String?
}

class AttendanceGroup: NSObject {
    var groupAbsentStudent:String?
    var classGroupName:String?
    var groupTotalStudent:String?
    var groupPresentStudent:String?
    var classGroupId:String?
}

//MARK:- Teachers Model Classes

class TeacherGroup: NSObject {
    var groupId : Int = 0
    var groupName: String?
}

class Staff: NSObject {
    var firstName: String?
    var lastName: String?
    var divisionTitle: String?
    var className: String?
    var employeeId: String?
}

class TeacherInfo : NSObject {
    var className: String?
    var mobileNumber:String?
    var profileImgUrl:String?
    var lastName:String?
    var firstName:String?
    var divisionTitle:String?
    var dateOfBirth:String?
    var emailId:String?
    var dateOfJoining:String?
    var teacherQualification:String?
    var address:String?
    var role:String?
    var gender:String?
    var lastDateOfService:String?
    var numberofYearExperience:String?
}

//MARK:- Student Profile Model Classes

class StudentInformation:NSObject {
    
    var grNumber:String?
    var fatherName:String?
    var fatherMobile:String?
    var address:String?
    var mother:String?
    var studentAdharNumber:String?
    var dateOfBirth:String?
    var udiseNo:String?
    var board:String?
    var motherMobile:String?
    var motherName:String?
    var bloodgroup:String?
    var profileImgUrl:String?
}

//MARK:- Messages View Controllers

//class Message: NSObject {
//    var messageID: NSInteger = NSInteger()
//    var messageDate: String = String()
//    var messageClass: String = String()
//    var messageDiv: String = String()
//    var messageSubject: String = String()
//    var message: String = String()
//    var attachmentUrl = [String]()
//}
