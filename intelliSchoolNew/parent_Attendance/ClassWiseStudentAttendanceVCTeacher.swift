//
//  ClassWiseStudentAttendanceVCTeacher.swift
//  intelliSchoolNew
//
//  Created by Intellinects Venture on 26/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ClassWiseStudentAttendanceVCTeacher: BaseViewControllerTeacher,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,DatePikerDelegate {
    let cellId = "CellId"
    var studentList = [StudentTeacher]()
    var noDataLbl = ""
    struct ColorCode {
        static let white = "#ffffff"
        static let red = "#ff0000"
        static let gray  = "#d3d3d3"
        static let lightRed = "#ffdae0"
        static let lightGreen = "#add8a6"
        static let yellow = "#fff8a6"
    }
    var selectedDate:String?
    var schoolClass: SchoolClassTeacher?
    var dateBarButtonItem = UIBarButtonItem()
    
    let collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0.5
        let clView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        clView.backgroundColor = UIColor(red:0.86, green:0.86, blue:0.86, alpha:1.0)
        clView.translatesAutoresizingMaskIntoConstraints = false
        return clView
    }()
    
    let headerView:UIView = {
        let headerView = UIView()
        headerView.backgroundColor = UIColor(red:0.86, green:0.86, blue:0.86, alpha:1.0)
        headerView.translatesAutoresizingMaskIntoConstraints = false
        return headerView
    }()
    
    let confirmButton : UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("CONFIRM", for: .normal)
        btn.tintColor = UIColor.white
        btn.addTarget(self, action: #selector(handleConfirm), for: .touchUpInside)
        btn.backgroundColor = UIColor(red:0.06, green:0.37, blue:0.24, alpha:1.0)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    //MARK:- Custom Views Setup Methods
    
    private func setUpConfirmButton() {
    
        view.addSubview(confirmButton)
        // needed constraints x,y,w,h
        confirmButton.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        confirmButton.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        confirmButton.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        confirmButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    private func setUpCollectionView() {
    
        view.addSubview(collectionView)
        collectionView.dataSource = self
        collectionView.delegate = self
        
        // needed constarunts x,y,w,h
        collectionView.topAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: confirmButton.topAnchor).isActive = true
    }
    
    private func setUpHeaderView() {
    
        view.addSubview(headerView)
        // needed constraints x,y,w,h
        if #available(iOS 11.0, *) {
            headerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        } else {
            headerView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        }
        headerView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        headerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        headerView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        let rollLbl = createLabel(title: "Roll", textAlignment: .justified)
        
        headerView.addSubview(rollLbl)
        // needed constraints x,y,w,h
        rollLbl.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: 8).isActive = true
        rollLbl.topAnchor.constraint(equalTo: headerView.topAnchor).isActive = true
        rollLbl.widthAnchor.constraint(equalToConstant: 40).isActive = true
        rollLbl.bottomAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
        
        let separatorView = UIView()
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        separatorView.backgroundColor = UIColor.gray
        
        headerView.addSubview(separatorView)
        // needed constraints x,y,w,h
        separatorView.leftAnchor.constraint(equalTo: rollLbl.rightAnchor).isActive = true
        separatorView.topAnchor.constraint(equalTo: headerView.topAnchor).isActive = true
        separatorView.widthAnchor.constraint(equalToConstant: 1).isActive = true
        separatorView.bottomAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
  
        let statusLbl = createLabel(title: "Status", textAlignment: .center)
        
        headerView.addSubview(statusLbl)
        // needed constraints x,y,w,h
        statusLbl.rightAnchor.constraint(equalTo: headerView.rightAnchor, constant: 8).isActive = true
        statusLbl.topAnchor.constraint(equalTo: headerView.topAnchor).isActive = true
        statusLbl.widthAnchor.constraint(equalToConstant: 60).isActive = true
        statusLbl.bottomAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
        
        let separatorView2 = UIView()
        separatorView2.translatesAutoresizingMaskIntoConstraints = false
        separatorView2.backgroundColor = UIColor.gray
        
        headerView.addSubview(separatorView2)
        // needed constraints x,y,w,h
        separatorView2.leftAnchor.constraint(equalTo: statusLbl.leftAnchor).isActive = true
        separatorView2.topAnchor.constraint(equalTo: headerView.topAnchor).isActive = true
        separatorView2.widthAnchor.constraint(equalToConstant: 1).isActive = true
        separatorView2.bottomAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
        
        let nameLbl = createLabel(title: "Name", textAlignment: .center)
        
        headerView.addSubview(nameLbl)
        // needed constraints x,y,w,h
        nameLbl.rightAnchor.constraint(equalTo: statusLbl.leftAnchor).isActive = true
        nameLbl.leftAnchor.constraint(equalTo: rollLbl.rightAnchor).isActive = true
        nameLbl.topAnchor.constraint(equalTo: headerView.topAnchor).isActive = true
        nameLbl.bottomAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
    }
    
    //MARK:- View Controllers Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        collectionView.register(StudentNameCell.self, forCellWithReuseIdentifier: cellId)
  //      self.title = "Class \(schoolClass!.className!) \(schoolClass!.divisionName!)"
        setUpHeaderView()
        setUpConfirmButton()
        setUpCollectionView()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: getFormattedDate(dateString: Date()), style: .plain, target: self, action:#selector(handleDateDisplay))
        selectedDate = getFormattedDate(dateString: Date())
        loadAttendance(dateString: Date())
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
    }
    
    // MARK:- Custom Action Methods
    
    private func createLabel(title: String?, textAlignment : NSTextAlignment) -> UILabel {
        let lbl = UILabel()
        lbl.text = title
        lbl.font = UIFont.boldSystemFont(ofSize: 13)
        lbl.textAlignment = textAlignment
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }
    func dismissDatePicker(_ isPresent: Bool) {}

    @objc func handleDateDisplay() {
        
        let alert = UIAlertController(title: "", message: "Are you sure,you want to edit the Attendance", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "NO", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { [weak self](action: UIAlertAction!) in
            DispatchQueue.main.async {
                self?.handleDisplayDate()
            }
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func handleDisplayDate() {
        let vc = DatePickerVC()
        vc.delegate = self
        vc.isRestrictPriviousDate = true
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChild(vc)
        view.addSubview(vc.view)
    }
    
    func handleWithSelectedDate(datePickerSelectedDate date: Date) {

        studentList.removeAll()
        loadAttendance(dateString: date)
        selectedDate = getFormattedDate(dateString: date)
        navigationItem.rightBarButtonItem?.title = getFormattedDate(dateString: date)
    }
 
    //MARK:- CollectionView Methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.studentList.count == 0{
            self.collectionView.setEmptyMessage(self.noDataLbl)
        }else{
            self.collectionView.restore()
        }
        return studentList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! StudentNameCell
        
        let student: StudentTeacher = studentList[indexPath.row]
        cell.backgroundColor = UIColor.white
        cell.rollLabel.text = student.studentRollNo.description
        cell.nameLabel.text = student.studentName?.uppercased()
        cell.statusLabel.text = student.status?.uppercased()
        cell.backgroundColor = hexStringToUIColor(hex: student.statusColor!)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: 45)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let cell = collectionView.cellForItem(at: indexPath) as? StudentNameCell
        let student: StudentTeacher = studentList[indexPath.row]
        let statusText = cell?.statusLabel.text
      
        if(statusText == "P" || statusText == "p"){
            cell?.backgroundColor = hexStringToUIColor(hex: ColorCode.red)
            cell?.statusLabel.text = "A"
            student.status = "A"
            student.statusColor = ColorCode.red
        } else if(statusText == "A" || statusText == "a"){
            cell?.backgroundColor = hexStringToUIColor(hex: ColorCode.gray)
            cell?.statusLabel.text = "S"
            student.status = "S"
            student.statusColor = ColorCode.gray
        } else if(statusText == "S" || statusText == "s"){
            cell?.backgroundColor = hexStringToUIColor(hex: ColorCode.lightRed)
            cell?.statusLabel.text = "HD"
            student.status = "HD"
            student.statusColor = ColorCode.lightRed
        } else if(statusText == "HD" || statusText == "hd"){
            cell?.backgroundColor = hexStringToUIColor(hex: ColorCode.lightGreen)
            cell?.statusLabel.text = "E"
            student.status = "E"
            student.statusColor = ColorCode.lightGreen
        } else if(statusText == "E" || statusText == "e"){
            cell?.backgroundColor = hexStringToUIColor(hex: ColorCode.yellow)
            cell?.statusLabel.text = "O"
            student.status = "O"
            student.statusColor = ColorCode.yellow
        } else if(statusText == "O" || statusText == "o"){
            cell?.backgroundColor = hexStringToUIColor(hex: ColorCode.white)
            cell?.statusLabel.text = "P"
            student.status = "P"
            student.statusColor = ColorCode.white
        }
    }
    
    //MARK:- Server Methods
    
    func loadAttendance(dateString: Date){

        if UtilTeacher.validateNetworkConnection(self){
            showActivityIndicatory(uiView: view)
            let userDefaults = UserDefaults.standard
            let parameters = [
                "school_db_settings_array":"{\n  \"enc_settings\" : \"gaywbsfZmN5JXL%2F6f6Bb44jpx%2Fs%2FKTl63nXgjYbc%2FEIsOMKmMm74t%2FXlLR2LO%2B3O5wC1PzJ9UB%2BVfKExwLGo%2BxUtAXRuiroLpEmzcW%2BtjDk%3D\"\n}",
                "action": "EditAttendance",
                "classid": "4",
                "divid": "1",
                "attenddate": "2019-12-30"
                ] as [String : Any]
            
       
            
            
            Alamofire.request(WebServiceUrls.attendanceService,method: .post,parameters: parameters).responseJSON { [weak self] (response) in
                 self?.stopActivityIndicator()
                print("parameters EditAttendance \(parameters) ")
                print("response EditAttendance \(response)")
                    if response.result.error != nil {
                        self?.noDataLbl = "Student not found!"
                        self?.collectionView.reloadData()
                        self?.confirmButton.isUserInteractionEnabled = false
                    }
                    if let jsonresponse = response.result.value {
                        let originalResponse = JSON(jsonresponse)
                        self?.getSuccessResponse(originalResponse: originalResponse)
                    }
            }
        }
    }
    
    func getSuccessResponse(originalResponse : JSON)
    {
        if(originalResponse.count > 0)
        {
            var status: String?
            var statusColor: String?
            
            for (_,subJson):(String, JSON) in originalResponse {

                status = subJson["attendance"].stringValue
                if(status == "P"){
                    statusColor = ColorCode.white
                } else if(status == "A"){
                    statusColor = ColorCode.red
                } else if(status == "S"){
                    statusColor = ColorCode.gray
                } else if(status == "HD"){
                    statusColor = ColorCode.lightRed
                } else if(status == "E"){
                    statusColor = ColorCode.lightGreen
                } else if(status == "O"){
                    statusColor = ColorCode.yellow
                }else {
                    statusColor = ColorCode.white
                }
                
                let student = StudentTeacher()
                student.studentName = subJson["StudentName"].stringValue
                student.studentId = Int(subJson["StudentId"].stringValue)!
                student.studentRollNo = Int(subJson["StudentRollno"].stringValue)!
                student.intellinectId = subJson["intellinectid"].stringValue
                student.status = status == "" ? "P" : status
                student.statusColor = statusColor
                studentList.append(student)
            }
            self.confirmButton.isUserInteractionEnabled = true
            collectionView.reloadData()
        }
    }
    
    @objc func handleConfirm() {
        
        var summaryA = [Int]()
        var summaryS = [Int]()
        var summaryHD = [Int]()
        var summaryE = [Int]()
        var summaryO = [Int]()
        
        for i in (0..<studentList.count){
            let student:StudentTeacher = studentList[i]
            if(student.status != "P"){
                switch student.status! {
                case "A":
                    summaryA.append(student.studentRollNo)
                    break
                case "S":
                    summaryS.append(student.studentRollNo)
                    break
                case "HD":
                    summaryHD.append(student.studentRollNo)
                    break
                case "E":
                    summaryE.append(student.studentRollNo)
                    break
                case "O":
                    summaryO.append(student.studentRollNo)
                    break
                default:
                    break
                }
            }
        }
        
        let messageString = " Absent - \(summaryA.count <  1 ? "NA" : summaryA.getJoinedString()) \n\n Sick Leave - \(summaryS.count < 1 ? "NA" : summaryS.getJoinedString()) \n\n Half Day - \(summaryHD.count < 1 ? "NA" : summaryHD.getJoinedString()) \n\n Sports/Extra Curr. - \(summaryE.count < 1 ? "NA" : summaryE.getJoinedString()) \n\n Others - \(summaryO.count < 1 ? "NA" : summaryO.getJoinedString()) "
  
        let summaryAlert = UIAlertController(title: "Details By Roll No", message: messageString, preferredStyle: UIAlertController.Style.alert)
        summaryAlert.addAction(UIAlertAction(title: "SAVE", style: .default, handler: { [weak self] (action: UIAlertAction!) in
            self?.attendanceSave(false)
        }))
        summaryAlert.addAction(UIAlertAction(title: "SAVE AND SMS", style: .default, handler: {[weak self] (action: UIAlertAction!) in
            self?.attendanceSave(true)
        }))
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.left
        
        //NSAttributedStringKey.paragraphStyle, NSAttributedString.UIFont  : UIFont(
        let messageText = NSMutableAttributedString(string: messageString, attributes: [NSAttributedString.Key.paragraphStyle     : paragraphStyle, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17)] )
//        let messageText = NSMutableAttributedString(
//            string: messageString,
//            attributes: [
//
//                NSParagraphStyleAttributeName: paragraphStyle,
//                NSFontAttributeName: UIFont.systemFont(ofSize: 17)
//
//            ]
//        )
        summaryAlert.setValue(messageText, forKey: "attributedMessage")
        summaryAlert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler:nil))
        present(summaryAlert, animated: true, completion: nil)
    }
    
    func attendanceSave(_ smsStatus : Bool) {

        let fullNameArr = selectedDate?.components(separatedBy: "-")
        let day = fullNameArr?[0]
        let month = fullNameArr?[1]
        let year = fullNameArr?[2]
        
        //create attendance array
        var attendance = [[String: AnyObject]]()
        for i in (0..<studentList.count){
            let student:StudentTeacher = studentList[i]
            if(student.status != "P"){
                //create object
                let obj = ["intellinectid":student.intellinectId, "attendance_code": student.status] as [String : AnyObject]
                if student.status != "--" {
                    attendance.append(obj)
                }
                
            }
        }
        
        var attendanceObj = [String:AnyObject]()
        
        if let classId = schoolClass?.classID, let divisionId = schoolClass?.divisionID, let month = month, let day = day, let year = year {
        
            attendanceObj = ["class_id": classId ,"division_id": divisionId, "attendance_array": attendance,"month":month,"year":year,"date":day] as [String:AnyObject]
        }
        
        let jsonData: NSData
        do {
            jsonData = try JSONSerialization.data(withJSONObject: attendanceObj, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String
            
            saveAndSendSMSAsyncTask(attendanceData: jsonString, smsStatus: smsStatus)
            
        } catch _ {
            print ("JSON Failure")
            return
        }
    }
    
    func saveAndSendSMSAsyncTask(attendanceData:String, smsStatus:Bool){
        
        if UtilTeacher.validateNetworkConnection(self){
            
            let userDefaults = UserDefaults.standard
            let parameters = [
                "school_db_settings_array":userDefaults.object(forKey: "SCHOOL_DB_SETTINGS") ?? "school_db_settings_array",
                "action": "SaveAttendance",
                "attendance_data": "2019-12-30",
                "sms_status": smsStatus.description,
                "sms_settings": userDefaults.object(forKey: "SMS_SETTINGS") ?? "sms_settings"
                ] as [String : Any]
            showActivityIndicatory(uiView: view)
            Alamofire.request(WebServiceUrls.attendanceService,method: .post,parameters: parameters)
                .responseJSON { [weak self] (response) in
                    self?.stopActivityIndicator()
                    if response.result.error != nil {
                        UtilTeacher.invokeAlertMethod("Failed!", strBody: "Error Occured While Processing Data ", delegate: nil,vcobj : self!)
                    }
                    if let jsonresponse = response.result.value {
                        
                        let originalResponse = JSON(jsonresponse)
                        self?.getSuccessRes(originalResponse: originalResponse)
                    }
            }
        }
    }
    
    func getSuccessRes(originalResponse : JSON)
    {
        if(originalResponse.count > 0)
        {
            if(originalResponse["save_status"].stringValue == "true"){
                
                let logoutAlert = UIAlertController(title: "", message: "Attendance has been taken", preferredStyle: UIAlertController.Style.alert)
                
                logoutAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                present(logoutAlert, animated: true, completion: nil)
            }
        }
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
 }

class StudentNameCell: UICollectionViewCell {
    
    let rollLabel: UILabel = {
        let rollLbl = UILabel()
        rollLbl.textAlignment = .center
        rollLbl.font = UIFont(name: "Helvetica Neue", size: 13)
        rollLbl.translatesAutoresizingMaskIntoConstraints = false
        return rollLbl
    }()
    
    let statusLabel: UILabel = {
        let statusLbl = UILabel()
        statusLbl.textAlignment = .center
        statusLbl.font = UIFont.boldSystemFont(ofSize: 13)
        statusLbl.translatesAutoresizingMaskIntoConstraints = false
        return statusLbl
    }()
    
    let nameLabel: UILabel = {
        let nameLbl = UILabel()
        nameLbl.font = UIFont(name: "Helvetica Neue", size: 15)
        nameLbl.textAlignment = .justified
        nameLbl.translatesAutoresizingMaskIntoConstraints = false
        return nameLbl
    }()

    func setViews() {
        
        self.addSubview(rollLabel)
        // needed constraints x,y,w,h
        rollLabel.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        rollLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        rollLabel.widthAnchor.constraint(equalToConstant: 40).isActive = true
        rollLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        self.addSubview(statusLabel)
        // needed constraints x,y,w,h
        statusLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 8).isActive = true
        statusLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        statusLabel.widthAnchor.constraint(equalToConstant: 60).isActive = true
        statusLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        self.addSubview(nameLabel)
        // needed constraints x,y,w,h
        nameLabel.rightAnchor.constraint(equalTo: statusLabel.leftAnchor).isActive = true
        nameLabel.leftAnchor.constraint(equalTo: rollLabel.rightAnchor,constant: 12).isActive = true
        nameLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        nameLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension Array {
 
    func getJoinedString() -> String {
      return  self.flatMap({ String(describing: $0) }).joined(separator: ",")
    }
}
