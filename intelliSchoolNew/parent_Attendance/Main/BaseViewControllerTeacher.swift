//
//  BaseViewControllerTeacher.swift
//  intelliSchoolNew
//
//  Created by Intellinects Venture on 26/12/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit

struct TheamColors {
    static let baseColor = UIColor(red:0.16, green:0.09, blue:0.44, alpha:1.0)
//        static let baseColor = GlobleConstants.baseColor
}

class BaseViewControllerTeacher: UIViewController {

    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nav = navigationController?.navigationBar
        nav?.barTintColor = TheamColors.baseColor //UIColor.white //UIColor(red:0.16, green:0.09, blue:0.44, alpha:1.0)
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]//UIColor.white]
        view.backgroundColor = UIColor.lightGray
    }
    
    func getFormattedDate(dateString: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "d-M-yyyy"
        return formatter.string(from: dateString)
    }
    
    func getYYYYMMDDFormattedDate(dateString: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: dateString)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showActivityIndicatory(uiView: UIView) {

        let loadingView: UIView = UIView()
        loadingView.frame =  CGRect(x: 0, y: 0, width: 80.0, height: 80.0)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor(red: 44/255, green: 44/255, blue: 44/255, alpha: 0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        actInd.frame = CGRect(x: 0, y: 0, width: 40.0, height: 40.0)
        actInd.style =
            UIActivityIndicatorView.Style.whiteLarge
        actInd.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        loadingView.addSubview(actInd)
        uiView.addSubview(loadingView)
        actInd.startAnimating()
    }
    
    func stopActivityIndicator() {
        actInd.stopAnimating()
        let view = actInd.superview
        view?.removeFromSuperview()
    }
}

class ClassCell : UITableViewCell {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        textLabel?.frame = CGRect(x: 12, y: textLabel!.frame.origin.y, width: contentView.frame.width, height: textLabel!.frame.height)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

