//
//  ForgetPassVC.swift
//  Intellinects
//
//  Created by Intellinects on 04/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class ForgetPassVC: UIViewController {

    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var image_logo: UIImageView!
    
    @IBOutlet var label_schoolName: UILabel!
    
    @IBOutlet var tf_mobileNo: UITextField!
    
    @IBOutlet var btn_send: UIButton!
    
    
    @IBOutlet var label_register: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tf_mobileNo.addDoneButtonOnKeyboard()
        //
         btn_send.layer.cornerRadius = 10.0
        //
        let fontAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14.0)]
        UITabBarItem.appearance().setTitleTextAttributes(fontAttributes, for: .normal)
        //keyboard scroling
        NotificationCenter.default.addObserver(self, selector: #selector(LoginVC.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginVC.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        //
    
        UITabBarItem.appearance().setTitleTextAttributes(fontAttributes, for: .normal)
        tf_mobileNo.setBottomBorder()
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            print("notification: Keyboard will show")
            if self.scrollView.frame.origin.y == 0{
                self.scrollView.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.scrollView.frame.origin.y != 0 {
                self.scrollView.frame.origin.y += keyboardSize.height
            }
        }
    }
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @IBAction func sendBtnTapped(_ sender: Any) {
        getForgotPassData()
    }
    
    func getForgotPassData()
    {
//        var url =           "https://isirs.skchigh.in/intellinect_dashboard/WS/index_main.php?action=newloginservice_registration_deviceId&mobilenumber=9172561899&deviceId=CAE122AA-154E-4904-A33B-51BDD1C474AC&SCHOOL_ID=327"
        
        guard let mobileNo = tf_mobileNo.text else {
            return
        }
        var url = "https://isirs.skchigh.in/intellinect_dashboard/WS/index_main.php?"
        let param = [
//            "action" : "newloginservice_registration_deviceId",
//            "mobilenumber":mobNo,
//            "deviceId" : "".getUDIDCode(),
//           // "boxcarToken" : "",
//            "SCHOOL_ID" :"327",
//          //  "mobilePlatformId": "2",
//           // "subSchoolIds": ""
//            "action" : "newloginservice_registration_deviceId",
//            "mobilenumber":mobNo as Any,
//            "deviceId":"".getUDIDCode(),
//            "SCHOOL_ID":"327"
            
            "action" : "newloginservice_registration_deviceId",
            "mobilenumber" : mobileNo,
            "deviceId" : "".getUDIDCode(),
            "boxcarToken" :"",
            "SCHOOL_ID" : "327",
            "mobilePlatformId": "2",
            "subSchoolIds": ""
            
            ] as [String : Any]
        
        print("mobNomobNomobNo\(mobileNo)")
        if Util.validateNetworkConnection(self){
            
            Alamofire.request(url,method: .get , parameters: param).responseJSON {[weak self] (response) in
                
                //  self?.stopActivityIndicator()
                if response.result.error != nil {
                }
                
                print("response1getForgotPassData>>>>\(response.result)")
                switch response.result {
                case .success:
                    self!.customAlert(title: "Alert", description: "Sms sent on your device.")
                    if let jsonresponse = response.result.value {
//                        self!.customAlert(title: "Alert!", description: "The device is already registered with a number.")
                        let originalResponse = JSON(jsonresponse)
                        
                        let data1 = originalResponse["employee"]
                        
                        // self?.parseData(json: originalResponse)
                        print("originalResponse1>>>>\(originalResponse)")
                       
                        
                        //                    }
                        //  print("originalResponse\(originalResponse)")
                        
                    }
                case .failure(_):
                    self!.customAlert(title: "Alert!", description: "Please contact Admin")
                    print("errror")
                }
                
            }
        }
        
    }
    func customAlert(title: String, description: String) {
        
        let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        //navigationController?.pushViewController(alert, animated: true)
        present(alert, animated: true, completion: nil)
    }
}
