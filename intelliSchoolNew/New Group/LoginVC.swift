//
//  LoginVC.swift
//  Intellinects
//
//  Created by Intellinects on 04/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Firebase
import FirebaseInstanceID
import FirebaseMessaging

class LoginVC: BaseViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var image_logo: UIImageView!
    @IBOutlet var label_schoolName: UILabel!
    @IBOutlet var tf_mobileNo: UITextField!
    @IBOutlet var tf_password: UITextField!
    @IBOutlet var label_forgetPassword: UILabel!
    @IBOutlet var btn_Login: UIButton!
    @IBOutlet weak var eyeBtn: UIButton!
    
    var iconClick = true

    
    override func viewDidLoad() {
        super.viewDidLoad()
        tf_mobileNo.addDoneButtonOnKeyboard()
        tf_password.addDoneButtonOnKeyboard()
        //
        btn_Login.layer.cornerRadius = 10.0
        //
        let fontAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14.0)]
        UITabBarItem.appearance().setTitleTextAttributes(fontAttributes, for: .normal)
        //keyboard scroling
        NotificationCenter.default.addObserver(self, selector: #selector(LoginVC.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginVC.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        // keyboard scroling end
       // getLoginData()
        tf_mobileNo.setBottomBorder()
        
       
        tf_password.setBottomBorder()
        
      
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
    }
    //
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            print("notification: Keyboard will show")
            if self.scrollView.frame.origin.y == 0{
                self.scrollView.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.scrollView.frame.origin.y != 0 {
                self.scrollView.frame.origin.y += keyboardSize.height
            }
        }
    }
    //Calls this function when the tap is recognized.keybord will disaper
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    

    @IBAction func eyeBtnTapped(_ sender: Any) {
       
            if(iconClick == true) {
                tf_password.isSecureTextEntry = false
                eyeBtn.setImage(UIImage(named: "eye_25"), for: .normal)
            } else {
                tf_password.isSecureTextEntry = true
                eyeBtn.setImage(UIImage(named: "closeeye_25"), for: .normal)
            }
            iconClick = !iconClick
        
    }
    @IBAction func LoginBtnTapped(_ sender: Any) {
        getLoginData()
    }
    
    func getLoginData()
    {
        
//        let mobile_no = tf_mobileNo.text
//        let password = tf_password
        guard let userId = tf_mobileNo.text,let password = tf_password.text else {
            return
        }
        if  userId.isEqual("")
        {
           customAlert(title: "Alert!", description: "Please Enter Mobile Number")
        } else if password.isEqual("") {
           customAlert(title: "Alert!", description: "please enter Password")
        }else {
            UserDefaults.standard.set(userId, forKey: "userId")
            UserDefaults.standard.set(password, forKey: "password")
            
                  // var url =           "https://isirs.skchigh.in/api/v1.5/login.php?action=newloginservice&mobile_no=9403252729&deviceId=357346100732607&boxcarToken=faTwDjckRg4:APA91bEsqkM7WRCzMqnISNaCvzwC-Pp-UeKYso7ZU14-F9OyD0fVhZO9wvYug1UxBfjEYigLCynStKF3oLuVQ30ZQykf0ZZmlUp8XQ50RQRZRzaHosoAUO2vOALhBSa9xHdsScnMrJM_&alias=357346100732607-327&loginType=1&password=ytcahe&school_id=327"

//        var url = "https://isirs.org/api/v1.5/login.php?"
//        //let token = Messaging.messaging().fcmToken
//        //    print("tokentokentoken\(token)")
//        let parameters = [
//            "action":"newloginservice",
//            "mobile_no":userId,
//            "password":password,
//            "loginType":"1",
//            "boxcarToken":"",
//           // "boxcarToken" :token ?? Util.getNsDefaultValue("SCHOOL_DEVICETOKEN"),
//            "deviceId":  "".getUDIDCode(), //"A7DA11F4-4735-4364-95E0-BBB7703AC8A8",
//            "school_id": 327,
//           // "alias": "\("".getUDIDCode())-\("327")",
//            "alias" : "866469038960100-29",
//            "subSchoolIds" : ""
//            ] as [String : Any]

            var url = "http://intellischools.com/schoolAdminApi/public/api/login"
            
            let parameters = [
              
                "mobile":userId,
                "password":password,
                "schoolcode" : "INTELL"
                ] as [String : Any]
        
           
        if Util.validateNetworkConnection(self){
           
            Alamofire.request(url,method: .post , parameters: parameters).responseJSON {[weak self] (response) in
             //   self!.showActivityIndicatory(uiView: self!.view)
                print("parametersparametersparameters\(parameters)")
                print("loginresponse#######>>>>\(response.result)")
                
                
                print("loginresponse#######VALUE>>>>\(response.result.value)")
                //  self?.stopActivityIndicator()
                if response.result.error != nil {
                }
                
                print("loginresponse1>>>>\(response.result)")
                switch response.result {
                case .success:
                    UserDefaults.standard.set(userId, forKey: "userId")
                    UserDefaults.standard.set(password, forKey: "password")
                    
                    if let jsonresponse = response.result.value {
                        let originalResponse = JSON(jsonresponse)
                        let sessionToken = originalResponse["sessionToken"].string
                        print("sessionTokensessionToken\(sessionToken)")
                        UserDefaults.standard.set(sessionToken, forKey: "sessionToken")
                        var session =  UserDefaults.standard.value(forKey: "sessionToken")
                        print("sesionsesionsesion\(session)")
                        
                        var school_db_settings_array = originalResponse["school_db_settings_array"].object
                       // UserDefaults.standard.set(school_db_settings_array, forKey: "SCHOOL_DB_SETTINGS")
                        UserDefaults.standard.set(String(describing: school_db_settings_array), forKey: "SCHOOL_DB_SETTINGS")
                        print("school_db_settings_array>>\(school_db_settings_array)")
                    print("userdefaultschool_db_settings_array\(UserDefaults.standard.value(forKey: "SCHOOL_DB_SETTINGS"))")
                        
                       
                        
                        var school_employee_class_setting_array = originalResponse["school_employee_class_setting_array"].arrayObject
                    print("school_employee_class_setting_array>>\(school_employee_class_setting_array)")
                      //  UserDefaults.standard.set(school_employee_class_setting_array, forKey: "school_employee_class_setting_array")
                        UserDefaults.standard.set(String(describing: school_employee_class_setting_array), forKey: "SUBJECT_SETTINGS")
                  
                        
        print("userdefaultschool_school_employee_class_setting_array\(UserDefaults.standard.value(forKey: "SUBJECT_SETTINGS"))")
                        
                        
                        
                        

                                               

                    let InstDetails: [NSDictionary]  = originalResponse["InstDetails"].arrayObject as! [NSDictionary]
                    print("InstDetailsInstDetails\(InstDetails)")
                        for item1 in InstDetails {
                                                                       
                                                                      // UserDefaults.standard.set(self!.name, forKey: "loginprofilename")
                        let code = item1.value(forKey: "code") as! String
                        print("code : \(code)")
                    //    UserDefaults.standard.set(code, forKey: "code")
                            UserDefaults.standard.set(code, forKey: "code")
                           
                        var accademicYear = item1.value(forKey: "accademicYear") as! String
                        print("accademicYear : \(accademicYear)")
                        UserDefaults.standard.set(accademicYear, forKey: "accademicYear")


                        }
                        let EmpSuccess = originalResponse["EmpSuccess"]
                        // self?.parseData(json: originalResponse)
                        //let valid = parent["valid"].string
                        let EmpMessage = originalResponse["EmpMessage"]
                        print("EmpMessageEmpMessageEmpMessageEmpMessage\(EmpMessage)")
                print("EmpSuccessEmpSuccess\(EmpSuccess)")
                       // print("validvalidvalidvalid>>>>\(valid)")
                       // print("parentparent\(parent)")
                        let ParentSuccess = originalResponse["ParentSuccess"]
                        print("ParentSuccessParentSuccess\(ParentSuccess)")
                        let EmpDetails: [NSDictionary]  = originalResponse["EmpDetails"].arrayObject as! [NSDictionary]
                     //   let name = EmpDetails["name"]
                        
                        print("EmpDetailsEmpDetailsEmpDetails\(EmpDetails)")
                
                        for item2 in EmpDetails {
                            let name = item2.value(forKey: "name")
                            print("namenamename\(name)")
                            UserDefaults.standard.set(name, forKey: "loginprofilename")
                            let employee_role_id = item2.value(forKey: "employee_role_id")
                            let employee_id = item2.value(forKey: "employee_id")

                          //  UserDefaults.standard.set(employee_role_id, forKey: "EMPLOYEE_ROLE_ID")
                           // UserDefaults.standard.set(employee_role_id, forKey: "EMPLOYEE_ROLE_ID")
                            UserDefaults.standard.set(employee_role_id,forKey: "EMPLOYEE_ROLE_ID")
                            UserDefaults.standard.set(employee_id,forKey: "EMPLOYEE_ID")
                        }
                        let ParentDetails: [NSDictionary]  = originalResponse["ParentDetails"].arrayObject as! [NSDictionary]
                        //   let name = EmpDetails["name"]
                        
                        print("ParentDetailsParentDetailsParentDetails\(ParentDetails)")
                        
                        for item3 in ParentDetails {
                            let name = item3.value(forKey: "name")
                            print("namenamename\(name)")
                            UserDefaults.standard.set(name, forKey: "loginprofilename2")
                        }
                        //}
                    if EmpSuccess == true || ParentSuccess == true
                    {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let controller = storyboard.instantiateViewController(withIdentifier: "T_CViewController")
                        self?.present(controller, animated: true, completion: nil)
                        }
                        
                    else{
                        self!.customAlert(title: "Alert", description: "Username or password is wrong")
                        print("valid is false")
                        }
                    }
                    
                case .failure(_):
                
                    print("switch errror")
                }
            }
         //   self.stopActivityIndicator()
        }
    }

}
    func customAlert(title: String, description: String) {
        
        let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        //navigationController?.pushViewController(alert, animated: true)
        present(alert, animated: true, completion: nil)
    }
    class func getNsDefaultValue(_ forKey:String) -> String
    {
        var NsValue = String()
        if UserDefaults.standard.string(forKey: forKey) != nil
        {
            NsValue = UserDefaults.standard.string(forKey: forKey)!
        }
        return NsValue
    }
}

