//
//  RegistrationVC.swift
//  Intellinects
//
//  Created by Intellinects on 04/10/19.
//  Copyright © 2019 rupali . All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ReactiveKit

class RegistrationVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var image_logo: UIImageView!
    @IBOutlet var label_schoolName: UILabel!
    @IBOutlet var et_mobileNo: UITextField!
    @IBOutlet var btn_register: UIButton!
    @IBOutlet var text_login: UILabel!
    
    
    @IBAction func register_onClick(_ sender: Any) {
        getRegistrationData()
       // showToast(message: "toast printed sucefuly")
       // sleep(5)
       

        
//        guard let mobileNo = et_mobileNo.text else {
//                        return
//             }
//             if mobileNo.isEqual("") {
//                        Util.alert("", message: "Please enter Mobile Number",controller:self)
//             } else if(mobileNo.count < 10 || mobileNo.count > 10){
//                        Util.alert("", message: "Please enter valid Mobile Number",controller:self)
//             }else{
//                        Util.alert("", message: "Please enter proper Mobile Number",controller:self)
//        }
//


    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        et_mobileNo.addDoneButtonOnKeyboard()
       // addDoneButtonOnKeyboard()
 //       textField.delegate = self
 et_mobileNo.delegate = self
        //keyboard scroling
        NotificationCenter.default.addObserver(self, selector: #selector(LoginVC.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginVC.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        // keyboard scroling end
        //tabbar font size chaneg
        let fontAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14.0)]
        UITabBarItem.appearance().setTitleTextAttributes(fontAttributes, for: .normal)
        //tabbar font size chaneg end
        btn_register.layer.cornerRadius = 10.0
        //   btn_register.layer.borderWidth = 0.0
        //  btn_register.layer.borderColor = UIColor.red.cgColor
     print("getUDIDCodegetUDIDCodegetUDIDCode>>>\( "" .getUDIDCode())")
       // et_mobileNo.borderStyle = .none
        et_mobileNo.setBottomBorder()
 
       
        ///
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        let textField = UITextField(frame: CGRect(x: 20.0, y:90.0, width: 280.0, height: 44.0))
//
//        textField.delegate = self
      //  UITextField.returnKeyType = .done
        
        
      //  UITextField.center = self.view.center
       // UITextField.backgroundColor = UIColor.lightGray
        
        
       // self.view.addSubview(textField)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        
        return true
    }

    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            print("notification: Keyboard will show")
            if self.scrollView.frame.origin.y == 0{
                self.scrollView.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.scrollView.frame.origin.y != 0 {
                self.scrollView.frame.origin.y += keyboardSize.height
            }
        }
    }
    //
//    class UnderlinedTextField: UITextField {
//
//        @IBOutlet weak var underlineView: UIView!
//
//        override func awakeFromNib() {
//            super.awakeFromNib()
//
//            underlineView.backgroundColor = UIColor.lightGray
//
//            // Highlight/unhighlight the underlined view when it's being edited.
//
//            reactive.controlEvents(.editingDidBegin).map { _ in
//                return UIColor.green
//                }.bind(to: underlineView.reactive.backgroundColor).dispose(in: reactive.bag)
//
//            reactive.controlEvents(.editingDidEnd).map { _ in
//                return UIColor.lightGray
//                }.bind(to: underlineView.reactive.backgroundColor).dispose(in: reactive.bag)
//        }
//    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
        // Do any additional setup after loading the view.
    
    
    
    
    func getRegistrationData()
    {
        
        guard let mobileNo = et_mobileNo.text else {
            return
        }
        if mobileNo.isEqual("") {
            Util.alert("", message: "Please enter Mobile Number",controller:self)
        } else if(mobileNo.count < 10 || mobileNo.count > 10){
            Util.alert("", message: "Please enter valid Mobile Number",controller:self)
        }
        else{
       // var url =           "https://isirs.skchigh.in/intellinect_dashboard/WS/index_main.php?action=newloginservice_registration_deviceId&mobilenumber=9403252729&deviceId=357346100732607&SCHOOL_ID=327"
            
         
            var url = "https://isirs.org/intellinect_dashboard/WS/index_main.php?"
        
        let param = [
            "action" : "newloginservice_registration_deviceId",
            "mobilenumber":mobileNo,
            "deviceId" : "".getUDIDCode(),
            "boxcarToken" : Util.getNsDefaultValue("SCHOOL_DEVICETOKEN"),
            "SCHOOL_ID" : "327",
            "mobilePlatformId": "2",
            "subSchoolIds": ""
            ] as [String : Any]
        
            if Util.validateNetworkConnection(self){

                Alamofire.request(url,method: .get , parameters: param).responseJSON {[weak self] (response) in
                    
                  //  self?.stopActivityIndicator()
                    if response.result.error != nil {
                    }
                    
                    print("response1>>>>\(response.result)")
                    switch response.result {
                    case .success:
                        self!.customAlert2(title: "", description: "You Have Registered Sucessfully!!!")
                        
//                        self!.customAlert(title: "Alert!", description: "You have registered sucessfully.")
                        
                        if let jsonresponse = response.result.value {
                            
                            let originalResponse = JSON(jsonresponse)
                            
                            let data1 = originalResponse["employee"]
                            
                            // self?.parseData(json: originalResponse)
                            print("originalResponse1>>>>\(originalResponse)")
                            print("full_name>>>>\(data1)")
                            
                            //                    }
                          //  print("originalResponse\(originalResponse)")
                            
                        }
                    case .failure(_):
                        self!.customAlert(title: "Alert!", description: "Please contact Admin")
                        print("errror")
                    }

                    }
                }
                
}
}
    func customAlert(title: String, description: String) {
        
        let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        //navigationController?.pushViewController(alert, animated: true)
        present(alert, animated: true, completion: nil)
    }
    func customAlert2(title: String, description: String) {
        let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)
        //    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        //   present(alert, animated: true, completion: nil)
        
        let CancelAction = UIAlertAction(title: "cancel", style: .cancel) { (action) in
            
        }
        
        
        let action = UIAlertAction(title: "OK", style: .default) { (action) -> Void in
            //            let viewControllerYouWantToPresent = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationVC")
            //            self.present(viewControllerYouWantToPresent!, animated: true, completion: nil)
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let destination = self.storyboard!.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(destination, animated: true)
        }
      //  alert.addAction(CancelAction)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }

    
    
}
extension String {
    
    func getUDIDCode()->String
    {
        let deviceUDID = UIDevice.current.identifierForVendor!.uuidString
        return deviceUDID
}
}
//extension UITextField {
//    func underlined(){
//        let border = CALayer()
//        let width = CGFloat(1.0)
//        border.borderColor = UIColor.lightGray.cgColor
//        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
//        border.borderWidth = width
//        self.layer.addSublayer(border)
//        self.layer.masksToBounds = true
//    }
//
//}

extension UITextField {
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
//    func setBottomBorderselected() {
//        self.borderStyle = .none
//        self.layer.backgroundColor = UIColor.white.cgColor
//
//        self.layer.masksToBounds = false
//        self.layer.shadowColor = UIColor.red.cgColor
//        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
//        self.layer.shadowOpacity = 1.0
//        self.layer.shadowRadius = 0.0
//    }
    func setBottomBorderHighlighted() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
         let myColor = UIColor(hexString: "#1862C6")
        self.layer.shadowColor = myColor.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
    
}
extension UIViewController {
    
    func showToast(message : String) {
        let toastLabel = UILabel(frame: CGRect(x: view.frame.size.width/2 - 150, y: view.frame.size.height-100, width: 300,  height : 35))
        
        
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        //  toastLabel.font = 1
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 03.0, delay: 0.0, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    } }
extension UITextField{
    
//    @IBInspectable var doneAccessory: Bool{
//        get{
//            return self.doneAccessory
//        }
//        set (hasDone) {
//            if hasDone{
//                addDoneButtonOnKeyboard()
//            }
//        }
//    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.resignFirstResponder()
    }
}
