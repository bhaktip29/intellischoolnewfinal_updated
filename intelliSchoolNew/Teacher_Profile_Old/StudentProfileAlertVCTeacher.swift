//
//  CustomController.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 11/06/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class StudentProfileAlertVCTeacher: CustomAlertVCTeacher,UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    let userDefaults = UserDefaults.standard
    var studentInfo = StudentTeacher()
    override func viewDidLoad() {
        super.viewDidLoad()
        msgTextView.delegate = self
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(SHeaderCellTeacher.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader , withReuseIdentifier: headerCellId)
    }
    
    let headers = ["Personal Information","Parents Information","Address Details"]
    
    override func handleSend() {
        
        if msgTextView.text == "" || msgTextView.text == "Enter your message here...." {
            let alert = UIAlertController(title: "Error", message: "Please Enter message", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok", style: .default, handler:nil))
            present(alert, animated: true, completion: nil)
        } else {
            if UtilTeacher.validateNetworkConnection(self) {
                showActivityIndicatory(uiView: view)
                
                guard let intellinectId = studentInfo.intellinectId else{
                    return
                }

                let parameters = [
                    "school_db_settings_array": userDefaults.object(forKey: "SCHOOL_DB_SETTINGS") ?? "school_db_settings_array",
                    "action": "SendSMS",
                    "intellinectid": intellinectId.description,
                    "employee_id": userDefaults.string(forKey: "EMPLOYEE_ID") ?? "employee_id",
                    "sms_message":msgTextView.text as String,
                    "sms_settings": userDefaults.object(forKey: "SMS_SETTINGS") ?? "sms_settings"
                    ] as [String : Any]
                Alamofire.request(WebServiceUrls.studentInfoService,method: .post,parameters: parameters).responseJSON { [weak self] (response) in
                    self?.stopActivityIndicator()
                    if response.result.error != nil {
                        UtilTeacher.invokeAlertMethod("Failed!", strBody: "Error Occured While Processing Data ", delegate: nil,vcobj : self!)
                    }
                    if let jsonresponse = response.result.value {
                        
                        let originalResponse = JSON(jsonresponse)
                        let result = originalResponse["send_sms"].stringValue
                        DispatchQueue.main.async {
                            if result == "true" {
                                let alert = UIAlertController(title: "Success", message: "SMS sent successfully..!!", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "ok", style: .default, handler:nil))
                                self?.present(alert, animated: true, completion: {
                                    self?.handleClose()
                                })
                            }else{
                                let alert = UIAlertController(title: "Error", message: "Error Occured While sending SMS", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "ok", style: .default, handler:nil))
                                self?.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                    }
                }
            }
        }
    }
    
    override func handleSave() {
        
        if msgTextView.text == "" || msgTextView.text == "Enter your message here...." {
            let alert = UIAlertController(title: "Error", message: "Please Enter message", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler:nil))
            present(alert, animated: true, completion: nil)
        } else if selectedCategoryId == "0" || selectedCategoryId == nil {
            let alert = UIAlertController(title: "Error", message: "Please select Category", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }else {
            if UtilTeacher.validateNetworkConnection(self) {
                showActivityIndicatory(uiView: view)
                guard let intellinectId = studentInfo.intellinectId,let categoryId = selectedCategoryId else{
                    return
                }
                
                let att_Status = attachmentList.count > 0 ? true : false
  
                let parameters = [
                    "school_db_settings_array": userDefaults.object(forKey: "SCHOOL_DB_SETTINGS") ?? "school_db_settings_array",
                    "action": "CreateJournal",
                    "intellinectid": intellinectId.description,
                    "category_id": categoryId.description,
                    "journal_content":msgTextView.text as String,
                    "employee_id": userDefaults.string(forKey: "EMPLOYEE_ID") ?? "employee_id",
                    "attachment_status": att_Status.description
                    ] as [String : Any]
                Alamofire.request(WebServiceUrls.studentInfoService,method: .post,parameters: parameters).responseJSON { [weak self] (response) in
                    print(response.result.value)
                    if response.result.error != nil {
                        UtilTeacher.invokeAlertMethod("Failed!", strBody: "Error Occured While Processing Data ", delegate: nil,vcobj : self!)
                    }
                    if let jsonresponse = response.result.value {
                        
                        let originalResponse = JSON(jsonresponse)
                        let result = originalResponse["save_status"].stringValue
                        DispatchQueue.main.async {
                            if result == "true" {
                                let journalId = originalResponse["journal_id"].stringValue
                                if att_Status {
                                    self?.uploadAttachment(journalId)
                                }else {
                                    self?.stopActivityIndicator()
                                    self?.showSuccessAlert()
                                }
                            } else{
                                let alert = UIAlertController(title: "Error", message: "Error occured while Journal Note save", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "ok", style: .default, handler:nil))
                                self?.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func uploadAttachment(_ journalId: String){
        let parameters = [
            "employee_id": userDefaults.string(forKey: "EMPLOYEE_ID") ?? "error",
            "school_db_settings_array": userDefaults.object(forKey: "SCHOOL_DB_SETTINGS") ?? "error",
            "action":"upload_attachment_student",
            "upload_code": "SUJOU",
            "update_ids": journalId,
            "school_code":userDefaults.object(forKey: "SCHOOL_CODE") ?? "N/A",
            ] as [String : AnyObject]
        Alamofire.upload(multipartFormData: { (multipartFormData : MultipartFormData) in
            if self.attachExtensions.count > 0{
                let count = self.attachData.count
                for i in 0..<count{
                    let uuid = UUID().uuidString
                    multipartFormData.append(self.attachData[i], withName: "uploaded_file", fileName: "\(uuid).\(self.attachExtensions[i])" , mimeType: self.mimeTypes[i])
                }
            }
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: WebServiceUrls.uploadAttachamentService) { (result) in
            switch result {
            case .success(let upload, _ , _):
                upload.uploadProgress(closure: { (progress) in
                    //print(upload.uploadProgress)
                })
                upload.responseJSON { response in
                    print(response.result.value)
                    switch response.result {
                    case .success(let value):
                        let json = JSON(value)
                        DispatchQueue.main.async {
                            self.stopActivityIndicator()
                            if json["attached"] == "true" {
                                self.showSuccessAlert()
                            }else {
                                self.handleClose()
                            }
                        }
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                }
            case .failure(let encodingError):
                print("failed")
                print(encodingError)
            }
        }
    }
    
    func showSuccessAlert() {
    
        let alert = UIAlertController(title: "Success", message: "Journal Note saved successfully..!!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler:nil))
        self.present(alert, animated: true, completion: {
            self.handleClose()
            self.delegate?.handleJournal(true)
        })
    }
    
    func generateBoundaryString() -> String
    {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func createBodyWithParameters(parameters: NSMutableDictionary?,boundary: String) -> NSData {
        let body = NSMutableData()
        let mimetype = "image/jpeg"
        if parameters != nil {
            for (key, value) in parameters! {
                
                if(value is String || value is NSString){
                    body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                    body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
                    body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
                }
                else if(value is [UIImage]){
                    var i = 0;
                    for image in value as! [UIImage]{
                        let filename = "image\(i).jpeg"
                        let data = image.jpegData(compressionQuality: 0.1)
                        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                        body.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n".data(using: String.Encoding.utf8)!)
                        body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
                        body.append(data!)
                        body.append("\r\n".data(using: String.Encoding.utf8)!)
                        i += 1;
                    }
                }
            }
        }
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        return body
    }
    
    func createRequest (param : NSMutableDictionary , strURL : String) -> NSURLRequest {
        
        let boundary = generateBoundaryString()
        
        let url = NSURL(string: strURL)
        let request = NSMutableURLRequest(url: url! as URL)
        request.timeoutInterval = 60 // seconds
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = createBodyWithParameters(parameters: param, boundary: boundary) as Data
        
        return request
    }
 
    override func handleClose() {
        self.removeFromParent()
        self.view.removeFromSuperview()
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if self.temp != "" {
            msgTextView.text = temp
        }else {
            textView.text = ""
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if section == 0 {
            return 5
        } else if section == 1 {
            return 2
        } else {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! TeacherInfoCell
        
        switch indexPath.section+1 {
        case 1:
            switch indexPath.row+1 {
            case 1:
                cell.headerLabel.text = "BloodGroup"
                
                if studentInformation.bloodgroup != "" && studentInformation.bloodgroup != "null" && studentInformation.bloodgroup != "NULL" {
                    cell.valueLabel.text = studentInformation.bloodgroup
                } else {
                    cell.valueLabel.text = "N/A"
                }
                break
            case 2:
                cell.headerLabel.text = "Date of Birth"
                
                if studentInformation.dateOfBirth != "" && studentInformation.dateOfBirth != "null" && studentInformation.dateOfBirth != "NULL" {
                    cell.valueLabel.text = studentInformation.dateOfBirth
                } else {
                    cell.valueLabel.text = "N/A"
                }
                break
            case 3:
                cell.headerLabel.text = "Allergy"
                cell.valueLabel.text = "N/A"
                break
            case 4:
                cell.headerLabel.text = "Adhar Card No"
                if studentInformation.studentAdharNumber != "" && studentInformation.studentAdharNumber != "null" && studentInformation.studentAdharNumber != "NULL" {
                    cell.valueLabel.text = studentInformation.studentAdharNumber
                } else {
                    cell.valueLabel.text = "N/A"
                }
                break
            case 5:
                cell.headerLabel.text = "UDISE No"
                
                if studentInformation.udiseNo != "" && studentInformation.udiseNo != "null" && studentInformation.udiseNo != "NULL" {
                    cell.valueLabel.text = studentInformation.udiseNo
                } else {
                    cell.valueLabel.text = "N/A"
                }
                break
                
            default:
                break
            }
            
        case 2:
            switch indexPath.row+1 {
                
            case 1:
                cell.headerLabel.text = "Father Name"
                if studentInformation.fatherName?.trimmingCharacters(in: .whitespaces) != "" && studentInformation.fatherName != "null" && studentInformation.fatherName != "NULL" {
                    cell.valueLabel.text = studentInformation.fatherName
                } else {
                    cell.valueLabel.text = "Not updateed"
                }
                break
            case 2:
                cell.headerLabel.text = "Mother Name"
                
                if studentInformation.motherName?.trimmingCharacters(in: .whitespaces) != "" && studentInformation.motherName != "null" && studentInformation.motherName != "NULL" {
                    cell.valueLabel.text = studentInformation.motherName
                } else {
                    cell.valueLabel.text = "Not updateed"
                }
                break
            default:
                break
            }
        case 3:
            switch indexPath.row+1 {
                
            case 1:
                cell.headerLabel.text = "Address"
                
                if studentInformation.address != "" && studentInformation.address != "null" && studentInformation.address != "NULL" {
                    cell.valueLabel.text = studentInformation.address
                } else {
                    cell.valueLabel.text = "N/A"
                }
                break
            default:
                break
            }
            
        default:
            break
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: popUpContainerView.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerCellId, for: indexPath as IndexPath) as! SHeaderCellTeacher
        headerView.headerLabel.text = headers[indexPath.section]
        
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
    {
        return CGSize(width: collectionView.bounds.width, height: 30)
    }
}
