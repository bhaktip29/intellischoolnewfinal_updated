//
//  StudentProfileDataHandleVC.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 09/06/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class StudentProfileDisplayVCTeacher: BaseProfileVCTeacher,CalenderDelegateTeacher,JournalDelegate,JournalAddedDelegate {
    
    var studentInfo = StudentTeacher()
    var studentInfomation = StudentInformation()
    var schoolClass: SchoolClassTeacher?
    
    //MARK:- View Controllers Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpStudentProfileViews()
        getProfileData()
        self.title = studentInfo.studentName
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "RollNo:\(studentInfo.studentRollNo.description)", style:.plain, target: nil, action:nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
    }
    
    //MARK:- Custom Action Methods
    
    override func handleButtonClick(sender:UIButton) {
        
        let vc = StudentProfileAlertVCTeacher()
        vc.studentInfo = studentInfo
        vc.studentOrStaff = 1
        vc.delegate = self
        vc.studentInformation = studentInfomation
        switch sender.tag {
        case 1:
            vc.clickedButtonIndex = 1
            break
        case 2:
            vc.clickedButtonIndex = 2
            break
        case 3:
            vc.clickedButtonIndex = 3
            break
        default: break
        }
        
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChild(vc)
        view.addSubview(vc.view)
    }
    
    //MARK:- Custom Methods
    
    func setUpCollectionViewForTab() {
        
        tabView.subviews.forEach({ $0.removeFromSuperview() })
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        
        func setCollection(_ collectionView:UICollectionView?) {
            collectionView?.translatesAutoresizingMaskIntoConstraints = false
            collectionView?.backgroundColor = UIColor.white
            tabView.addSubview(collectionView!)
            collectionView?.topAnchor.constraint(equalTo: tabView.topAnchor,constant:1).isActive = true
            collectionView?.leftAnchor.constraint(equalTo: tabView.leftAnchor,constant:1).isActive = true
            collectionView?.rightAnchor.constraint(equalTo: tabView.rightAnchor,constant:-2).isActive = true
            collectionView?.bottomAnchor.constraint(equalTo: tabView.bottomAnchor,constant:-2).isActive = true
        }
        
        switch selectedIndex+1 {
        case 1:
            
            let collectionView = PVAttendanceCVCTeacher(frame: CGRect.zero, collectionViewLayout: layout)
            setCollection(collectionView)
            guard let board = studentInfomation.board, let intid = studentInfo.intellinectId, let cId = schoolClass?.classID else {
                return
            }
            collectionView.cDelegate = self
            collectionView.board = board
            collectionView.intellinectsId = intid
            collectionView.classId = cId.description
            collectionView.getData()
            break
        case 2:
            let lbl = UILabel()
            lbl.translatesAutoresizingMaskIntoConstraints = false
            lbl.font = UIFont.systemFont(ofSize: 14.0)
            lbl.text = "This service will be available in the next version of the App"
            lbl.numberOfLines = 0
            lbl.sizeToFit()
            lbl.textAlignment = .center
            tabView.addSubview(lbl)
            lbl.topAnchor.constraint(equalTo: tabView.topAnchor,constant:1).isActive = true
            lbl.leftAnchor.constraint(equalTo: tabView.leftAnchor,constant:8).isActive = true
            lbl.widthAnchor.constraint(equalTo: tabView.widthAnchor,constant:-16).isActive = true
            lbl.bottomAnchor.constraint(equalTo: tabView.bottomAnchor,constant:-2).isActive = true
            
            break
        case 3:
            let collectionView = StudentJournalCVCTeacher(frame: CGRect.zero, collectionViewLayout: layout)
            setCollection(collectionView)
            collectionView.jDelegate = self
            guard let intid = studentInfo.intellinectId else {
                return
            }
            collectionView.intellinectsId = intid
            collectionView.getData()
            break
        default:
            break
        }
    }
    
    //MARK:- Custom Delegate Methods
    
    func handleDisplayCalender(_ attendance: PVAttendance){
        let calenderVC = StudentCalenderVCTeacher()
        guard let intid = studentInfo.intellinectId else {
            return
        }
        calenderVC.intellinectsId = intid
        calenderVC.cmonth = attendance.month
        navigationController?.pushViewController(calenderVC, animated: true)
    }
    
    func handleDisplayJournalByCategory(_ category:PVCategory){
        let vc = StudentJournalListVCTeacher()
        vc.title = self.title
        guard let intid = studentInfo.intellinectId,let cat = category.categoryName else {
            return
        }
        vc.categoryName.text = "Category: \(cat)"
        vc.intellinectsId = intid
        vc.category = category
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func handleJournal(_ status: Bool) {
        if status {
            segmentControl.selectedSegmentIndex = 2
            selectedIndex = 2
            setUpCollectionViewForTab()
        }
    }
    
    //MARK:- Server Methods
    
    func getProfileData(){
        
        if UtilTeacher.validateNetworkConnection(self) {
            showActivityIndicatory(uiView: view)
            let userDefaults = UserDefaults.standard
            guard let intellinectId = studentInfo.intellinectId else{
                return
            }
            let parameters = [
                "school_db_settings_array":  userDefaults.object(forKey: "SCHOOL_DB_SETTINGS") ?? "school_db_settings_array",
                "action": "StudentInfo",
                "intellinectid": intellinectId as String,
                "school_code": userDefaults.string(forKey: "SCHOOL_CODE") ?? "school_code"
                ] as [String : Any]
            Alamofire.request(WebServiceUrls.studentInfoService,method: .post,parameters: parameters).responseJSON { [weak self] (response) in
                self?.stopActivityIndicator()
                print("StudentProfileDisplayVCTeacher response \(response)")
                print("StudentProfileDisplayVCTeacher response.result.value  \(response)")
                if response.result.error != nil {
                    UtilTeacher.invokeAlertMethod("Failed!", strBody: "Error Occured While Processing Data ", delegate: nil,vcobj : self!)
                }
                if let jsonresponse = response.result.value {
                    let originalResponse = JSON(jsonresponse)
                    self?.getSuccessResponse(originalResponse: originalResponse)
                    self?.setUpCollectionViewForTab()
                }
            }
        }
    }
    
    func getSuccessResponse(originalResponse : JSON)
    {
//        print("Original Response", originalResponse)
        if(originalResponse.count > 0)
        {
            studentInfomation.grNumber = originalResponse["gr_no"].stringValue
            studentInfomation.fatherName = originalResponse["father_name"].stringValue
            studentInfomation.fatherMobile = originalResponse["father_mobile"].stringValue
            studentInfomation.address = originalResponse["address"].stringValue
            studentInfomation.motherMobile = originalResponse["mother_mobile"].stringValue
            studentInfomation.studentAdharNumber = originalResponse["StudentAdharCardNo"].stringValue
            studentInfomation.dateOfBirth = originalResponse["birthDate"].stringValue
            studentInfomation.udiseNo = originalResponse["udiseNo"].stringValue
            studentInfomation.board = originalResponse["board"].stringValue
            studentInfomation.motherName = originalResponse["mother_name"].stringValue
            studentInfomation.bloodgroup = originalResponse["bloodGroup"].stringValue
            studentInfomation.profileImgUrl = originalResponse["photo_url"].stringValue
            guard let imgUrl = studentInfomation.profileImgUrl?.removingPercentEncoding, imgUrl != "",
                let tempUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed), let url = URL(string: imgUrl) else{
                    return
            }
            print("URL", url)
            print("url :::\(url)")
//            let data = try? Data(contentsOf: url)
//            print("datadatadatadata\(data)")
//            DispatchQueue.main.async {
//                if data != nil {
//                    self.profileImageView.image = UIImage(data: data!)
//                }
//            }
        }
    }
    
    override func handleSwipes(sender: UISwipeGestureRecognizer) {
        if (sender.direction == .right) {
            if selectedIndex > 0 {
                selectedIndex = selectedIndex - 1
            }
        }
        if (sender.direction == .left) {
            if selectedIndex < 2 {
                selectedIndex = selectedIndex + 1
            }
        }
        segmentControl.selectedSegmentIndex = selectedIndex
        setUpCollectionViewForTab()
    }
    
    override func handleSegmentSelection(sender: UISegmentedControl) {
        selectedIndex = sender.selectedSegmentIndex
        setUpCollectionViewForTab()
    }
}
