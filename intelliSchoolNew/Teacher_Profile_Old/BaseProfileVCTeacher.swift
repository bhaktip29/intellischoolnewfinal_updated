//
//  StudentProfileVC.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 09/06/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit

struct CustomColor {
    //static let greenColor: UIColor = UIColor(red:0.13, green:0.33, blue:0.18, alpha:1.0)
     static let greenColor: UIColor = UIColor(red:0.13, green:0.33, blue:0.18, alpha:1.0)//GlobleConstants.homeScreenIconColor
}

class BaseProfileVCTeacher: BaseViewControllerTeacher,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    let userDefaults = UserDefaults.standard
    var attachmentImgsData = [UIImage]()
    struct ButtonNames {
        static let sendsms = "Send SMS"
        static let journalEntry = "Journal Note"
        static let profile = "Profile"
    }
    
    var topAnchorForMiddleView: NSLayoutConstraint?
    var roleId = ""
    let imagePicker = UIImagePickerController()
    
    let cellId = "Digital"
    let headerCellId  = "HeaderCell"
    
    var selectedIndex: Int = 0
    var staff: Staff? {
        didSet{
            navigationItem.title = "\(staff?.firstName ?? "") \(staff?.lastName ?? "")"
        }
    }
    var student : StudentTeacher? {
        didSet{
            navigationItem.title = student?.studentName
        }
    }
    
    let middleView : UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    let profileImageView: UIImageView = {
        let imgView = UIImageView()
        imgView.layer.cornerRadius = 3
        imgView.layer.masksToBounds = true
        imgView.contentMode = .scaleAspectFit
        imgView.image = UIImage(named: "placeholder_image")
        imgView.translatesAutoresizingMaskIntoConstraints  = false
        return imgView
    }()
    
    let sendSMSBtn: UIButton = {
        let btn = UIButton(type: .system)
        btn.setupButton()
        btn.tag = 1
        btn.layer.cornerRadius = 2
        btn.layer.masksToBounds = true
        btn.addTarget(self, action: #selector(handleButtonClick), for: .touchUpInside)
        btn.setTitle(ButtonNames.sendsms, for: .normal)
        return btn
    }()
    
    let journalEntry: UIButton = {
        let btn = UIButton(type: .system)
        btn.setupButton()
        btn.tag = 2
        btn.layer.cornerRadius = 2
        btn.layer.masksToBounds = true
        btn.addTarget(self, action: #selector(handleButtonClick), for: .touchUpInside)
        btn.setTitle(ButtonNames.journalEntry, for: .normal)
        return btn
    }()
    
   let profile: UIButton = {
        let btn = UIButton(type: .system)
        btn.setupButton()
        btn.tag = 3
        btn.layer.cornerRadius = 2
        btn.layer.masksToBounds = true
        btn.addTarget(self, action: #selector(handleButtonClick), for: .touchUpInside)
        btn.setTitle(ButtonNames.profile, for: .normal)
        return btn
    }()
    
    let tabView : UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    let rightConatinerView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    let segmentControl : UISegmentedControl = {
        let segControl = UISegmentedControl()
        segControl.backgroundColor = CustomColor.greenColor
        segControl.tintColor = UIColor.white
        segControl.translatesAutoresizingMaskIntoConstraints = false
        return segControl
    }()
    
    let headerLbl : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.backgroundColor = UIColor.cyan.withAlphaComponent(0.2)
        lbl.textAlignment = .center
        return lbl
    }()
    
    //MARK: - Custom View Setup Methods
    
     func setUpHeaderLbl() {
        
        view.addSubview(headerLbl)
        // needed constarints
        headerLbl.topAnchor.constraint(equalTo: middleView.bottomAnchor,constant: 4).isActive = true
        headerLbl.leftAnchor.constraint(equalTo: view.leftAnchor,constant:1).isActive = true
        headerLbl.rightAnchor.constraint(equalTo: view.rightAnchor,constant:-2).isActive = true
        headerLbl.heightAnchor.constraint(equalToConstant:35).isActive = true
    }
        
    private func setMiddelView() {
        
        view.addSubview(middleView)
        // needed constarint x,y,w,h
        middleView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        if #available(iOS 11.0, *) {
            topAnchorForMiddleView = middleView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant:8)
        } else {
            topAnchorForMiddleView = middleView.topAnchor.constraint(equalTo: view.topAnchor,constant:8)
        }
        topAnchorForMiddleView?.isActive = true
        middleView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        middleView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 1/3).isActive = true
   
        // setup tabs
        setUpTabView()
        setupSegmentControl()
        
        middleView.addSubview(profileImageView)
        // needed constarint x,y,w,h
        profileImageView.leftAnchor.constraint(equalTo: middleView.leftAnchor,constant: 8).isActive = true
        profileImageView.topAnchor.constraint(equalTo: middleView.topAnchor,constant: 8).isActive = true
        profileImageView.widthAnchor.constraint(equalTo: middleView.widthAnchor, multiplier: 1/3,constant:20).isActive = true
        profileImageView.bottomAnchor.constraint(equalTo: segmentControl.topAnchor, constant: -12).isActive = true
        
        middleView.addSubview(rightConatinerView)
        // needed constarint x,y,w,h
        rightConatinerView.rightAnchor.constraint(equalTo: middleView.rightAnchor,constant: -4).isActive = true
        rightConatinerView.topAnchor.constraint(equalTo: middleView.topAnchor).isActive = true
        rightConatinerView.widthAnchor.constraint(equalTo: middleView.widthAnchor, multiplier: 0.67,constant: -24).isActive = true
        rightConatinerView.bottomAnchor.constraint(equalTo: segmentControl.topAnchor).isActive = true
        
        // setup rightcontainer buttons
        if roleId == "3" {
            setUpRightContainerButtonsFoRole()
        }else{
            setUpRightContainerButtons()
        }
    }
    
    private func setupSegmentControl() {
        
        middleView.addSubview(segmentControl)
        // needed constarint x,y,w,h
        segmentControl.leftAnchor.constraint(equalTo: middleView.leftAnchor,constant:-1).isActive = true
        segmentControl.bottomAnchor.constraint(equalTo: middleView.bottomAnchor).isActive = true
        segmentControl.widthAnchor.constraint(equalTo: middleView.widthAnchor,constant:2).isActive = true
        segmentControl.heightAnchor.constraint(equalToConstant: 44).isActive = true
    }
    
    private func setUpRightContainerButtons() {
    
        rightConatinerView.addSubview(journalEntry)
        // needed constarint x,y,w,h
        journalEntry.centerXAnchor.constraint(equalTo: rightConatinerView.centerXAnchor,constant: 8).isActive = true
        journalEntry.centerYAnchor.constraint(equalTo: rightConatinerView.centerYAnchor).isActive = true
        journalEntry.widthAnchor.constraint(equalTo: rightConatinerView.widthAnchor,multiplier:1/2,constant:40).isActive = true
        journalEntry.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        rightConatinerView.addSubview(sendSMSBtn)
        // needed constarint x,y,w,h
        sendSMSBtn.centerXAnchor.constraint(equalTo: rightConatinerView.centerXAnchor,constant: 8).isActive = true
        sendSMSBtn.bottomAnchor.constraint(equalTo: journalEntry.topAnchor,constant: -12).isActive = true
        sendSMSBtn.widthAnchor.constraint(equalTo: rightConatinerView.widthAnchor,multiplier:1/2,constant:40).isActive = true
        sendSMSBtn.heightAnchor.constraint(equalToConstant: 35).isActive = true

        rightConatinerView.addSubview(profile)
        // needed constarint x,y,w,h
        profile.centerXAnchor.constraint(equalTo: rightConatinerView.centerXAnchor,constant: 8).isActive = true
        profile.topAnchor.constraint(equalTo: journalEntry.bottomAnchor,constant: 12).isActive = true
        profile.widthAnchor.constraint(equalTo: rightConatinerView.widthAnchor,multiplier:1/2,constant:40).isActive = true
        profile.heightAnchor.constraint(equalToConstant: 35).isActive = true
    }
    
    private func setUpRightContainerButtonsFoRole() {
        
        rightConatinerView.addSubview(sendSMSBtn)
        // needed constarint x,y,w,h
        sendSMSBtn.centerXAnchor.constraint(equalTo: rightConatinerView.centerXAnchor,constant: 8).isActive = true
        sendSMSBtn.centerYAnchor.constraint(equalTo: rightConatinerView.centerYAnchor,constant: -30).isActive = true
        sendSMSBtn.widthAnchor.constraint(equalTo: rightConatinerView.widthAnchor,multiplier:1/2,constant:40).isActive = true
        sendSMSBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        rightConatinerView.addSubview(profile)
        // needed constarint x,y,w,h
        profile.centerXAnchor.constraint(equalTo: rightConatinerView.centerXAnchor,constant: 8).isActive = true
        profile.centerYAnchor.constraint(equalTo: rightConatinerView.centerYAnchor,constant: 30).isActive = true
        profile.widthAnchor.constraint(equalTo: rightConatinerView.widthAnchor,multiplier:1/2,constant:40).isActive = true
        profile.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    private func setUpTabView() {
        view.addSubview(tabView)
        // needed constarint x,y,w,h
        tabView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tabView.topAnchor.constraint(equalTo: middleView.bottomAnchor,constant: 4).isActive = true
        tabView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        tabView.bottomAnchor.constraint(equalTo:view.bottomAnchor).isActive = true
        tabView.backgroundColor = UIColor.groupTableViewBackground.withAlphaComponent(0.1)
    }
    
    //MARK: - View Controller Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        addSwipeFeatureToTabView()
    }

    //MARK:- Custom Methods 
    
    func setUpTeacherProfileViews() {
        setMiddelView()
        setUpHeaderLbl()
        addSegmentsToSegmentControl(1)
//        if let emproleId = userDefaults.string(forKey: "EMPLOYEE_ROLE_ID"), emproleId == "5" {
//            imagePicker.delegate = self
//            imagePicker.allowsEditing = true
//            profileImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleProfile)))
//            profileImageView.isUserInteractionEnabled = true
//        }
    }
    
    func handleProfile() {
        
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default)
        {
            UIAlertAction in
            
            self.openCamera()
            
        }
        let gallaryAction = UIAlertAction(title: "Gallary", style: .default)
        {
            UIAlertAction in
            self.imagePicker.sourceType = .photoLibrary
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        {
            UIAlertAction in
            
        }
        // Add the actions
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            self.imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            openGallary()
        }
    }
    
    func openGallary()
    {
        self.imagePicker.sourceType = .photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImg = info[UIImagePickerController.InfoKey.originalImage.rawValue] as? UIImage {
            profileImageView.contentMode = .scaleToFill
            profileImageView.image = pickedImg
            attachmentImgsData.append(pickedImg)
            
        }
//        dismiss(animated: true, completion: nil)
        dismiss(animated: true) {
            self.uploadRequest()
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    func setUpStudentProfileViews() {
        
        let userDefaults = UserDefaults.standard
        guard let emproleId = userDefaults.string(forKey: "EMPLOYEE_ROLE_ID") else {
            return
        }
        roleId = emproleId
        
        setMiddelView()
        topAnchorForMiddleView?.constant = 0
        addSegmentsToSegmentControl(2)
    }
    
    func addSwipeFeatureToTabView() {
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes))
        swipeLeft.direction = .left
        tabView.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes))
        swipeRight.direction = .right
        tabView.addGestureRecognizer(swipeRight)
    }
    
    func addSegmentsToSegmentControl(_ id : Int) {
        
        let firstTabImg = id  == 1 ? UIImage(named:"ic_usage_white") : UIImage(named:"ic_attendance_white")
        let secondTabImg = id  == 1 ? UIImage(named:"ic_assignmentwhite") : UIImage(named:"ic_evaluation_white")
        
        segmentControl.addTarget(self, action: #selector(handleSegmentSelection), for: .valueChanged)
        segmentControl.insertSegment(with:firstTabImg, at: 0, animated: true)
        segmentControl.insertSegment(with: secondTabImg, at: 1, animated: true)
        segmentControl.insertSegment(with: UIImage(named:"ic_student_journal_white"), at: 2, animated: true)
        segmentControl.selectedSegmentIndex = 0
    }
    
    @objc func handleSwipes(sender:UISwipeGestureRecognizer) {}

    @objc func handleSegmentSelection(sender:UISegmentedControl){}
    
    @objc func handleButtonClick(sender: UIButton) {
        
    }
    
    func uploadRequest()
    {
//        url - https://isirs.org/intellischools_teachers_app/teachers_script/upload_api.php
//        parameter - school_db_settings_array, employee_id, upload_code = TPROF , school_code ,uploaded_file

        let parameters = [
            "employee_id": userDefaults.string(forKey: "EMPLOYEE_ID") ?? "error",
            "school_db_settings_array": userDefaults.object(forKey: "SCHOOL_DB_SETTINGS") ?? "error",
            "action":"upload_profile",
            "upload_code": "TPROF",
            "school_code":userDefaults.object(forKey: "SCHOOL_CODE") ?? "N/A",
            "uploaded_file": attachmentImgsData
            ] as [String : Any]
        
        let request =  createRequest(param: parameters as! NSMutableDictionary, strURL: WebServiceUrls.profileUpload)
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest) { [weak self](data, response, error) in
            guard let data:Data = data as Data?, let _:URLResponse = response, error == nil else {
                UtilTeacher.LoaderEnd()
                UtilTeacher.invokeAlertMethod("Failed!", strBody: "Can't upload attachement", delegate: nil,vcobj : self!)
                return
            }
            
            do {
                
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Any]
                let rsult = json["attached"] as? String
                DispatchQueue.main.async {
                    UtilTeacher.LoaderEnd()
                    if rsult == "true" {
                        self?.showSuccessAlert()
                    }
                }
            } catch let error as NSError {
                print(error)
            }
        }
        task.resume()
    }
    
    func showSuccessAlert() {
        
        let alert = UIAlertController(title: "Success", message: "Profile image saved successfully..!!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler:nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func generateBoundaryString() -> String
    {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func createBodyWithParameters(parameters: NSMutableDictionary?,boundary: String) -> NSData {
        let body = NSMutableData()
        let mimetype = "image/png"
        if parameters != nil {
            for (key, value) in parameters! {
                
                if(value is String || value is NSString){
                    body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                    body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
                    body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
                }
                else if(value is [UIImage]){
                    var i = 0;
                    for image in value as! [UIImage]{
                        let filename = "image\(i).png"
                        let data = image.pngData()
                        
                        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                        body.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n".data(using: String.Encoding.utf8)!)
                        body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
                        body.append(data!)
                        body.append("\r\n".data(using: String.Encoding.utf8)!)
                        i += 1;
                    }
                }
            }
        }
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        return body
    }
    
    func createRequest (param : NSMutableDictionary , strURL : String) -> NSURLRequest {
        
        let boundary = generateBoundaryString()
        
        let url = NSURL(string: strURL)
        let request = NSMutableURLRequest(url: url! as URL)
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = createBodyWithParameters(parameters: param, boundary: boundary) as Data
        
        return request
    }
}

extension UIButton {
    func setupButton() {
        self.backgroundColor = CustomColor.greenColor
        self.tintColor = UIColor.white
        self.translatesAutoresizingMaskIntoConstraints = false
    }
}
