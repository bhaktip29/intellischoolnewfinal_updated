//
//  StudentNameListByClassOrDivisionVC.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 09/06/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class StudentNameListByClassOrDivisionVCTeacher: BaseViewControllerTeacher,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    let cellId = "CellId"
    var studentList = [StudentTeacher]()
    var schoolClass: SchoolClassTeacher?
    var screenFlag = ""
    let collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0.5
        let clView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        clView.backgroundColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
        clView.translatesAutoresizingMaskIntoConstraints = false
        return clView
    }()
    
    let headerView:UIView = {
        let headerView = UIView()
        headerView.backgroundColor = UIColor(red:0.86, green:0.86, blue:0.86, alpha:1.0)
        headerView.translatesAutoresizingMaskIntoConstraints = false
        return headerView
    }()
    
    private func setUpCollectionView() {
        
        view.addSubview(collectionView)
        collectionView.dataSource = self
        collectionView.delegate = self
        
        // needed constarunts x,y,w,h
        collectionView.topAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    private func setUpHeaderView() {
        
        view.addSubview(headerView)
        // needed constraints x,y,w,h
        headerView.topAnchor.constraint(equalTo: view.topAnchor,constant:64).isActive = true
        headerView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        headerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        headerView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        let rollLbl = createLabel(title: "Roll", textAlignment: .justified)
        
        headerView.addSubview(rollLbl)
        // needed constraints x,y,w,h
        rollLbl.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: 8).isActive = true
        rollLbl.topAnchor.constraint(equalTo: headerView.topAnchor).isActive = true
        rollLbl.widthAnchor.constraint(equalToConstant: 40).isActive = true
        rollLbl.bottomAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
        
        let separatorView = UIView()
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        separatorView.backgroundColor = UIColor.gray
        
        headerView.addSubview(separatorView)
        // needed constraints x,y,w,h
        separatorView.leftAnchor.constraint(equalTo: rollLbl.rightAnchor).isActive = true
        separatorView.topAnchor.constraint(equalTo: headerView.topAnchor).isActive = true
        separatorView.widthAnchor.constraint(equalToConstant: 1).isActive = true
        separatorView.bottomAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
        
        let nameLbl = createLabel(title: "Name", textAlignment: .justified)
        
        headerView.addSubview(nameLbl)
        // needed constraints x,y,w,h
        nameLbl.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        nameLbl.leftAnchor.constraint(equalTo: rollLbl.rightAnchor,constant:10).isActive = true
        nameLbl.topAnchor.constraint(equalTo: headerView.topAnchor).isActive = true
        nameLbl.bottomAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
    }
    
    private func createLabel(title: String?, textAlignment : NSTextAlignment) -> UILabel {
        let lbl = UILabel()
        lbl.text = title
        lbl.font = UIFont.boldSystemFont(ofSize: 13)
        lbl.textAlignment = textAlignment
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }
    
    //MARK:- View Controllers Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        collectionView.register(StudentNameCell.self, forCellWithReuseIdentifier: cellId)
        if let className = schoolClass?.className, let div = schoolClass?.divisionName {
            self.title = "Class \(className) \(div)"
        }
        setUpHeaderView()
        setUpCollectionView()
        loadAttendance(dateString: getYYYYMMDDFormattedDate(dateString: Date()))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
    }
    
    //MARK:- CollectionView Methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return studentList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! StudentNameCell
        
        let student: StudentTeacher = studentList[indexPath.row]
        cell.backgroundColor = UIColor.white
        cell.rollLabel.text = student.studentRollNo.description
        cell.nameLabel.text = student.studentName
        if indexPath.row % 2 == 0 {
            //even number
        } else {
            cell.backgroundColor = UIColor(red:0.90, green:0.90, blue:0.90, alpha:1.0)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
       // if self.screenFlag == "fromStudentProfile"{
            let vc = StudentProfileDisplayVCTeacher()
            vc.studentInfo = studentList[indexPath.row]
            vc.schoolClass = schoolClass
            navigationController?.pushViewController(vc, animated: true)
//        }else if self.screenFlag == "fromJournalEntry"{
//            let vc = JournalEntryListCountVC()
//            vc.intellinectID = studentList[indexPath.row].intellinectId ?? ""
//            vc.title = studentList[indexPath.row].studentName ?? ""
//            vc.studentInfo = studentList[indexPath.row]
//            navigationController?.pushViewController(vc, animated: true)
//            let cell = self.collectionView.cellForItem(at: indexPath)  as! StudentNameCell
//            let vc = JouranalEntryMainVC()
//            vc.studentInfo = studentList[indexPath.row]
//            vc.studName = cell.nameLabel.text ?? ""
//            vc.studentOrStaff = 1
//            navigationController?.pushViewController(vc, animated: true)
        //}
    }
    //MARK:- Server Methods
    
    func loadAttendance(dateString: String){
        
        if UtilTeacher.validateNetworkConnection(self) {
            showActivityIndicatory(uiView: view)
            let userDefaults = UserDefaults.standard
            let parameters = [
                "school_db_settings_array": userDefaults.object(forKey: "SCHOOL_DB_SETTINGS") ?? "school_db_settings_array",
                "action": "StudentList",
                "class_id": schoolClass!.classID.description,
                "division_id": schoolClass!.divisionID.description,
                ] as [String : Any]
            Alamofire.request(WebServiceUrls.attendanceService,method: .post,parameters: parameters).responseJSON { [weak self] (response) in
                self?.stopActivityIndicator()
                if response.result.error != nil {
                    UtilTeacher.invokeAlertMethod("Failed!", strBody: "Error Occured While Processing Data ", delegate: nil,vcobj : self!)
                }
                if let jsonresponse = response.result.value {
                    
                    let originalResponse = JSON(jsonresponse)
                    self?.getSuccessResponse(originalResponse: originalResponse)
                }
            }
        }
    }
    
    func getSuccessResponse(originalResponse : JSON)
    {
        if(originalResponse.count > 0)
        {
            for (key,subJson):(String, JSON) in originalResponse {
                if key == "student_list_array" {
                    for item in subJson.arrayValue {
                        let student = StudentTeacher()
                        student.studentName = item["student_name"].stringValue.capitalized
                        student.studentId = Int(item["student_id"].stringValue)!
                        student.studentRollNo = Int(item["student_rollno"].stringValue)!
                        student.intellinectId = item["intellinectid"].stringValue
                        studentList.append(student)
                    }
                }
            }
            collectionView.reloadData()
        }
    }
}

