//
//  PVAttendanceCVC.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 06/07/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol CalenderDelegateTeacher {
    func handleDisplayCalender(_ attendance: PVAttendance)
}

class PVAttendanceCVCTeacher: UICollectionView,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    let cellId = "CellId"
    let headerCellId  = "HeaderCell"
    var attendanceCountList = [PVAttendance]()
    var classId = ""
    var board = ""
    var intellinectsId = ""
    var cDelegate: CalenderDelegateTeacher?
    var noDataLbl = ""
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        self.register(PVAttendanceCell.self, forCellWithReuseIdentifier: cellId)
        self.register(PVAttendanceHeaderCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader , withReuseIdentifier: headerCellId)
        delegate = self
        dataSource = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- CollectionView Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if attendanceCountList.count == 0{
            self.setEmptyMessage(self.noDataLbl)
        }else{
            self.restore()
        }
        return attendanceCountList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! PVAttendanceCell
        let attendance = attendanceCountList[indexPath.row]
        cell.monthLbl.text = attendance.month
        cell.workingDaysLbl.text = attendance.workingDays
        cell.presentDaysLbl.text = attendance.presentDays
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerCellId, for: indexPath as IndexPath) as! PVAttendanceHeaderCell
        headerView.headerLbl.text = "Month"
        headerView.headerLbl1.text = "Working Days"
        headerView.headerLbl2.text = "Present Days"
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
    {
        return CGSize(width: collectionView.bounds.width, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        cDelegate?.handleDisplayCalender(attendanceCountList[indexPath.row])
    }
    
    //MARK:- Server Methods
    
    func getData(){
        
        if UtilTeacher.validateNetworkConnection(self) {
            showActivityIndicatory(actInd)
            let userDefaults = UserDefaults.standard
            let parameters = [
                "school_db_settings_array": userDefaults.object(forKey: "SCHOOL_DB_SETTINGS") ?? "school_db_settings_array",
                "action": "MonthWiseAttendanceCount",
                "intellinectid": intellinectsId,
                "class_id": classId,
                "board": board
                ] as [String : Any]
            Alamofire.request(WebServiceUrls.studentInfoService,method: .post,parameters: parameters).responseJSON { [weak self] (response) in
                self?.stopActivityIndicator(self?.actInd)
                if let jsonresponse = response.result.value {
                    
                    let originalResponse = JSON(jsonresponse)
                    self?.getSuccessResponse(originalResponse: originalResponse)
                }else{
                    self?.noDataLbl = "Attendance not found."
                    self?.reloadData()
                }
            }
        }
    }
    
    func getSuccessResponse(originalResponse : JSON)
    {
        if(originalResponse.count > 0)
        {
            for dic in originalResponse.arrayValue {
                let attendance = PVAttendance()
                attendance.month = dic["month"].stringValue
                attendance.presentDays = dic["presentDays"].stringValue
                attendance.workingDays = dic["workingDays"].stringValue
                attendance.monthPOS = dic["monthPOS"].stringValue
                attendanceCountList.append(attendance)
            }
            reloadData()
        }
    }
}

// Model Class

class PVAttendance {
    var workingDays: String?
    var monthPOS: String?
    var presentDays: String?
    var month: String?
}

// Custom Cell Class

class PVAttendanceCell: UICollectionViewCell {
    
    let monthLbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.black
        lbl.font = UIFont(name: "Helvetica Neue", size: 13)
        lbl.textAlignment = .center
        return lbl
    }()
    
    let workingDaysLbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.black
        lbl.font = UIFont(name: "Helvetica Neue", size: 13)
        lbl.textAlignment = .center
        return lbl
    }()
    
    let presentDaysLbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.black
        lbl.font = UIFont(name: "Helvetica Neue", size: 13)
        lbl.textAlignment = .center
        return lbl
    }()
    
    let accessoryView: UIImageView = {
        let v = UIImageView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.image = UIImage(named: "right")
        return v
    }()
    
    private func setUpLbels() {
        
        self.addSubview(accessoryView)
        // needed constarints
        accessoryView.rightAnchor.constraint(equalTo: self.rightAnchor,constant:-10).isActive = true
        accessoryView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        accessoryView.widthAnchor.constraint(equalToConstant:17).isActive = true
        accessoryView.heightAnchor.constraint(equalToConstant:17).isActive = true
        
        self.addSubview(monthLbl)
        // needed constarints
        monthLbl.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        monthLbl.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1/3, constant: -25).isActive = true
        monthLbl.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        monthLbl.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        
        let separatorView = UIView()
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        separatorView.backgroundColor = UIColor.gray
        
        self.addSubview(separatorView)
        // needed constraints x,y,w,h
        separatorView.leftAnchor.constraint(equalTo: monthLbl.rightAnchor).isActive = true
        separatorView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        separatorView.widthAnchor.constraint(equalToConstant: 0.5).isActive = true
        separatorView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        self.addSubview(workingDaysLbl)
        // needed constarints
        workingDaysLbl.leftAnchor.constraint(equalTo: monthLbl.rightAnchor).isActive = true
        workingDaysLbl.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1/3).isActive = true
        workingDaysLbl.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        workingDaysLbl.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        
        let separatorView1 = UIView()
        separatorView1.translatesAutoresizingMaskIntoConstraints = false
        separatorView1.backgroundColor = UIColor.gray
        
        self.addSubview(separatorView1)
        // needed constraints x,y,w,h
        separatorView1.leftAnchor.constraint(equalTo: workingDaysLbl.rightAnchor).isActive = true
        separatorView1.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        separatorView1.widthAnchor.constraint(equalToConstant: 0.5).isActive = true
        separatorView1.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        self.addSubview(presentDaysLbl)
        // needed constarints
        presentDaysLbl.leftAnchor.constraint(equalTo: workingDaysLbl.rightAnchor).isActive = true
        presentDaysLbl.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1/3).isActive = true
        presentDaysLbl.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        presentDaysLbl.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        self.layer.borderColor = UIColor.darkGray.cgColor
        self.layer.borderWidth = 0.3
        setUpLbels()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class PVAttendanceHeaderCell: UICollectionReusableView {
    
    let headerLbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.black
        lbl.font = UIFont(name: "Helvetica Neue", size: 13)
        lbl.textAlignment = .center
        return lbl
    }()
    
    let headerLbl1: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.black
        lbl.font = UIFont(name: "Helvetica Neue", size: 13)
        lbl.textAlignment = .center
        return lbl
    }()
    
    let headerLbl2: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.black
        lbl.font = UIFont(name: "Helvetica Neue", size: 13)
        lbl.textAlignment = .center
        return lbl
    }()
    private func setUpLbels() {

        self.addSubview(headerLbl)
        // needed constarints
        headerLbl.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        headerLbl.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1/3, constant: -25).isActive = true
        headerLbl.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        headerLbl.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        
        let separatorView = UIView()
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        separatorView.backgroundColor = UIColor.gray
        
        self.addSubview(separatorView)
        // needed constraints x,y,w,h
        separatorView.leftAnchor.constraint(equalTo: headerLbl.rightAnchor).isActive = true
        separatorView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        separatorView.widthAnchor.constraint(equalToConstant: 0.5).isActive = true
        separatorView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        self.addSubview(headerLbl1)
        // needed constarints
        headerLbl1.leftAnchor.constraint(equalTo: headerLbl.rightAnchor).isActive = true
        headerLbl1.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1/3).isActive = true
        headerLbl1.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        headerLbl1.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        
        let separatorView1 = UIView()
        separatorView1.translatesAutoresizingMaskIntoConstraints = false
        separatorView1.backgroundColor = UIColor.gray
        
        self.addSubview(separatorView1)
        // needed constraints x,y,w,h
        separatorView1.leftAnchor.constraint(equalTo: headerLbl1.rightAnchor).isActive = true
        separatorView1.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        separatorView1.widthAnchor.constraint(equalToConstant: 0.5).isActive = true
        separatorView1.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        self.addSubview(headerLbl2)
        // needed constarints
        headerLbl2.leftAnchor.constraint(equalTo: headerLbl1.rightAnchor).isActive = true
        headerLbl2.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1/3).isActive = true
        headerLbl2.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        headerLbl2.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.cyan.withAlphaComponent(0.2)
        self.layer.borderColor = UIColor.darkGray.cgColor
        self.layer.borderWidth = 0.3
        setUpLbels()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
