//
//  StudentJournalListVC.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 06/07/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import QuickLook

class StudentJournalListVCTeacher: BaseViewControllerTeacher,UITableViewDelegate,UITableViewDataSource,QLPreviewControllerDataSource {
    
    let cellId = "CellId"
    var category = PVCategory()
    let vw = UIView()
    var intellinectsId = ""
    var categoryCountList = [JornalCategory]()
    var gradientLayer: CAGradientLayer!
    let quickLookController = QLPreviewController()
    var fileURLs = [NSURL]()
    
    let tableView: UITableView = {
        let tbl = UITableView()
        tbl.translatesAutoresizingMaskIntoConstraints = false
        return tbl
    }()
    
    let categoryName : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.white
        lbl.backgroundColor = CustomColor.greenColor
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    //MARK:- Custom views Setup Methods
    
    private func setUpLabel() {
        
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = CustomColor.greenColor
        
        view.addSubview(vw)
        // needed constraints x,y,w,h
        vw.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 0).isActive = true
        vw.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        vw.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        vw.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        vw.addSubview(categoryName)
        // needed constraints x,y,w,h
        categoryName.topAnchor.constraint(equalTo: vw.topAnchor).isActive = true
        categoryName.leftAnchor.constraint(equalTo: vw.leftAnchor,constant:8).isActive = true
        categoryName.widthAnchor.constraint(equalTo: vw.widthAnchor,constant:-16).isActive = true
        categoryName.heightAnchor.constraint(equalTo:vw.heightAnchor).isActive = true
    }
    
    private func setUpTableView() {
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        tableView.backgroundColor = UIColor.clear
        
        view.addSubview(tableView)
        // needed constraints
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: vw.bottomAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    //MARK:- View Controller Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLabel()
        setUpTableView()
        tableView.register(CategoryCell.self, forCellReuseIdentifier: cellId)
        quickLookController.dataSource = self
    }
    var vLoaded = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.backgroundColor = UIColor.gray
//        createGradientLayer()
        if !vLoaded {
            getData()
            vLoaded = true
        }
    }
    
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [UIColor.white.cgColor,UIColor.blue.cgColor]
        gradientLayer.locations = [0.0,1.0]
        self.view.layer.addSublayer(gradientLayer)
        view.bringSubviewToFront(vw)
        view.bringSubviewToFront(tableView)
        view.bringSubviewToFront(actInd)
    }
    
    public func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return fileURLs.count
    }
    
    public func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem
    {
        return fileURLs[index]
    }
    
    // MARK:- TableView Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryCountList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! CategoryCell
        
        let jCategory = categoryCountList[indexPath.row]
        cell.dateLbl.text = convertDate(jCategory.journalDate ?? "")
        cell.classLbl.text = "Class: \(jCategory.className ?? "")-\(jCategory.divisionName ?? "")"
        cell.nameLbl.text = jCategory.employeeName
        cell.descriptionLbl.text = jCategory.journalDetails
        
        if jCategory.attachmentUrl == "" || jCategory.attachmentUrl == nil {
            cell.dImageView.isHidden = true
        }else {
            cell.dImageView.isHidden = false
            cell.dImageView.tag = indexPath.row
            cell.dImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(download)))
            cell.dImageView.isUserInteractionEnabled = true
        }
        return cell
    }
    
    func convertDate(_ inputDate:String)-> String {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd"
        let showDate = inputFormatter.date(from: inputDate)
        inputFormatter.dateFormat = "dd MMM yyyy"
        let resultString = inputFormatter.string(from: showDate!)
        return resultString
    }
    
    @objc func download(_ sender:UIGestureRecognizer) {
        
        guard let rowId = sender.view?.tag else {
            return
        }
        let jCategory = categoryCountList[rowId]
        
        let attachment = jCategory.attachmentUrl?.removingPercentEncoding!
        let name :[String] = attachment!.components(separatedBy: "/")
        let docpath = name.last
        let fileManager = FileManager.default
        let rootPath = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let pdfPathInDocument = rootPath.appendingPathComponent(docpath!)
        
        if fileManager.fileExists(atPath: pdfPathInDocument.path){
            
            fileURLs.removeAll()
            
            fileURLs.append(pdfPathInDocument as NSURL)
            
            if QLPreviewController.canPreview((fileURLs[0])) {
                quickLookController.reloadData()
                quickLookController.currentPreviewItemIndex = 0
                navigationController?.pushViewController(quickLookController, animated: true)
            }
        }
        else
        {
            if UtilTeacher.validateNetworkConnection(self) {
                
                let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                    var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                    documentsURL.appendPathComponent(docpath!)
                    return (documentsURL, [.removePreviousFile])
                }
                showActivityIndicatory(uiView: view)
                Alamofire.download(attachment!, to: destination).responseData { [weak self]response in
                    if let destinationUrl = response.destinationURL {
                        self?.stopActivityIndicator()
                        self?.fileURLs.removeAll()
                        self?.fileURLs.append(destinationUrl as NSURL)
                        
                        if QLPreviewController.canPreview((self?.fileURLs[0])!) {
                            self?.quickLookController.reloadData()
                            self?.quickLookController.currentPreviewItemIndex = 0
                            self?.navigationController?.pushViewController((self?.quickLookController)!, animated: true)
                        }
                    }
                }
                
            }
            
        }
    }
    
    //MARK:- Server Methods
    
    func getData(){
        
        if UtilTeacher.validateNetworkConnection(self) {
            categoryCountList.removeAll()
            showActivityIndicatory(uiView: view)
            let userDefaults = UserDefaults.standard
            let parameters = [
                "action": "JournalListParticularCategory",
                "intellinectid": intellinectsId,
                "category_id": category.categoryId?.description ?? "",
                "school_code": UserDefaults.standard.string(forKey: "schoolCode") as! String
                ] as [String : Any]
            
            Alamofire.request(WebServiceUrls.journalService,method: .post,parameters: parameters).responseJSON { [weak self] (response) in
                self?.stopActivityIndicator()
                print("StudentJournalListVCTeacher parameter : \(parameters)")
                print("StudentJournalListVCTeacher response : \(response)")
                print("StudentJournalListVCTeacher respose.result : \(response.result.value)")

                if let jsonresponse = response.result.value {
                    
                    let originalResponse = JSON(jsonresponse)
                    self?.getSuccessResponse(originalResponse: originalResponse)
                }
            }
        }
    }
    
    func getSuccessResponse(originalResponse : JSON)
    {
        if(originalResponse.count > 0)
        {
            for dic in originalResponse.arrayValue {
                
                let jCategory = JornalCategory()
                
                if dic["attachment_url"].stringValue != "NULL" {
                    let arr = dic["attachment_url"].arrayValue
                    jCategory.attachmentUrl = arr.count > 0 ? arr[0].stringValue : ""
                }
                jCategory.categoryId = dic["category_id"].stringValue
                jCategory.categoryName = dic["category_name"].stringValue
                jCategory.className = dic["class_name"].stringValue
                jCategory.divisionName = dic["division_name"].stringValue
                jCategory.employeeName = dic["employee_id"].stringValue
                jCategory.employeeName = dic["employee_name"].stringValue
                jCategory.journalDate = dic["journal_date"].stringValue
                jCategory.journalDetails = dic["journal_details"].stringValue
                jCategory.journalId = dic["journal_id"].stringValue
                categoryCountList.append(jCategory)
            }
            tableView.reloadData()
            tableView.animateTable()
        }
    }
}

class JornalCategory {
    
    var attachmentUrl: String?
    var categoryId: String?
    var categoryName: String?
    var className: String?
    var divisionName: String?
    var employeeName: String?
    var employeeId: String?
    var journalDate: String?
    var journalDetails: String?
    var journalId: String?
}

class CategoryCell: UITableViewCell {
    
    let dateLbl: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .justified
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let classLbl: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.textAlignment = .right
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let nameLbl:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.black
        lbl.textAlignment = .justified
        lbl.numberOfLines = 0
        lbl.font = UIFont.boldSystemFont(ofSize: 13)
        return lbl
    }()
    
    let descriptionLbl:UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .justified
        let font = UIFont.systemFont(ofSize: 12)
        lbl.font = font
        lbl.adjustsFontSizeToFitWidth = true
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 0
        lbl.backgroundColor = UIColor.lightGray.withAlphaComponent(0.4)
        lbl.layer.cornerRadius = 3
        lbl.layer.masksToBounds = true
        let text = lbl.text ?? "" as String
        let size = text.size(withAttributes: [NSAttributedString.Key.font:font])
        lbl.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height+20)
        lbl.sizeToFit()
        return lbl
    }()
    
    let dImageView: UIImageView = {
        let imgView = UIImageView()
        imgView.layer.cornerRadius = 3
        imgView.layer.masksToBounds = true
        imgView.contentMode = .scaleToFill
        imgView.image = UIImage(named: "ic_attachment")
        imgView.translatesAutoresizingMaskIntoConstraints  = false
        return imgView
    }()
    
    let containerView:UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = UIColor.white
        v.layer.cornerRadius = 3
        v.layer.masksToBounds = true
        return v
    }()
    
    func setUpViews() {
        
        self.addSubview(containerView)
        // needed constraints
        containerView.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 8).isActive = true
        containerView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        containerView.widthAnchor.constraint(equalTo:self.widthAnchor,constant: -16).isActive = true
        containerView.heightAnchor.constraint(equalTo: self.heightAnchor,constant: -8).isActive = true
        
        containerView.addSubview(dateLbl)
        // needed constraints
        dateLbl.leftAnchor.constraint(equalTo: containerView.leftAnchor,constant: 8).isActive = true
        dateLbl.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        dateLbl.widthAnchor.constraint(equalTo:containerView.widthAnchor,multiplier:1/2,constant:-8).isActive = true
        dateLbl.heightAnchor.constraint(equalToConstant:30).isActive = true
        
        containerView.addSubview(classLbl)
        // needed constraints
        classLbl.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        classLbl.rightAnchor.constraint(equalTo: containerView.rightAnchor,constant: -8).isActive = true
        classLbl.widthAnchor.constraint(equalTo: containerView.widthAnchor, multiplier: 1/2,constant: -8).isActive = true
        classLbl.heightAnchor.constraint(equalToConstant:30).isActive = true
        
        containerView.addSubview(dImageView)
        // needed constraints
        dImageView.topAnchor.constraint(equalTo: classLbl.bottomAnchor).isActive = true
        dImageView.rightAnchor.constraint(equalTo: containerView.rightAnchor,constant:-8).isActive = true
        dImageView.widthAnchor.constraint(equalToConstant:30).isActive = true
        dImageView.heightAnchor.constraint(equalToConstant:30).isActive = true
        
        containerView.addSubview(nameLbl)
        // needed constraints
        nameLbl.topAnchor.constraint(equalTo: dateLbl.bottomAnchor).isActive = true
        nameLbl.leftAnchor.constraint(equalTo: containerView.leftAnchor,constant: 8).isActive = true
        nameLbl.rightAnchor.constraint(equalTo: dImageView.leftAnchor).isActive = true
        nameLbl.heightAnchor.constraint(equalToConstant:30).isActive = true
        
        containerView.addSubview(descriptionLbl)
        // needed constraints
        descriptionLbl.topAnchor.constraint(equalTo: nameLbl.bottomAnchor,constant: 8).isActive = true
        descriptionLbl.rightAnchor.constraint(equalTo: containerView.rightAnchor,constant: -8).isActive = true
        descriptionLbl.widthAnchor.constraint(equalTo: containerView.widthAnchor,constant: -16).isActive = true
        descriptionLbl.bottomAnchor.constraint(equalTo: containerView.bottomAnchor,constant: -8).isActive = true
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        setUpViews()
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

