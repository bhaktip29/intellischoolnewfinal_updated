//
//  StudentJournalCVC.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 06/07/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol JournalDelegate {
    func handleDisplayJournalByCategory(_ category:PVCategory)
}

class StudentJournalCVCTeacher: UICollectionView,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    let cellId = "CellId"
    let headerCellId  = "HeaderCell"
    var journalCountList = [PVCategory]()
    var intellinectsId = ""
    var jDelegate:JournalDelegate?
    var noDataLbl = ""
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        self.register(PVJournalCell.self, forCellWithReuseIdentifier: cellId)
        self.register(PVJournalHeaderCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader , withReuseIdentifier: headerCellId)
        delegate = self
        dataSource = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- CollectionView Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.journalCountList.count == 0{
            self.setEmptyMessage(self.noDataLbl)
        }else{
            self.restore()
        }
        return journalCountList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! PVJournalCell
        let category = journalCountList[indexPath.row]
        cell.categoryLbl.text =  category.categoryName
        cell.notesLbl.text = category.count
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerCellId, for: indexPath as IndexPath) as! PVJournalHeaderCell
        headerView.headerLbl.text = "Category"
        headerView.headerLbl1.text = "Notes"
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
    {
        return CGSize(width: collectionView.bounds.width, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        jDelegate?.handleDisplayJournalByCategory(journalCountList[indexPath.row])
    }
    
    //MARK:- Server Methods
    
    func getData(){
        
        if UtilTeacher.validateNetworkConnection(self) {
            showActivityIndicatory(actInd)
            let userDefaults = UserDefaults.standard
            var scholcoed = UserDefaults.standard.string(forKey: "schoolCode")
                    print("scholcoedscholcoed\(scholcoed)")
            let parameters = [
            
                "school_db_settings_array": userDefaults.object(forKey: "SCHOOL_DB_SETTINGS") ?? "school_db_settings_array",
                "action": "JournalListCount",
                "intellinectid": intellinectsId,
                "school_code": UserDefaults.standard.string(forKey: "schoolCode") as! String,
                ] as [String : Any]

            Alamofire.request(WebServiceUrls.journalService,method: .post,parameters: parameters).responseJSON { [weak self] (response) in
                self?.stopActivityIndicator(self?.actInd)
                print("StudentJournalCVCTeacher parameters:\(parameters)")
                print("StudentJournalCVCTeacher response:\(response)")
                print("StudentJournalCVCTeacher reponse.value:\(response.result.value)")
                if let jsonresponse = response.result.value {
                    let originalResponse = JSON(jsonresponse)
                    self?.getSuccessResponse(originalResponse: originalResponse)
                }else{
                    self?.noDataLbl = "Journal not found."
                    self?.reloadData()
                }
            }
        }
    }
    
    func getSuccessResponse(originalResponse : JSON)
    {
        if(originalResponse.count > 0)
        {
            for dic in originalResponse.arrayValue {
            
                let category = PVCategory()
                category.categoryName = dic["category_name"].stringValue
                category.count = dic["count"].stringValue
                category.categoryId = dic["category_id"].stringValue
                category.studentId = dic["student_id"].stringValue
                journalCountList.append(category)
            }
            reloadData()
        }
    }
}

// Model Class

class PVCategory {
    var categoryName: String?
    var count: String?
    var categoryId: String?
    var studentId: String?
}

// Custom Cell Class

class PVJournalCell: UICollectionViewCell {
    
    let categoryLbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.black
        lbl.font = UIFont(name: "Helvetica Neue", size: 13)
        lbl.textAlignment = .center
        return lbl
    }()
    
    let notesLbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.black
        lbl.font = UIFont(name: "Helvetica Neue", size: 13)
        lbl.textAlignment = .center
        return lbl
    }()
    
    let accessoryView: UIImageView = {
        let v = UIImageView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.image = UIImage(named: "right")
        return v
    }()
    
    private func setUpLbels() {
        
        self.addSubview(accessoryView)
        // needed constarints
        accessoryView.rightAnchor.constraint(equalTo: self.rightAnchor,constant:-10).isActive = true
        accessoryView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        accessoryView.widthAnchor.constraint(equalToConstant:17).isActive = true
        accessoryView.heightAnchor.constraint(equalToConstant:17).isActive = true
        
        self.addSubview(categoryLbl)
        // needed constarints
        categoryLbl.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        categoryLbl.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1/2, constant: -25).isActive = true
        categoryLbl.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        categoryLbl.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        
        let separatorView = UIView()
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        separatorView.backgroundColor = UIColor.gray
        
        self.addSubview(separatorView)
        // needed constraints x,y,w,h
        separatorView.leftAnchor.constraint(equalTo: categoryLbl.rightAnchor).isActive = true
        separatorView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        separatorView.widthAnchor.constraint(equalToConstant: 0.5).isActive = true
        separatorView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        self.addSubview(notesLbl)
        // needed constarints
        notesLbl.leftAnchor.constraint(equalTo: categoryLbl.rightAnchor).isActive = true
        notesLbl.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1/2).isActive = true
        notesLbl.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        notesLbl.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        self.layer.borderColor = UIColor.darkGray.cgColor
        self.layer.borderWidth = 0.3
        setUpLbels()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class PVJournalHeaderCell: UICollectionReusableView {
    
    let headerLbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.black
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.textAlignment = .center
        return lbl
    }()
    
    let headerLbl1: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.black
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.textAlignment = .center
        return lbl
    }()

    let accessoryView: UIImageView = {
        let v = UIImageView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    private func setUpLbels() {
   
        self.addSubview(headerLbl)
        // needed constarints
        headerLbl.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        headerLbl.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1/2, constant: -25).isActive = true
        headerLbl.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        headerLbl.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        
        let separatorView = UIView()
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        separatorView.backgroundColor = UIColor.gray
        
        self.addSubview(separatorView)
        // needed constraints x,y,w,h
        separatorView.leftAnchor.constraint(equalTo: headerLbl.rightAnchor).isActive = true
        separatorView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        separatorView.widthAnchor.constraint(equalToConstant: 0.5).isActive = true
        separatorView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true

        self.addSubview(headerLbl1)
        // needed constarints
        headerLbl1.leftAnchor.constraint(equalTo: headerLbl.rightAnchor).isActive = true
        headerLbl1.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1/2).isActive = true
        headerLbl1.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        headerLbl1.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.cyan.withAlphaComponent(0.2)
        self.layer.borderColor = UIColor.darkGray.cgColor
        self.layer.borderWidth = 0.3
        setUpLbels()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
