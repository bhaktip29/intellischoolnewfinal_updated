//
//  JournalEntryListCountVC.swift
//  HolyCross
//
//  Created by Intellinects on 21/05/19.
//  Copyright © 2019 Intellinects. All rights reserved.
//

import UIKit

class JournalEntryListCountVC: UIViewController, JournalDelegate,JournalAddedDelegate {

    var intellinectID = ""
    var studentInfo = StudentTeacher()
    var studentInfomation = StudentInformation()
    let btnAdd : UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(addNewJournal), for: .touchUpInside)
        btn.setImage(#imageLiteral(resourceName: "iconGreenPlus"), for: .normal)
        return btn
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.setupView()
        self.setupButton()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
    }
    func setupView(){
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        let collectionView = StudentJournalCVCTeacher(frame: CGRect.zero, collectionViewLayout: layout)
        setCollection(collectionView)
        collectionView.jDelegate = self
        collectionView.intellinectsId = self.intellinectID
        collectionView.getData()
    }
    fileprivate func setupButton(){
        self.view.addSubview(self.btnAdd)
        self.btnAdd.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor, constant: -16.0).isActive = true
        self.btnAdd.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -16.0).isActive = true
        self.btnAdd.widthAnchor.constraint(equalToConstant: 60.0).isActive = true
        self.btnAdd.heightAnchor.constraint(equalToConstant: 60.0).isActive = true
    }
    func setCollection(_ collectionView:UICollectionView?) {
        collectionView?.translatesAutoresizingMaskIntoConstraints = false
        collectionView?.backgroundColor = UIColor.white
        self.view.addSubview(collectionView!)
        collectionView?.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        collectionView?.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor).isActive = true
        collectionView?.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor).isActive = true
        collectionView?.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    func handleJournal(_ status: Bool) {
        if status{
            self.setupView()
            self.setupButton()
        }
    }
    func handleDisplayJournalByCategory(_ category: PVCategory) {
        let vc = StudentJournalListVCTeacher()
        vc.title = self.title
        guard let cat = category.categoryName else {
            return
        }
        vc.categoryName.text = "Category: \(cat)"
        vc.intellinectsId = self.intellinectID
        vc.category = category
        navigationController?.pushViewController(vc, animated: true)
    }
    @objc func addNewJournal(){
        let vc = StudentProfileAlertVCTeacher()
        vc.studentInfo = studentInfo
        vc.studentOrStaff = 1
        vc.delegate = self
        vc.studentInformation = studentInfomation
        vc.clickedButtonIndex = 2
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChild(vc)
        view.addSubview(vc.view)
    }
}
