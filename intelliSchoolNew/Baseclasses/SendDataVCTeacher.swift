//
//  SendDataVC.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 27/06/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MobileCoreServices

class SendDataVCTeacher: SendVCTeacher, UIPickerViewDelegate,UIPickerViewDataSource {

    var boardId = [Int]()
    var classId = [Int]()
    var path = [String]()
    var reply: String!
    var isAttached: Bool = false
    var messageId: String?
    let userDafaults = UserDefaults.standard
    var subjectList = [Subject]()
    var subjectId = ""
    
    let subjectPicker: UIPickerView = {
        let picker = UIPickerView()
        picker.translatesAutoresizingMaskIntoConstraints = false
        return picker
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(UserDefaults.standard.string(forKey: "HMC") == "3")
               {
                   print("In Circular")
                 caseNumber = 1
               }
               else if(UserDefaults.standard.string(forKey: "HMC") == "2")
               {
                   print("In Message")
                caseNumber = 2
               }
               else if(UserDefaults.standard.string(forKey: "HMC") == "1")
               {
                   print("In Homework")
                caseNumber = 3
               }
        if caseNumber == 3 {
            var subject = Subject()
            subject.subjectId = ""
            subject.subjectName = "--Scroll for Subject--"
            subjectList.append(subject)
            getSubjectList()
            setUpSubjectPicker()
        }
    }
    
    func setUpSubjectPicker(){
        
        subjectPicker.delegate = self
        subjectPicker.dataSource = self
        
        bottomView.addSubview(subjectPicker)
        // needed constraints x,y,w,h
        subjectPicker.topAnchor.constraint(equalTo: bottomView.topAnchor).isActive = true
        subjectPicker.leftAnchor.constraint(equalTo: attachmentBtn.rightAnchor).isActive = true
        subjectPicker.rightAnchor.constraint(equalTo: sendBtn.leftAnchor).isActive = true
        subjectPicker.bottomAnchor.constraint(equalTo:bottomView.bottomAnchor).isActive = true
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return subjectList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        
        let subject = subjectList[row]
        
        let titleData = subject.subjectName
        
        let myTitle = NSAttributedString(string: titleData!, attributes: [NSAttributedString.Key.font:UIFont(name: "Georgia", size: 15.0)!,NSAttributedString.Key.foregroundColor:UIColor.white])
        
        return myTitle
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let subject = subjectList[row]
        subjectId = subject.subjectId!
    }
    
    override func handleSend() {
        let messageToBeSend:String = textView.text
        
        switch caseNumber {

        case 1:
            if textField.text == "" || textField.text == PlaceHolderStrings.tfPlacehoder {
                UtilTeacher.invokeAlertMethod("Sending Failed!", strBody: "Please enter Subject", delegate: nil,vcobj:self)
                return
            }
            break
        case 3:
            if subjectId == "" {
                UtilTeacher.invokeAlertMethod("Sending Failed!", strBody: "Please select Subject", delegate: nil,vcobj:self)
                return
            }
            break
        default:
            print("default")
            break
        }
        
        if (messageToBeSend.isEqual("") || messageToBeSend.isEqual(PlaceHolderStrings.tvPlaceHolder)){
              print("if")
            UtilTeacher.invokeAlertMethod("Sending Failed!", strBody: "Please enter message", delegate: nil,vcobj:self)
        }else {
              print("else")
            uploadRequest(messageToBeSend)
        }
    }
    
    func reSetAllControls() {
        textView.text = PlaceHolderStrings.tvPlaceHolder
        textField.placeholder = PlaceHolderStrings.tfPlacehoder
        attachData.removeAll()
        mimeTypes.removeAll()
        attachExtensions.removeAll()
        attachmentList.removeAll()
        tableView.reloadData()
    }
    
    // Send Circular data
    
    func sendData(_ message:String,_ path:[String]) {
        
        // Finding collection of board_ids
        for i in (0..<prodArray.count){
            let dic = prodArray[i] as! [String:Any]
            guard let bId = dic["board_id"] else {
                return
            }
            if !boardId.contains(bId as! Int) {
                boardId.append(bId as! Int)
            }
        }
        // Finding collection of class_ids associated with board id and sending submitting to the server
        for j in (0..<boardId.count){
            classId.removeAll()
            for k in (0..<prodArray.count){
                let dic = prodArray[k] as! [String:Any]
                guard let bId = dic["board_id"], bId as! Int == boardId[j],let cId = dic["class_id"] else {
                    continue // return
                }
                
                if !classId.contains(cId as! Int) {
                    classId.append(cId as! Int)
                }
            }
            let userDefaults = UserDefaults.standard
            
            var attachmentPath = ""
            if path.count > 0 {
                attachmentPath = path.description
            }else {
                attachmentPath = ""
            }
            let stringClassIds = classId.map{ String($0) }
            var url = ""
            var parameters = [String : Any]()
            switch caseNumber {
            case 1:
                url = WebServiceUrls.circularService
                parameters = [
                    "employee_id": userDefaults.string(forKey: "EMPLOYEE_ID") ?? "error",
                    "school_db_settings_array": userDefaults.string(forKey: "DB_SETTING") ?? "NA", //userDefaults.object(forKey: "SCHOOL_DB_SETTINGS") ?? "error",
                    "circular_content": message,
                    "circular_subject" : textField.text ?? "",
                    "path": attachmentPath,
                    "class_ids": stringClassIds.description,
                    "action": "NewCreateCirculars",
                    "board_id":boardId[j]] as [String : Any]
                break
            case 2:
                print("case 2 msg")
                url = WebServiceUrls.messagesService
                print(url)
                parameters = [
                    "employee_id": userDefaults.string(forKey: "EMPLOYEE_ID") ?? "error",
                    "school_db_settings_array":userDefaults.string(forKey: "DB_SETTING") ?? "NA", // userDefaults.object(forKey: "SCHOOL_DB_SETTINGS") ?? "error",
                    "message_content": message,
                    "path": attachmentPath,
                    "class_details": JSON(prodArray).description,
                    "action": "CreateMessages"] as [String : Any]
                print(parameters)
                break
            case 3:
                url = WebServiceUrls.homeworkService
                
                parameters = [
                    "class_details": JSON(prodArray).description,
                    "employee_id": userDafaults.string(forKey: "EMPLOYEE_ID") ?? "error",
                    "school_db_settings_array": userDefaults.string(forKey: "DB_SETTING") ?? "NA",//userDafaults.string(forKey: "SCHOOL_DB_SETTINGS") ?? "error",
                    "homework_content": message,
                    "action":"CreateHomework",
                    "path": attachmentPath,
                    "subject_id": subjectId] as [String : Any]
                break
            default:
                break
            }
            
            Alamofire.request(url,method: .post,parameters: parameters)
                .responseJSON { [weak self] (response) in
                     print("Parameters for msg send : \(parameters)")
                    print("Response for msg send : \(response.result.value)")
                    if response.result.error != nil {
                        UtilTeacher.invokeAlertMethod("Failed!", strBody: "Error Occured While Processing Data ", delegate: nil,vcobj : self!)
                    }
                    if let jsonresponse = response.result.value {
                        let originalResponse = JSON(jsonresponse)
                        self?.getSuccessResponse(originalResponse: originalResponse)
                    }
            }
        }
    }
    
    func getSuccessResponse(originalResponse : JSON)
    {
        print("OriginalResponse", originalResponse)
        if(originalResponse.count > 0)
        {
            for (key,subJson):(String, JSON) in originalResponse {
                if key == "save_status" {
                    if subJson.boolValue {
                        let alertController = UIAlertController(title: "", message: "Details submitted successfully", preferredStyle: UIAlertController.Style.alert)
                        
                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
                        { [weak self](result : UIAlertAction) -> Void in
                            
                            var controllers = self?.navigationController?.viewControllers
                            controllers?.removeLast()
                            let vc = MenuListVCTeacher()
                            vc.caseNumber = (self?.caseNumber)!
                            controllers?.append(vc)
                            self?.navigationController?.viewControllers = controllers!
                        }
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                    break
                }
            }
        }
    }
    
    func getSubjectList() {
        
        showActivityIndicatory(uiView: view)
        if Util.validateNetworkConnection(self) {
            
            let userDefaults = UserDefaults.standard
            let Parameters = [
                "school_db_settings_array": userDefaults.string(forKey: "DB_SETTING") ?? "NA",
 //userDefaults.object(forKey: "SCHOOL_DB_SETTINGS") ?? "school_db_settings_array",
                "action": "SubjectName",
                "employee_id": userDefaults.object(forKey: "EMPLOYEE_ID") ?? "employee_id"] as [String : Any]
            
            Alamofire.request(WebServiceUrls.uploadAttachamentService,method: .post ,parameters: Parameters)
                .responseJSON { [weak self] (response) in
                    self?.stopActivityIndicator()
                    print("subject list param : \(Parameters)")
                     print("subject list Response : \(response.result.value )")
                    if response.result.error != nil {
                        UtilTeacher.invokeAlertMethod("Failed!", strBody: "Error Occured While Processing Data ", delegate: nil,vcobj : self!)
                    }
                    if let jsonresponse = response.result.value {
                        
                        let originalResponse = JSON(jsonresponse)
                        self?.getSuccessRes(originalResponse: originalResponse)
                    }
            }
        }
    }
    
    func getSuccessRes(originalResponse : JSON){
        if(originalResponse.count > 0)
        {
            for (_,subJson):(String, JSON) in originalResponse {
                var subject = Subject()
                subject.subjectName = subJson["subject_name"].stringValue
                subject.subjectId = subJson["subject_id"].stringValue
                subjectList.append(subject)
            }
            subjectPicker.reloadAllComponents()
        }
    }
    
    func uploadRequest(_ messageToBeSend:String)
    {
        var uploadCode = ""
        switch caseNumber {
        case 1:
            uploadCode = UploadCode.circularUploadCode
            break
        case 2:
            print("msg")
            uploadCode = UploadCode.messagesUploadCode
            break
        case 3:
            uploadCode = UploadCode.homeworkUploadCode
            break
        default:
            
            break
        }
        
        // new_upload_attachment
        let url = WebServiceUrls.uploadAttachamentService
        let parameters = [
            "employee_id": userDafaults.string(forKey: "EMPLOYEE_ID") ?? "error",
            "school_db_settings_array": userDafaults.string(forKey: "DB_SETTING") ?? "NA", //userDafaults.object(forKey: "SCHOOL_DB_SETTINGS") ?? "error",
            "action":"upload_attachment",
            "upload_code": uploadCode,
            "school_code": "INTELL", //userDafaults.object(forKey: "SCHOOL_CODE") ?? "N/A",
            "uploaded_file[]": attachData
            ] as NSMutableDictionary
        
        let request =  createRequest(param: parameters , strURL: url)
         print("Send Parameters : \(parameters)")
        
        let session = URLSession.shared
        showActivityIndicatory(uiView: view)
        let task = session.dataTask(with: request as URLRequest) { [weak self](data, response, error) in
            print("Send Response : \(response)")
            guard let data:Data = data as Data?, let _:URLResponse = response, error == nil else {
                self?.stopActivityIndicator()
                UtilTeacher.invokeAlertMethod("Failed!", strBody: "Can't upload attachement", delegate: nil,vcobj : self!)
                return
            }
            
    
            do {
          
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: Any]
                let path = jsonResult["path"] as? [String] ?? []
    
                self?.sendData(messageToBeSend,path)
                DispatchQueue.main.async {
                    self?.stopActivityIndicator()
                    self?.reSetAllControls()
                }
            } catch let error as NSError {
                print(error)
            }
        }
        task.resume()
    }
    
    func generateBoundaryString() -> String
    {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func createBodyWithParameters(parameters: NSMutableDictionary?,boundary: String) -> NSData {
        let body = NSMutableData()
        if parameters != nil {
            for (key, value) in parameters! {
                if(value is String || value is NSString){
                    body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                    body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
                    body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
                }else if(value is [Data]){
                    var i = 0;
                    for data in value as! [Data]{
                        let uuidStr = UUID().uuidString
                        let filename = "\(uuidStr).\(attachExtensions[i])"
                        print("filename", filename)
                         print("Mime", mimeTypes[i])
                        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                        body.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n".data(using: String.Encoding.utf8)!)
                        body.append("Content-Type: \(mimeTypes[i])\r\n\r\n".data(using: String.Encoding.utf8)!)
                        body.append(data)
                        body.append("\r\n".data(using: String.Encoding.utf8)!)
                        i += 1;
                    }
                }
            }
        }
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        return body
    }
    
    func createRequest (param : NSMutableDictionary , strURL : String) -> NSURLRequest {
        let boundary = generateBoundaryString()
        let url = NSURL(string: strURL)
        let request = NSMutableURLRequest(url: url! as URL)
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = createBodyWithParameters(parameters: param , boundary: boundary) as Data
        return request
    }
}
struct Subject {
    var subjectName: String?
    var subjectId: String?
}

