//
//  ViewController.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 25/07/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CountClass {
    var groupName: String?
    var count: String?
    var classGroupId : String?
}

class BaseMenuVCTeacher: BaseViewControllerTeacher {

    let listCellId = "ListCell"
    var caseNumber:Int = 0
    var classGroup : [JSON] = []
    var message : [JSON] = []
    var group = [String]()
    var months = [String]()
    var msgCounts = [ParticularMessage]()
    
    let headerView : UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = CustomColor.greenColor
        return v
    }()
    let collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        let clView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        clView.backgroundColor = UIColor.white
        clView.translatesAutoresizingMaskIntoConstraints = false
        return clView
    }()
    
    // MARK:- Custom Views Setup Methods
    
    private func setUpHeaderViews() {
        
        view.addSubview(headerView)
        // needed constraints
        if #available(iOS 11.0, *) {
            headerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        } else {
            headerView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        }
        headerView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        headerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        headerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        func getLabel(labelText:String) -> UILabel {
            let lbl = UILabel()
            lbl.textColor = .white
            lbl.textAlignment = .center
            lbl.text = labelText
            return lbl
        }
        
        var lbls = [UILabel]()
        for i in 0..<group.count {
            lbls.append(getLabel(labelText: group[i]))
        }
        
        let stackView = UIStackView(arrangedSubviews: lbls)
        stackView.alignment = .fill
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = 0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        headerView.addSubview(stackView)
        // needed constarints
        stackView.leftAnchor.constraint(equalTo: headerView.leftAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: headerView.topAnchor).isActive = true
        stackView.widthAnchor.constraint(equalTo: headerView.widthAnchor).isActive = true
        stackView.heightAnchor.constraint(equalTo: headerView.heightAnchor).isActive = true
    }
    
    private func setUpCollectionView() {
        
        view.addSubview(collectionView)

        // needed constarunts x,y,w,h
        collectionView.topAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    //MARK:- View Controllers Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        collectionView.register(ListMenuCell.self, forCellWithReuseIdentifier: listCellId)
    }
    
    func setViewsAfterResponse() {
        setUpHeaderViews()
        setUpCollectionView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
    }
}

class ListMenuCell:UICollectionViewCell {

    var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .fill
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    private func setUpLbels() {
        
        self.addSubview(stackView)
        // needed constarints
        stackView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        stackView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        stackView.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpLbels()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
