//
//  SendVC.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 16/06/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit
import AMXFontAutoScale
import OpalImagePicker
import Photos
import MobileCoreServices
class SendVCTeacher: BaseViewControllerTeacher,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate,UITableViewDelegate,UITableViewDataSource,OpalImagePickerControllerDelegate {
    
    var cellId = "AttachmentCell"
    var classNameList:String?
    let imagePicker = UIImagePickerController()
    var caseNumber:Int = 0
    var attachmentList = [String]()
    var attachData = [Data]()
    var mimeTypes = [String]()
    var attachExtensions = [String]()
    //var attachmentImgsData = [UIImage]()
    var prodArray:NSMutableArray = NSMutableArray()
    
    let classNameListLbl : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.white
        lbl.backgroundColor = CustomColor.greenColor
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let textView : UITextView = {
        let txtView = UITextView()
        txtView.translatesAutoresizingMaskIntoConstraints = false
        txtView.layer.borderColor = UIColor.gray.cgColor
        txtView.layer.borderWidth = 0.3
        txtView.layer.cornerRadius = 2
        txtView.layer.masksToBounds = true
        txtView.font = UIFont.systemFont(ofSize: 15)
        txtView.autocorrectionType = .no
        txtView.backgroundColor = UIColor.groupTableViewBackground.withAlphaComponent(0.1)
        txtView.text = PlaceHolderStrings.tvPlaceHolder
        txtView.amx_autoScaleFont(forReferenceScreenSize: .size4Inch)
        return txtView
    }()
    
    let bottomView : UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = CustomColor.greenColor
        return v
    }()
    
    let attachmentBtn : UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setImage(UIImage(named: "ic_file_attachment_white"), for: .normal)
        btn.addTarget(self, action: #selector(handleAttachment), for: .touchUpInside)
        return btn
    }()
    
    let sendBtn : UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setImage(UIImage(named: "ic_send"), for: .normal)
        btn.addTarget(self, action: #selector(handleSend), for: .touchUpInside)
        return btn
    }()
    
    let textField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.layer.borderColor = UIColor.gray.cgColor
        tf.layer.borderWidth = 0.3
        tf.layer.cornerRadius = 2
        tf.font = UIFont.systemFont(ofSize: 15)
        tf.layer.masksToBounds = true
        tf.keyboardType = UIKeyboardType.default
        tf.autocorrectionType =  UITextAutocorrectionType.no
        tf.backgroundColor = UIColor.groupTableViewBackground.withAlphaComponent(0.1)
        tf.placeholder = PlaceHolderStrings.tfPlacehoder
        return tf
    }()
    
    let tableView : UITableView = {
        let tblView = UITableView()
        tblView.translatesAutoresizingMaskIntoConstraints = false
        tblView.separatorInset = UIEdgeInsets.zero
        tblView.layer.borderColor = UIColor.gray.cgColor
        tblView.layer.borderWidth = 0.3
        tblView.layer.cornerRadius = 2
        return tblView
    }()
    
    var yAnchor : NSLayoutConstraint?
    var txtViewBottomAnchor:NSLayoutConstraint?
    
    //MARK:- Custom Views Setup Methods
    private func setUpViews() {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = CustomColor.greenColor
        
        view.addSubview(vw)
        // needed constraints x,y,w,h
        if #available(iOS 11.0, *) {
            vw.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        } else {
            vw.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        }
        vw.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        vw.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        vw.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        vw.addSubview(classNameListLbl)
        // needed constraints x,y,w,h
        classNameListLbl.topAnchor.constraint(equalTo: vw.topAnchor).isActive = true
        classNameListLbl.leftAnchor.constraint(equalTo: vw.leftAnchor,constant:8).isActive = true
        classNameListLbl.widthAnchor.constraint(equalTo: vw.widthAnchor,constant:-16).isActive = true
        classNameListLbl.heightAnchor.constraint(equalTo:vw.heightAnchor).isActive = true
        
        setUpBottomView()
        
        view.addSubview(textField)
        // needed constraints x,y,w,h
        textField.topAnchor.constraint(equalTo: vw.bottomAnchor,constant:2).isActive = true
        textField.leftAnchor.constraint(equalTo: view.leftAnchor,constant:1).isActive = true
        textField.widthAnchor.constraint(equalTo: view.widthAnchor,constant:-2).isActive = true
        textField.heightAnchor.constraint(equalToConstant:50).isActive = true
        
        view.addSubview(textView)
        // needed constraints x,y,w,h
        if caseNumber == 2 || caseNumber == 3 {
            textField.isHidden = true
            textView.topAnchor.constraint(equalTo: vw.bottomAnchor,constant:1).isActive = true
        }else {
            textView.topAnchor.constraint(equalTo: textField.bottomAnchor,constant:2).isActive = true
        }
        textView.leftAnchor.constraint(equalTo: view.leftAnchor,constant:1).isActive = true
        textView.widthAnchor.constraint(equalTo: view.widthAnchor,constant:-2).isActive = true
        txtViewBottomAnchor = textView.bottomAnchor.constraint(equalTo:view.bottomAnchor,constant:-92)
        txtViewBottomAnchor?.isActive = true
    }
    
    private func setUpBottomView() {
        
        view.addSubview(bottomView)
        // needed constraints x,y,w,h
        yAnchor = bottomView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        yAnchor?.isActive = true
        bottomView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        bottomView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        bottomView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        bottomView.addSubview(attachmentBtn)
        // needed constraints x,y,w,h
        attachmentBtn.centerYAnchor.constraint(equalTo: bottomView.centerYAnchor).isActive = true
        attachmentBtn.leftAnchor.constraint(equalTo: bottomView.leftAnchor,constant:8).isActive = true
        attachmentBtn.widthAnchor.constraint(equalToConstant: 45).isActive = true
        attachmentBtn.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        bottomView.addSubview(sendBtn)
        // needed constraints x,y,w,h
        sendBtn.centerYAnchor.constraint(equalTo: bottomView.centerYAnchor).isActive = true
        sendBtn.rightAnchor.constraint(equalTo: bottomView.rightAnchor,constant:-8).isActive = true
        sendBtn.widthAnchor.constraint(equalToConstant: 45).isActive = true
        sendBtn.heightAnchor.constraint(equalToConstant: 45).isActive = true
    }
    
    var tHeightAnchor:NSLayoutConstraint?
    
    private func setUpTableView() {
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 40
        view.addSubview(tableView)
        // needed constyarints
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor,constant:1).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor,constant:-51).isActive = true
        tableView.widthAnchor.constraint(equalTo: view.widthAnchor,constant:-2).isActive = true
        tHeightAnchor = tableView.heightAnchor.constraint(equalToConstant:40)
        tHeightAnchor?.isActive = true
    }
    
    //MARK:- View Controllers Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViews()
        setUpTableView()
        imagePicker.delegate = self
        imagePicker.sourceType = .savedPhotosAlbum
        textView.delegate = self
        view.backgroundColor = UIColor.white
        classNameListLbl.text = classNameList ?? ""
        tableView.register(DownloadeAbleCell.self, forCellReuseIdentifier: cellId)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupKeyboardObservers()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK:- TableView Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if attachmentList.count < 4 {
            tHeightAnchor?.constant = CGFloat(40 * attachmentList.count)
            txtViewBottomAnchor?.constant = -((tHeightAnchor?.constant)!+52)
        }else {
            tHeightAnchor?.constant = 120
            txtViewBottomAnchor?.constant = -172
        }
        return attachmentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! DownloadeAbleCell
        cell.cImageView.image = UIImage(named: "iconDelete")
        cell.cImageView.tag = indexPath.row
        cell.cImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SendVCTeacher.handleDeleteAttachment(_:))))
        cell.cImageView.isUserInteractionEnabled = true
        cell.nameLabel.text = attachmentList[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //MARK:- Handler Methods
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == PlaceHolderStrings.tvPlaceHolder {
            textView.text = ""
        }
    }
    
    @objc func handleDeleteAttachment(_ sender:UIGestureRecognizer) {
        guard let tag = sender.view?.tag else {
            return
        }
        self.attachmentList.remove(at: tag)
        self.attachData.remove(at: tag)
        self.attachExtensions.remove(at: tag)
        self.mimeTypes.remove(at: tag)
        // attachmentImgsData.remove(at: tag)
        tableView.reloadData()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        textView.resignFirstResponder()
        textField.resignFirstResponder()
    }
    
    @objc func handleSend() {}
    
    @objc func handleAttachment() {
        if attachmentList.count == 5{
            let alert = UIAlertController(title: nil, message: "You can upload maximum 5 attachments.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Photos", style: .default, handler: { (alert) in
                self.pickImage()
            }))
            alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (alert) in
                self.pickDocument()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func pickDocument(){
        let documentPicker = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        documentPicker.delegate = self
        self.present(documentPicker, animated: true, completion: nil)
    }
    func pickImage(){
        let imagePicker = OpalImagePickerController()
        //Change color of selection overlay to white
        imagePicker.selectionTintColor = UIColor.white.withAlphaComponent(0.7)
        
        //Change color of image tint to black
        imagePicker.selectionImageTintColor = UIColor.black
        
        //Change image to X rather than checkmark
        //        imagePicker.selectionImage = UIImage(named: "x_image")
        
        //Change status bar style
        imagePicker.statusBarPreference = UIStatusBarStyle.lightContent
        
        //Limit maximum allowed selections to 5
        imagePicker.maximumSelectionsAllowed = 5 - self.attachmentList.count
        
        //Only allow image media type assets
        imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
        imagePicker.imagePickerDelegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    //MARK:- ImagePickerView Methods
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        for i in 0..<images.count{
            self.attachData.append(images[i].jpegData(compressionQuality: 0.5)!)
            self.mimeTypes.append("image/jpeg")
            self.attachExtensions.append("jpeg")
        }
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingAssets assets: [PHAsset]) {
        for asset in assets {
            if let name = asset.value(forKey: "filename") {
                attachmentList.append(name as! String)
            }
        }
        tableView.reloadData()
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Keyborad Show/Hide Methods
    
    func setupKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func handleKeyboardWillShow(_ notification: Notification) {
        let keyboardFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
        let keyboardDuration = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue
        yAnchor?.constant = -(keyboardFrame!.height+1)
        txtViewBottomAnchor?.constant = -(keyboardFrame!.height+52)
        UIView.animate(withDuration: keyboardDuration!, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func handleKeyboardWillHide(_ notification: Notification) {
        let keyboardDuration = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue
        
        yAnchor?.constant = 0
        if attachmentList.count < 4 {
            tHeightAnchor?.constant = CGFloat(40 * attachmentList.count)
            txtViewBottomAnchor?.constant = -((tHeightAnchor?.constant)!+52)
        }
        
        UIView.animate(withDuration: keyboardDuration!, animations: {
            self.view.layoutIfNeeded()
            if self.textField.text == "" {
                self.textField.placeholder = PlaceHolderStrings.tfPlacehoder
            }
            if self.textView.text == "" {
                self.textView.text = PlaceHolderStrings.tvPlaceHolder
            }
        })
    }
}
extension SendVCTeacher : UIDocumentPickerDelegate{
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let docUrl = url as URL
        let docData  = try! Data(contentsOf: docUrl)
        self.attachData.append(docData)
        self.attachmentList.append(url.lastPathComponent)
        self.attachExtensions.append("pdf")
        self.mimeTypes.append("application/pdf")
        self.tableView.reloadData()
    }
}

