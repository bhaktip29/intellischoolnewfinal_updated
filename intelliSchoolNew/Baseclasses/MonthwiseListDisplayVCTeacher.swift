//
//  MonthwiseListDisplayVC.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 19/06/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MonthwiseListDisplayVCTeacher: BaseViewControllerTeacher,UITableViewDelegate,UITableViewDataSource {
    
    let listDisplayCellId = "MonthwiseCountCell"
    var messages = [Message]()
    var selectedMonth : String = String()
    var selectedYear : String = String()
    var caseNumber:Int = 0
    var gradientLayer: CAGradientLayer!
    var noDataLabel = ""
    let tableView: UITableView = {
        let tbl = UITableView()
        tbl.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tbl.translatesAutoresizingMaskIntoConstraints = false
        return tbl
    }()
    
    //MARK:- Custom Views Setup Methods
    
    private func setUpTableView() {
        
        tableView.delegate = self
        tableView.dataSource = self
        
        view.addSubview(tableView)
        // needed constyarints
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    //MARK:- View Controller Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("@@**@@")
        tableView.register(ListDisplayCell.self, forCellReuseIdentifier: listDisplayCellId)
        setUpTableView()
        tableView.tableFooterView = UIView()
        if(UserDefaults.standard.string(forKey: "HMC") == "3")
        {
                caseNumber = 1
        }
        else if(UserDefaults.standard.string(forKey: "HMC") == "2")
        {
                caseNumber = 2
        }
        else if(UserDefaults.standard.string(forKey: "HMC") == "1")
        {
                caseNumber = 3
        }
        switch caseNumber {
        case 1:
            getCircularsData()
            break
        case 2:
            getMessagesData()
            break
        case 3:
            getHomeworksData()
            break
        default:
            break
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        
    }
    
    
    //MARK:- Table View Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.messages.count == 0{
            self.tableView.setEmptyMessage(self.noDataLabel)
        }else{
            self.tableView.restore()
        }
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: listDisplayCellId, for: indexPath) as! ListDisplayCell
        
        messages.sort(by: { $0.messageID > $1.messageID })
        
        let msg:Message = messages[indexPath.row]
        
        let date = UtilTeacher.dateConvert("yyyy-MM-dd",format2: "dd-MMM",datetoconvert: msg.messageDate).replacingOccurrences(of: "-", with: " ")
        
        switch caseNumber {
        case 1:
            cell.subjectLbl.text = "Title: \(msg.messageSubject)"
            cell.dateLbl.text = "Date: \(date)"
            if msg.messageDiv == ""{
                cell.classLbl.text = "Class: \(msg.messageClass) \(msg.messageDiv)"
            }else{
                cell.classLbl.text = "Class: \(msg.messageClass)-\(msg.messageDiv)"
            }
            break
        case 2:
            cell.subjectLbl.isHidden = true
            cell.lTopAnchor?.constant = 13.5
            cell.dateLbl.text = "Date: \(date)"
            if msg.messageDiv == ""{
                cell.classLbl.text = "Class: \(msg.messageClass) \(msg.messageDiv)"
            }else{
                cell.classLbl.text = "Class: \(msg.messageClass)-\(msg.messageDiv)"
            }
            break
        case 3:
            cell.subjectLbl.text = "Subject: \(msg.messageSubject)"
            cell.dateLbl.text = "Date: \(date)"
            if msg.messageDiv == ""{
                cell.classLbl.text = "Class: \(msg.messageClass) \(msg.messageDiv)"
            }else{
                cell.classLbl.text = "Class: \(msg.messageClass)-\(msg.messageDiv)"
            }
            break
        default:
            break
        }
        
        if(msg.attachmentUrl.count < 1){
            cell.attachmentView.isHidden = true
        }else {
            cell.attachmentView.isHidden = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DetailVCTeacher()
        vc.caseNumber = caseNumber
        if let title = self.title {
            vc.title = "\(String(title.dropLast())) Details"
        }
        vc.message = messages[indexPath.row]
        vc.selectedYear = selectedYear
        vc.selectedMonth = selectedMonth
        navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Server Methods
    // Get Homeworks Data
    private func getHomeworksData()
    {
        print("@@***@@")
        if UtilTeacher.validateNetworkConnection(self) {
            
//            For role :  if (role > 4)
//                ➢    parameter : school_db_settings_array , school_employee_class_setting_array, action = HomeworkListTeacher, month , year , employee_id
//            For role : else if (role  == 4)
//                ➢    parameter : school_db_settings_array , action = HomeworkListSupervisor, month , year , employee_id
//            For role : else  (role – 1,2,3)
//            ➢    parameter : school_db_settings_array , action = HomeworkListPrincipal, month , year

            
            let userDefaults = UserDefaults.standard
            var parameters = [String:Any]()
            guard let roleId = userDefaults.string(forKey: "EMPLOYEE_ROLE_ID") else {
                return
            }
            
            if(roleId == "1" || roleId == "2" || roleId == "3"){
                parameters = [
                    "school_db_settings_array":  userDefaults.string(forKey: "DB_SETTING") ?? "NA", //userDefaults.string(forKey: "SCHOOL_DB_SETTINGS") ?? "school_db_settings",
                    "action":"HomeworkListPrincipal",
                    "month":selectedMonth,
                    "year":selectedYear] as [String : Any]
            }else if(roleId == "4") {
                parameters = [
                    "school_db_settings_array":  userDefaults.string(forKey: "DB_SETTING") ?? "NA", //userDefaults.string(forKey: "SCHOOL_DB_SETTINGS") ?? "school_db_settings",
                    "action":"HomeworkListSupervisor",
                    "month":selectedMonth,
                    "year":selectedYear,
                    "employee_id": userDefaults.string(forKey: "EMPLOYEE_ID") ?? "employee_id"] as [String : Any]
            }else{
                parameters = [
                    "school_db_settings_array":  userDefaults.string(forKey: "DB_SETTING") ?? "NA", //userDefaults.string(forKey: "SCHOOL_DB_SETTINGS") ?? "school_db_settings",
                    "school_employee_class_setting_array":  userDefaults.string(forKey: "SUBJECT_SETTINGS") ?? "subject_settings",
                    "action":"HomeworkListTeacher",
                    "month":selectedMonth,
                    "year":selectedYear,
                    "employee_id": userDefaults.string(forKey: "EMPLOYEE_ID") ?? "employee_id"] as [String : Any]
            }
            showActivityIndicatory(uiView: view)
            Alamofire.request(WebServiceUrls.homeworkService,method: .post,parameters: parameters)
                .responseJSON { [weak self] (response) in
                    print("MonthwiseList Parameters : \(parameters)")
                    print("MonthwiseList Responce : \(response)")
                    self?.stopActivityIndicator()
                    if response.result.error != nil {
                        self?.noDataLabel = "\(self?.title ?? "") not found!"
                        self?.tableView.reloadData()
                        self?.tableView.animateTable()
//                        UtilTeacher.invokeAlertMethod("Failed!", strBody: "Error Occured While Processing Data ", delegate: nil,vcobj : self!)
                    }
                    if let jsonresponse = response.result.value {
                        let originalResponse = JSON(jsonresponse)
                        self?.getSuccessResponse(originalResponse: originalResponse)
                    }
            }
        }
    }
    
    private func getSuccessResponse(originalResponse : JSON)
    {
        print("Original Response", originalResponse)
        if(originalResponse.count > 0 && originalResponse.array![0] != "null")
        {
            for (_,subJson):(String, JSON) in originalResponse {
                
                let msg = Message()
                
                if let value = NSInteger (subJson["homework_id"].stringValue) {
                    msg.messageID = value
                }
                msg.messageDate = subJson["homework_date"].stringValue
                msg.messageClass = subJson["class_name"].stringValue
                msg.messageDiv = subJson["division_title"].stringValue
                msg.messageSubject = subJson["subject_name"].stringValue
                msg.message = subJson["homework_details"].stringValue
                let att = subJson["attachment_url"]
                let representation = att.rawString([.castNilToNSNull: true])
                if representation != "NULL" {
                    if let attArray = subJson["attachment_url"].rawValue as? [String],attArray.count > 0 {
                        msg.attachmentUrl = attArray
                    }else {
                        msg.attachmentUrl.append(att.stringValue )
                    }
                }
                messages.append(msg)
            }
            tableView.reloadData()
            tableView.animateTable()
        }else{
            self.noDataLabel = "\(self.title ?? "") not found!"
            tableView.reloadData()
            tableView.animateTable()
        }
    }
    
    // Get Messages Data
    private func getMessagesData()
    {
         print("@@****@@")
        if UtilTeacher.validateNetworkConnection(self) {
            
//            parameter : school_db_settings_array , action = MessageListParticularMonth , month , year , employee_id , role
            
            let userDefaults = UserDefaults.standard
            let parameters = [
                "school_db_settings_array":  userDefaults.string(forKey: "DB_SETTING") ?? "NA", //userDefaults.string(forKey: "SCHOOL_DB_SETTINGS") ?? "school_db_settings",
                "action":"MessageListParticularMonth",
                "month":selectedMonth,
                "year":selectedYear,
                "employee_id": userDefaults.string(forKey: "EMPLOYEE_ID") ?? "employee_id",
                "role": userDefaults.string(forKey: "EMPLOYEE_ROLE_ID") ?? "employee_role_id"] as [String : Any]
            showActivityIndicatory(uiView: view)
            Alamofire.request(WebServiceUrls.messagesService,method: .post,parameters: parameters)
                .responseJSON {[weak self] (response) in
                    print("messagesService parameters\(parameters)")
                    print("messagesService response\(response)")
                    self?.stopActivityIndicator()
                    if response.result.error != nil {
                        self?.noDataLabel = "\(self?.title ?? "") not found!"
                        self?.tableView.reloadData()
                        self?.tableView.animateTable()
//                        UtilTeacher.invokeAlertMethod("Failed!", strBody: "Error Occured While Processing Data ", delegate: nil,vcobj : self!)
                    }
                    if let jsonresponse = response.result.value {
                        
                        let originalResponse = JSON(jsonresponse)
                        self?.getMessagesResponse(originalResponse: originalResponse)
                    }
            }
        }
    }
    private func getMessagesResponse(originalResponse : JSON)
    {
        print("Original Response", originalResponse)
        if(originalResponse.count > 0)
        {
            for (_,subJson):(String, JSON) in originalResponse {
                
                let msg = Message()
                msg.messageID = NSInteger (subJson["message_id"].stringValue)!
                msg.messageDate = subJson["message_date"].stringValue
                msg.messageClass = subJson["class_name"].stringValue
                msg.messageDiv = subJson["division_title"].stringValue
                msg.messageSubject = ""
                msg.message = subJson["msgContent"].stringValue
                let att = subJson["attachment"]
                let representation = att.rawString([.castNilToNSNull: true])
                
                if representation != "NULL" {
                    if let attArray = subJson["attachment"].rawValue as? [String],attArray.count > 0 {
                        msg.attachmentUrl = attArray
                    }else {
                        msg.attachmentUrl.append(att.stringValue )
                    }
                }
                messages.append(msg)
            }
            tableView.reloadData()
            tableView.animateTable()
        }else{
            self.noDataLabel = "\(self.title ?? "") not found!"
            tableView.reloadData()
            tableView.animateTable()
        }
    }
    
    // Get Circulars Data
    func getCircularsData()
    {
        if UtilTeacher.validateNetworkConnection(self) {
//            parameter : school_db_settings_array , action = CircularListParticularMonth , month , year
            
            let userDefaults = UserDefaults.standard
            let parameters = [
                "school_db_settings_array": userDefaults.string(forKey: "DB_SETTING") ?? "NA", //userDefaults.string(forKey: "SCHOOL_DB_SETTINGS") ?? "school_db_settings",
                "action":"CircularListParticularMonth",
                "month":selectedMonth,
                "year":selectedYear] as [String : Any]
            showActivityIndicatory(uiView: view)
            Alamofire.request(WebServiceUrls.circularService,method: .post,parameters: parameters)
                .responseJSON { [weak self](response) in
                    self?.stopActivityIndicator()
                    if response.result.error != nil {
                        self?.noDataLabel = "\(self?.title ?? "") not found!"
                        self?.tableView.reloadData()
                        self?.tableView.animateTable()
//                        UtilTeacher.invokeAlertMethod("Failed!", strBody: "Error Occured While Processing Data ", delegate: nil,vcobj : self!)
                    }
                    if let jsonresponse = response.result.value {
                        let originalResponse = JSON(jsonresponse)
                        self?.getCirculasResponse(originalResponse: originalResponse)
                    }
            }
        }
    }
    
    private func getCirculasResponse(originalResponse : JSON)
    {
        if(originalResponse.count > 0)
        {
            for (_,subJson):(String, JSON) in originalResponse {
                
                let msg = Message()
                msg.messageID = NSInteger (subJson["circular_id"].stringValue)!
                msg.messageDate = subJson["circular_date"].stringValue
                msg.messageClass = subJson["class_name"].stringValue
                msg.messageDiv = ""
                msg.messageSubject = subJson["circular_subject"].stringValue
                msg.message = subJson["circular_details"].stringValue
                let att = subJson["attachment"]
                let representation = att.rawString([.castNilToNSNull: true])
                
                if representation != "NULL" {
                    if let attArray = subJson["attachment"].rawValue as? [String],attArray.count > 0 {
                         msg.attachmentUrl = attArray
                    }else {
                        msg.attachmentUrl.append(att.stringValue )
                    }
                }
                messages.append(msg)
            }
            tableView.reloadData()
            tableView.animateTable()
        }else{
            self.noDataLabel = "\(self.title ?? "") not found!"
            tableView.reloadData()
            tableView.animateTable()
        }
    }
}

class ListDisplayCell: UITableViewCell {
    
    let dateLbl: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let classLbl: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .justified
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let subjectLbl: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .justified
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.sizeToFit()
        return lbl
    }()
    
    let attachmentView:UIImageView = {
        let imgView = UIImageView()
        imgView.image = UIImage(named:"ic_file_attachment_black")
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.contentMode = .scaleToFill
        return imgView
    }()
    
    var lTopAnchor: NSLayoutConstraint?
    
    private func setUpViews() {
        
        let vv = UIView()
        vv.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(vv)
        // needed constraints
        vv.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        lTopAnchor = vv.topAnchor.constraint(equalTo: contentView.topAnchor)
        lTopAnchor?.isActive  = true
        vv.widthAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
        vv.heightAnchor.constraint(equalTo:contentView.heightAnchor,multiplier:0.5).isActive = true
        
        vv.addSubview(attachmentView)
        // needed constraints
        attachmentView.rightAnchor.constraint(equalTo: vv.rightAnchor).isActive = true
        attachmentView.topAnchor.constraint(equalTo: vv.topAnchor,constant:4).isActive = true
        attachmentView.widthAnchor.constraint(equalToConstant: 25).isActive = true
        attachmentView.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        vv.addSubview(dateLbl)
        // needed constraints
        dateLbl.topAnchor.constraint(equalTo: vv.topAnchor,constant:4).isActive = true
        dateLbl.leftAnchor.constraint(equalTo: vv.leftAnchor,constant:8).isActive = true
        dateLbl.widthAnchor.constraint(equalTo: vv.widthAnchor,multiplier:0.5).isActive = true
        dateLbl.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        contentView.addSubview(subjectLbl)
        // needed constraints
        subjectLbl.leftAnchor.constraint(equalTo: contentView.leftAnchor,constant:8).isActive = true
        subjectLbl.bottomAnchor.constraint(equalTo: contentView.bottomAnchor,constant:-4).isActive  = true
        subjectLbl.widthAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
        subjectLbl.topAnchor.constraint(equalTo:vv.bottomAnchor,constant:-2).isActive = true
        
        vv.addSubview(classLbl)
        // needed constraints
        classLbl.leftAnchor.constraint(equalTo: dateLbl.rightAnchor).isActive = true
        classLbl.topAnchor.constraint(equalTo: vv.topAnchor,constant:4).isActive  = true
        classLbl.rightAnchor.constraint(equalTo: attachmentView.leftAnchor).isActive = true
        classLbl.heightAnchor.constraint(equalToConstant: 25).isActive = true
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        self.accessoryType = .disclosureIndicator
        setUpViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
