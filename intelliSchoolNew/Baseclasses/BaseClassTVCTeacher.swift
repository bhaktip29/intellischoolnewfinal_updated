//
//  BaseClassTVC.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 16/06/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class BaseClassTVCTeacher: BaseViewControllerTeacher,UITableViewDelegate,UITableViewDataSource {
    
    let vw = UIView()
    var classList = [SchoolClassTeacher]()
    let cellId = "CellIdentifire"
    var caseNumber:Int = 0
    var noOfItemsChecked: Int = 0
    var isAllSelected = false
    var noDataMsg = ""
    let bottomLabel : UILabel = {
        let lbl = UILabel()
        lbl.text = DisplayTextMsgs.calssDivisionSelectionText
        lbl.font = UIFont.boldSystemFont(ofSize: 13)
        lbl.backgroundColor = UIColor.lightGray
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.white
        lbl.numberOfLines = 0
        return lbl
    }()
    
    let headerView:UIView = {
        let headerView = UIView()
        //Changed
       // headerView.backgroundColor = UIColor(red:0.86, green:0.86, blue:0.86, alpha:1.0)
        headerView.backgroundColor = UIColor.lightGray
        headerView.translatesAutoresizingMaskIntoConstraints = false
        return headerView
    }()
    
    let sendButton:UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setImage(UIImage(named: "ic_send_All"), for: .normal)
        btn.addTarget(self, action: #selector(handleSend), for: .touchUpInside)
        return btn
    }()
    
    let tableView : UITableView = {
        
        let tblView = UITableView()
        tblView.translatesAutoresizingMaskIntoConstraints = false
        tblView.separatorInset = UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 4)
        tblView.separatorInset = UIEdgeInsets.zero
        return tblView
    }()
    
    let selectAllBtn:UIButton = {
        let btn = UIButton(type: .system)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Select All", for: .normal)
        btn.addTarget(self, action: #selector(handleSelectAll), for: .touchUpInside)
        return btn
    }()
    
    //MARK:- Custom Views Setup Methods
    
    private func setUpSelectAll() {
    
        view.addSubview(headerView)
        // needed constraints x,y,w,h
        if #available(iOS 11.0, *) {
            headerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        } else {
            headerView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        }
        headerView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        headerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        headerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        headerView.addSubview(selectAllBtn)
        // needed constraints x,y,w,h
        selectAllBtn.centerXAnchor.constraint(equalTo: headerView.centerXAnchor).isActive = true
        selectAllBtn.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        selectAllBtn.widthAnchor.constraint(equalToConstant:90).isActive = true
        selectAllBtn.heightAnchor.constraint(equalTo: headerView.heightAnchor).isActive = true
    }
    
    private func setUpTableView() {
        
        view.addSubview(tableView)
        tableView.dataSource = self
        tableView.delegate = self
        
        // needed constarunts x,y,w,h
        if caseNumber == 3 {
            if #available(iOS 11.0, *) {
                tableView.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor).isActive = true
            } else {
                tableView.topAnchor.constraint(equalTo:view.topAnchor).isActive = true
            }
        } else {
            tableView.topAnchor.constraint(equalTo:headerView.bottomAnchor).isActive = true
        }
        
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: bottomLabel.topAnchor).isActive = true
    }
   
    private func setUpBottomLabel() {
    
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = UIColor.lightGray
        view.addSubview(vw)
        // needed constarunts x,y,w,h
        vw.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        if #available(iOS 11.0, *) {
            vw.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            vw.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
        vw.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        vw.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        vw.addSubview(bottomLabel)
        // needed constarunts x,y,w,h
        bottomLabel.leftAnchor.constraint(equalTo: view.leftAnchor,constant: 8).isActive = true
        if #available(iOS 11.0, *) {
            bottomLabel.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            bottomLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
        bottomLabel.widthAnchor.constraint(equalTo: view.widthAnchor,constant:-60).isActive = true
        bottomLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        bottomLabel.sizeToFit()
        
        vw.addSubview(sendButton)
        // needed constarunts x,y,w,h
        sendButton.rightAnchor.constraint(equalTo: vw.rightAnchor,constant: -10).isActive = true
        sendButton.centerYAnchor.constraint(equalTo: vw.centerYAnchor,constant:-12).isActive = true
        sendButton.widthAnchor.constraint(equalToConstant:45).isActive = true
        sendButton.heightAnchor.constraint(equalToConstant: 45).isActive = true
    }
    
    private func setUpMenuList(){
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image:UIImage(named:"iconList"), style: .plain, target: self, action: #selector(handleList))
        navigationItem.rightBarButtonItem?.tintColor = .white//GlobleConstants.navigationBarColorTint
    }

    //MARK:- View Controllers Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.cellLayoutMarginsFollowReadableWidth = false
        navigationItem.title = ViewControllerTitlesTeacher.classOrDivison
        tableView.allowsMultipleSelection = true
        tableView.tableFooterView = UIView()
        tableView.setEditing(true, animated: true)
        tableView.register(ClassCell.self, forCellReuseIdentifier: cellId)
        if caseNumber != 3 {
            setUpSelectAll()
        }
        setUpBottomLabel()
        setUpTableView()
        setUpMenuList()
        view.bringSubviewToFront(vw)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if classList.count == 0{
            self.tableView.setEmptyMessage(self.noDataMsg)
        }else{
            self.tableView.restore()
        }
        return classList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ClassCell
        let schoolClass:SchoolClassTeacher = classList[indexPath.row]
        cell.textLabel?.text = "Class \(schoolClass.className!) \(schoolClass.divisionName!)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return UITableViewCell.EditingStyle.init(rawValue: 3)!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        let schoolClass = classList[indexPath.row]
        schoolClass.isChecked = !schoolClass.isChecked
        noOfItemsChecked = noOfItemsChecked < classList.count ? noOfItemsChecked+1 : 0
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath : IndexPath) {
        let schoolClass = classList[indexPath.row]
        schoolClass.isChecked = !schoolClass.isChecked
        noOfItemsChecked = noOfItemsChecked > 0 ? noOfItemsChecked-1 : 0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    //MARK:- Handler Methods
    
    @objc func handleSend() {}
    @objc func handleList() {}
    
    @objc func handleSelectAll() {
        
        if isAllSelected {
            isAllSelected = false
            let totalRows = tableView.numberOfRows(inSection: 0)
            for row in 0..<totalRows {
                let index = IndexPath(row: row, section: 0)
                tableView.deselectRow(at: index, animated: false)
                let schoolClass = classList[row]
                schoolClass.isChecked = !schoolClass.isChecked
                noOfItemsChecked = 0
            }
        }else{
            isAllSelected = true
            let totalRows = tableView.numberOfRows(inSection: 0)
            classList.forEach({ $0.isChecked = false })
            for row in 0..<totalRows {
                let index = IndexPath(row: row, section: 0)
                tableView.selectRow(at: index, animated: false, scrollPosition: .none)
                let schoolClass = classList[row]
                schoolClass.isChecked = !schoolClass.isChecked
                noOfItemsChecked = classList.count
            }
        }
    }
}

