//
//  MenuListVC.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 18/06/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ParticularMessage {
    var year: String?
    var month: String?
    var countArray = [CountClass]()
}

class MenuListVCTeacher: BaseMenuVCTeacher,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    //MARK:- View Controllers Methods
    var noDataLabel = ""
  //  var caseNumber = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 25.0/255.0, green: 89.0/255.0, blue: 194.0/255.0, alpha: 1.0)
               
               self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

               
               navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(handleSlide))
               navigationItem.leftBarButtonItem?.tintColor = .white
        

     //   navigationItem.leftBarButtonItem = UIBarButtonItem(image:UIImage(named:"back-arrow"), style: .plain, target: self, action: #selector(handleSlide))
               navigationItem.leftBarButtonItem?.tintColor = .white
        
        collectionView.dataSource = self
        collectionView.delegate = self
         var caseNumber = 0
        if(UserDefaults.standard.string(forKey: "HMC") == "3")
        {
            print("In Circular")
          caseNumber = 1
        }
        else if(UserDefaults.standard.string(forKey: "HMC") == "2")
        {
            print("In Message")
         caseNumber = 2
        }
        else if(UserDefaults.standard.string(forKey: "HMC") == "1")
        {
            print("In Homework")
         caseNumber = 3
        }
        switch caseNumber {
        case 1:
            getCircularsCountData()
            self.title = "Circulars"
            break
        case 2:
            getMessagesCountData()
            self.title = "Messages"
            break
        case 3:
            getHomeworksCountData()
            self.title = "Homeworks"
            break
        default:
            break
        }
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
     //    navigationController?.navigationBar.barTintColor = UIColor(red: 25.0/255.0, green: 89.0/255.0, blue: 194.0/255.0, alpha: 1.0)

     //   navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(handleSlide))
                navigationItem.leftBarButtonItem?.tintColor = .white
    }
    
    @objc func handleSlide() {
           print("back button tapped..")
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
                  let controller = storyboard.instantiateViewController(withIdentifier: "vc")
                  self.present(controller, animated: true, completion: nil)
       }
    
    //MARK:- CollectionView Methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.msgCounts.count == 0{
            self.collectionView.setEmptyMessage(self.noDataLabel)
        }else{
            self.collectionView.restore()
        }
        return msgCounts.count
    }
    
    func getLabel() -> UILabel {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.font = UIFont(name: "Helvetica Neue", size: 13)
        lbl.textAlignment = .center
        lbl.layer.borderColor = UIColor.lightGray.cgColor
        lbl.layer.borderWidth = 0.3
        return lbl
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: listCellId, for: indexPath) as! ListMenuCell
        
        let pMsg = msgCounts[indexPath.row]

        for i in 0...pMsg.countArray.count {
            let lbl = getLabel()
            switch i+1 {
            case 1:
                lbl.text = months[indexPath.row]
                break
            case 2,3,4,5:
                let counClass = pMsg.countArray[i-1]
                lbl.text = counClass.count
                break
            default:
                break
            }
            cell.stackView.addArrangedSubview(lbl)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let vc = MonthwiseListDisplayVCTeacher()
        vc.title = self.title
        vc.caseNumber = caseNumber
        let pMsg = msgCounts[indexPath.row]
        if let yr = pMsg.year, let mnth = pMsg.month {
            vc.selectedYear = yr
            vc.selectedMonth = mnth
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Server Methods
    
    func clearValues() {
        months.removeAll()
        msgCounts.removeAll()
        group.removeAll()
        group.append("Month")
    }
    
    // Get Homework count Data
    func getHomeworksCountData() {
        
        var parameters = [String:Any]()
        if UtilTeacher.validateNetworkConnection(self) {
            
//            ➢    For role id  =  1,2,3,4
//            parameter : school_db_settings_array , action = NewGetAllListHwkCount , employee_id, employee_role_id
    
            let userDefaults = UserDefaults.standard
            guard let roleId = userDefaults.string(forKey: "EMPLOYEE_ROLE_ID") else {
                return
            }
        //    https://isirs.org/intellischools_teachers_app/teachers_script/homework_api.php
            if(roleId == "1" || roleId == "2" || roleId == "3" || roleId == "4"){
                parameters = [
                    "school_db_settings_array": userDefaults.string(forKey: "DB_SETTING") ?? "NA", //userDefaults.string(forKey: "SCHOOL_DB_SETTINGS") ?? "NA",
                    "action": "NewGetAllListHwkCount",
                    "employee_id": userDefaults.string(forKey: "INTELLINECTS_ID") ?? "NA", //userDefaults.string(forKey: "EMPLOYEE_ID") ?? "NA",
                    "employee_role_id": roleId] as [String : Any]
            }else {
                parameters = [
                    "school_db_settings_array":  userDefaults.string(forKey: "DB_SETTING") ?? "NA",
                    "action": "ListHwkCount",
                    "employee_id": userDefaults.string(forKey: "INTELLINECTS_ID") ?? "NA", //"INTELL19E00SCH006"
                "school_employee_class_setting_array": userDefaults.string(forKey: "SUBJECT_SETTINGS") ?? "NA"] as [String : Any]
                    //"school_employee_class_setting_array": userDefaults.string(forKey: "SUBJECT_SETTINGS") ?? "NA"] as [String : Any]
            }
         //   showActivityIndicatory(uiView: view)
            Alamofire.request(WebServiceUrls.homeworkService,method: .post,parameters: parameters)
                .responseJSON {[weak self] (response) in
                    
                    print("homeworkService Parameters : \(parameters)")
                     print("homeworkService Response : \(response)")
                //    self?.stopActivityIndicator()
                    if response.result.error != nil {
                        print("$$**$$")
                        self?.noDataLabel = "\(self?.title ?? "") not found!"
                        self?.headerView.backgroundColor = .white
                        self?.setViewsAfterResponse()
                        self?.collectionView.reloadData()
                    }
                    if let jsonresponse = response.result.value {
                        print("$$**$$**")
                        self?.getSuccessResponse(originalResponse: JSON(jsonresponse))
                    }
            }
        }
    }
    
    func getSuccessResponse(originalResponse : JSON)
    {
        clearValues()
        if originalResponse.count > 0
        {
            for (key,subJson):(String, JSON) in originalResponse {
                if(key == "classgroup")
                {
                    if subJson.stringValue != "NULL" {
                        for item in subJson.arrayValue {
                            let groupItem = item.dictionaryValue
                            if let groupName = groupItem["groupname"]?.stringValue {
                                switch groupName {
                                case "Pre Primary":
                                    group.append("Pre-Pri")
                                    break
                                case "Primary":
                                    group.append("Pri")
                                    break
                                case "Secondary":
                                    group.append("Sec")
                                    break
                                case "Jr College":
                                    group.append("Jr Clg")
                                    break
                                default:
                                    break
                                }
                            }
                        }
                    }
                }
                
                if(key == "finalgroup")
                {
                    for dic in subJson.arrayValue {
                        let pMsg = ParticularMessage()
                        var tempArray = [CountClass]()
                        for item in dic["finalvalue"].arrayValue {
                            let msgCount = CountClass()
                            msgCount.count = item.stringValue
                            tempArray.append(msgCount)
                        }
                        months.append(dic["month"].stringValue)
                        pMsg.month = dic["month_no"].stringValue
                        pMsg.year = dic["year"].stringValue
                        pMsg.countArray = tempArray
                        msgCounts.append(pMsg)
                    }
                }
            }
            setViewsAfterResponse()
            collectionView.reloadData()
        }
    }
    
    // Get Messages Count Data
    func getMessagesCountData() {
        print("$$$$$$S")
        var parameters = [String:Any]()
        if UtilTeacher.validateNetworkConnection(self) {
            
//            ➢    For role id  =  1,2,3,4
//            parameter : school_db_settings_array , action = NewGetAllListMsgsCount , employee_id, employee_role_id
//
//            ➢    For other role
//            parameter : school_db_settings_array , action = ListMsgsCount,employee_id, school_employee_class_setting_array

            let userDefaults = UserDefaults.standard
            guard let roleId = userDefaults.string(forKey: "EMPLOYEE_ROLE_ID") else {
                return
            }
            
            if(roleId == "1" || roleId == "2" || roleId == "3" || roleId == "4"){
                parameters = [
                //    "school_db_settings_array": userDefaults.string(forKey: "SCHOOL_DB_SETTINGS") ?? "NA",
                    "school_db_settings_array": userDefaults.string(forKey: "DB_SETTING") ?? "NA",
                    "action": "NewGetAllListMsgsCount",
                    "employee_id": userDefaults.string(forKey: "INTELLINECTS_ID") ?? "NA", //userDefaults.string(forKey: "EMPLOYEE_ID") ?? "NA",
                    "employee_role_id": roleId //userDefaults.string(forKey: "EMPLOYEE_ROLE_ID")!
                    ] as [String : Any]
                
            }else {
                parameters = [
                    "school_db_settings_array": userDefaults.string(forKey: "DB_SETTING") ?? "NA", //userDefaults.string(forKey: "SCHOOL_DB_SETTINGS") ?? "NA",
                    "action": "ListMsgsCount",
                    "employee_id": userDefaults.string(forKey: "INTELLINECTS_ID") ?? "NA", //userDefaults.string(forKey: "EMPLOYEE_ID") ?? "NA",
                   // "school_employee_class_setting_array": userDefaults.string(forKey: "SUBJECT_SETTINGS") ?? "NA"] as [String : Any]
                    "school_employee_class_setting_array": userDefaults.string(forKey: "SUBJECT_SETTINGS") ?? "NA"] as [String : Any]
            }
            showActivityIndicatory(uiView: view)
             print("$$$$$$S Parameters :\(parameters)")
            Alamofire.request(WebServiceUrls.messagesService,method: .post,parameters: parameters)
                .responseJSON { [weak self] (response) in
                   self?.stopActivityIndicator()
                     print("$$$$$$S Responce :\(response)")
                    if response.result.error != nil {
                         print("$$$$$$S Error nil")
                        self?.noDataLabel = "\(self?.title ?? "") not found!"
                        self?.headerView.backgroundColor = .white
                        self?.setViewsAfterResponse()
                        self?.collectionView.reloadData()
                    }
                    if let jsonresponse = response.result.value {
                        let originalResponse = JSON(jsonresponse)
                        self?.getMessageCountResponse(originalResponse: originalResponse)
                    }
            }
        }
    }

    func getMessageCountResponse(originalResponse : JSON)
    {
        clearValues()
        if originalResponse.count > 0
        {
            for (key,subJson):(String, JSON) in originalResponse {
                if(key == "classgroup")
                {
                    if subJson.stringValue != "NULL" {
                        for item in subJson.arrayValue {
                            let groupItem = item.dictionaryValue
                            if let groupName = groupItem["groupname"]?.stringValue {
                                switch groupName {
                                case "Pre Primary":
                                    group.append("Pre-Pri")
                                    break
                                case "Primary":
                                    group.append("Pri")
                                    break
                                case "Secondary":
                                    group.append("Sec")
                                    break
                                case "Jr College":
                                    group.append("Jr Clg")
                                    break
                                default:
                                    break
                                }
                            }
                        }
                    }
                }
                
                if(key == "month")
                {
                    for item in subJson.arrayValue {
                        months.append(item.stringValue)
                    }
                }
                
                if(key == "message")
                {
                    for dic in subJson.arrayValue {
                       let pMsg = ParticularMessage()
                        var tempArray = [CountClass]()
                        for item in dic["particular_message"].arrayValue {
                            let msgCount = CountClass()
                            msgCount.groupName = item.dictionaryValue["group_name"]?.stringValue
                            msgCount.count = item.dictionaryValue["no_count"]?.stringValue
                            msgCount.classGroupId = item.dictionaryValue["classGroup_id"]?.stringValue
                            tempArray.append(msgCount)
                        }
                        pMsg.month = dic["month_no"].stringValue
                        pMsg.year = dic["year"].stringValue
                        pMsg.countArray = tempArray
                        msgCounts.append(pMsg)
                    }
                }
            }
            setViewsAfterResponse()
            collectionView.reloadData()
        }
    }
    
    // Get Circulars count Data
    func getCircularsCountData() {
        
        var parameters = [String:Any]()
        if UtilTeacher.validateNetworkConnection(self) {
            let userDefaults = UserDefaults.standard
            parameters = [
                "school_db_settings_array": userDefaults.string(forKey: "DB_SETTING") ?? "NA", //userDefaults.string(forKey: "SCHOOL_DB_SETTINGS") ?? "NA",
                "action": "GetAllCircularCount"] as [String : Any]
            showActivityIndicatory(uiView: view)
            Alamofire.request(WebServiceUrls.circularService,method: .post,parameters: parameters)
                .responseJSON { [weak self] (response) in
                    self?.stopActivityIndicator()
                    if response.result.error != nil {
                        self?.noDataLabel = "\(self?.title ?? "") not found!"
                        self?.headerView.backgroundColor = .white
                        self?.setViewsAfterResponse()
                        self?.collectionView.reloadData()
//                        UtilTeacher.invokeAlertMethod("Failed!", strBody: "Error Occured While Processing Data ", delegate: nil,vcobj : self!)
                    }
                    if let jsonresponse = response.result.value {
                        
                        let originalResponse = JSON(jsonresponse)
                        self?.getCircularsCountResponse(originalResponse: originalResponse)
                    }
            }
        }
    }
    
    func getCircularsCountResponse(originalResponse : JSON)
    {
        clearValues()
        if originalResponse.count > 0
        {
            for (key,subJson):(String, JSON) in originalResponse {
                if(key == "classgroup")
                {
                    if subJson.stringValue != "NULL" {
                        for item in subJson.arrayValue {
                            let groupItem = item.dictionaryValue
                            if let groupName = groupItem["groupname"]?.stringValue {
                                switch groupName {
                                case "Pre Primary":
                                    group.append("Pre-Pri")
                                    break
                                case "Primary":
                                    group.append("Pri")
                                    break
                                case "Secondary":
                                    group.append("Sec")
                                    break
                                case "Jr College":
                                    group.append("Jr Clg")
                                    break
                                default:
                                    break
                                }
                            }
                        }
                    }
                }

                if(key == "finalgroup")
                {
                    for dic in subJson.arrayValue {
                        let pMsg = ParticularMessage()
                        var tempArray = [CountClass]()
                        for item in dic["finalvalue"].arrayValue {
                            let msgCount = CountClass()
                            msgCount.count = item.stringValue
                            tempArray.append(msgCount)
                        }
                        months.append(dic["month"].stringValue)
                        pMsg.month = dic["month_no"].stringValue
                        pMsg.year = dic["year"].stringValue
                        pMsg.countArray = tempArray
                        msgCounts.append(pMsg)
                    }
                }
            }
            self.headerView.backgroundColor = CustomColor.greenColor
            setViewsAfterResponse()
            collectionView.reloadData()
        }
    }
}
