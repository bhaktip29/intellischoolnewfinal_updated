//
//  DetailVC.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 19/06/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import QuickLook

class DetailVCTeacher: BaseViewControllerTeacher,UITableViewDelegate,UITableViewDataSource,QLPreviewControllerDataSource {
    
    var message = Message()
    let cellId = "DownloadCell"
    var selectedMonth : String = String()
    var selectedYear : String = String()
    var attachment : [String] = []
    var dateConverted : String! = nil
    var names : [String] = []
    var subUrls: [String]  = []
    var caseNumber:Int = 0
    
    let quickLookController = QLPreviewController()
    var fileURLs = [NSURL]()
    let containerView:UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.layer.cornerRadius = 5
        v.layer.masksToBounds = true
        v.backgroundColor = UIColor.white
        return v
    }()
    
    let classLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.white
        lbl.backgroundColor = CustomColor.greenColor
        lbl.textAlignment = .center
        return lbl
    }()
    
    let monthView: DetailDisplay = {
        let lv = DetailDisplay()
        lv.imgView.image = UIImage(named: "ic_month")
        lv.translatesAutoresizingMaskIntoConstraints = false
        return lv
    }()
    
    let subjectView: DetailDisplay = {
        let lv = DetailDisplay()
        lv.imgView.image = UIImage(named: "ic_subject")
        lv.translatesAutoresizingMaskIntoConstraints = false
        return lv
    }()
    
    let descTxtView: UITextView = {
        let tv = UITextView()
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.isEditable = false
        tv.textAlignment = .justified
        tv.layer.cornerRadius = 2
        tv.layer.masksToBounds = true
        tv.backgroundColor = UIColor.lightGray.withAlphaComponent(0.3)
        //tv.font = UIFont(name: "HelveticaNeue", size: 14.0)
        tv.font = .systemFont(ofSize: 14)
        return tv
    }()
    
    let descImgView: UIImageView = {
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.contentMode = .scaleToFill
        imgView.image = UIImage(named: "ic_description")
        return imgView
    }()
    
    let descView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    let tableView: UITableView = {
        let tbl = UITableView()
        tbl.translatesAutoresizingMaskIntoConstraints = false
        tbl.layer.cornerRadius = 5
        tbl.layer.masksToBounds = true
        tbl.backgroundColor = UIColor.white
        tbl.separatorInset = UIEdgeInsets.zero
        return tbl
    }()
    
    var refViewNumber = 0
    
    var multiplier:CGFloat = 0
    //MARK:- Custom Views Setup Methods
    
    private func setUpViews() {
        
        view.addSubview(containerView)
        // needed constraints x,y,w,h
        if #available(iOS 11.0, *) {
            containerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 8.0).isActive = true
        } else {
            containerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 8.0).isActive = true
        }
        containerView.leftAnchor.constraint(equalTo: view.leftAnchor,constant:8).isActive = true
        containerView.widthAnchor.constraint(equalTo: view.widthAnchor,constant:-16).isActive = true
        containerView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.6).isActive = true
        
        containerView.addSubview(classLabel)
        // needed constraints x,y,w,h
        classLabel.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        classLabel.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
        classLabel.widthAnchor.constraint(equalTo: containerView.widthAnchor).isActive = true
        classLabel.heightAnchor.constraint(equalToConstant:50).isActive = true
        
        containerView.addSubview(monthView)
        // needed constraints x,y,w,h
        monthView.topAnchor.constraint(equalTo: classLabel.bottomAnchor).isActive = true
        monthView.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
        monthView.widthAnchor.constraint(equalTo: containerView.widthAnchor).isActive = true
        monthView.heightAnchor.constraint(equalToConstant:50).isActive = true
    }
    
    private func setUpSubjectLable() {
        
        containerView.addSubview(subjectView)
        // needed constraints x,y,w,h
        subjectView.topAnchor.constraint(equalTo: monthView.bottomAnchor).isActive = true
        subjectView.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
        subjectView.widthAnchor.constraint(equalTo: containerView.widthAnchor).isActive = true
        subjectView.heightAnchor.constraint(equalToConstant:50).isActive = true
    }
    
    private func setUpDescViewLabel() {
        
        containerView.addSubview(descView)
        // needed constraints x,y,w,h
        if refViewNumber == 1 {
            descView.topAnchor.constraint(equalTo: subjectView.bottomAnchor).isActive = true
        }else{
            descView.topAnchor.constraint(equalTo: monthView.bottomAnchor).isActive = true
        }
        descView.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
        descView.widthAnchor.constraint(equalTo: containerView.widthAnchor).isActive = true
        descView.heightAnchor.constraint(equalTo: containerView.heightAnchor, constant: (caseNumber == 2 ? -100.0 : -150.0)).isActive = true
        
        descView.addSubview(descImgView)
        // needed constriants x,y,w,h
        descImgView.leftAnchor.constraint(equalTo: descView.leftAnchor,constant:8).isActive = true
        descImgView.topAnchor.constraint(equalTo: descView.topAnchor,constant:8).isActive = true
        descImgView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        descImgView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        descView.addSubview(descTxtView)
        // needed constriants x,y,w,h
        descTxtView.leftAnchor.constraint(equalTo: descImgView.rightAnchor,constant:8).isActive = true
        descTxtView.topAnchor.constraint(equalTo: descView.topAnchor,constant:8).isActive = true
        descTxtView.rightAnchor.constraint(equalTo:descView.rightAnchor,constant:-8).isActive = true
        descTxtView.heightAnchor.constraint(equalTo:descView.heightAnchor,constant: -16).isActive = true
    }
    
    private func setUpTableView() {
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 40
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        view.addSubview(tableView)
        //needed constraints x,y,w,h
        tableView.topAnchor.constraint(equalTo: containerView.bottomAnchor,constant:8).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor,constant:8).isActive = true
        tableView.widthAnchor.constraint(equalTo: view.widthAnchor,constant:-16).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor,constant:-8).isActive = true
    }
    
    //MARK:- View Controller Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.lightGray
        
        if(UserDefaults.standard.string(forKey: "HMC") == "3")
               {
                   print("In Circular")
                 caseNumber = 1
               }
               else if(UserDefaults.standard.string(forKey: "HMC") == "2")
               {
                   print("In Message")
                caseNumber = 2
               }
               else if(UserDefaults.standard.string(forKey: "HMC") == "1")
               {
                   print("In Homework")
                caseNumber = 3
               }
        
        
        switch caseNumber {
        case 1:
            self.title = "Circular Details"
            refViewNumber = 1
            setUpViews()
            setUpSubjectLable()
            setUpDescViewLabel()
            break
        case 2:
            self.title = "Message Details"
            refViewNumber = 0
            setUpViews()
            setUpDescViewLabel()
            subjectView.isHidden = true
            break
        case 3:
            self.title = "Homework Details"
            refViewNumber = 1
            setUpViews()
            setUpSubjectLable()
            setUpDescViewLabel()
            break
        default:
            break
        }
        tableView.register(DownloadeAbleCellTeacher.self, forCellReuseIdentifier: cellId)
        setUpTableView()
        setUplabelText()
        quickLookController.dataSource = self
    }
    
    //MARK:- Custom Methods
    
    private func setUplabelText() {
        monthView.textLabel.text = UtilTeacher.dateConvert("yyyy-MM-dd",format2: "dd-MMM",datetoconvert: message.messageDate).replacingOccurrences(of: "-", with: "-")
        subjectView.textLabel.text = message.messageSubject
        classLabel.text = "Class: \(message.messageClass) \(message.messageDiv)"
        descTxtView.text = message.message
        if(message.attachmentUrl.count < 1){
            tableView.isHidden = true
        }
    }
    
    public func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return fileURLs.count
    }
    
    public func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem
    {
        return fileURLs[index]
    }
    
    //MARK:- TableView Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return message.attachmentUrl.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! DownloadeAbleCellTeacher
        cell.cImageView.image = UIImage(named: "ic_attachment")
        cell.layer.masksToBounds = true
        let attachment = message.attachmentUrl
        let str = attachment[indexPath.row].removingPercentEncoding!
        let name :[String] = str.components(separatedBy: "/")
        if name.count > 6 {
            cell.nameLabel.text = name[6]
        }
        if #available(iOS 11.0, *) {
            if indexPath.row == 0 || indexPath.row == self.tableView(tableView, numberOfRowsInSection: 0) - 1{
                cell.layer.cornerRadius = 5
                if message.attachmentUrl.count != 1{
                    switch indexPath.row {
                    case 0:
                        cell.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
                    case self.tableView(tableView, numberOfRowsInSection: 0) - 1:
                        cell.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
                    default:
                        cell.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
                    }
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        download(indexPath.row)
    }
    
    func download(_ row : Int) {
        
        let attachment = message.attachmentUrl
        let str = attachment[row].removingPercentEncoding!
        let name :[String] = str.components(separatedBy: "/")
        guard name.count > 6 else {
            return
        }
        let docpath = name[6]
        let fileManager = FileManager.default
        let rootPath = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let pdfPathInDocument = rootPath.appendingPathComponent(docpath)
        
        if fileManager.fileExists(atPath: pdfPathInDocument.path){
            
            fileURLs.removeAll()
            
            fileURLs.append(pdfPathInDocument as NSURL)
            
            if QLPreviewController.canPreview((fileURLs[0])) {
                quickLookController.reloadData()
                quickLookController.currentPreviewItemIndex = 0
                quickLookController.reloadData()
                navigationController?.pushViewController(quickLookController, animated: true)
            }
        }
        else
        {
            if UtilTeacher.validateNetworkConnection(self) {
                
                let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                    var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                    documentsURL.appendPathComponent(docpath)
                    return (documentsURL, [.removePreviousFile])
                }
                showActivityIndicatory(uiView: view)
                self.view.isUserInteractionEnabled = false
                Alamofire.download(str, to: destination).responseData { [weak self]response in
                    if let destinationUrl = response.destinationURL {
                        self?.stopActivityIndicator()
                        self?.view.isUserInteractionEnabled = true
                        self?.fileURLs.removeAll()
                        self?.fileURLs.append(destinationUrl as NSURL)
                        if QLPreviewController.canPreview((self?.fileURLs[0])!) {
                            self?.quickLookController.reloadData()
                            self?.quickLookController.currentPreviewItemIndex = 0
                            self?.navigationController?.pushViewController((self?.quickLookController)!, animated: true)
                        }
                    }
                }
            }
        }
    }
}

class DownloadeAbleCellTeacher :UITableViewCell {
    
    let nameLabel:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.numberOfLines = 0
        lbl.sizeToFit()
        return lbl
    }()
    
    let cImageView:UIImageView = {
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.contentMode = .scaleToFill
        return imgView
    }()
    
    private func setUpViews() {
        
        contentView.addSubview(cImageView)
        //needed constarints x,y,w,h
        cImageView.rightAnchor.constraint(equalTo: contentView.rightAnchor,constant:-8).isActive = true
        cImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        cImageView.widthAnchor.constraint(equalToConstant:30).isActive = true
        cImageView.heightAnchor.constraint(equalToConstant:30).isActive = true
        
        contentView.addSubview(nameLabel)
        //needed constarints x,y,w,h
        nameLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor,constant:8).isActive = true
        nameLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        nameLabel.rightAnchor.constraint(equalTo: cImageView.leftAnchor).isActive = true
        nameLabel.heightAnchor.constraint(equalTo: contentView.heightAnchor).isActive = true
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        setUpViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class DetailDisplay: UIView {
    
    let imgView:UIImageView  = {
        let img = UIImageView()
        img.contentMode = .scaleToFill
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    let textLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = .boldSystemFont(ofSize: 14)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    private func setUpViews() {
        
        self.addSubview(imgView)
        // needed constriants x,y,w,h
        imgView.leftAnchor.constraint(equalTo: self.leftAnchor,constant:8).isActive = true
        imgView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        imgView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        imgView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        self.addSubview(textLabel)
        // needed constriants x,y,w,h
        textLabel.leftAnchor.constraint(equalTo: imgView.rightAnchor,constant:8).isActive = true
        textLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        textLabel.rightAnchor.constraint(equalTo:self.rightAnchor).isActive = true
        textLabel.heightAnchor.constraint(equalTo:self.heightAnchor).isActive = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
