//
//  BaseClassController.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 16/06/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//
import UIKit
import Alamofire
import SwiftyJSON
class BaseClassControllerTeacher: BaseClassTVCTeacher {
//MARK:- View Controller Methods

override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
}

override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    guard let selectedIndexPaths = tableView.indexPathsForSelectedRows, selectedIndexPaths.count > 0 else {
        return
    }
    for indexPath in selectedIndexPaths {
        tableView.deselectRow(at: indexPath , animated: true)
        let schoolClass:SchoolClassTeacher = classList[indexPath.row]
        schoolClass.isChecked = !schoolClass.isChecked
    }
    noOfItemsChecked = 0
}

override func viewDidLoad() {
    super.viewDidLoad()
    
    navigationController?.navigationBar.barTintColor = UIColor(red: 25.0/255.0, green: 89.0/255.0, blue: 194.0/255.0, alpha: 1.0)
    
    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

    
    navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(handleSlide))
    navigationItem.leftBarButtonItem?.tintColor = .white
    
    getClassesAndDivisions()
    
    
}
@objc func handleSlide() {
       print("back button tapped..")
       let storyboard = UIStoryboard(name: "Main", bundle: nil)
              let controller = storyboard.instantiateViewController(withIdentifier: "vc")
              self.present(controller, animated: true, completion: nil)
   }

override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    isAllSelected = false
}
//MARK:- Action Methods

override func handleList() {
    
    let vc = MenuListVCTeacher()
    vc.caseNumber = caseNumber
    navigationController?.pushViewController(vc, animated: true)
}

override func handleSend() {
    
    if(noOfItemsChecked == 0){
        UtilTeacher.invokeAlertMethod("", strBody: "Select at least one class", delegate: nil,vcobj:self)
    }else {
        var classListString : String = "Class: "
        let prodArray:NSMutableArray = NSMutableArray()
        
        for i in (0..<classList.count){
            let schoolClass:SchoolClassTeacher = classList[i]
            if(schoolClass.isChecked){
                classListString += schoolClass.className! + " " + schoolClass.divisionName!+","
                let para: NSMutableDictionary = NSMutableDictionary()
                para.setValue(schoolClass.classID, forKey: "class_id")
                para.setValue(schoolClass.divisionID, forKey: "division_id")
                para.setValue(schoolClass.boardID, forKey: "board_id")
                prodArray.add(para)
            }
        }
        classListString.removeLast()
        if(noOfItemsChecked == classList.count){
            classListString = "All Classes"
        }
        
        let vc = SendDataVCTeacher()
        vc.classNameList = classListString
        vc.caseNumber = caseNumber
        vc.prodArray = prodArray
        switch caseNumber {
            
        case 1:
            vc.title = "Circular"
            break
        case 2:
            vc.title = "Message"
            break
        case 3:
            vc.title = "Homework"
            break
        default:
            break
        }
        navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- Server methods

func getClassesAndDivisions()
{
    if UtilTeacher.validateNetworkConnection(self) {
        let userDefaults = UserDefaults.standard
        
        let id  = userDefaults.string(forKey: "EMPLOYEE_ROLE_ID") ?? "employee_role_id"
        
        //Its not used because  its not show class list for homework
//            let cid = (id == "4" || id == "1" || id == "2" || id == "5") && caseNumber == 3 ? "5" : id

            //this is apply for homework only not for other functionality
            let parameters = [
                "school_db_settings_array":  userDefaults.object(forKey: "DB_SETTING") ?? "school_db_settings", //userDefaults.object(forKey: "SCHOOL_DB_SETTINGS") ?? "school_db_settings",
                "school_employee_class_setting_array":  userDefaults.object(forKey: "SUBJECT_SETTINGS") ?? "subject_settings",
                "action":"ClassListIOS",
                "employee_id" : userDefaults.string(forKey: "EMPLOYEE_ID") ?? "employee_id" ,
                "employee_role_id":     id] as [String : Any]
            showActivityIndicatory(uiView: view)
            Alamofire.request(WebServiceUrls.attendanceService, method:.post,parameters: parameters)
                .responseJSON{ [weak self] (response)  in
                    print("Attendance service parameters: (parameters)")
                    print("Attendance service Responce: (response)")
                    self?.stopActivityIndicator()
                    if response.result.error != nil {
                        UtilTeacher.invokeAlertMethod("Failed!", strBody: "Error Occured While Processing Data ", delegate: nil,vcobj : self!)
                    }
                    if let jsonresponse = response.result.value {

                        let originalResponse = JSON(jsonresponse)
                        self?.getSuccessResponse(originalResponse: originalResponse)
                    }
            }
        }
    }

    func getSuccessResponse(originalResponse : JSON)
    {
        if(originalResponse.count > 0)
        {
            for (key,subJson):(String, JSON) in originalResponse {

                if key == "response" {
                    var controllers = navigationController?.viewControllers
                    controllers?.removeLast()
                    let vc = MenuListVCTeacher()
                    vc.caseNumber = caseNumber
                    controllers?.append(vc)
                    navigationController?.viewControllers = controllers!
                    break
                }else{

                    let schoolClass : SchoolClassTeacher = SchoolClassTeacher()
                    schoolClass.classID = Int(subJson["class_id"].stringValue)!
                    schoolClass.boardID = Int(subJson["board_id"].stringValue)!
                    schoolClass.divisionID = Int(subJson["division_id"].stringValue)!
                    schoolClass.className = subJson["class_name"].stringValue
                    schoolClass.divisionName = subJson["division_title"].stringValue
                    classList.append(schoolClass)
                }
            }
        }
        else{
            self.noDataMsg = "Classes/Divisions not available!"
        }
        tableView.reloadData()
        tableView.animateTable()
    }
}
