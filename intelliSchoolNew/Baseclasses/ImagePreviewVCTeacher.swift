//
//  ImagePreviewVC.swift
//  IntellinectsVentures
//
//  Created by Intellinects on 04/07/17.
//  Copyright © 2017 Intellinects. All rights reserved.
//

import UIKit

class ImagePreviewVCTeacher: BaseViewControllerTeacher {

    let imgView : UIImageView =  {
        let imgV = UIImageView()
        imgV.translatesAutoresizingMaskIntoConstraints = false
        return imgV
    }()
    var local:Bool = false
    var url : String?
    
    private func setUpImageView() {
    
        view.addSubview(imgView)
        // needed constraaints
        imgView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        imgView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        imgView.topAnchor.constraint(equalTo: view.topAnchor,constant:64).isActive = true
        imgView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let url = url {
            let imgUrl = URL(string: url)
            if local {
                imgView.image = UIImage(contentsOfFile: (imgUrl?.path)!)
            }else {
                let data = try? Data(contentsOf: imgUrl!)
                imgView.image = UIImage(data: data!)
            }
        }
        setUpImageView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
